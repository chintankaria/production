<?php

include_once("class.client.inc.php");
include_once("class.user.inc.php");
include_once("class.task.inc.php");

class Project {

    private $_id = null;
    private $_name = null;
    private $_description = null;
    private $_isBillable = null;
    private $_clientId = null;
    private $_clientName = null;
    private $_client = null;
    private $_parentId = null;
    private $_parent = null;
    private $_startDate = null;
    private $_endDate = null;
    private $_countTasks = null;
    private $_tasks = null;
    private $_countUsers = null;
    private $_users = null;
    private $_countSubprojects = null;
    private $_role;
    private $_closed;
    private $_status;
    private $_allow_subprojects;
    private $_allow_task_types;
    private $_show_in_calendar = null;
    private $_deliveryLocation;
    private $_channel;
    private $_serviceType;
    private $_practiceArea;
    private $_domain;
    private $_clientProjectLead;
    private $_partnerProjectLead;
    private $_clientLogo;
    private static $_Sql = "
            SELECT
                a.id, a.name, a.description, a.client_id, a.parent_id, UNIX_TIMESTAMP(a.start_date) AS start_date, a.status,a.show_in_calendar,
                UNIX_TIMESTAMP(a.end_date) AS end_date, b.name AS client_name, (a.end_date < CURRENT_DATE) AS closed , 
                a.allow_subprojects, a.allow_task_types, a.is_billable, a.delivery_location, a.channel, a.service_type,
                a.practice_area, a.domain, a.client_project_lead, a.partner_project_lead, a.client_logo
            FROM projects a
            INNER JOIN clients b ON b.id = a.client_id
        ";

    private static $_subProjectIds = array();
    
    public function __construct($id = null, $client_id = null) {
        $id = pintval($id);
        if ($id > 0) {
            $and = get_project_ids_clause("AND", "a.id");
            $sql = self::$_Sql . "
                    WHERE a.id = {$id}
                    {$and}
                ";

            if (!is_null($client_id)) {
                $client_id = pintval($client_id);
                $sql .= "
                        AND a.client_id = {$client_id}
                    ";
            }

            $row = db_get_row($sql);
            if ($row)
                self::_Init($row, $this);
        }
    }

    private static function _Init($array, $object = null) {
        if (!$object instanceof self)
            $object = new self;
        $object->_id = $array["id"];
        $object->_name = $array["name"];
        $object->_description = $array["description"];
        $object->_clientId = $array["client_id"];
        $object->_clientName = $array["client_name"];
        $object->_parentId = $array["parent_id"];
        $object->_parentName = $array["parent_name"];
        $object->_startDate = $array["start_date"];
        $object->_endDate = $array["end_date"];
        $object->_countTasks = $array["count_tasks"];
        $object->_role = $array["role"];
        $object->_closed = $array["closed"];
        $object->_status = $array["status"];
        $object->_show_in_calendar = $array["show_in_calendar"];
        $object->_isBillable = $array["is_billable"];
        $object->_allow_subprojects = $array["allow_subprojects"];
        $object->_allow_task_types = $array["allow_task_types"];
        $object->_deliveryLocation = $array["delivery_location"];
        $object->_channel = $array["channel"];
        $object->_serviceType = $array["service_type"];
        $object->_practiceArea = $array["practice_area"];
        $object->_domain = $array["domain"];
        $object->_clientProjectLead = $array["client_project_lead"];
        $object->_partnerProjectLead = $array["partner_project_lead"];
        $object->_clientLogo = $array["client_logo"];
        return $object;
    }

    public function getId() {
        return $this->_id;
    }

    public function getDescription() {
        return $this->_description;
    }

    public function getStartDate() {
        return $this->_startDate;
    }

    public function getEndDate() {
        return $this->_endDate;
    }

    public function isDeleted() {
        return $this->_deleted;
    }

    public function getClientId() {
        return $this->_clientId;
    }

    public function getClientName() {
        return $this->_clientName;
    }

    public function getParentId() {
        return $this->_parentId;
    }

    public function getRole() {
        return $this->_role;
    }

    public function getClosed() {
        return $this->_closed;
    }

    public function getStatus() {
        return $this->_status;
    }

    public function getAllowSubprojects() {
        return $this->_allow_subprojects;
    }

    public function getAllowTaskTypes() {
        return $this->_allow_task_types;
    }

    public function getIsBillable() {
        return $this->_isBillable;
    }

    public function getDeliveryLocation() {
        return $this->_deliveryLocation;
    }

    public function getChannel() {
        return $this->_channel;
    }

    public function getServiceType() {
        return $this->_serviceType;
    }

    public function getpracticeArea() {
        return $this->_practiceArea;
    }

    public function getDomain() {
        return $this->_domain;
    }

    public function getClientProjectLead() {
        return $this->_clientProjectLead;
    }

    public function getPartnerProjectLead() {
        return $this->_partnerProjectLead;
    }

    public function getClientLogo() {
        return $this->_clientLogo;
    }

    public function getName($include_parent_name = false) {
        if ($include_parent_name && $this->_parentName)
            return "{$this->_parentName}/{$this->_name}";
        else
            return $this->_name;
    }

    public function getClient() {
        if (is_null($this->_client))
            $this->_client = new Client($this->_clientId);
        return $this->_client;
    }

    private function _loadParent() {
        if (is_null($this->_parent) && $this->_parentId)
            $this->_parent = new self($this->_parentId, $this->_clientId);
        if ($this->_parent && $this->_parent->getId() < 1) {
            $this->_parentId = null;
            $this->_parent = false;
        }
    }

    public function isSubproject() {
        $this->_loadParent();
        return $this->_parentId;
    }

    public function getParent() {
        $this->_loadParent();
        return $this->_parent;
    }

    public function getParentName() {
        $this->_loadParent();
        if ($this->_parent)
            return $this->_parent->getName();
        else
            return false;
    }

    private function _loadUsers() {
        if (is_null($this->_users)) {
            $this->_users = User::GetUsersByProject($this);
            $this->_countUsers = count($this->_users);
        }
    }

    public function countUsers() {
        $this->_loadUsers;
        return $this->_countUsers;
    }

    public function getUsers() {
        $this->_loadUsers();
        return $this->_users;
    }

    private function _loadTasks() {
        if (is_null($this->_tasks)) {
            $this->_tasks = Task::GetTasksByProject($this);
            $this->_countTasks = count($this->_tasks);
        }
    }

    public function countTasks() {
        $this->_loadTasks();
        return $this->_countTasks;
    }

    public function getTasks() {
        $this->_loadTasks();
        return $this->_tasks;
    }

    private function _loadSubprojects($user_id = 0) {
        if (!$user_id) {
            $this->_subprojects = self::GetSubprojectsByProject($this);
            $this->_countSubprojects = count($this->_subprojects);
        } else {
            $user = new User($user_id);
            $this->_subprojects = self::GetSubprojectsByUser($this, $user);
            $this->_countSubprojects = count($this->_subprojects);
        }
    }

    public function countSubprojects() {
        $this->_loadSubprojects();
        return $this->_countSubprojects;
    }

    public function getSubprojects($user_id = 0) {
        $this->_loadSubprojects($user_id);
        return $this->_subprojects;
    }

    public function getSubproject($subproject) {
        if ($subproject instanceof self)
            $subproject_id = $subproject->getId();
        else
            $subproject_id = intval($subproject);

        if ($subproject_id > 0) {
            $this->_loadSubprojects();
            if ($this->_subprojects[$subproject_id])
                return $this->_subprojects[$subproject_id];
        }

        return false;
    }

    public function getShowInCalendar() {
        return $this->_show_in_calendar;
    }

    public function setShowInCalendar($show_in_calendar) {
        $this->_show_in_calendar = (bool) $show_in_calendar;
    }

    public function setIsBillable($is_billable) {
        $this->_isBillable = intval($is_billable);
    }

    public function setStatus($status) {
        $this->_status = trim($status);
        if (!$this->_status)
            e("Missing or invalid project status.");
    }

    public function setName($name) {
        $this->_name = trim($name);
        if (!$this->_name)
            e("Missing or invalid project name.");
    }

    public function setDescription($description) {
        $this->_description = trim($description);
    }

    public function setClientId($client) {
        if ($client instanceof Client)
            $client_id = $client->getId();
        else
            $client_id = pintval($client);

        if ($client_id > 0)
            $this->_clientId = $client_id;
        else
            e("Missing or invalid client name.");
    }

    public function setParentId($project) {
        if (!$project instanceof self)
            $project = new Project($project);
        //if($project && $project->getId() > 0 && !$project->isSubproject()) $this->_parentId = $project->getId();
        if ($project && $project->getId() > 0) {
            $this->_parentId = $project->getId();
        }
    }

    public function setStartDate($date) {
        $this->_startDate = is_date($date);
        if (!$this->_startDate)
            e("Missing or invalid project start date.");
    }

    public function setEndDate($date) {
        $this->_endDate = is_date($date);
    }

    public function compareDate($startdate, $enddate) {
        $result = false;
        if ($this->_parentId) {
            $parent = new Project($this->_parentId);
            $pstartDate = $parent->_startDate;
            $pendDate = $parent->_endDate;
            if (strtotime($startdate) < $pstartDate) {
                $result = "subproject start date is before the parent project start date";
            } else if (is_date($pendDate) && strtotime($startdate) > $pendDate) {
                $result = "subproject start date is after the parent project start date";
            } else if (is_date($pendDate) && $enddate && strtotime($enddate) < $pstartDate) {
                $result = "subproject end date is before the parent project start date";
            } else if (is_date($pendDate) && $enddate && strtotime($enddate) > $pendDate) {
                $result = "subproject end date is after the parent project end date";
            } else if ($enddate && strtotime($startdate) > strtotime($enddate)) {
                $result = "subproject end date is before the start date";
            }
        } else if ($enddate && strtotime($startdate) > strtotime($enddate)) {
            $result = "project end date is before the start date";
        } else {
            if ($enddate == '')
                return $result;
            $subprojects = $this->getSubprojects();
            if ($subprojects) {
                foreach ($subprojects as $subproj) {
                    $sub_start_date = $subproj->_startDate;
                    $sub_end_date = $subproj->_endDate;
                    if ($sub_end_date > strtotime($enddate)) {
                        $result = "<a href='project.view.php?id={$subproj->_id}'><b>{$subproj->_name}</b></a> subproject end date is after the parent project end date specified.";
                        break;
                    }
                }
            }
        }
        return $result;
    }

    public function setAllowSubprojects($AllowSubprojects) {
        $this->_allow_subprojects = $AllowSubprojects;
    }

    public function setAllowTaskTypes($AllowTaskTypes) {
        $this->_allow_task_types = $AllowTaskTypes;
    }

    public function setDeliveryLocation($DeliveryLocation) {
        $this->_deliveryLocation = trim($DeliveryLocation);
    }

    public function setChannel($Channel) {
        $this->_channel = $Channel;
    }

    public function setServiceType($ServiceType) {
        $this->_serviceType = $ServiceType;
    }

    public function setPracticeArea($PracticeArea) {
        $this->_practiceArea = $PracticeArea;
    }

    public function setDomain($Domain) {
        $this->_domain = $Domain;
    }

    public function setClientProjectLead($ClientProjectLead) {
        $this->_clientProjectLead = $ClientProjectLead;
    }

    public function setPartnerProjectLead($PartnerProjectLead) {
        $this->_partnerProjectLead = $PartnerProjectLead;
    }

    public function setClientLogo($ClientLogo) {
        $this->_clientLogo = $ClientLogo;
    }

    public function setUsers($params) {

        $users = $params["users"];
        $manager = $params["manager"];

        // if(!u()->isAdmin()) return false;

        if (!is_array($users))
            $users = array($users);
        $uids = array();
        foreach ($users as $user) {
            if ($user instanceof User)
                $uid = $user->getId();
            else
                $uid = pintval($user);

            if ($uid > 0)
                $uids[] = $uid;
        }

        $uids_list = implode(", ", $uids);

        if (!is_array($manager))
            $manager = array($manager);
        $muids = array();
        foreach ($manager as $manager_user) {
            if ($manager_user instanceof User)
                $muid = $manager_user->getId();
            else
                $muid = pintval($manager_user);

            if ($muid > 0)
                $muids[] = $muid;
        }

        $muids_list = implode(", ", $muids);

        $subprojects_obj = self::GetSubprojectsByProject($this->_id);
        $subproject = array();
        foreach ($subprojects_obj as $subprojects)
            $subproject[] = $subprojects->getId();
        $subprojectids = implode(", ", $subproject);


        //debug($this->_id);
        // Validate users list.

        if (!u()->isAdmin()) {
            if (count($subproject) > 0)
                $sql = "
						DELETE FROM project_users 
						WHERE project_id IN ({$subprojectids})
		            ";
            else
                $sql = "
		                DELETE FROM project_users 
		                WHERE project_id = {$this->_id}
		            ";
        } else {
            $sql = "
		                DELETE FROM project_users 
		                WHERE project_id = {$this->_id}
		            ";
        }
        if (db_execute($sql)) {
            if ($muid) {
                $sql1 = "
                        INSERT INTO project_users (user_id, project_id, role)
                    ";
                $role1 = 'MANAGER';
                if ($this->isSubproject()) {
                    $sql1 .= "
                            SELECT user_id, {$this->_id}, '{$role1}' FROM project_users
                            WHERE project_id = {$this->_parentId}
                            AND user_id IN ({$muids_list})
                        ";
                } else {
                    $sql1 .= "
                            SELECT id, {$this->_id}, '{$role1}' FROM users
                            WHERE id IN ({$muids_list})
                        ";
                }


                db_execute($sql1);
            }

            if ($uid) {
                $sql2 = "
                        INSERT INTO project_users (user_id, project_id, role)
                    ";
                $role2 = 'MEMBER';
                if ($this->isSubproject()) {
                    $sql2 .= "
                            SELECT user_id, {$this->_id}, '{$role2}'  FROM project_users
                            WHERE project_id = {$this->_parentId}
                            AND user_id IN ({$uids_list})
                        ";
                } else {
                    $sql2 .= "
                            SELECT id, {$this->_id}, '{$role2}' FROM users
                            WHERE id IN ({$uids_list})
                        ";
                }
                db_execute($sql2);
            }


            return true;
        }
        return false;
    }

    public function save() {
        /*     if(u()->isAdmin()){
          if($this->_id) return $this->_update();
          else return $this->_add();
          }
          else return false; */
        if ($this->_id)
            return $this->_update();
        else
            return $this->_add();
    }

    private function _verifyParentId() {
        $sql = "
                SELECT id FROM projects
                WHERE client_id = {$this->_clientId}
                AND id = {$this->_parentId}";

        if (!db_get_var($sql)) {
            $this->_parentId = null;
        }
        return $this->_parentId;
    }

    private function _add() {

        if ($this->_name && $this->_startDate) {

            $sql = "
                    INSERT INTO projects (name, description, is_billable, client_id, parent_id, start_date, end_date, 
                        allow_subprojects, allow_task_types, status, show_in_calendar, delivery_location, channel, service_type, 
                        practice_area, domain, client_project_lead, partner_project_lead, client_logo)
                    VALUES (
                        " . q($this->_name) . ",
                        " . q($this->_description) . ",
                        " . $this->_isBillable . ",
                        " . n($this->_clientId) . ",
                        " . n($this->_parentId) . ",
                        '" . ymd($this->_startDate) . "',
                        " . q(ymd($this->_endDate)) . ",
                        '" . $this->_allow_subprojects . "',
                        '" . $this->_allow_task_types . "',
                        '" . $this->_status . "',
                        '" . $this->_show_in_calendar . "',
                        " . q($this->_deliveryLocation) . ",
                        " . q($this->_channel) . ",
                        " . n($this->_serviceType) . ",
                        " . n($this->_practiceArea) . ",
                        " . n($this->_domain) . ",
                        " . q($this->_clientProjectLead) . ",
                        " . q($this->_partnerProjectLead) . ",
                        '" . $this->_clientLogo . "'
                    )
                ";

            if (db_execute($sql)) {
                $this->_id = db_insert_id();
                return true;
            }
        }

        return false;
    }

    private function _update() {
        if ($this->_id && $this->_name && $this->_startDate) {
            $this->_verifyParentId();

            $sql = "
                    UPDATE projects SET
                        name = " . q($this->_name) . ",
                        description = " . q($this->_description) . ",
                        is_billable = " . n($this->_isBillable) . ",
                        client_id = " . n($this->_clientId) . ",
                        parent_id = " . n($this->_parentId) . ",
                        start_date = '" . ymd($this->_startDate) . "',
                        end_date = " . q(ymd($this->_endDate)) . ",
                        allow_subprojects = '" . $this->_allow_subprojects . "',
                        allow_task_types = '" . $this->_allow_task_types . "',
                        status = '" . $this->_status . "',
                        show_in_calendar = '" . $this->_show_in_calendar . "',
                        delivery_location = '" . $this->_deliveryLocation . "',
                        channel = '" . $this->_channel . "',
                        service_type = '" . $this->_serviceType . "',
                        practice_area = '" . $this->_practiceArea . "',
                        domain = '" . $this->_domain . "',
                        client_project_lead = '" . $this->_clientProjectLead . "',
                        partner_project_lead = '" . $this->_partnerProjectLead . "',
                        client_logo = '" . $this->_clientLogo . "'
                    WHERE id = {$this->_id}
                ";

            return db_execute($sql);
        }

        return false;
    }

    public function close($op = false, $parent_id = false, $end_date = false) {
        if ($end_date) {
            $new_end_date = $end_date;
            $end_date = q(ymd(strtotime($end_date)));
        } else {
            if ($op)
                $new_end_date = $end_date = date("Y-m-d");
            else
                $end_date = q("");
        }
        $curr_date = date("Y-m-d");
        if (strtotime($curr_date) > strtotime($new_end_date)) {
            e("Could not complete the request as project cannot be opened with end date in the past.");
            return false;
        }

        if ($this->_id > 0 && u()->isAdmin()) {
            $return = false;

            $sql = "
					UPDATE projects SET
						end_date = " . $end_date . "
					WHERE id = {$this->_id}
				";

            if ($op)
                $status = "Closed";
            else
                $status = "Opened";

            if (db_execute($sql)) {
                if (!$parent_id && $op != 0) {
                    $sql_sub = "
							UPDATE projects SET
								end_date = " . $end_date . "
							WHERE parent_id = {$this->_id}
						";
                    db_execute($sql_sub);
                }
                s("{$status} project {$this->_name}");
                $return = true;
            }
        }
        return $return;
    }

    public function delete() {
        $return = false;
        if ($this->_id > 0 && u()->isAdmin()) {
            // Delete tasks and  project
            $sql = "
                    UPDATE tasks SET
                        id = -id,
                        project_id = -project_id
                    WHERE project_id = $this->_id
                    AND id > 0
                ";
            if (db_execute($sql)) {
                s("Deleted tasks under project {$this->_name}");
                $str = "Deleted tasks under project <i>{$this->_name}({$this->_id})</i>";
                watchdog("SUCCESS", "DELETE", $str);
                $sql = "
                        UPDATE projects SET
                            id = -id
                        WHERE id = {$this->_id}
                    ";
                if (db_execute($sql)) {
                    if (!$this->isSubproject()) {
                        $sql_subproj = "
		                        UPDATE projects SET
		                            id = -id
		                        WHERE parent_id = {$this->_id}
		                    ";
                        db_execute($sql_subproj);
                    }
                    s("Deleted project {$this->_name}");
                    $str = "Deleted project <i>{$this->_name}({$this->_id})</i>";
                    watchdog("SUCCESS", "DELETE", $str);
                    $return = true;
                } else
                    e("Failed to delete project {$this->_name}");
            } else
                e("Failed to delete tasks under project {$this->_name}");
        }

        return $return;
    }

    public static function GetProjects($include_children = false) {
        $return = array();
        $and = get_project_ids_clause("AND", "a.id");
        $sql = self::$_Sql . "
                WHERE a.id > 0 AND a.id != 1000
                {$and}
                GROUP BY a.id
                ORDER BY a.name
            ";

        $rows = db_get_all($sql);
        foreach ($rows as $row) {
            if ($row["parent_id"] && !$include_children)
                continue;
            $return[$row["id"]] = self::_Init($row);
        }
        return $return;
    }

    public static function GetProjectsByClient($client, $only_parents = false) {
        if ($client instanceof Client)
            $client_id = $client->getId();
        else
            $client_id = pintval($client);
        $user = u();
        $return = array();
        if (!$user->isAdmin())
            $project_id_report = report_filter_manager();

        if ($only_parents)
            $parents_clause = "AND a.parent_id IS NULL";
        else
            $parents_clause = "";

        if ($client_id > 0) {
            if ($user->isAdmin())
                $and = get_project_ids_clause("AND", "a.id");
            else
                $and = "AND d.user_id = " . $user->getId();

            $sql = "SELECT a.id, a.name, a.description, a.client_id, a.parent_id, a.allow_task_types,a.status,a.is_billable,
                            UNIX_TIMESTAMP(a.start_date) AS start_date, 
                            UNIX_TIMESTAMP(a.end_date) AS end_date, b.name AS client_name , 
                            d.role,  (a.end_date < CURRENT_DATE) AS closed, a.allow_task_types
			            FROM projects a
			            INNER JOIN clients b ON b.id = a.client_id
			            LEFT JOIN project_users d ON d.project_id = a.id
			            WHERE a.id > 0 AND a.id != 1000 
				        {$parents_clause}
			            {$and}
			            AND a.client_id = {$client_id}
			            {$project_id_report}
			            GROUP BY a.id
			            ORDER BY a.name
                ";

            $rows = db_get_all($sql);
            foreach ($rows as $row) {
                if ($row["parent_id"])
                    continue;
                $return[$row["id"]] = self::_Init($row);
            }
        }

        return $return;
    }

    public static function GetProjectsByUser($user, $client = false, $only_parents = false) {
        if ($user instanceof User)
            $user_id = $user->getId();
        else
            $user_id = pintval($user);

        if ($client instanceof Client)
            $client_id = $client->getId();
        else
            $client_id = pintval($client);

        if ($client_id)
            $client_clause = "AND a.client_id = {$client_id}";

        $return = array();
        if (stristr($_SERVER['SCRIPT_NAME'], 'report.list.php') == 'report.list.php' && !u()->isAdmin())
            $project_id_report = report_filter_manager();


        if ($only_parents)
            $parents_clause = "AND a.parent_id IS NULL";
        else
            $parents_clause = "";

        if ($user_id > 0) {
            $sql = "
                        SELECT
                            a.id, a.name, a.description, a.client_id, a.parent_id, a.allow_task_types,a.status,a.show_in_calendar,a.is_billable,
                            UNIX_TIMESTAMP(a.start_date) AS start_date,
                            UNIX_TIMESTAMP(a.end_date) AS end_date,
                            b.name AS client_name, d.role, (a.end_date < CURRENT_DATE) AS closed
                        FROM projects a
                        INNER JOIN clients b ON b.id = a.client_id
                        LEFT JOIN project_users d ON d.project_id = a.id  
                        WHERE a.id > 0 AND a.id != 1000 
                        {$parents_clause}
                        AND d.user_id = {$user_id}
                        {$client_clause}
                        {$project_id_report}
                        GROUP BY a.id
                        ORDER BY a.start_date desc 
                        ";
            $rows = db_get_all($sql);
            foreach ($rows as $row) {
                $return[$row["id"]] = self::_Init($row);
            }
        }
        return $return;
    }

    public static function GetProjectsFilterByUser($user, $client = false, $only_parents = false, $end_date = null) {
        if ($user instanceof User)
            $user_id = $user->getId();
        else
            $user_id = pintval($user);

        if ($client instanceof Client)
            $client_id = $client->getId();
        else
            $client_id = pintval($client);

        $sub = "";
        $sub_sql = "";
        if ($end_date) {
            $end_date = date("Y-m-d", strtotime($end_date));
            $sub = "SELECT a.id 
                            FROM projects a
                            INNER JOIN project_users pu ON pu.project_id = a.id AND pu.user_id = {$user_id}
                            WHERE ( a.end_date >= '$end_date' OR a.end_date IS NULL) AND a.status='CONFIRMED'";
        }
        if ($client_id) {
            $client_clause = "AND a.client_id = {$client_id}";
            if ($end_date)
                $sub .= " AND a.client_id = $client_id";
        }

        $return = array();
        if (stristr($_SERVER['SCRIPT_NAME'], 'report.list.php') == 'report.list.php' && !u()->isAdmin())
            $project_id_report = report_filter_manager();

        $parents_clause = "";
        if ($only_parents) {
            $parents_clause = "AND a.parent_id IS NULL";
            if ($end_date)
                $sub .= " AND a.parent_id IS NULL";
        }

        if ($sub)
            $sub_sql = " AND a.id IN ($sub)";
        if ($user_id > 0) {
            $sql = "
                        SELECT
                            a.id, a.name, a.description, a.client_id, a.parent_id, a.allow_task_types,a.status,a.show_in_calendar,a.is_billable,
                            UNIX_TIMESTAMP(a.start_date) AS start_date,
                            UNIX_TIMESTAMP(a.end_date) AS end_date,
                            b.name AS client_name, d.role, (a.end_date < CURRENT_DATE) AS closed
                        FROM projects a
                        INNER JOIN clients b ON b.id = a.client_id
                        LEFT JOIN project_users d ON d.project_id = a.id  
                        WHERE a.id > 0 AND a.id != 1000 
                        $sub_sql
                        {$parents_clause}
                        AND d.user_id = {$user_id}
                        {$client_clause}
                        {$project_id_report}
                        AND a.status = 'CONFIRMED'
                        GROUP BY a.id
                        ORDER BY a.name 
                        ";
            $rows = db_get_all($sql);
            foreach ($rows as $row) {
                $return[$row["id"]] = self::_Init($row);
            }
        }
        return $return;
    }

    public static function GetAllSubprojects() {
        $return = array();
        $and = get_project_ids_clause("AND", "a.id");
        $sql = self::$_Sql . "
                INNER JOIN projects d ON d.id = a.parent_id
                WHERE a.id > 0  
                AND a.parent_id IS NOT NULL
                {$and}
                GROUP BY a.id
                ORDER BY a.name
            ";

        $rows = db_get_all($sql);
        foreach ($rows as $row)
            $return[$row["id"]] = self::_Init($row);
        return $return;
    }

    public static function GetSubprojectsByProject($project) {
        if ($project instanceof self)
            $project_id = $project->getId();
        else
            $project_id = pintval($project);

        $return = array();
        //return $return ;
        $user = u();
        if ($project_id > 0) {
            if ($user->isAdmin())
                $and = get_project_ids_clause("AND", "a.id");
            else
                $and = "AND pu.user_id = " . $user->getId();

            $sql = "SELECT a.id, a.name, a.description, a.client_id, a.parent_id, a.status,a.show_in_calendar,a.is_billable,
                            UNIX_TIMESTAMP(a.start_date) AS start_date, UNIX_TIMESTAMP(a.end_date) AS end_date,
                            b.name AS client_name, pu.role,  (a.end_date < CURRENT_DATE) AS closed ,  
                            a.allow_task_types
						FROM projects a
						INNER JOIN clients b ON b.id = a.client_id
	                    INNER JOIN projects d ON d.id = a.parent_id
						LEFT JOIN project_users pu ON pu.project_id =  a.id
	                    WHERE a.id > 0
	                    AND a.parent_id = {$project_id}
	                    {$and}
	                    GROUP BY a.id
                ";
            $rows = db_get_all($sql);
            foreach ($rows as $row)
                $return[$row["id"]] = self::_Init($row);
        }

        return $return;
    }

    public static function GetSubprojectsFilterByUser($project, $user, $client = NULL, $end_date = null) {
        if ($user instanceof User)
            $user_id = $user->getId();
        else
            $user_id = pintval($user);

        if ($project instanceof self)
            $project_id = $project->getId();
        else
            $project_id = pintval($project);

        if ($client instanceof Client)
            $client_id = $client->getId();
        else
            $client_id = pintval($client);

        $return = array();
        if ($project_id > 0) {
            if ($user->isAdmin())
                $user_and = get_project_ids_clause("AND", "a.id");
            else
                $user_and = "AND pu.user_id = " . $user->getId();

            $sub = "";
            $sub_sql = "";
            if ($end_date) {
                $end_date = date("Y-m-d", strtotime($end_date));
                $sub = "SELECT a.id 
                            FROM projects a
                            INNER JOIN project_users pu ON pu.project_id = a.id AND pu.user_id = {$user->getId()}
                            WHERE ( a.end_date >= '$end_date' OR a.end_date IS NULL) AND a.parent_id = $project_id AND a.status = 'CONFIRMED'";
            }

            if ($client_id) {
                $client_and = "AND a.client_id = " . $client_id;
                if ($end_date)
                    $sub .= " AND a.client_id = $client_id";
            }
            #else  $client_and = "AND a.client_id = ".$client_id;

            if ($sub)
                $sub_sql = " AND a.id IN ($sub)";

            $sql = "SELECT
							a.id, a.name, a.description, a.client_id, a.parent_id, a.status,a.show_in_calendar,a.is_billable,
                            UNIX_TIMESTAMP(a.start_date) AS start_date, UNIX_TIMESTAMP(a.end_date) AS end_date,
                            b.name AS client_name, pu.role,  (a.end_date < CURRENT_DATE) AS closed ,  
                            a.allow_task_types
						FROM projects a
						INNER JOIN clients b ON b.id = a.client_id
						INNER JOIN project_users pu ON pu.project_id = a.id
	                    WHERE a.id > 0 AND a.parent_id = {$project_id}
                        $sub_sql
	                    {$user_and}
	                    {$client_and}
                        AND a.status = 'CONFIRMED'
	                    GROUP BY a.id
                ";
            $rows = db_get_all($sql);
            foreach ($rows as $row)
                $return[$row["id"]] = self::_Init($row);
        }

        return $return;
    }

    public static function GetSubprojectsByUser($project, $user, $client = NULL) {
        if ($user instanceof User)
            $user_id = $user->getId();
        else
            $user_id = pintval($user);

        if ($project instanceof self)
            $project_id = $project->getId();
        else
            $project_id = pintval($project);

        if ($client instanceof Client)
            $client_id = $client->getId();
        else
            $client_id = pintval($client);

        $return = array();
        if ($project_id > 0) {
            if ($user->isAdmin())
                $user_and = get_project_ids_clause("AND", "a.id");
            else
                $user_and = "AND pu.user_id = " . $user->getId();

            if ($client_id)
                $client_and = "AND a.client_id = " . $client_id;
            else
                $client_and = "AND a.client_id = " . $client_id;

            $sql = "SELECT
							a.id, a.name, a.description, a.client_id, a.parent_id, a.status,a.show_in_calendar,a.is_billable,
                            UNIX_TIMESTAMP(a.start_date) AS start_date, UNIX_TIMESTAMP(a.end_date) AS end_date,
                            b.name AS client_name, pu.role,  (a.end_date < CURRENT_DATE) AS closed ,  
                            a.allow_task_types
						FROM projects a
						INNER JOIN clients b ON b.id = a.client_id
						INNER JOIN project_users pu ON pu.project_id = a.id
	                    WHERE a.id > 0 AND a.parent_id = {$project_id}
	                    {$user_and}
	                    {$client_and}
	                    GROUP BY a.id
                ";
            $rows = db_get_all($sql);
            foreach ($rows as $row)
                $return[$row["id"]] = self::_Init($row);
        }

        return $return;
    }

    public static function getUserRoleByProject($pid, $uid) {
        $sql = "
                   SELECT role
                   FROM project_users pu
                   INNER JOIN users u ON u.id = pu.user_id
                   WHERE pu.project_id = $pid AND pu.user_id = $uid
                   
            ";
        $rows = db_get_row($sql);
        return $rows;
    }

    public static function getUsersByManager($uid) {

        $sql = "
                    SELECT DISTINCT(pu.user_id), u.first_name, u.middle_name, u.last_name
					FROM project_users  pu
					INNER JOIN users u ON u.id = pu.user_id
					WHERE project_id IN (
						SELECT d.project_id
						FROM project_users d 
						WHERE d.project_id > 0
						AND d.user_id = $uid
						AND d.role = 'MANAGER'
					) 
            ";
        //debug($sql);
        $rows = db_get_all($sql);
        return $rows;
    }

    public static function getUsersByManagerReport($uid, $proj_id = null, $subproj_id = null) {
        if ($proj_id && !$subproj_id)
            $project_id = "AND d.project_id = $proj_id";
        else if ($proj_id && $subproj_id)
            $project_id = "AND d.project_id = $subproj_id";
        else
            $project_id = "";
        $sql = "
            SELECT DISTINCT(pu.user_id), u.first_name, u.middle_name, u.last_name
                    FROM project_users  pu
                    INNER JOIN users u ON u.id = pu.user_id
                    WHERE project_id IN (
                        SELECT d.project_id
                        FROM project_users d 
                        WHERE d.project_id > 0
                        $project_id
                        AND d.user_id = $uid
                        AND d.role = 'MANAGER'
                    ) 
            ";
        //debug($sql);
        $rows = db_get_all($sql);
        if ($proj_id || $subproj_id)
            return $rows;
    }

    public static function checkUserByProject($pid, $uid) {
        $sql = "
                   SELECT user_id
                   FROM project_users 
                   WHERE project_id = $pid AND user_id = $uid
                   
            ";
        $rows = db_get_row($sql);
        return $rows;
    }
    
    public static function SubprojectsByUser($userId, $projectId, $endDate){        
         $sql = "SELECT p.id, p.parent_id,p.client_id, p.name, p.allow_task_types, p.start_date, p.end_date, p.is_billable 
                     FROM  projects as p 
                     INNER JOIN project_users pu 
                     ON p.id=pu.project_id 
                     AND pu.user_id = {$userId}  
                     AND ( p.end_date >= '$endDate' OR p.end_date IS NULL)  
                     AND p.parent_id = {$projectId} 
                     AND p.status = 'CONFIRMED' ";
        $rows = db_get_all($sql);
        
        $children = array();
                
        foreach($rows as $row){
            $children[$row['id']][0]  =  self::_Init($row);
            $children[$row['id']][1] = self::SubprojectsByUser($userId, $row[id], $endDate);
        }
         return $children;
    }
    
    public static function SortSubProjectsByUser($projects){
        if(is_array($projects)){
            foreach($projects as $key =>$project){
               // echo '<pre>'; print_r($project[0]); echo '</pre>';
                self::$_subProjectIds[$key] = $project[0];
                self::SortSubProjectsByUser($project[1]);
            }
        }
    }
    
    public static function getSortSubProjectsByUser(){
        return  self::$_subProjectIds;
    }
    
    public static function resetSortSubProjectsByUser(){
         self::$_subProjectIds = array();
    }
    
    public static function GetMainProject($id=null){
        if($id){
            $sql = "SELECT * FROM projects WHERE id = {$id}";
            $rows = db_get_row($sql);
            if($rows['parent_id']){
                return self::GetMainProject($rows['parent_id']);
            }
            else{
                return $id;
            }
        }
        
    }

}
