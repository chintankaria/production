<?php
    include_once("../config.php");
    require_perms("hr_manager","admin");

    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.department.inc.php");
    include_once("../classes/class.designation.inc.php");
    include_once("../classes/class.product.knowledge.inc.php");
    include_once("../classes/class.technology.practice.inc.php");
    include_once("../classes/class.technology.skills.inc.php");
    include_once("../classes/class.training.skills.inc.php");
    include_once("../classes/class.employee.types.inc.php");
    include_once("../classes/class.employee.location.inc.php");

     if (trim($_POST['save_user_details']) == "Save"){

         $user = new User($_POST["id"]);

        if($user->getId() > 0){

            if(trim($_POST['txtUserName']) != $user->getEmail()) {
                if($user->getUserIdByEmailId(trim($_POST['txtUserName']))) {
                    // User Name already exist
                    e("<b>{$_POST['txtUserName']}</b> member already exist.");
                    redirect("user.edit.php?id={$_POST["id"]}&tab={$_POST['tab']}");
                }
                $user->setEmail(trim($_POST["txtUserName"]));
            }

            $given_name = explode(" ", $_POST['txtGivenName'],2);
            $user->setFirstName(trim($given_name[0]));
            $user->setMiddleName(trim($given_name[1]));
            $user->setLastName(trim($_POST["txtFamilyName"]));
            $status = $user->save();

            if($status) {
                if($_FILES) {
                        // User Pic
                        $file = array();
                        foreach($_FILES['file']['error'] as $k => $v){
                            if(!$v){
                                $ext = getExtension(stripslashes($_FILES['file']['name'][$k]));
                                $imginfo_array = getimagesize($_FILES['file']['tmp_name'][$k]);
                                if ($imginfo_array !== false) {
                                    if(intval($_FILES['file']['size'][$k]) > 5120) {
                                        $file = array(
                                            'name' => addslashes($_FILES['file']['name'][$k]),
                                            'type' => $_FILES['file']['type'][$k],
                                            'tmp_name' => $_FILES['file']['tmp_name'][$k],
                                            'size' => $_FILES['file']['size'][$k],
                                        );
                                    }
                                    else {
                                        e("Image size limit exceeded.");
                                    }
                                }
                                else {
                                    e("Invalid image file uploaded.");
                                }
                            }
                        }
                        if(is_array($file) && count($file) > 0) {
                            if($user->SaveUserFiles($file, 'PROFILE_PIC')) {
                                $str = "Profile & Picture saved successfully";
                                s("Profile & Picture saved successfully.");
                                watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                            }
                            else {
                                $str = "Failed to change picture";
                                e("Failed to change picture.");
                                watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                            }
                        }

                        // User CV
                        $file = array();
                        foreach($_FILES['file_cv']['error'] as $k => $v){
                            if(!$v){
                                $file = array(
                                    'name' => addslashes($_FILES['file_cv']['name'][$k]),
                                    'type' => $_FILES['file_cv']['type'][$k],
                                    'tmp_name' => $_FILES['file_cv']['tmp_name'][$k],
                                    'size' => $_FILES['file_cv']['size'][$k],
                                );
                            }
                        }
                        if(is_array($file) && count($file) > 0) {
                            if($user->SaveUserFiles($file, 'USER_CV')) {
                                $str = "CV updated successfully";
                                s("CV updated  successfully.");
                                watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                            }
                            else {
                                $str = "Failed to update CV";
                                e("Failed to update CV.");
                                watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                            }
                        }
                    }

                    $employee = new Employee(0, $user->getId());
                    $employee->setUserId($user->getId());

                    $employee->setTechPracticeIds($_POST["tech_practice_ids"]);
                    $employee->setDesignationId($_POST["designation_id"]);
                    $employee->setLocationId(trim($_POST["location_id"]));
                    $employee->setMobile1($_POST["txtMobile1"]);
                    $employee->setMobile2($_POST["txtMobile2"]);
                    $employee->setSkypeId($_POST["txtSkypeId"]);
                    $employee->setYahooId($_POST["txtYahooId"]);

                    // Save Employee Details
                    $employee->save();
            }
        }

        else $status = false;
            if($status) {
                $str = "Member information updated ";
                s("Member information updated.");
                watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
            }
            else {
                $str = "Failed to update member information";
                e("Failed to update member information.");
                watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
            }

            redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");


     }   

    $user = new User($_GET["id"]);
    if(!$user || $user->getId() < 1) redirect("user.list.php");
    if($user->getId() == 1) {
        e("Administrator account can not be edited!");
        redirect($_SERVER['HTTP_REFERER']);
        exit;
    }
    $isBusinessManager = in_array("business_manager" ,u()->getRoles());



    $employee = $user->getEmployee();
    $departments = Department::GetAll();    
    $designations = Designation::GetAll();    
    $prod_knowledge = ProductKnowledge::GetAll();
    $tech_practice = TechnologyPractice::GetAll();
    $tech_skills = TechnologySkills::GetAll();
    $training_skills = TrainingSkills::GetAll();
    $employee_types = EmployeeTypes::GetAll();
    $employee_location = EmployeeLocation::GetAll();

    $country = getCountryList();

    $trainer_style = $employee->IsTrainer() ? '' : 'display:none';

    $tabArr = array('eInfo', 'pInfo', 'cInfo','passInfo');

    $tab = isset($_GET['tab']) ? $_GET['tab'] : 'eInfo';
    if(!in_array($tab, $tabArr)) {
        $tab = 'eInfo';
    }    

    sm_assign("tab", $tab);
    sm_assign("trainer_style", $trainer_style);
    sm_assign("country", $country);
    sm_assign("departments", $departments);
    sm_assign("designations", $designations);
    sm_assign("prod_knowledge",$prod_knowledge);
    sm_assign("tech_practice", $tech_practice);
    sm_assign("tech_skills", $tech_skills);
    sm_assign("training_skills", $training_skills);
    sm_assign("employee_location", $employee_location);
    sm_assign("employee_types", $employee_types);
    sm_assign("employee", $employee);
    sm_assign("isBusinessManager", $isBusinessManager);
    sm_assign("user", $user);
    sm_display("employee_details.html");
?>