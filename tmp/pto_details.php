<?php
    include_once("../config.php");
    

    include_once("../classes/class.user.inc.php");

    $user = new User($_GET["id"]);
    if(!$user || $user->getId() < 1) redirect("user.list.php");
    if($user->getId() == 1) {
        e("Administrator account can not be edited!");
        redirect($_SERVER['HTTP_REFERER']);
        exit;
    }
    $isBusinessManager = in_array("business_manager" ,u()->getRoles());

    $employee = $user->getEmployee();
    
    sm_assign("employee", $employee);
    sm_assign("user", $user);
    sm_display("project_history.html");
?>