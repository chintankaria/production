<?php
include_once("../config.php");
function generate_pdf_report ($source_file,$destination_file,$param = array()) {
	include_once(AppPath::$HTML2PDF."/public_html/samples/sample.simplest.from.file.php");

    ini_set("max_execution_time", 1000);
    ini_set("max_input_time", 1000);
    ini_set("memory_limit", "2048M");

    $param['config'] = (isset($param['config']) && $param['config'] != "") ? $param['config'] : '';

    if($param['media_margins'] == "") {
        // Default Media Margins
        $param['media_margins'] = array('left'   => 0,
                                        'right'  => 0,
                                        'top'    => 10,
                                        'bottom' => 10
                                    );
    }

    if($param['config'] == "") {
        // Default PDF Configuration
        $param['config'] = array(
                        'pagewidth'          => 1024,
                        'margins'            => array(
                                                     'left'    => 30,
                                                     'right'   => 15,
                                                     'top'     => 15,
                                                     'bottom'  => 15,
                                                ),
                        //'media'               => 'A4',
                        'media'             => 'Letter',
                        'method'            => 'fpdf',
                        'mode'              => 'html',
                        'cssmedia'          => 'screen',
                        'pdfversion'        => '1.6',
                        'pslevel'           => 3,
                        'scalepoints'       => '1',
                        'renderimages'      => true,
                        'renderlinks'       => true,
                        'renderfields'      => true,
                        'renderforms'       => false,
                        'smartpagebreak'    => true,
                        'html2xhtml'        => true, // new param
                        'encoding'          => '',
                       );
    }

    $param['header'] = (isset($param['header']) && $param['header'] != "") ? $param['header'] : '';

    if($param['footer'] == "") {
        // Default footer
        $param['footer'] = '
            <table width="95%" cellspacing="0" cellpadding="0" border="0" align="center">
                <tr>
                    <td align="right">
                        <div style="border-top: solid #C4C4C4 1px; padding-top: 5px;">
                            Page <span>##PAGE##</span> of <span>##PAGES##</span>
                        </div>
                    </td>
                </tr>
            </table>
        ';
    }


	if(file_exists($source_file)) {
	    return convert_to_pdf("$source_file","$destination_file",$param);
	}
	else
		return false;
}
?>
