<?php
include_once("config.nologin.php");

if($_SERVER['HTTP_HOST']){
    // Disable script if any one calling from Web Browser.
    print "<font size='20pt'>:-)</font>";
    exit;
}

#print date("t", strtotime("2009-11-23"));
$year = 2009;
$month= 11;
$day = 23;

for($y=$year; $y < 2011; $y++) {
    for($m = $month; $m <= 12; $m++) {
        for($d = $day; $d <= date("t", strtotime("$y-$m-$day")); $d++) {
            $dt = date("Y-m-d", strtotime("$y-$m-$d"));
            $dt_ts = strtotime($dt);
            $week_no = date("W",$dt_ts);
            $week_days[$y][$week_no][$dt] = $dt;
        }
        $day=1;
    }
    $month=1;
}
#print_r($week_days);
#exit;
$sql = "SELECT id FROM users WHERE NOT blocked";
$sql = "SELECT id FROM users WHERE NOT blocked and id=22";
$uRows = db_get_all($sql);
if($uRows) {
    $tDays = array();
    foreach($uRows as $uRow) {
        foreach($week_days as $yr => $weeks) {
            foreach($weeks as $week_no => $days) {
                $start_date = reset($days);
                $end_date = end($days);
                $sql = "SELECT id,task_date,duration_mins,project_id FROM tasks WHERE user_id = {$uRow['id']} AND (task_date BETWEEN '$start_date' AND '$end_date') ";
                $sql = "SELECT a.id, a.task_date, a.duration_mins, a.project_id, b.is_billable
                        FROM tasks a
                        INNER JOIN projects b ON b.id = a.project_id
                        WHERE a.user_id = {$uRow['id']} AND (a.task_date BETWEEN '$start_date' AND '$end_date')
                        ";
                $rows = db_get_all($sql);
                if($rows) {
                    //$tasks_arr = calculate_weekly_timecard($uRow['id'], $start_date, $end_date,true);
                    foreach($rows as $row) {
                        #$tSubmission[$uRow['id']][$row['task_date']] = date("W",strtotime($row['task_date']));
                        $tDays[$row['task_date']]['total_pto_mins'] = 0;
                        $tDays[$row['task_date']]['total_billable_mins'] = 0;
                        $tDays[$row['task_date']]['total_mins'] = 0;
                        if($row['project_id'] == 1000) {
                            $tDays[$row['task_date']]['total_pto_mins'] = intval($tDays[$row['task_date']]['total_pto_mins']) + intval($row['duration_mins']);
                        }
                        else {
                            $tDays[$row['task_date']]['total_mins'] = intval($tDays[$row['task_date']]['total_mins']) + intval($row['duration_mins']);
                        }
                        if($row['is_billable']) {
                            $tDays[$row['task_date']]['total_billable_mins'] = intval($tDays[$row['task_date']]['total_billable_mins']) + intval($row['duration_mins']);
                        }
                        //$task_sql = "UPDATE tasks SET task_status = 'SUBMITTED' WHERE id={$row['id']};";
                        //db_execute($task_sql);
                    }
                }
                foreach($days as $day) {
                    if(!in_array($day, $tDays)) {
                        $arr = array("total_mins" => $tDays[$day]['total_mins'], 
                                    "total_billable_mins" => $tDays[$day]['total_billable_mins'], 
                                    "total_pto_mins" => $tDays[$day]['total_pto_mins']);
                        $tasks[$uRow['id']][$day] = $arr;
                        $tasks[$uRow['id']][$day]['week_number'] = $week_no;
                    }
                    else {
                        $tasks[$uRow['id']][$day] = $tasks_arr[$day];
                        $tasks[$uRow['id']][$day]['week_number'] = $week_no;
                    }
                }
            }
        }
    }
    $created_by = 'admin.timesheet@globalss.com';
    foreach($tasks as $uid => $days) {
        foreach($days as $d => $v) {
            $v['unassigned_mins'] = isset($v['unassigned_mins']) ? $v['unassigned_mins'] : 0;
            $v['is_holiday'] = isset($v['is_holiday']) ? $v['is_holiday'] : 0;
            $timecard_sql = "INSERT INTO timecard_data (user_id, week_number, timecard_date, billable_minutes, pto_minutes, total_minutes,
                                                unassigned_minutes, is_holiday, status, create_date, created_by, update_date, updated_by
                                                )
                            VALUES (
                                    $uid,
                                    '{$v["week_number"]}',
                                    '$d',
                                    '{$v["total_billable_mins"]}',
                                    '{$v["total_pto_mins"]}',
                                    '{$v["total_mins"]}',
                                    '{$v["unassigned_mins"]}',
                                    '{$v["is_holiday"]}',
                                    'SUBMITTED',
                                    NOW(), '$created_by', NOW(), '$created_by'
                                );
                    ";
            db_execute($timecard_sql);
        }
    } 
}
print_r($tasks);
?>
