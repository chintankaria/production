-- phpMyAdmin SQL Dump
-- version 3.1.2deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 11, 2009 at 08:04 PM
-- Server version: 5.0.75
-- PHP Version: 5.2.6-3ubuntu4.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `timesheet`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(1024) NOT NULL,
  `create_date` datetime NOT NULL,
  `rec_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `name`, `create_date`, `rec_date`) VALUES
(1, 'TapDynamics Inc.', '2009-05-08 22:52:23', '2009-05-08 22:52:23'),
(2, 'Physicianshare Inc.', '2009-05-08 22:52:23', '2009-05-08 22:52:23'),
(3, 'My new company ltd', '2009-05-09 14:55:12', '2009-05-09 14:55:12'),
(4, 'My new company ltd', '2009-05-09 14:58:03', '2009-05-09 14:58:03'),
(5, 'Another client', '2009-05-09 16:18:59', '2009-05-09 16:18:59'),
(6, 'dfdfdf tasdjfh dsfhjsdf sfjhsjfk', '2009-05-10 08:40:32', '2009-05-10 08:40:32');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL auto_increment,
  `client_id` int(11) NOT NULL,
  `parent_id` int(11) default NULL,
  `name` varchar(1024) NOT NULL,
  `description` text NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `rec_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `client_id` (`client_id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `client_id`, `parent_id`, `name`, `description`, `deleted`, `start_date`, `end_date`, `rec_date`) VALUES
(1, 2, NULL, 'Sample project', 'This is a sample project.\r\nThis is a sample project.', 0, '2009-03-03', '2009-04-03', '2009-05-09 17:42:16'),
(2, 3, NULL, 'Another sample project', 'This is a sample project for this client.', 0, '2009-04-06', '2009-09-09', '2009-05-09 21:07:33'),
(3, 3, NULL, 'Another sample project', 'This is a sample project for this client.', 0, '2009-04-05', '2009-09-10', '2009-05-09 19:16:42'),
(4, 1, NULL, 'TA', 'Tapadvisor etc.', 0, '2008-03-03', '2009-03-03', '2009-05-11 18:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `project_users`
--

CREATE TABLE IF NOT EXISTS `project_users` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `rec_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`,`project_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `project_users`
--

INSERT INTO `project_users` (`id`, `user_id`, `project_id`, `rec_date`) VALUES
(9, 2, 2, '2009-05-11 19:56:21'),
(8, 3, 2, '2009-05-11 19:56:21');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `sesskey` varchar(64) NOT NULL default '',
  `expiry` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `expireref` varchar(250) default '',
  `created` timestamp NOT NULL default '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL default '0000-00-00 00:00:00',
  `sessdata` longtext,
  PRIMARY KEY  (`sesskey`),
  KEY `sess2_expiry` (`expiry`),
  KEY `sess2_expireref` (`expireref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`sesskey`, `expiry`, `expireref`, `created`, `modified`, `sessdata`) VALUES
('f9e0c385fd6842845fdee3ddcb5bd085', '2009-05-11 20:27:57', '', '2009-05-11 18:50:07', '2009-05-11 20:03:57', 'errors%7Cs%3A0%3A%22%22%3Bsuccess%7Cs%3A0%3A%22%22%3Bpost%7Cs%3A0%3A%22%22%3Buser%7Cs%3A1%3A%221%22%3B');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `detail` text NOT NULL,
  `task_date` date NOT NULL,
  `duration_mins` int(11) NOT NULL,
  `rec_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  KEY `member_id` (`user_id`),
  KEY `project_id` (`project_id`),
  KEY `task_date` (`task_date`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tasks`
--

INSERT INTO `tasks` (`id`, `user_id`, `project_id`, `detail`, `task_date`, `duration_mins`, `rec_date`) VALUES
(1, 2, 1, 'rr', '2005-11-09', 3, '2009-05-11 16:19:11'),
(2, 2, 1, 'rtrg rt ret e', '2005-11-09', 32, '2009-05-11 16:19:16'),
(3, 2, 3, 'dsf dsff dsf', '2005-11-09', 32, '2009-05-11 16:20:19'),
(4, 2, 1, 'dfdsf dsf', '2005-12-09', 23, '2009-05-11 16:20:30'),
(5, 2, 1, 'dsf dfds fsdf s', '2005-12-09', 45, '2009-05-11 16:20:34'),
(6, 2, 4, 'Hahaha', '2005-11-09', 765, '2009-05-11 18:45:07'),
(7, 1, 2, 'dfdsf', '2005-11-09', 1, '2009-05-11 19:47:21');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL auto_increment,
  `email` varchar(255) NOT NULL,
  `name` varchar(1024) NOT NULL,
  `password` varchar(32) NOT NULL,
  `register_date` datetime NOT NULL,
  `update_date` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `register_date`, `update_date`) VALUES
(1, 'admin.timesheet@globalss.com', 'Administrator', 'e10adc3949ba59abbe56e057f20f883e', '2009-05-09 22:08:13', '2009-05-10 09:57:27'),
(2, 'anup.shukla@globalss.com', 'Anup Shukla', 'e10adc3949ba59abbe56e057f20f883e', '2009-05-09 22:08:13', '2009-05-10 09:57:27'),
(3, 'AN@AN.AN', 'AN', '6512bd43d9caa6e02c990b0a82652dca', '2009-05-11 18:30:20', '2009-05-11 18:30:20');
