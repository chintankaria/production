<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.settings.inc.php");
    
    if($_POST) {
        // take all those values which are configured by the user
        $reminder_day = $_POST['reminder_day'];
        $reminder_time = $_POST['reminder_time'];
        $reminder_text = $_POST['reminder_text'];
        $notify_reminder_bm = $_POST['notify_reminder_bm'];
        $enable_reminder = $_POST['enable_reminder'];
        // First remove the first value as its used for just adding rows dynamically
        unset($_POST['reminder_day']);
        unset($_POST['reminder_time']);
        unset($_POST['reminder_text']);
        unset($_POST['notify_reminder_bm']);
        unset($_POST['enable_reminder']);
        // re-assign the all those confirued values
        foreach($reminder_day as $k => $v) {
            if($k == 0) continue;
            $_POST['reminder_day'][] = $reminder_day[$k];
            $_POST['reminder_time'][] = $reminder_time[$k];
            $_POST['reminder_text'][] = $reminder_text[$k];
            $_POST['notify_reminder_bm'][] = $notify_reminder_bm[$k];
            $_POST['enable_reminder'][] = $enable_reminder[$k];
        }

        foreach($_POST['reminder_day'] as $k => $v) {
            $i = $k;
            $enable_reminder = (isset($_POST['enable_reminder'][$i]) && $_POST['enable_reminder'][$i] == 1) ? $_POST['enable_reminder'][$i] : '0';
            unset($_POST['enable_reminder'][$i]);
            $_POST['enable_reminder'][$k] = $enable_reminder;
            $notify_reminder_bm = (isset($_POST['notify_reminder_bm'][$i]) && $_POST['notify_reminder_bm'][$i] == 1) ? $_POST['notify_reminder_bm'][$i]:'0';
            unset($_POST['notify_reminder_bm'][$i]);
            $_POST['notify_reminder_bm'][$k] = $notify_reminder_bm; 
        }
        $blnStatus=true;
        foreach($_POST as $k => $v) { 
            $settings = new Settings($k);
            $settings->setSettingsKey($k);
            $settings->setSettingsValue($v);
            if(!$settings->save()) {
                $blnStatus=false;
                break;
            }
            
        }
        if($blnStatus) {
            s("Alert settings saved.");
            watchdog('SUCCESS','EDIT', "Alert settings saved.");
        }
        else {
            e("Failed to save alert settings");
            watchdog('FAILED','EDIT', "Failed to save alert settings");
        }
        if($_POST['fetch'] == 1) {
            sm_assign("fetch", 1);
            print "Done";
            exit;
        }
        else {
            redirect("alerts.list.php");
        }
    }
?>
