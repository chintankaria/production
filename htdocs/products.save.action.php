<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.products.inc.php");
    $products = new Products($_POST["id"]);
    $products->setCode($_POST["code"]);
    $products->setName($_POST["name"]);
	
	$modules = array();
	if(is_array($_POST['module_codes'])) {
		foreach($_POST['module_codes'] as $v) {
			$m = Module::Get($v);
			if($m) {
				$modules[] = $m;
			}
		}
	}
	
    if($_POST['action'] == "DELETE") {
        if($products->delete()) {
            s("Product <i><b>{$_POST['name']}</b></i> deleted.");
            $str = "Product <i><b>{$_POST['name']}</b></i> deleted.";
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            e("Failed to delete product.");
            $str = "Failed to delete product.";
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$products->checkName()) {
            if($product_id = $products->save()) {
				Products::SaveModules($product_id, $modules);
                foreach($_POST['version'] as $k => $v) {
                    if($k == 0) continue;
					
					$license_version = intval($_POST['license_version'][$k]);
					if($license_version != 2) {
						$license_version = 1;
					}

                    if($_POST['action'] == "EDIT" && $_POST['version_id'][$k])
                    {
                        $res = Products::EditVersion($product_id, $_POST['version_id'][$k], $_POST['version'][$k], $license_version, $_POST['build'][$k], $_POST['path'][$k]);
                        if(!$res)
                        {
                            e("Version Number <i><b>{$_POST["version"][$k]}</b></i> and Build Number <i><b>{$_POST["build"][$k]}</b></i> already exist!");
                            die;
                        }
                    }
                    else
                    {
                        $res = Products::AddVersion($product_id, $_POST['version'][$k], $license_version, $_POST['build'][$k], $_POST['path'][$k]);
                        if(!$res)
                        {
                            e("Version Number <i><b>{$_POST["version"][$k]}</b></i> and Build Number <i><b>{$_POST["build"][$k]}</b></i> already exist!");
                            die;
                        }
                    }
                }

                s("Product <i><b>{$products->getName()}</b></i> saved.");
                $str = "Product <i><b>{$products->getName()}</b></i> saved.";
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                e("Failed to save product {$_POST["name"]}");
                $str = "Failed to save product {$_POST["name"]}";
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Product <i><b>{$_POST["name"]}</b></i> already exist!");
    }

    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("products.list.php");
    }
