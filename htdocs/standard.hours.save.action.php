<?php
    include_once("../config.php");
    require_perms("admin");
    
    if($_POST) {
        if($_POST['standard_hours']) {
            if(saveStandardHours($_POST['standard_hours'],$_POST['id'])) {
                s("Standard hours saved.");
                watchdog('SUCCESS','EDIT', "Standard hours saved.");
            }
            else {
                e("Failed to save standard hours.");
                watchdog('SUCCESS','EDIT', "Failed to save standard hours.");
            }
        }
        if($_POST['fetch'] == 1) {
            sm_assign("fetch", 1);
            print "Done";
            exit;
        }
        else {
            redirect("standard.hours.list.php");
        }
    }

function saveStandardHours($hours, $id = 0){
    //$hours = (intval($hours) > 0) ? intval($hours) : 8;
    $hours = hm2m($hours);
    if($id > 0) {
        $sql = "UPDATE standard_hours SET
                    standard_hours_in_mins='$hours', 
                    updated_by = '".u()->getEmail()."',
                    update_date=NOW()
                WHERE id=$id
                ";
    }
    else {
        $sql = "INSERT INTO standard_hours (
                        standard_hours_in_mins, created_by, create_date, updated_by, update_date
                    )
                    VALUES (
                        '$hours',
                        '".u()->getEmail()."',
                        NOW(),
                        '".u()->getEmail()."',
                        NOW()
                    )
                ";
    }
    return db_execute($sql);
}
?>
