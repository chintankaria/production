<?php
    include_once("../config.nologin.php");
    include_once("../classes/class.user.inc.php");
    $user = new User($_POST["email"]);
    if($user->getId() && $user->checkPassword($_POST["password"])){
        u($user);
        watchdog('SUCCESS','LOGIN', "{$user->getFullName()}({$user->getEmail()}) logged in successfully.");
        
        # Media Wiki Authentication with Influence Members
        require_once('../MySQLActiveUser.php');
        global $MySQLActiveUserData ;
        global $DBConnection ;
 
        $userName = $user->getEmail() ;
        $sql = "SELECT * FROM users WHERE email='".$userName."';" ;
        #$result = mysql_query($command, $DBConnection);
        #$account = mysql_fetch_assoc($result) ;
        $account = db_get_row($sql);
        if ($account) {
            $current_mw_timecode = gmdate( 'YmdHis' ) ;
            # generate unique name out of email address for the WIKI
            $wiki_user_email = $account['email'];
            $tmp = explode("@",$wiki_user_email);
            $wiki_user_name = $tmp[0];
            if(strpos($tmp[0],".")) {
                $wiki_user_name = str_replace(".","-",$tmp[0]);
            }

            $MySQLActiveUserData->set_cookie($wiki_user_name,
                                             $account['id'],
                                             $_POST['password'],
                                             $account['email'],
                                             $current_mw_timecode) ;
        }
        #####################################
        if(isset($_POST['from_url'])){
            redirect(trim($_POST['from_url']));
        }
		else if($user->isAdmin()) {
            redirect("user.home.php");
        }
		else {
            redirect("task.timecard.php");
        }

    }
    else{
        e("Login failed. Invalid email/password.");
        watchdog('FAILED','LOGIN', "Login failed for ({$_POST['email']}). Invalid email/password. ");
        p(array("email" => $_POST["email"]));
        redirect("user.login.php");
    }
