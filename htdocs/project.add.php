<?php
    include_once("../config.php");
    require_perms("admin","manager");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.project.inc.php");
 

    $client = new Client($_GET["client_id"]);
    if($client->getId()){
        sm_assign("client", $client);
    }
    else{
        $clients = Client::GetClients(array("name", "ASC"));
        sm_assign("clients", $clients);
        foreach($clients as $clientId => $o_client) {
            $a_client = array(
                "id" => $clientId,
                "name" => $o_client->getName(),
            );
            $o_projects = $o_client->getProjectsByClient($clientId, true);
            foreach($o_projects as $proj_id => $o_project) {
                $a_project = array(
                    "id" => $proj_id, 
                    "name" => $o_project->getName(),
                );
                $a_client["projects"][$proj_id] = $a_project;
            }
            $data[$clientId] = $a_client;
        }
        sm_assign("data", json_encode($data));
        #debug($data);
    }

    $parent = new Project($_GET["parent_id"]);
    if($parent && $parent->getId() > 0 && !$parent->isSubproject()){
        sm_assign("parent", $parent);
        $o_users = $parent->getUsers();
    }
    else{
        $o_users = User::GetUsers();
        $parent = false;
    }
 
    $users = array();
    foreach($o_users as $o_user){
        if($parent)
		$role = Project::getUserRoleByProject($parent->getId(),$o_user->getId());
		
        $users[$o_user->getId()] = array(
            "id" => $o_user->getId(),
            "name" => $o_user->getName(),
            "email" => $o_user->getEmail(),
            "checked" => (bool) $parent,
            "role" => $role['role'],
        );
    }
    $status = "PENDING";
    $referer = strrev(substr(strrev($_SERVER['HTTP_REFERER']),0,strpos(strrev($_SERVER['HTTP_REFERER']),"/")));
    if($referer == "calendar.view.php") 
        $status ="PENDING";
    
    sm_assign("referer",$referer);   
    sm_assign("status",$status);   
    sm_assign("users", $users);
    sm_display("project.add.html");
