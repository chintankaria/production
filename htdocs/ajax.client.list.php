<?php
    include_once("../config.ajax.php");
    include_once("../classes/class.client.inc.php");
    if($_GET["query"]) {
        $clients = Client::GetClientListByName(trim($_GET['query']));
        $json = array();
        $names = array();
        $ides = array();
        $json["query"] = $_GET["query"];
        if($clients) {
            foreach($clients as $client){
                $ids[] = $client["id"];
                $names[] = $client["name"];
                $json["suggestions"] = $names;
                $json["data"] = $ids;
            }
        }
        else {
            $json["suggestions"] = $names;
            $json["data"] = $ids;
        }
        ob_clean();
        print json_encode($json);
    }
?>
