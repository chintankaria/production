<?php

include_once("../config.php");
require_perms("admin");

include_once("../classes/class.license.inc.php");
include_once("../classes/class.products.inc.php");
include_once("../classes/class.productmodule.inc.php");
include_once("../classes/class.client.contacts.inc.php");
include_once("../classes/class.licensetypes.inc.php");

include_once("../libs/alpha/alphaID.inc.php");

function return_json_error($message) {
    ob_clean();
    print json_encode(array(
        'status' => false,
        'error_message' => $message,
    ));
    die();
}

function return_json_success($data) {
    ob_clean();
    print json_encode(array(
        'status' => true,
        'payload' => $data,
    ));
    die();
}

$post = $_POST;

$product = new Products(intval($post['product_id']));
if($product->getId() < 1) {
    return_json_error("Invalid product");
}

$product_code = $product->getCode();

$license_version = $post['license_version'];
$license_start_date = date('Y-m-d', strtotime($post['license_start_date']));

$license = new License();
$license->setLicenseStartDate($license_start_date);

if($license_version == "1") {
	include_once("../libs/license/LicenseHelper_v1.php");

	$license->setLicenseVersionNumber(1);
	$license_type = LicenseTypes::Find($_POST['license_type']);
	if($license_type) {
		$license->setLicenseTypeId($license_type->getId());
	}
	
	if($post['license_type'] == '1') { // Evaluation
		$license_periods = new LicensePeriods($_POST["license_period_id"]);
		$duration = $license_periods->getDuration();
		$daymonthyear = $license_periods->getDaymonthyear();

		if($daymonthyear == 'Day(s)') $duration_months = $duration / 30;
		elseif($daymonthyear == 'Month(s)') $duration_months = $duration;
		elseif($daymonthyear == 'Year(s)') $duration_months = $duration * 12;
		if($duration_months < 1 || $duration_months > 3) $duration_months = 3; // As of now only a maximum of 3 months validity is allowed for EVAL licenses.

		try {
			$license_helper_v1 = new LicenseHelper_v1();
			$license_obj = $license_helper_v1->generateLicense($product_code, $duration_months, $license_start_date);
			$license->setLicensePeriodId($post['license_period_id']);
			$license->setLicenseKey($license_obj->getKey(true));
			$license->setLicenseEndDate($license_obj->getExpiryDate());
		} catch (Exception $e) {
			return_json_error($e->getMessage());
		}
	} else { // Commercial
		try {
			$license_helper_v1 = new LicenseHelper_v1();
			$license_obj = $license_helper_v1->generateLicense($product_code);
			$license->setLicensePeriodId(0);
			$license->setLicenseKey($license_obj->getKey(true));
			$license->setLicenseEndDate($license_obj->getExpiryDate());
		} catch (Exception $e) {
			return_json_error($e->getMessage());
		}
	}
} else {
	include_once("../libs/license/LicenseHelper.php");
	
	$license_end_date = date('Y-m-d', strtotime($post['license_end_date']));
	
	$license->setLicenseVersionNumber(2);
	$license_type = LicenseTypes::Find($_POST['license_type_id']);
	if($license_type) {
		$license->setLicenseTypeId($license_type->getId());
	}

	$modules = $product->getModules();
	$post_module_ids = array_filter($post['modules'], 'intval');

	$module_codes = array();
	$module_ids = array();
	foreach($post_module_ids as $v) {
		$m = $modules[$v];
		if($m) {
			$module_codes[$m->getCode()] = $m->getCode();
			$module_ids[$m->getId()] = $m->getId();
		}
	}

	try {
		$license_helper_v2 = new LicenseHelper();
		$license_key = $license_helper_v2->generateLicense($product_code, $license_end_date, $module_codes);
		$license->setLicensePeriodId(0);
		$license->setLicenseKey($license_key);
		$license->setLicenseEndDate($license_end_date);
	} catch(Exception $e) {
		return_json_error($e->getMessage());
	}
}	

$license->setProductId($post["product_id"]);
$license->setProductVersionId($post["product_version_id"]);
$license->setClientId($post["client_id"]);
$license->setAgreementId($post["agreement_id"]);
$license->setLocation($post["location"]);
$license->setGsRm($post["gs_rm"]);
$license->setTechnicalContactId($post["technical_contact_id"]);
$license->setBusinessContactId($post["business_contact_id"]);
$license->setNoticesContactId($post["notices_contact_id"]);
$license->setNotes($post["notes"]);
$license->setModuleIds($module_ids);

if($ret = $license->save()){
    $license_id = $license->getId();

    // Reload everything
    $license = new License($license_id);
    $product = new Products($license->getProductId());
    $contact = new ClientContact($license->getTechnicalContactId());

    sm_assign("product_name", $product->getName());
    sm_assign("contact_name", $contact->getFullName());
    sm_assign("license_type", ucfirst($license->getLicenseType()));
    sm_assign("download_key_validity", date('F jS Y', mktime(0, 0, 0, date("m")  , date("d")+6, date("Y"))));
    sm_assign("license_key", $license->getLicenseKey());
    sm_assign("license_start_date", $license->getLicenseStartDate());
    sm_assign("license_period", $license->getLicensePeriodName());
    sm_assign("client_name", $license->getClientName());

    // sm_display("product.download.email.html");
    
    $from = array('info@globalss.com' => 'GlobalsSoft Support');
    _::Mail(
        $from,
        array($contact->getEmailId() => $contact->getFullName()),
        "Thank you for downloading {$product->getName()}",
        sm_fetch("product.download.email.html")
    );

    watchdog('SUCCESS','ADD', "License saved successfully.");
    /*
    ob_end_clean();
    redirect("license.view.php?id={$license_id}");
    */
    return_json_success(array('license_id' => $license_id));
} else{
    watchdog('FAILED','ADD', "Failed to save license.");
    return_json_error("Failed to save license.");
}
