<?php
    include_once("../config.php");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.client.contacts.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");

    $client = new Client($_GET["id"]);
    if($client->getId() < 1) redirect("client.list.php");

    /* AJAX check.  Below if block is used in License section. */
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && $_GET["ajax"] == 'yes') {
        $contacts = $client->getContacts();
        $jsonArr = array();
        foreach($contacts as $v)
        {
            $jsonArr[] = array('Id' => $v->getId(), 'Name' => $v->getFirstName() . ' ' . $v->getLastName(), 'Email' => $v->getEmailId(), 'Phone' => $v->getPhone(), 'JobTitle' => $v->getJobTitle());
        }
        print_r(json_encode($jsonArr));
        die;
    }

    sm_assign("client", $client);

    sm_assign("files", TimesheetFiles::GetByClient($client->getId()));
    
    /*
    $files = TimesheetFiles::GetByClient($client->getId());
    $files = TimesheetFiles::SortByAssociations($files);
    sm_assign("files", $files);
    */

    sm_display("client.view.html");
