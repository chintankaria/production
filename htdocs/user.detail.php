<?php
    include_once("../config.php");
    require_perms("authuser");
    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.department.inc.php");
    include_once("../classes/class.designation.inc.php");
    include_once("../classes/class.product.knowledge.inc.php");
    include_once("../classes/class.technology.practice.inc.php");
    include_once("../classes/class.technology.skills.inc.php");
    include_once("../classes/class.training.skills.inc.php");

   
    if(u()->isAdmin()) 
        $user = new User($_GET["id"]);
    else
        $user = new User(u()->getId());

    //if(!$user || $user->getId() < 1) redirect("user.list.php");

    if($user->getId() == 1) {
        e("Administrator account can not be viewed!");
        redirect($_SERVER['HTTP_REFERER']);
        exit;
    }
    $isBusinessManager = in_array("business_manager" ,u()->getRoles());

    $user_roles = array(
                        "business_manager" => "Business Manager",
                        "calendar_manager" => "Calendar Manager",
                        "report_manager" => "Report Manager",
                        "minutes_manager" => "Minutes Manager",
                        "smt_member" => "SMT Member",
                        "hr_manager" => "HR Manager"
                        );


    if($user && $user->getId() > 0) {
        $r = $user->getRoles();
        $rRoles=array();
        foreach($r as $r1) {
            if($user_roles[$r1])
            $rRoles[] = $user_roles[$r1];
        }
        if($rRoles)
            $user->user_role = @implode(", ", $rRoles);
        else
            $user->user_role = "Member";
    } else { 
        $user = u();
        $r = $user->getRoles();
        $rRoles=array();
        foreach($r as $r1) {
            if($user_roles[$r1])
            $rRoles[] = $user_roles[$r1];
        }
        if($rRoles)
            $user->user_role = @implode(", ", $rRoles);
        else
            $user->user_role = "Member";
    }
    $employee = $user->getEmployee();
    $departments = Department::GetAll();    
    $designations = Designation::GetAll();    
    $prod_knowledge = ProductKnowledge::GetAll();
    $tech_practice = TechnologyPractice::GetAll();
    $tech_skills = TechnologySkills::GetAll();
    $training_skills = TrainingSkills::GetAll();

    $country = getCountryList();

    $trainer_style = $employee->IsTrainer() ? '' : 'display:none';
    
    $tabArr = array('eInfo', 'pInfo', 'cInfo','passInfo');

    $tab = isset($_GET['tab']) ? $_GET['tab'] : 'eInfo';
    if(!in_array($tab, $tabArr)) {
        $tab = 'eInfo';
    }   

    sm_assign("tab", $tab);
    sm_assign("trainer_style", $trainer_style);
    sm_assign("country", $country);
    sm_assign("departments", $departments);
    sm_assign("designations", $designations);
    sm_assign("prod_knowledge",$prod_knowledge);
    sm_assign("tech_practice", $tech_practice);
    sm_assign("tech_skills", $tech_skills);
    sm_assign("training_skills", $training_skills);
    sm_assign("employee", $employee);
    sm_assign("isBusinessManager", $isBusinessManager);
    sm_assign("user", $user);
    sm_display("user.detail.html");
