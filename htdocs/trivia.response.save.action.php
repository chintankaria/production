<?php
include_once("../config.php");
include_once("../classes/class.trivia.inc.php");
include_once("../classes/class.trivia.responses.inc.php");
include_once("../classes/class.trivia.points.inc.php");

$trivia = new Trivia($_POST["id"]);

if($trivia->getId() < 1) redirect("trivia.list.php");


if(isset($_POST["action"]) && $_POST["action"] == "TRIVIA_POINTS") {
    $points = (isset($_POST['rating']) && $_POST['rating'] == 'YES') ? 10 : 0;
    if($_POST['uID'] > 0 && $_POST['rID'] > 0) {
        $tPoint = new TriviaPoints(intval($_POST['rpID']));
        $tPoint->setUserId($_POST['uID']);
        $tPoint->setTriviaId($_POST['id']);
        $tPoint->setTriviaResponseId($_POST['rID']);
        $tPoint->setPoints($points);
        $tPoint->setDateTime(time());
        if($tPoint->save()) {
            print "Done!";
        }
    }
    exit;
}
else if($_POST["txtResponse"] != "") {

    $responses = new TriviaResponses();

    $responses->setDateTime(time());
    $responses->setUserId(u()->getId());
    $responses->setResponse($_POST["txtResponse"]);
    $responses->setTriviaId($_POST["id"]);
    $responses->setParentId($_POST["parent_id"]);
    if($responses->save()) {
        s("Trivia reponse posted successlfully.");
        redirect("trivia.view.php?id=".$_POST["id"]."#comment-{$responses->getId()}");
    }
    else {
        e("Failed to add response.");
        p($_POST);
        redirect("trivia.view.php?id=".$_POST["id"]."#frm");
    }
}
else {
    e("Failed to add response.");
    p($_POST);
}
redirect("trivia.view.php?id=".$_POST["id"]."#frm");
?>
