<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.meetingtypes.inc.php");
    $user = u();
    $mType = new MeetingTypes($_POST["id"]);
    $mType->setName($_POST["name"]);
    $mType->setCreatedBy($user->getEmail());
    $mType->setUpdatedBy($user->getEmail());
    if($_POST['action'] == "DELETE") {
        if($mType->delete()) {
            $str = "Meeting Type <i><b>{$_POST['name']}</b></i> deleted.";
            s("Meeting Type <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete meeting type.";
            e("Failed to delete meeting type.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$mType->checkName()) {
            if($mType->save()) {
                $str = "Meeting Type  <i><b>{$mType->getName()}</b></i> saved.";
                s("Meeting Type  <i><b>{$mType->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save meeting type {$_POST["name"]}";
                e("Failed to save meeting type {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Meeting type <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("meetingtypes.list.php");
    }
