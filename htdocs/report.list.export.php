<?php
    include_once("../config.php");
    require_perms("admin");
	include_once("../classes/class.task.inc.php");
	include_once("../classes/class.project.inc.php");
   // include_once("../classes/class.project.inc.php");	
	
	
	$user_obj = u();
	$user_id = $_GET["user_id"];
	$report_type = $_GET['report_type'];
	$grp_by = array();
	$grp_by = $_GET['grp_by'];
	if($report_type == 'custom_date'){
		$from =  $_GET["from"];
		$to =  $_GET["to"];
		$month_year = '';
	}else if($report_type == 'month'){
		$month_year =  $_GET["month_year"];
		$from =  '';
		$to = '';
	}
	
	$clients = Client::GetClients();
    sm_assign("clients", $clients);

    $client = new Client($_GET["client_id"]);
    if($client->getId() > 0){
	    $client_id = $client->getId();
        sm_assign("client", $client);
        $projects = $client->getProjects();
        sm_assign("projects", $projects);

        $project = $client->getProject($_GET["project_id"]);
		
        if($project && $project->getId() > 0){
			$has_subproject = $project->GetSubprojectsByProject($_GET["project_id"]);
	        sm_assign("has_subproject", $has_subproject);
			$parent_id = '';
			if($has_subproject)
			$parent_id = $_GET["project_id"];
			$project_id = $_GET["project_id"];

            sm_assign("project", $project);
            $subproject = $project->getSubproject($_GET["subproject_id"]);
			$subproject_id = $_GET["subproject_id"];
            sm_assign("subproject", $subproject);

            $subprojects = $project->getSubprojects();
            sm_assign("subprojects", $subprojects);
        }
    }

	
	$params = array();
	$params1 = array();
	if($user_id && !$client_id){

		$projects = Project::GetProjectsByUser($user_id);
		//foreach ($projects as $proj){
		 $params[] = array(
			'client_id' => $client_id,
			'user_id' => $user_id,
			'project_id' => $project_id,
			'parent_id' => $parent_id,
			'subproject_id' => $subproject_id,
			'from' => $from,
			'to' => $to,
			'month_year' => $month_year,
			'grp_by' => $grp_by,
		 );
		//}
		$task_list = array();
		foreach ($params as $in_params ){
		$tasks = Task::GetTasksReports($in_params);
		if($tasks)
		$task_list[] = $tasks;
		}
	}else {
		$params1 =  $_GET;
		$tasks = Task::GetTasksReports($params1);
		if($tasks)
		$task_list[] = $tasks;
		sm_assign("show_all", "1");
		$th_username = '<th class="aleft" width="200">Name</th>';

	}
		
	
	
	$html[] = '<tr>
                    <th class="aleft ui-corner-tl" width="200">Date</th>
					'.$th_username.'
                    <th class="aleft" width="200">Project</th>
					<th class="aleft" width="200">Sub project</th>
                    <th class="aleft" width="200">Task type</th>
                    <th class="aright" width="100">Task</th>
                    <th class="ui-corner-tr" width="100" colspan="2">Total hours</th>
                </tr>';
	$group_count = count($grp_by);
	
	foreach ($task_list as $task_list1){
		foreach ($task_list1 as $task_list2){
		if(!$user_id) 
		if($group_count) 	$duration_min = "<td><strong>" .m2hm($task_list2->getDurationMins()) . "<strong></td>";	
	    else $duration_min = "<td colspan='2'>" .m2hm($task_list2->getDurationMins()) . "</td>";	

		$td_username = "<td>" .$task_list2->getUserName() . "</td>";
		$project_name = $task_list2->getProjectParentId()?$task_list2->getParentName():$task_list2->getProjectName();	
		$sub_project_name = $task_list2->getProjectParentId()?$task_list2->getProjectName():"";
		$html[] = "<tr><td>" . date("m/d/Y",$task_list2->getTaskDate()) . "</td>
						".$td_username."
						<td>" .$project_name . "</td>
						<td>" .$sub_project_name . "</td>
						<td>" .$task_list2->getTaskTypeName(true) . "</td>
						<td>" .$task_list2->getDetail() . "</td>
						".$duration_min."
						<td></td></tr>";
		
		}
	  
	}
	$html = "<table border='1'>" . implode("\r\n", $html) . "</table>";	
	
	include_once("../classes/class.excelwriter.inc.php");
	$excel = new ExcelWriter();
	$excel->setHTMLTable($html, "TIMESHEET REPORT");
	$fileName = date("d-m-Y").'_mysql.xls';
	ob_clean();
	$excel->send($fileName);
	$excel->close();
	exit;
	
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=$fileName");
	echo $html;
	exit;	
	
	
