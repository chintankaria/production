<?php
include_once("../config.php");

$id = intval($_GET["id"]);
if($id) {
    $sql = "SELECT * FROM user_files WHERE id='$id' LIMIT 1";
    if($file = db_get_row($sql)){
        switch(strtoupper($file['file_type'])) {
            case 'USER_CV':
                $file['file_path'] = AppPath::$FILES."/user-cv/{$file['user_id']}";
                break;
            case 'PROFILE_PIC':
                $file['file_path'] = AppPath::$FILES."/pic/{$file['user_id']}";
                break;
        }
        if(!is_null($file['file_path'])) {
            ob_clean();
            header("Content-Type: {$file['mime_type']}");
            header("Content-Length: {$file['file_size']}");
            header("Content-Disposition: filename=\"{$file['file_name']}\"");
            readfile("{$file['file_path']}/{$file['md5_name']}");
            exit;
        }
        else {
            // No such file found
            e("No such user file exists!");
            redirect("user.list.php");
        }
    }
    else {
        // No such file found
        e("No such user file exists!");
        redirect("user.list.php");
    }
}
else {
    // Invalid ID
    e("Invalid user file!");
    redirect("user.list.php");
}
?>
