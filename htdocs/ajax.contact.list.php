<?php
    include_once("../config.ajax.php");
    include_once("../classes/class.contact.inc.php");
    if($_POST['type']) {
        if($_POST['id'] > 0) {
            $contact = new Contact(intval($_POST["id"]));
            if($contact->getId() < 1) {
                print "1|No contact details available";
                exit;
            }
            $html = "
                        <h4 class='connection-name'>
                            <a href='contact.view.php?id={$contact->getId()}'>{$contact->getFullName()}</a>
                        </h4>
                        <p class='headline'>{$contact->getClientName()}</p>
                    ";
            $html .= "<ul class='user-details'>";
            if($contact->getCommunication()) {
                $html .=" <li>";
                foreach($contact->getCommunication() as $v) {
                    if($v->getIsPrimaryCommunication()) {
                        $html .= " <div class='description'>Communication</div>
                                    <div class='detail'>{$v->getCommunicationDetails()} ";
                        if($v->getCommunicationExtension() > 0) {
                            $html .= " - {$v->getCommunicationExtension()}";
                        } 
                        $html .="</div>";
                    }
                }
                $html .=" </li>";
            }
            if($contact->getAddressBook()) {
                foreach($contact->getAddressBook() as $v) {
                    if($v->getIsPrimaryAddress()) {
                        $html .= "<li>
                                    <div class='description'>Address</div><div class='detail'>{$v->getAddress()}</div>
                                  </li>
                                  <li>
                                    <div class='description'>City</div><div class='detail'>{$v->getCity()}</div>
                                  </li>
                                  <li>
                                    <div class='description'>Zip</div><div class='detail'>{$v->getZip()}</div>
                                  </li>
                                  <li>
                                    <div class='description'>State</div><div class='detail'>{$v->getState()}</div>
                                  </li>
                                  <li>
                                    <div class='description'>Country</div><div class='detail'>{$v->getCountry()}</div>
                                  </li>
                                ";
                    }
                }
            } 
            $html .="</ul>
                    <div class='clear-both' style='float:right;'>
                        <a href='javascript:;' onclick='openWindow({$contact->getId()},\"".$contact->getFullName()."\")'>Print Version</a>
                        &nbsp;&nbsp;<a href='contact.view.php?id={$contact->getId()}'>more..</a>
                    </div>";
            print "0|$html"; exit;
        }
        else {
            $client_id = ($_POST['client_id'] > 0) ? $_POST['client_id'] : 0;
            $group_id = isset($_POST['group_id']) ? $_POST['group_id'] : 0;
            $con_alpha = Contact::GetContactsByAlpha(trim($_POST['type']),$client_id, $group_id);
            if(is_array($con_alpha) && count($con_alpha) > 0) {
                foreach($con_alpha as $val) {
                    $i = 0;
                    $html .= "<li class='letter-divider'><span class='{$val['name']}'>{$val['name']}</span></li>\n";
                    foreach($val['contacts'] as $v) {
                        $class = ($i == 0) ? 'first' : '';
                        $html .= "
                                <li class='$class' id='{$v->getId()}' stext='".strtolower($v->getFullName())."' onclick='contact_panel(this.id)'>
                                <div class='conn-wrapper'>
                                    <span class='conn-name'>{$v->getFullName()}</span>
                                    <span class='conn-headline'>
                                    <span class='company-name'>{$v->getClientName()}</span>
                                ";
                                if($v->getCommunication()) {
                                    foreach($v->getCommunication() as $b) {
                                        if($b->getIsPrimaryCommunication() == 1) {
                                            $html1 .= "{$b->getCommunicationDetails()}";
                                        if($b->getCommunicationExtension() > 0)
                                            $html1 .= "-{$b->getCommunicationExtension()}";
                                        }
                                    }
                                }
                        $html .= "
                                    </span>
                                </div>
                                <!--<div title='' class='conn-count'></div>-->
                                </li>
                                ";
                        $i++;
                    }
                }
                print "0|$html"; exit;
            }
            else {
                print "1|No contacts found."; exit;
            }
        }
    }
    print "Error!"; exit;

?>
