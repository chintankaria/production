<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.project.inc.php");

    $project = new Project($_GET["id"]);
    if($project->getId() < 1) redirect("project.list.php");

    sm_assign("project", $project);
    sm_display("project.delete.html");
