<?php
    include_once("../config.php");
    include_once("../classes/class.meetingtypes.inc.php");
    include_once("../classes/class.meeting.inc.php");
    if(!u()->isAdmin() && !u()->isMeetinMinutesManager()) {
        redirect("meeting.list.php");
    }
    $durations = Meeting::$MeetingDurations;
    $meeting_types = MeetingTypes::GetAll();
    $participants = MeetingTypes::GetStandardParticipants();
    $user_id = (!u()->isAdmin()) ? u()->getId() : 0; 
    $user_id = 0;
    $data = array();
    $usersArr = array();
    if($meeting_types){
    foreach($meeting_types as $key => $val) {
        $m_data = array(
                        "id" => $val->getId(),
                        "name" => $val->getName(),
                    );
        if($val->GetParticipants()) {
            foreach($val->GetParticipants() as $k => $v) {
                $a_participants = array(
                                    "id" => $v->getParticipantsId(),
                                    "user_id" => $v->getUserId(),
                                    "email" => $v->getEmail(),
                                );
                $m_data["participants"][$k] = $a_participants;
            }
        }
        $a_data[$key] = $m_data;
    }
    }
    $users = User::GetUsers();
    foreach($users as $o_user) {
        $a_user = array("id" => $o_user->getId(), "name" => $o_user->getName(), "email" => $o_user->getEmail());
        $usersArr[$o_user->getId()] = $a_user;
    }
    $data["meeting_types"] = $a_data;
    $data["users"] = $usersArr;

    sm_assign("data", json_encode($data));
    sm_assign("durations", $durations);
    sm_assign("participants", $participants);
    sm_assign("meeting_types", $meeting_types);
    sm_assign("users",$usersArr);
    sm_assign("user_id", $user_id);
    sm_display("meeting.add.html");
?>
