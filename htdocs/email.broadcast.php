<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.user.inc.php");

    $users = User::GetUsers();

    
    sm_assign("users", $users);

    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("email.broadcast.html");
        exit;
    }
    else {
        sm_display("email.broadcast.html");
        sm_assign("fetch", 0);
    }
?>
