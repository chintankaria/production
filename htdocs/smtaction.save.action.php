<?php
    include_once("../config.php");
    require_perms("smt_member","admin");
    include_once("../classes/class.smtaction.inc.php");
    include_once("../classes/class.smtaction.comments.inc.php");
    include_once("../libs/iCalendar/class.iCal.inc.php");

    if(isset($_POST["action"]) && $_POST["action"] == "DELETE") {
        $actionItem = new SMTAction($_POST["id"]);
        if($actionItem->Delete()) {
            $str = "Action item  <i><b>".$actionItem->getTitle()."</b></i> deleted successlfully.";
            s("Action item  <i><b>".$actionItem->getTitle()."</b></i> deleted successlfully.");
            watchdog("SUCCESS", "DELETE", $str);
        }
        else { 
            $str = "Failed to delete action item <i><b>".$actionItem->getTitle()."</b></i>.";
            e("Failed to delete action item <i><b>".$actionItem->getTitle()."</b></i>.");
            watchdog("FAILED", "DELETE", $str);
        }
        redirect("smtaction.list.php");
    }
    else {
        $server_path = strrev(substr(strrev($_SERVER['REQUEST_URI']),strpos(strrev($_SERVER['REQUEST_URI']),"/"),strlen($_SERVER['REQUEST_URI']) - strpos(strrev($_SERVER['REQUEST_URI']),"/")));

        if($_POST['action'] == "COMMENTS") {
            // Item Action comments
            $actionItem = new SMTAction($_POST["smt_id"]);
            $comments = new SMTComments();
            $comments->setSmtId($actionItem->getId());
            $comments->setComment($_POST['txtComment']);
            $comments->save();
            $str = "Comment(ID:{$comments->getId()}) added to <i>{$actionItem->getTitle}</i> action item.";
            watchdog("SUCCESS", "ADD", $str);
            redirect($_SERVER['HTTP_REFERER']);
        }
        else {
            $server_url = "http://".$_SERVER["SERVER_NAME"]."$server_path";
            $actionItem = new SMTAction($_POST["id"]);
            $actionItem->setTitle($_POST["txtTitle"]);
            $actionItem->setDescription($_POST["txtDescription"]);
            $actionItem->setDueDate($_POST["txtDueDate"]);
            $actionItem->setStatus($_POST["txtStatus"]);
            $actionItem->setCreatedBy(u()->getEmail()); 
            $users = implode($_POST["users"], ",");
            $actionItem->setAssignedTo($users);

            if ($actionItem->save()) {
                // prepare for sending vCalendar Invite
                $ics_file = create_ics();
                $organizer = array(u()->getEmail() => u()->getFullName());
                sm_assign("type" ,"ICS");
                sm_assign("description", $_POST['txtDescription']);
                if(is_array($_POST['users']) && count($_POST['users']) > 0) {
                    foreach($_POST['users'] as $uid) {
                        $attendees = array();
                        $u = new user($uid);
                        $attendees[$u->getEmail()] = $u->getFullName();
                        sm_assign("full_name", $u->getFullName());
                        _::Mail($organizer,$attendees,"SMT Action Item :: {$_POST['txtTitle']}", sm_fetch("smtaction.letter.html"), $ics_file);
                        $str = "SMT email notification to attendee Mr.{$u->getFullName()}({$u->getEmail()})";
                        watchdog("SUCCESS", "EMAIL", $str);
                    }
                }
                $attendees = array();
                $attendees[u()->getEmail()] = u()->getFullName();
                _::Mail($organizer,$attendees,"SMT Action Item :: {$_POST['txtTitle']}", sm_fetch("smtaction.letter.html"), $ics_file);


                $str = "Action item  <i><b>".$_POST['txtTitle']."</b></i> saved successlfully.";
                watchdog("SUCCESS", "ADD", $str);
                
                s("Action item  <i><b>".$_POST['txtTitle']."</b></i> saved successlfully.");
                redirect("smtaction.list.php");
            } else {
                $str = "Failed to add <i><b>".$_POST['txtTitle']."</b></i>action item.";
                watchdog("FAILED", "ADD", $str);
                e("Failed to add action item.");
                p($_POST);
                if ($actionItem->getId() > 0)
                    redirect("smtaction.edit.php");
                else
                    redirect("smtaction.add.php");
            }
        }
    }

    function create_ics() {
        // ICS File
        $days = array(2);
        $organizer = array(u()->getFullName(), u()->getEmail());
        $categories = array('SMT Action Item - Reminder');
        $attendees = array();   
        $attendees[u()->getFullName()] = u()->getEmail().',0';
        if(is_array($_POST['users']) && count($_POST['users']) > 0) {
            foreach($_POST['users'] as $uid) {
                $u = new user($uid);
                $attendees[$u->getFullName()] = $u->getEmail().',1';
            }
        } 
        $alarm = array( 0, 5, $_POST['txtTitle'], $_POST['txtDescription'], $attendees, 5, 3);

        $iCal = new iCal('',0,'');
        putenv("TZ=America/Los_Angeles"); 
        $start_time = strtotime("{$_POST['txtDueDate']} 09:00:00"); 
        $end_time = strtotime("{$_POST['txtDueDate']} 10:00:00"); 

        $iCal->addEvent(
                        $organizer, $start_time, $end_time, '', 0, $categories, $_POST['txtDescription'], $_POST['txtTitle'],2,$attendees,
                        5,0,$end_time,'',$days,1,'',$alarm,1,'','en',''
                    );

        //$iCal->outputFile('ics');
        
        $ics = $iCal->getOutput('ics');
        if(!is_dir(AppPath::$FILES."/iCal")) {
            @mkdir(AppPath::$FILES."/iCal", 0777);
        }
        $due_date = date("Y-m-d",strtotime($_POST['txtDueDate']));
        putenv("TZ=");
        $fname = AppPath::$FILES."/iCal/SMT_".$due_date."_".md5(uniqid()).".ics"; 
        @file_put_contents($fname,$ics);
        return $fname;
    }
?>
