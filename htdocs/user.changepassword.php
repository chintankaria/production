<?php
    include_once("../config.php");
    require_perms("authuser");

    sm_assign("user", u());
    sm_display("user.changepassword.html");
