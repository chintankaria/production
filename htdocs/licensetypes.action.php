<?php

include_once("../config.php");
require_perms("admin");

require_once('../classes/class.licensetypes.inc.php');

if($_POST['action'] == 'delete') {
	$license_type = LicenseTypes::Find($_POST['id']);
	
	if($license_type->getActiveLicenseCount() > 0) {
		e('There are active licenses using this type; cannot delete license type.');
		print json_encode(array('status' => false));
	} else {
		if(LicenseTypes::Delete($license_type)) {
			s('License type <i>"' . $license_type->getName() . '"</i> deleted.');
			print json_encode(array('status' => true));
		} else {
			print json_encode(array('status' => false));
		}
	}
} elseif($_POST['action'] == 'save') {
	$license_type = LicenseTypes::Save(new LicenseType(array(
		'id' => $_POST['id'], 
		'name' => $_POST['name'], 
		'description' => $_POST['description']
	)));

	if($license_type) {
		s('License type <i>"' . $license_type->getName() . '"</i> saved.');
		print json_encode(array('status' => true, 'id' => $license_type->getId()));
	} else {
		print json_encode(array('status' => false));
	}
}
