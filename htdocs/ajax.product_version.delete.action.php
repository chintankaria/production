<?php

/**
 * This should ideally be done via a class.
 * But we do not have a class for Product Version
 * Hence the dirty way.
 * 
 * Additionally, this DOES NOT clean up licenses.
 * 
 * @TODO: Make this better
 */

include_once("../config.php");
require_perms("admin");

$product_id = intval($_POST['product_id']);
$product_version_id = intval($_POST['product_version_id']);

$status = false;

if(db_execute("DELETE FROM product_versions WHERE id = '{$product_version_id}' AND product_id = '{$product_id}'")) {
	$license_ids = array();
	foreach(db_get_all("SELECT id FROM licenses WHERE product_id = '{$product_id}' AND product_version_id = '{$product_version_id}'") as $v) {
		$license_ids[] = $v['id'];
	}

	$license_ids_list = implode(', ', $license_ids);
	db_execute("DELETE FROM licenses WHERE id IN ({$license_ids_list})");
	db_execute("DELETE FROM licenses_logs WHERE license_id IN ({$license_ids_list})");
	db_execute("DELETE FROM license_modules WHERE license_id IN ({$license_ids_list})");
	
	$status = true;
}

ob_clean();
print json_encode(array('status' => $status));
