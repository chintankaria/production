<?php
include_once("../config.php");
include_once("../classes/class.project.users.php");

$parentProject = new Project($_GET["pid"]);
sm_assign("parentProject", $parentProject);

$project = new Project($_GET["id"]);
sm_assign("project", $project);

if ($project->getId() == 1000) {
    redirect("project.list.php");
}

$projectMemberStats = array();
$projectMemberStats['AuthorizedHours'] = 0;
$projectMemberStats['HoursIncurred'] = 0;
$projectMemberStats['RemainingHours'] = 0;
$projectMemberStats['AuthorizedAmount'] = 0;
$projectMemberStats['AmountIncurred'] = 0;
$projectMemberStats['RemaingAmount'] = 0;

$projectUserInfo = ProjectUsers::GetProjectUsers($project->getId());
//echo '<pre>'; print_r($projectUserInfo); echo '</pre>';
if($projectUserInfo){
    foreach($projectUserInfo as $memberData){        
        $projectMemberStats['AuthorizedHours'] += $memberData->getAuthHours(); 
        $projectMemberStats['AuthorizedAmount'] += ($memberData->getAuthHours() * $memberData->getBillingRate()); 
        
        $minIncurred = Task::getHoursIncurred($memberData->getUserId(), $project->getId(), $memberData->getProjectRoleId());        
        $hoursIncurred = ($minIncurred/60);
        
        $projectMemberStats['HoursIncurred'] += $hoursIncurred;
        $projectMemberStats['AmountIncurred'] += ($hoursIncurred * $memberData->getBillingRate());       
    }
} 

$projectMemberStats['RemainingHours'] =  ($projectMemberStats['AuthorizedHours'] - $projectMemberStats['HoursIncurred']);
$projectMemberStats['RemaingAmount'] =  ($projectMemberStats['AuthorizedAmount'] - $projectMemberStats['AmountIncurred']);

$activeTabIndex = isset($_GET["tab"]) ? $_GET["tab"] : 0;

if (isset($_SESSION['project_setup_message'])) {
    $msg = $_SESSION['project_setup_message'];
    unset($_SESSION['project_setup_message']);
}

if (stristr($msg, 'Error') || stristr($msg, 'Failed')) {
    $errors = $msg;
} else {
    $messages = $msg;
}
//echo '<pre>'; print_r($projectMemberStats); echo '</pre>';

$projectMemberStats['AuthorizedHours'] = number_format($projectMemberStats['AuthorizedHours'], 2, '.', ',');
$projectMemberStats['HoursIncurred'] = number_format($projectMemberStats['HoursIncurred'], 2, '.', ',');
$projectMemberStats['RemainingHours'] = number_format($projectMemberStats['RemainingHours'], 2, '.', ',');
$projectMemberStats['AuthorizedAmount'] = number_format($projectMemberStats['AuthorizedAmount'], 2, '.', ',');
$projectMemberStats['AmountIncurred'] = number_format($projectMemberStats['AmountIncurred'], 2, '.', ',');
$projectMemberStats['RemaingAmount'] = number_format($projectMemberStats['RemaingAmount'], 2, '.', ',');

sm_assign("projectMemberStats", $projectMemberStats);
sm_assign("activeTabIndex", $activeTabIndex);
sm_assign("messages", $messages);
sm_assign("errors", $errors);
sm_assign("pid", $_GET["pid"]);
sm_display("project.detail.html");