<?php
    include_once("../config.php");
    

    include_once("../classes/class.user.inc.php");

    $user = new User($_GET["id"]);
    $employee = $user->getEmployee();
    $adj_date = $_GET["adj_date"];
    $employee = $user->getEmployee();

    $adjusted = $employee->getAdjustment(format_date($adj_date,"Y-m"),$_GET["id"]);
    $adj_hours = $adjusted['adjusted_hours'];
    $adj_id = $adjusted['id'];
    $adj_reason = $adjusted['reason'];


    sm_assign("adj_date", $adj_date);
    sm_assign("adj_id", $adj_id);
    sm_assign("adj_hours",$adj_hours);
    sm_assign("adj_reason", $adj_reason);
    sm_assign("employee", $employee);
    sm_assign("user", $user);
    sm_display("adjust_pto.html");
?>