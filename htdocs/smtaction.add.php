<?php
    include_once("../config.php");
    require_perms("smt_member","admin");
    include_once("../classes/class.smtaction.inc.php");
    if(!u()->isAdmin() && !u()->isSMTMember()) {
        redirect("smtaction.list.php");
    }
    $user_id = (!u()->isAdmin()) ? u()->getId() : 0; 
    $users = User::GetUsers();
    foreach($users as $o_user) {
        if ($o_user->isSMTMember()) {
            $a_user = array("id" => $o_user->getId(), "name" => $o_user->getName(), "email" => $o_user->getEmail());
            $usersArr[$o_user->getId()] = $a_user;
        }
    }
    sm_assign("user_id", $user_id);
    sm_assign("users", $usersArr);
    sm_display("smtaction.add.html");
?>
