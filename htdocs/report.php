<?php
    include_once('../config.php');
    require_perms('admin', 'manager');
    header('Content-Type: text/html; charset=iso-8859-1');

    $login_user = u();
    $login_user_id = $login_user->getId();

    $_GET = $_REQUEST;
    $liScss = '';
    if(isset($_REQUEST['tab']) && strtolower($_REQUEST['tab']) == 'summary') {
        $liScss = 'active';
        $styleSummary = '';
        $styleUtil = 'display:none;';
    }
    
    $liUcss = '';
    if(isset($_REQUEST['tab']) && strtolower($_REQUEST['tab']) == 'util') {
        $liUcss = 'active';
        $styleSummary = 'display:none;';
        $styleUtil = '';
    }
    $tab = strtolower($_REQUEST['tab']);
    if($liScss == "" && $liUcss == "") {
        $liUcss = '';
        $liScss = 'active';
        $tab = 'summary'; 
        $styleSummary = '';
        $styleUtil = 'display:none;';
    }

    if($_REQUEST['d'] == 2){
        $ts = strtotime("{$_REQUEST['y']}-{$_REQUEST['m']}-01");
        if($ts){
            $dates = get_dates_of_month(date('Y-m-d', $ts));
            $from_ts = array_shift($dates);
            $to_ts = array_pop($dates);
        }
    }
    else{
        $ts = strtotime($_REQUEST['f']);
        if($ts){
            $from_ts = $ts;
            $ts = strtotime($_REQUEST['t']);
            if($ts) $to_ts = $ts;
        }
    }

    if(!$from_ts){
        $from_ts = strtotime('-1 month');
        $to_ts = strtotime('yesterday');
    }

    if(!$to_ts) $to_ts = $from_ts;

    if($from_ts > $to_ts) list($from_ts, $to_ts) = array($to_ts, $from_ts);
    sm_assign('from_ts', $from_ts);
    sm_assign('to_ts', $to_ts);
    $from = ymd($from_ts);
    $to = ymd($to_ts);

    if($login_user->isAdmin()) {
        $user_id_clause = '';
        $prj_id_clause = '';
        $subprj_id_clause = '';
    }
    else if($login_user->isReportManager()) {
        $prj_ids = $login_user->getProjectIds();
        $prj_id_clause = " AND c.id IN(".implode(", ", $prj_ids).")";
        $subprj_id_clause = " AND f.id IN(".implode(", ", $prj_ids).")";
        $user_id_clause = '';
    }
    else {
        $user_id_clause = "AND b.id = '{$login_user_id}'";
        $prj_id_clause = '';
        $subprj_id_clause = '';
    }

    $table = 'tmp_' . md5(uniqid(mt_rand(), true));
    //$table = 'tmp_report';
        //CREATE TEMPORARY TABLE {$table}
    $sql = "
        (SELECT
            a.task_date AS date, CONCAT_WS(' ', b.first_name, b.last_name) AS user,
            d.name AS client, c.name AS project, NULL as subproject, e.name AS task, a.detail AS task_detail,c.is_billable,
            a.duration_mins, UNIX_TIMESTAMP(a.task_date) AS task_date_ts,
            a.user_id, d.id AS client_id, a.project_id, NULL as subproject_id, a.task_type_id
        FROM tasks a
        INNER JOIN users b ON b.id = a.user_id
        INNER JOIN projects c ON c.id = a.project_id
        INNER JOIN clients d ON d.id = c.client_id
        LEFT JOIN task_types e ON e.id = a.task_type_id
        WHERE a.task_date BETWEEN '{$from}' AND '{$to}'
        {$user_id_clause}
        $prj_id_clause
        AND NOT a.deleted
        AND c.id > 0
        AND c.parent_id IS NULL)
        UNION
        (SELECT
            a.task_date AS date, CONCAT_WS(' ', b.first_name, b.last_name) AS user,
            d.name AS client, f.name AS project, c.name AS subproject, e.name AS task, a.detail AS task_detail,c.is_billable,
            a.duration_mins, UNIX_TIMESTAMP(a.task_date) AS task_date_ts,
            a.user_id, d.id AS client_id, c.parent_id AS project_id, a.project_id as subproject_id, a.task_type_id
        FROM tasks a
        INNER JOIN users b ON b.id = a.user_id
        INNER JOIN projects c ON c.id = a.project_id
        INNER JOIN clients d ON d.id = c.client_id
        LEFT JOIN task_types e ON e.id = a.task_type_id
        INNER JOIN projects f ON f.id = c.parent_id
        WHERE a.task_date BETWEEN '{$from}' AND '{$to}'
        {$user_id_clause}
        $subprj_id_clause
        AND NOT a.deleted
        AND c.id > 0
        AND c.parent_id IS NOT NULL)
    ";
    $sql = " CREATE TEMPORARY TABLE {$table} $sql";
    db_execute($sql);

    // Load users, clients, projects and sub projects list from the current scope of reports.
    $users = $clients = $projects = $subprojects = array();
    $sql = "
        SELECT DISTINCT
            user_id AS uid, user AS uname, client_id AS cid, client AS cname,
            project_id AS pid, project AS pname, subproject_id AS spid, subproject as spname
        FROM {$table}
    ";
    $user_ids = array();
    foreach(db_get_all($sql) as $v){
        if($v['pid'] == 1000) continue;

        $user_ids[$v['uid']] = $v['uid'];
        $users[$v['uid']]['id'] = $v['uid'];
        $users[$v['uid']]['name'] = $v['uname'];
        $users[$v['uid']]['clients'][$v['cid']] = $v['cid'];

        $clients[$v['cid']]['id'] = $v['cid'];
        $clients[$v['cid']]['name'] = $v['cname'];
        $clients[$v['cid']]['projects'][$v['pid']] = $v['pid'];

        $projects[$v['pid']]['id'] = $v['pid'];
        $projects[$v['pid']]['name'] = $v['pname'];

        if($v['spid'] > 0){
            $projects[$v['pid']]['subprojects'][$v['spid']] = $v['spid'];
            $subprojects[$v['spid']]['id'] = $v['spid'];
            $subprojects[$v['spid']]['name'] = $v['spname'];
        }
    }

    function _sort_by_name($a, $b){
        return strcasecmp($a['name'], $b['name']);
    }

    uasort($users, '_sort_by_name');
    uasort($clients, '_sort_by_name');
    uasort($projects, '_sort_by_name');
    uasort($subprojects, '_sort_by_name');

    sm_assign('user_data', json_encode(array('users' => $users, 'clients' => $clients, 'projects' => $projects, 'subprojects' => $subprojects)));
    sm_assign('users', $users);
    sm_assign('clients', $clients);
    sm_assign('projects', $projects);
    sm_assign('subprojects', $subprojects);

    $default_order = array(
        'date' => 'date',
        'user' => 'user',
        'client' => 'client',
        'project' => 'project',
        'subproject' => 'subproject',
        'is_billable' => 'is_billable',
        'task' => 'task',
    );

    $filters = array();
    if(($_SERVER['REQUEST_METHOD'] == 'POST') || ($tab == 'util')) {
        foreach(array('user_id', 'client_id', 'project_id', 'subproject_id') as $k){
            if($_POST["{$k}_all"] == 'all' || !is_array($_POST[$k])) $filters[$k] = array();
            else $filters[$k] = map_assoc(array_filter($_POST[$k], 'pintval'));
            $filters["{$k}_all"] = ($_POST["{$k}_all"] == 'all');
        }

        // Process grouping options
        $filters['g'] = $_POST['g'];
        if($filters['g']['subproject']) $filters['g']['project'] = 'project';
        $group = array();
        foreach($default_order as $k => $v){
            if($filters['g'][$k]) $group[$k] = $v;
        }
        $filters['g'] = $group;

        // Process options
        foreach(array('includepto', 'hidedetails', 'showtaskdetails','showtask','billableonly') as $v){
            if($_POST['o'][$v]) $filters['o'][$v] = $v;
        }

        if($filters['o']['includepto']) $filters['project_id'][] = 1000;

        $_SESSION['report-filter'] = $filters;
    }
    elseif($_REQUEST['fid']) $filters = get_saved_filter($_REQUEST['fid']);
    elseif($_SESSION['report-filter']) $filters = $_SESSION['report-filter'];
    else $filters['user_id_all'] = $filters['client_id_all'] = $filters['project_id_all'] = $filters['subproject_id_all'] = 'all';

    if(isset($_POST['save_and_apply'])){
        $filter_name = trim($_POST['filter_name']);
        if($filter_name){
            $filter_name = q($filter_name);
            $filter_data = q(serialize($filters));
            $sql = "
                INSERT INTO filters (user_id, name, data)
                VALUES ({$login_user_id}, {$filter_name}, {$filter_data})
                ON DUPLICATE KEY UPDATE
                data = VALUES(data),
                id = LAST_INSERT_ID(id)
            ";
            if(db_execute($sql)) $filters = get_saved_filter(db_insert_id());
        }
    }
    if($filters['user_id_all'] == 'all') $filters['user_id'] = map_assoc(array_keys($users));
    if($filters['client_id_all'] == 'all') $filters['client_id'] = map_assoc(array_keys($clients));
    if($filters['project_id_all'] == 'all') $filters['project_id'] = map_assoc(array_keys($projects));
    if($filters['subproject_id_all'] == 'all') $filters['subproject_id'] = map_assoc(array_keys($subprojects));

    sm_assign('filters', $filters);

    if($filters['user_id']) {
        $user_id_clause = 'AND user_id IN (' . implode(', ', $filters['user_id']) . ')';
        $user_ids = $filters['user_id'];
    }
    else if(is_array($user_ids) && count($user_ids) > 0) {
        $user_id_clause = 'AND user_id IN (' . implode(', ', $user_ids) . ')';
    }
    else $user_id_clause = "AND user_id IS NULL OR user_id = ''";

    if($filters['client_id']) $client_id_clause = 'AND client_id IN (' . implode(', ', $filters['client_id']) . ')';
    else $client_id_clause = "AND client_id IS NULL OR client_id = ''";

    if($filters['project_id']) $project_id_clause = 'AND project_id IN (' . implode(', ', $filters['project_id']) . ')';
    else $project_id_clause = "AND project_id IS NULL OR project_id = ''";

    $subproject_id_condition = array("subproject_id IS NULL OR subproject_id = ''");
    if($filters['subproject_id']) $subproject_id_condition[] = 'subproject_id IN (' . implode(', ', $filters['subproject_id']) . ')';
    $subproject_id_clause = 'AND (' . implode(' OR ', $subproject_id_condition) . ')';

    $billable_clause = '';
    if($filters['o']['billableonly']) 
        $billable_clause = ' AND is_billable = 1';

    $order = $filters['g'];
    foreach($default_order as $k => $v){
        if($order[$k]) continue;
        $order[$k] = $v;
    }

    $f = array_values($order);
    $fields = implode(', ', $f);
    $report_detail = $report_summary = array();
    $sql = "
        SELECT {$fields}, task_detail, duration_mins, task_date_ts, user_id, client_id, project_id, subproject_id, task_type_id
        FROM {$table}
        WHERE 1
        {$user_id_clause}
        {$client_id_clause}
        {$project_id_clause}
        {$subproject_id_clause}
        {$pto_clause}
        {$billable_clause}
        ORDER BY {$fields}
    ";

    $uname_ids = array();
    foreach(db_get_all($sql) as $row){
        $report_detail[$row[$f[0]]][$row[$f[1]]][$row[$f[2]]][$row[$f[3]]][$row[$f[4]]][] = $row;

        if($filters['g'][$f[3]]) $report_summary[$row[$f[0]]][$row[$f[1]]][$row[$f[2]]][$row[$f[3]]]['duration_mins'] += $row['duration_mins'];
        if($filters['g'][$f[2]]) $report_summary[$row[$f[0]]][$row[$f[1]]][$row[$f[2]]]['duration_mins'] += $row['duration_mins'];
        if($filters['g'][$f[1]]) $report_summary[$row[$f[0]]][$row[$f[1]]]['duration_mins'] += $row['duration_mins'];
        if($filters['g'][$f[0]]) $report_summary[$row[$f[0]]]['duration_mins'] += $row['duration_mins'];
        $report_summary['duration_mins'] += $row['duration_mins'];

        $user_names[$row['user_id']] = $row['user'];
        $uname_ids[$row['user']] = $row['user_id'];
    }
    if($tab == 'util') {
        if(is_array($uname_ids) && count($uname_ids) > 0) {
            # sort array
            $a = natksort($uname_ids);
            $userCount = array();
            foreach($uname_ids as $u_name => $u_id) {
                $data = calculate_productivity_utilization($u_id, $from, $to,true);
                $total_pto = 0;
                $clientCount = array();
                $projectCount = array();
                foreach($data as $key => $val) {
                    $blnBillable=false;
                    if($key == 'timecard_data') {
                        foreach($val as $cname => $v) {
                            #if(!$v['is_billable']) continue;
                            if(strpos($cname, "PTO") > 0) {
                                $total_pto += $data['total_PTO'];
                                unset($data[$key][$cname]);
                            }
                            else {
                                foreach($v['projects'] as $k1 => $v1) {
                                    $data[$key][$cname]['projects'][$k1]['total_billable'] = intval($v1['total_billable_mins']) + intval($v1['total_holiday_billable_mins']);
                                    
                                    $projectCount[$k1] = $k1;
                                    # User Project Total HOurs
                                    $gdata[$u_id]['project'][$k1]['Billable'] = intval($v1['total_billable_mins']) + intval($v1['total_holiday_billable_mins']);
                                    $gdata[$u_id]['project'][$k1]['Unassigned'] = $v1['total_mins'];
                                    # Project Total HOurs
                                    $gdata['all']['project'][$k1]['Billable'] += intval($v1['total_billable_mins']) + intval($v1['total_holiday_billable_mins']);
                                    $gdata['all']['project'][$k1]['Unassigned'] += $v1['total_mins'];
                                }
                                $data[$key][$cname]['total_billable'] = intval($v['total_billable_mins']) + intval($v['total_holiday_billable_mins']);
                                //$data['standard_hours_in_mins'] += intval($data[$key][$cname]['standard_hours_in_mins']);
                                $clientCount[$cname] = $cname;
                                # User Client Total HOurs
                                $gdata[$u_id]['client'][$cname]['Billable'] = intval($v['total_billable_mins']) + intval($v['total_holiday_billable_mins']);
                                $gdata[$u_id]['client'][$cname]['Unassigned'] = $v['total_mins'];
                                # Client Total HOurs
                                $gdata['all']['client'][$cname]['Billable'] += intval($v['total_billable_mins']) + intval($v['total_holiday_billable_mins']);
                                $gdata['all']['client'][$cname]['Unassigned'] += $v['total_mins'];
                            }
                        }
                    }
                }
                $userCount[$u_id] = $u_id;
                $data['total_pto'] = $total_pto;
                $data['total_billable'] = intval($data['total_billable_mins']) + intval($data['total_holiday_billable_mins']);
                # User Total HOurs
                $gdata[$u_id]['total_clients'] = count($clientCount);
                $gdata[$u_id]['total_projects'] = count($projectCount);
                $gdata[$u_id]['total']['PTO'] = intval($total_pto);
                $gdata[$u_id]['total']['Billable'] = intval($data['total_billable']);
                $gdata[$u_id]['total']['Unassigned'] = intval($data['total_standard_hours_in_mins']) - intval($data['total_billable_mins']);
                # Total Hours
                $gdata['all']['total_clients'] += count($clientCount);
                $gdata['all']['total_projects'] += count($projectCount);
                $gdata['all']['total_users'] += count($userCount);
                $gdata['all']['users'][$u_name]['Billable'] = intval($data['total_billable']);
                $gdata['all']['users'][$u_name]['Unassigned'] = intval($data['total_standard_hours_in_mins']) - intval($data['total_billable_mins']);
                $gdata['all']['total']['PTO'] += intval($total_pto);
                $gdata['all']['total']['Billable'] += intval($data['total_billable']);
                $gdata['all']['total']['Unassigned'] += intval($data['total_standard_hours_in_mins']) - intval($data['total_billable_mins']);
                $data['user_id'] = $u_id;
                $day_count += $data['day_count'];
                $timecard_utilization[$u_name] = $data; 
            }
        }
        $standard_hours = getStandardHours();
        $standard_hours_in_mins = $standard_hours["standard_hours_in_mins"];
        $utilization = @round(($gdata['all']['total']['Billable'] / (($day_count * $standard_hours_in_mins) - $gdata['all']['total']['PTO']) * 100), 2);
        sm_assign("cdata", json_encode($gdata));
        sm_assign("gdata", $gdata['all']);
        sm_assign("utilization", $utilization);
    }
    else {
        $timecard_utilization = calculate_weekly_timecard($filters['user_id'], $from, $to);
        sm_assign("cdata", 0);
    }
    #debug($timecard_utilization);

    sm_assign('timecard_utilization', $timecard_utilization);
    sm_assign('report_detail', $report_detail);
    sm_assign('report_summary', $report_summary);
    sm_assign('fields', $f);

    sm_assign("liScss", $liScss);
    sm_assign("liUcss", $liUcss);
    sm_assign("tab", $tab);
    sm_assign("styleSummary", $styleSummary);
    sm_assign("styleUtil", $styleUtil);

    if($_REQUEST['excel'] == 1){
	   include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch('table.report.html'), 'Reports');
    	$file_name = date('d-m-Y').'_reports.xls';
    	ob_clean();
        header('Content-Type: text/html; charset=iso-8859-1');
    	$excel->send($file_name);
    	$excel->close();
    }
    else{
        // For dropdowns: months, years
        sm_assign('months', array(1 => 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'));

        $years = array(); $cyear = date('Y');
        for($i = 2009; $i <= $cyear; $i++) $years[$i] = $i;
        sm_assign('years', $years);

        // Load saved filters
        $sql = "
            SELECT id, name FROM filters
            WHERE user_id = '{$login_user_id}'
            ORDER BY name
        ";
        sm_assign('saved_filters', db_get_all($sql));

        sm_display('report.html');
    }
    exit;

    function get_saved_filter($fid){
        global $login_user_id;

        $filters = array();
        $fid = intval($fid);
        $sql = "
            SELECT id, name, data FROM filters
            WHERE id = '{$fid}'
            AND user_id = '{$login_user_id}'
        ";
        $rs = db_get_row($sql);
        if($rs){
            $filters = unserialize($rs['data']);
            $filters['filter_id'] = $rs['id'];
            $filters['filter_name'] = $rs['name'];
        }

        return $filters;
    }
    function natksort(&$aToBeSorted) {
        $aResult = array();
        if(is_array($aToBeSorted) && count($aToBeSorted) > 0 ) {
            $aKeys = array_keys($aToBeSorted);
            natcasesort($aKeys);
            foreach ($aKeys as $sKey) {
                $aResult[$sKey] = $aToBeSorted[$sKey];
            }
            $aToBeSorted = $aResult;
            return True;
        }
        return false;
    }

