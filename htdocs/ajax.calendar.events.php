<?php
include_once("../config.ajax.php");
include_once("../classes/class.calendarevents.inc.php");

$start_date = ymd($_GET['start']);
$end_date = ymd($_GET['end']);
$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
$client_id = isset($_GET['client_id']) ? $_GET['client_id'] : 0;
$project_id = isset($_GET['project_id']) ? $_GET['project_id'] : 0;
$subproject_id = isset($_GET['subproject_id']) ? $_GET['subproject_id'] : 0;
$chkUserColor = isset($_GET['chkUserColor']) ? $_GET['chkUserColor'] : 0;

$param['start_date'] = $start_date;
$param['end_date'] = $end_date;
$param['client_id'] = $client_id;
$param['project_id'] = $project_id;
$param['subproject_id'] = $subproject_id;
$param['user_id'] = $user_id;
$param['chkUserColor'] = $chkUserColor;

#$events = CalendarEvents::GetJavascriptEventsByUser(u(), $param);
$events = CalendarEvents::GetProjectTimeline($param);

ob_clean();
print json_encode($events);

