<?php
    include_once("../config.php");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.client.inc.php");
    
    if($_GET["client_id"]){
        $client = new Client($_GET["client_id"]);
        if($client && $client->getId() > 0){
            sm_assign("client", $client);
            $projects = $client->getProjects();
        }
    }
    elseif($_GET["parent_id"]){
        $parent = new Project($_GET["parent_id"]);
        
        if($parent && $parent->getId() > 0){
            sm_assign("parent", $parent);
            $projects = $parent->getSubprojects();
        }
    }
    else {
        $user = u();
        if($user->isAdmin()){    
			$excel_no_field = 7;
            $projects = Project::GetProjects(); 
        }else{
			$excel_no_field = 8;
            $projects = Project::GetProjectsByUser($user, false, true);
		}
    }
    $clients = Client::GetClients();
    $pending = 0;
    if(isset($_SESSION['pending_prj'])) {
        if(isset($_GET['pending']) > 0)  {
            $pending = intval($_GET['pending']);
        }
        else {
            $pending = $_SESSION['pending_prj'];
        }
    }
    $_SESSION['pending_prj'] = $pending;
    $closed = 0;
    if(isset($_SESSION['closed_prj'])) {
        if(isset($_GET['closed']))  {
            $closed = intval($_GET['closed']);
        }
        else {
            $closed = $_SESSION['closed_prj'];
        }
    }
    $_SESSION['closed_prj'] = $closed;
    $proj = $projects;
    $projects = array();
    foreach ($proj as $proj_id => $v) {
        $client_id = $v->getClientId();
        if(!$pending && $v->getStatus() == "PENDING") 
            continue;
        $projects[$proj_id] = $proj[$proj_id];
    }
    if(u()->isAdmin()) {
        $allClients = Client::GetClients(0,1); 
        foreach ($allClients as $client_id => $v) {
            if(!$v->getIsActive())
                continue;
            $prj_details[$client_id]['name'] = $v->getName();
            $prj_details[$client_id]['projects'] = array();
            $projs = Project::GetProjectsByClient($client_id);
            foreach ($projs as $proj_id => $w) {
                if(!$pending && $w->getStatus() == "PENDING")
                    continue;
                $prj_details[$client_id]['projects'][$proj_id]['name'] = $w->getName();
                if($w->countSubprojects() > 0) {
                    foreach($w->getSubprojects() as $val) {
                        $prj_details[$client_id]['projects'][$proj_id]['subprojects'][$val->getId()]['name'] = $val->getName();
                    }
                } 
            }
        }
    }  
    
    sm_assign("closed", $closed);
    sm_assign("pending", $pending);
    sm_assign("clients", $clients);
    sm_assign("projects", $projects);
    sm_assign("prj_details", json_encode($prj_details));

    if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
		$excel->setHTMLTable(sm_fetch("table.project.list.html"), "Project List", null, array($excel_no_field));    	
    	$fileName = date("d-m-Y").'_project_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    sm_display("project.list.html");
