<?php
    include_once("../config.php");
    require_perms("hr_manager","admin");
    include_once("../classes/class.employees.inc.php");

    $member = u();

    $is_timesheet = isset($_GET['is_timesheet']) ? intval($_GET['is_timesheet']) : 0; // By default users

    $users = User::GetUsers(true,false,$is_timesheet);
    $blocked = (isset($_GET['blocked']) && $_GET['blocked'] > 0) ? 1 : 0;

    $user_details = $users;
    $users = array();
    $user_roles = array(
                        "business_manager" => "Business Manager",
                        "calendar_manager" => "Calendar Manager",
                        "report_manager" => "Report Manager",
                        "minutes_manager" => "Minutes Manager",
                        "smt_member" => "SMT Member",
                        "hr_manager" => "HR Manager"
                    );
    foreach($user_details as $k => $v) {
        $r = $v->getRoles();
        $rRoles=array();
        foreach($r as $r1) {
            if($user_roles[$r1])
            $rRoles[] = $user_roles[$r1];
        }
        if($rRoles)
            $v->user_role = @implode(" ", $rRoles);
        else 
            $v->user_role = "Member";
        if($blocked && $v->isBlocked()) {
            $users[$k] = $user_details[$k];
        }
        else {
            if(!$v->isBlocked()) 
                $users[$k] = $user_details[$k];
        }
    }

	#debug($users);
    sm_assign("blocked", $blocked);
    sm_assign("users", $users);
    sm_assign("member", $member);

	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.user.list.html"), "User List", null, array(6));
    	$fileName = date("d-m-Y").'_user_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    sm_display("user.list.html");
