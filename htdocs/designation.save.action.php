<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.designation.inc.php");
    $designation = new Designation($_POST["id"]);
    $designation->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($designation->delete()) {
            s("Designation  <i><b>{$_POST['name']}</b></i> deleted.");
            $str = "Designation  <i><b>{$_POST['name']}</b></i> deleted.";
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            e("Failed to delete designation.");
            $str = "Failed to delete designation.";
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$designation->checkName()) {
            if($designation->save()) {
                s("Designation <i><b>{$designation->getName()}</b></i> saved.");
                $str = "Designation <i><b>{$designation->getName()}</b></i> saved.";
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                e("Failed to save designation {$_POST["name"]}");
                $str = "Failed to save designation {$_POST["name"]}";
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Designation <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("designation.list.php");
    }
