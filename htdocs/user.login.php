<?php
    include_once("../config.nologin.php");
    include_once("../classes/class.user.inc.php");
    $user = u();
    $url = "task.timecard.php";
    if(isset($_GET['url'])) {
        $url = trim($_GET['url']);
    }
    sm_assign("from_url", $url);
    if($user->isLogin()) {
        redirect($url);
    }
    else sm_display("user.login.html");
    
