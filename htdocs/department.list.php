<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.department.inc.php");
    $department = Department::GetAll();

    sm_assign("department", $department);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.department.list.html"), "Department list",null, array(3));
    	$fileName = date("d-m-Y").'_department_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("department.list.html");
        exit;
    }
    else {
        sm_display("department.list.html");
    }
