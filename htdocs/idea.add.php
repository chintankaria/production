<?php
    include_once("../config.php");
    include_once("../classes/class.ideatracker.inc.php");
    $user_id = (!u()->isAdmin()) ? u()->getId() : 0; 
    $user_id = 0;
    $data = array();
    $usersArr = array();
    
    $users = User::GetUsers();
    foreach($users as $o_user) {
        $a_user = array("id" => $o_user->getId(), "name" => $o_user->getName(), "email" => $o_user->getEmail());
        $usersArr[$o_user->getId()] = $a_user;
    }
    $data["users"] = $usersArr;

    sm_assign("data", json_encode($data));
    sm_assign("users",$usersArr);
    sm_assign("user_id", $user_id);
    sm_display("idea.add.html");
?>
