<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.employee.location.inc.php");
    $employee_location = new EmployeeLocation($_POST["id"]);
    $employee_location->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($employee_location->delete()) {
            s("Employee Location  <i><b>{$_POST['name']}</b></i> deleted.");
            $str = "Employee Location  <i><b>{$_POST['name']}</b></i> deleted.";
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            e("Failed to delete employee location.");
            $str = "Failed to delete employee location.";
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$employee_location->checkName()) {
            if($employee_location->save()) {
                s("Employee Location <i><b>{$employee_location->getName()}</b></i> saved.");
                $str = "Employee Location <i><b>{$employee_location->getName()}</b></i> saved.";
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                e("Failed to save employee location {$_POST["name"]}");
                $str = "Failed to save employee location {$_POST["name"]}";
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Employee Location <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("employeelocation.list.php");
    }
