<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.category.inc.php");
    $category = new Category($_POST["id"],'GENERAL_DOCUMENT');
    $category->setGroupId($_POST["group_id"]);
    $category->setName($_POST["name"]);
    $category->setCategoryType('GENERAL_DOCUMENT');
    if($_POST['action'] == "DELETE") {
        if($category->delete()) {
            $str = "Document Type  <i><b>{$_POST['name']}</b></i> deleted.";
            s("Document Type  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete category.";
            e("Failed to delete category.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$category->checkName()) {
            if($category->save()) {
                $str = "Document Type  <i><b>{$category->getName()}</b></i> saved.";
                s("Document Type  <i><b>{$category->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save category {$_POST["name"]}";
                e("Failed to save category {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Document Type <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("general.category.list.php");
    }
