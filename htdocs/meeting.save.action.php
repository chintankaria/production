<?php
    include_once("../config.php");
    include_once("../classes/class.meeting.inc.php");

    if(isset($_POST["action"]) && $_POST["action"] == "DELETE") {
        $meeting = new Meeting($_POST["id"]);
        if($meeting->Delete()) {
            $str = "Meeting <i><b>".$meeting->getTitle()."</b></i> deleted successlfully.";
            s("Meeting <i><b>".$meeting->getTitle()."</b></i> deleted successlfully.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete meeting <i><b>".$meeting->getTitle()."</b></i>.";
            e("Failed to delete meeting <i><b>".$meeting->getTitle()."</b></i>.");
            watchdog('FAILED','DELETE', "$str");
        }
        redirect("meeting.list.php");
    }
    else {
    $server_path = strrev(substr(strrev($_SERVER['REQUEST_URI']),strpos(strrev($_SERVER['REQUEST_URI']),"/"),strlen($_SERVER['REQUEST_URI']) - strpos(strrev($_SERVER['REQUEST_URI']),"/")));
    $server_url = "http://".$_SERVER["SERVER_NAME"]."$server_path";
    $send_notifiation = 1;
    $meeting = new Meeting($_POST["id"]);
    if($_POST["id"] > 0) {
        if(!$meeting->getParentId())
            $meeting->setParentId($_POST["id"]);
        $send_notifiation = isset($_POST["send_notification"]) ? $_POST["send_notification"] : 0;
    }
    $meeting->setTitle($_POST["txtTitle"]);
    $meeting->setDateTime($_POST["txtDateTime"]);
    $meeting->setMeetingDuration($_POST["meeting_duration"]);
    $meeting->setMeetingTypeId($_POST["meeting_type_id"]);
    $meeting->setMeetingMinutes($_POST["txtMinutes"]);
    $meeting->setActionItems($_POST["txtActionItems"]);
    #$meeting->setStatus($_POST["txtStatus"]);
    $meeting->setCreatedBy(u()->getEmail());

    //if(!$meeting->checkTitle()) {
        if($meeting->save()) {
            $meeting->markInactive($_POST["id"]);
            foreach($_POST["users"] as $v) {
                $tmp = explode("|",$v);
                $meeting->saveParticipants($tmp[0],$tmp[1]);
            }
            if($_POST["txtOtherParticipants"] != "") {
                if(strpos($_POST["txtOtherParticipants"],",") > 0) {
                    $tmp = explode(",",$_POST["txtOtherParticipants"]);
                    for($i=0; $i < count($tmp); $i++) {
                        if($tmp[$i] != "") {
                            $meeting->saveParticipants(0,$tmp[$i]);
                        }
                    }
                }
                else {
                    $meeting->saveParticipants(0,$_POST["txtOtherParticipants"]);
                }
            }
            $new_meeting = new Meeting($meeting->getId());
            sm_assign("meeting", $new_meeting);
            sm_assign("server_url", $server_url);
            if($send_notifiation) {
                if($new_meeting->GetParticipants()) {
                     // get the CSS
                    $css = '';
                    $handle = fopen("css/style.css", "r");
                    $css .= fread($handle, filesize("css/style.css"));
                    fclose($handle);
                    $handle = fopen("css/jquery-ui.css", "r");
                    $css .= fread($handle, filesize("css/jquery-ui.css"));
                    fclose($handle);
                    sm_assign("css", $css);
                    $from = array(u()->getEmail() => u()->getFullName());
                    foreach($new_meeting->GetParticipants() as $k => $v) {
                        $full_name = $v->getEmail();
                        if($v->getUserId() > 0) $full_name = $v->getFullName();
                        $m_participants = array( $v->getEmail() => $full_name);
                        $subject = $new_meeting->getDateTime() ." - ".$new_meeting->getTitle(); 
                        if($new_meeting->getRevision() > 1)
                            $subject = $new_meeting->getDateTime() ." - ".$new_meeting->getTitle() ." [Revison:".$new_meeting->getRevision()."]";
                        _::Mail($from, $m_participants, $subject,sm_fetch("meeting.letter.html"));
                        $str = "Sending meeting <b><i>$subject</i></b> notification to $full_name({$v->getEmail()}).";
                        watchdog('SUCCESS','EMAIL', "$str");
                    }
                }
            }
            $str = "Meeting <i><b>".$_POST['txtTitle']."</b></i> saved successlfully.";
            s("Meeting <i><b>".$_POST['txtTitle']."</b></i> saved successlfully.");
            watchdog('SUCCESS','ADD', "$str");
            redirect("meeting.list.php");
        }
        else {
            $str = "Failed to add meeting {$_POST['txtTitle']}.";
            e("Failed to add meeting.");
            watchdog('FAILED','ADD', "$str");
            p($_POST);
            redirect("meeting.add.php");
        }
    /*}
    else {
        if($_POST["id"] > 0) {
            e("Meeting <i><b>{$_POST["txtTitle"]}</b></i> already exist!");
            redirect("meeting.edit.php?id={$_POST["id"]}");
        }
        else {
            e("Meeting <i><b>{$_POST["txtTitle"]}</b></i> already exist!");
            p($_POST);
            redirect("meeting.add.php");
        }
    }*/
    }
?>
