<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.employee.types.inc.php");
    $employee_types = EmployeeTypes::GetAll();

    sm_assign("employee_types", $employee_types);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.employeetypes.list.html"), "Employee Types list",null, array(3));
    	$fileName = date("d-m-Y").'_employeetypes.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("employeetypes.list.html");
        exit;
    }
    else {
        sm_display("employeetypes.list.html");
    }
