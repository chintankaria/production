<?php
    include_once("../config.php");
    require_perms("manager","admin");
    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.task.inc.php");

	$user = new User($_GET["user_id"]);
	if($user->getId() < 1) $user = u();

			
    if(!u()->isAdmin()){
	    $users = array();
		$usersList = Project::getUsersByManager(u()->getId());
		foreach( $usersList as $list){
		    $userslist[] = array(
				"id" => $list['user_id'],
				"name" => $list['first_name']." ".$list['middle_name']." ".$list['last_name'],
			);
		}
		sm_assign("userslist", $userslist);
	}
	
	
    sm_assign("user", $user);
	sm_assign("users", User::GetUsers());

    $date = ymd(is_date($_GET["date"], true));
	$manager_id = u()->getId();
    $tasks = $user->getTimecard($date,$manager_id);
    //echo '<pre>'; print_r($tasks); echo '</pre>';
    if($tasks && $manager_id) {
        foreach($tasks as $k =>$v) {
            $frm_dt = $v['from'];
            $to_dt = $v['to'];
            $tasks[$k]['timecard_utilization'] = calculate_weekly_timecard($user->getId(),$frm_dt, $to_dt);
        }
    }
    $prev_month = date("m/01/Y", strtotime("{$date} -1 month"));
    $next_month = date("m/01/Y", strtotime("{$date} +1 month"));
	
    sm_assign("date", date("F", strtotime($date)));
    sm_assign("prev_month", $prev_month);
    sm_assign("next_month", $next_month);

    sm_assign("tasks", $tasks);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.task.timecard.html"), "Task timecard list",null, array(19));
    	$fileName = date("d-m-Y").'_task_timecard.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    sm_display("task.timecard.html");
