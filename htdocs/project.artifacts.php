<?php
include_once("../config.php");
include_once("../classes/class.category.inc.php");
include_once("../classes/class.timesheetfiles.inc.php");

$project = new Project($_GET["id"]);
sm_assign("project", $project);

$category = Category::GetAll('PROJECT_DOCUMENT');
$files = TimesheetFiles::GetByProject($project->getId(), false, true);

foreach ($files as $k => $v) {
    $v->userFullName = User::getFullNameByEmailId($v->getCreatedBy());
    $v->userId = User::getUserIdByEmailId($v->getCreatedBy());
    $v->createdDate = date("m/d/Y h:i a", $v->getUploadDate());
    if ($v->getFileHistory()) {
        foreach ($v->getFileHistory() as $k1 => $v1) {
            $v1->userFullName = User::getFullNameByEmailId($v1->getCreatedBy());
            $v1->userId = User::getUserIdByEmailId($v1->getCreatedBy());
            $v1->createdDate = date("m/d/Y h:i a", $v1->getUploadDate());
        }
    }
}
//echo '<pre>'; print_r($files); echo '</pre>';
sm_assign("files", $files);
sm_assign("category", $category);
sm_display("project.artifacts.html");