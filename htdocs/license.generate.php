<?php

include_once("../config.php");
require_perms("admin");

include_once("../libs/license/LicenseHelper.php");
include_once("../classes/class.productmodule.inc.php");

$licenseHelper = new LicenseHelper();
$return = array();

$product_id = intval($_POST['product_id']);
$product_code = db_get_var("SELECT code FROM products WHERE id = '{$product_id}'");

$product_modules = ProductModule::GetModulesByProductId($product_id);
$post_module_ids = array_filter($_POST['modules'], 'intval');

$module_codes = array();
foreach($post_module_ids as $v) {
	$m = $product_modules[$v];
	if($m) {
		$module_codes[] = $m->getCode();
	}
}

try {
    $return['license'] = $licenseHelper->generateLicense($product_code, $_POST['license_end_date'], $module_codes);
    $return['modules'] = $module_codes;
    $return['status'] = true;
} catch(Exception $e) {
    $return['status'] = false;
    $return['error_message'] = $e->getMessage();
}

ob_clean();
print json_encode($return);
