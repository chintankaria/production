<?php
    include_once("../config.php");
    include_once("../classes/class.contact.inc.php");
    include_once("../classes/class.contactgroups.inc.php");
    include_once("../classes/class.communicationtype.inc.php");
    include_once("../classes/class.contacttype.inc.php");
    $contact = new Contact(intval($_GET["id"]));
    if($contact->getId() < 1) redirect("contact.list.php");
    
    
    $groups = ContactGroups::GetAll();
    $country = getCountryList();
    $comm_types = CommunicationType::GetAll();
    $contact_types = ContactType::GetAll();

    sm_assign("contact", $contact);
    sm_assign("comm_types", $comm_types);
    sm_assign("contact_types", $contact_types);
    sm_assign("groups", $groups);
    sm_assign("country", $country);
    if($_GET["type"] == "print") {
        sm_display("contact.view.print.html");
    }
    else {
        sm_display("contact.view.html");
    }
?>
