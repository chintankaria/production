<?php
    include_once("../config.php");
    include_once("../classes/class.emailbroadcast.inc.php");
    require_perms("admin");

    $from = array("admin.timesheet@globalss.com" => "GSS Administrator");
    if($_POST) {
        $subject = $_POST['subject'];
        $body = $_POST['msg_body'];
        sm_assign("subject", $subject);
        sm_assign("body", $body);
        foreach($_POST['users'] as $uid) {
            $oUser = new User($uid);
            $full_name = $oUser->getFullName();
            $email = $oUser->getEmail();
            $to = array($oUser->getEmail() => $oUser->getFullName());
            sm_assign("full_name", $full_name);
            _::Mail($from,
                array($oUser->getEmail() => $oUser->getFullName()),
                "$subject",
                sm_fetch("email.broadcast.letter.html")
            );
            $str = "Email Broadcast sent to Mr.{$oUser->getFullName()}({$oUser->getEmail()})";
            watchdog('SUCCESS','EMAIL', $str);        
            $unames[] = $full_name;
        }
        # Save Email Broadcast
        email_broadcast_save();
        
        s("Email Broadcast sent to the members: <b>".@implode(", ", $unames)."</b>");
        #sm_display("email.broadcast.letter.html");
    } 

    
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("email.broadcast.php");
    }

function email_broadcast_save() {
    if($_POST) {
        $subject = $_POST['subject'];
        $body = $_POST['msg_body'];
        $broadcast = new EmailBroadcast();

        $broadcast->setSubject($subject);
        $broadcast->setBody($body);
        
        if($broadcast->save()) {
            foreach($_POST['users'] as $uid) {
                $broadcast->setUserId($uid);
                $broadcast->saveBroadcastLogs();
            }
        }
    }
}
?>
