<?php
    include_once("../config.php");
    require_perms("hr_manager","admin");
    include_once("../classes/class.employees.inc.php");

    $member = u();

    $is_timesheet = isset($_GET['is_timesheet']) ? intval($_GET['is_timesheet']) : 0; // By default users

    $users = User::GetUsers(true,false,$is_timesheet);
    $blocked = (isset($_GET['blocked']) && $_GET['blocked'] > 0) ? 1 : 0;

    $user_details = $users;
    $users = array();
    $user_roles = array(
                        "business_manager" => "Business Manager",
                        "calendar_manager" => "Calendar Manager",
                        "report_manager" => "Report Manager",
                        "minutes_manager" => "Minutes Manager",
                        "smt_member" => "SMT Member",
                        "hr_manager" => "HR Manager"
                    );
    foreach($user_details as $k => $v) {
        $r = $v->getRoles();
        $rRoles=array();
        foreach($r as $r1) {
            if($user_roles[$r1])
            $rRoles[] = $user_roles[$r1];
        }
        if($rRoles)
            $v->user_role = @implode(" ", $rRoles);
        else 
            $v->user_role = "Member";
        if($blocked && $v->isBlocked()) {
            $users[$k] = $user_details[$k];
        }
        else {
            if(!$v->isBlocked()) 
                $users[$k] = $user_details[$k];
        }
    }

	#debug($users);

    $empByLocation = Employee::GetEmployeesByLocation();
    $empTotalByLocation = Employee::GetEmployeesTotalByLocation();
    $empTotalByDepartment = Employee::GetEmployeesTotalByDepartment();

    sm_assign("empByLocation", $empByLocation);
    sm_assign("empByTotalDepartment", $empTotalByDepartment);
    sm_assign("empByTotalLocation", $empTotalByLocation);
    sm_assign("blocked", $blocked);
    sm_assign("users", $users);
    sm_assign("member", $member);

	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	//$excel->setHTMLTable(sm_fetch("table.user.list.report.html"), "User List", null, array(7));
    	$excel->setHTMLTable(sm_fetch("table.user.list.report.html"), null, null, array(7));
    	$fileName = date("d-m-Y").'_user_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	else if($_GET["pdf"]){
        include_once("../html2pdf_report.php");
        // Load CSS
        $handle = fopen(BASEDIR . "/htdocs/css/style-pdf.css","r");
        $css = fread($handle, filesize(BASEDIR . "/htdocs/css/style-pdf.css"));
        fclose($handle);
        
        $html = '<html>
                    <head>
                    <style type="text/css">
                        '.$css.'
                        #tbl_tasks a:hover {
                            color: #0066CB;
                            background-color: #ffffff;
                        }
                        a:hover {
                            color: #0066CB;
                            background-color: #ffffff;
                        }
                    </style>
                    </head>
                    <body>
                        <div id="content" class="d-block-table">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td width="20%" class="pt3 pb5 pL10">
                                        <h1>Team Members</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="pR5">
                                        <div class="d-dashed"></div>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <table cellpadding="5" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td>
                                        <span style="background-color: #3BA365; border-radius: 3px 3px 3px 3px; color: #FFFFFF; font-size: 11px; margin: 3px 1px 1px; padding: 2px 5px; text-align: right; width: 55px;">
                                        <b>Total Active Members: '.$member->GetTotalActiveUsers().'</b>
                                        </span>
                                        &nbsp;&nbsp;
                                        <span style="background-color: #C15252; border-radius: 3px 3px 3px 3px; color: #FFFFFF; font-size: 11px; margin: 3px 1px 1px; padding: 2px 5px; text-align: right; width: 75px;">
                                        <b>Total Blocked Members: '.$member->GetTotalBlockedUsers().'</b>
                                        </span>
                                    </td>
                                </tr>
                            </table>
                 ';
        $html .= sm_fetch("table.user.list.report.html");

        $html .= "
                            </div>
                        </body>
                    </html>
                 ";
        

        $pdf_path = AppPath::$FILES ."/pdf";
        if(!is_dir($pdf_path))
            @mkdir($pdf_path, 0755, true);

    	$sFileName = date("d-m-Y").'_user_list.html';
    	$dFileName = date("d-m-Y").'_user_list.pdf';

        $source_file = "$pdf_path/$sFileName";
        $destination_file = "$pdf_path/$dFileName";

        $handle = fopen($source_file, "w");
        $contents = fwrite($handle, $html);
        fclose($handle);

        // PDF Parameters
        $logo_url = "http://dev.globalss.com/shekhar/globalss/influence-v2/htdocs/img/globalsoft_logo.jpg";
        $logo_url = "http://groovesw.com/timesheet-test/htdocs/img/globalsoft_logo.jpg";

        $header ='
                <table width="100%" align="center">
                    <tr>
                        <td align="left" width="410">
                            <div>
                                <a target="_blank" href="http://globalss.com/">
                                    <img width="215" height="50" border="0" src="'.$logo_url.'"/>
                                </a>
                            </div>
                            <div style="border-bottom: 3px solid #161311;margin-top:15px"></div>
                        </td>
                    </tr>
                </table>
                <div style="padding-bottom:25px;"></div>
            ';

        $pdf_media_margins = array(
                                'left'   => 10,
                                'right'  => 10,
                                'top'    => 20,
                                'bottom' => 10
                            );
        $pdf_config = array(
                        'pagewidth'          => 1024,
                        'margins'            => array(
                                                     'left'    => 10,
                                                     'right'   => 10,
                                                     'top'     => 20,
                                                     'bottom'  => 12,
                                                ),
                        'media'             => 'Letter',
                        'method'            => 'fpdf',
                        'mode'              => 'html',
                        'cssmedia'          => 'screen',
                        'pdfversion'        => '1.6',
                        'pslevel'           => 3,
                        'scalepoints'       => '1',
                        'renderimages'      => true,
                        'renderlinks'       => true,
                        'renderfields'      => true,
                        'renderforms'       => false,
                        'html2xhtml'        => true, // new param
                        'smartpagebreak'    => true,
                        'encoding'          => false,// ''  
                   );
        $param['config'] = $pdf_config;
        $param['media_margins'] = $pdf_media_margins;
        $param['header'] = $header;

        // Generate PDF 
        generate_pdf_report($source_file, $destination_file, $param);

        download_file($destination_file);

        @unlink($source_file);
        @unlink($destination_file);
        exit;

    }
    sm_display("user.list.report.html");
