<?php
    include_once("../config.php");
    if($_POST["answer"] != "yes") redirect("task.list.php");

    include_once("../classes/class.task.inc.php");

    $task = new Task($_POST["id"]);
    if($task && $task->getId() > 0) $task->delete();

    redirect("task.list.php");
