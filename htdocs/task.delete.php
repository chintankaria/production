<?php
    include_once("../config.php");
    include_once("../classes/class.task.inc.php");

    $task = new Task($_GET["id"]);
    if($task->getId() < 1) redirect("task.list.php");

    sm_assign("task", $task);
    sm_display("task.delete.html");
