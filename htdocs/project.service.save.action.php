<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.project.servicetype.inc.php");
    $project_service_type = new ProjectServiceTypes($_POST["id"]);
    $project_service_type->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($project_service_type->delete()) {
            $str = "Project Service Type <i><b>{$_POST['name']}</b></i> deleted.";
            s("Service Type  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete project Service Type.";
            e("Failed to delete project Service Type.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$project_service_type->checkName()) {
            $project_service_type->setIsActive(1);
            if($project_service_type->save()) {
                $str = "Project Service Type  <i><b>{$project_service_type->getName()}</b></i> saved.";
                s("Project Service Type  <i><b>{$project_service_type->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save project Service Type {$_POST["name"]}";
                e("Failed to save project Service Type {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Project Service Type <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("project.service.list.php");
    }
