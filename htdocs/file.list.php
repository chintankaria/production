<?php

include_once("../config.php");
//require_perms("admin");

include_once("../classes/class.user.inc.php");
include_once("../classes/class.client.inc.php");
include_once("../classes/class.project.inc.php");
include_once("../classes/class.timesheetfiles.inc.php");
include_once("../classes/class.category.inc.php");

$show_file_version = isset($_GET["show_file_version"]) ? $_GET["show_file_version"] : 0;
$show_general_doc = isset($_GET["show_general_doc"]) ? $_GET["show_general_doc"] : 0;
$show_project_doc = isset($_GET["show_project_doc"]) ? $_GET["show_project_doc"] : 0;
$client_id = pintval($_GET["client_id"]);
$project_id = 0;
$subproject_id = 0;

$clients = Client::GetClients();
$projects = array();
$subprojects = array();

$prj_category = array();
$gen_category = array();
$prj_category = Category::GetAll('PROJECT_DOCUMENT');
$gen_category = Category::GetAll('GENERAL_DOCUMENT');

if($clients[$client_id]){
    $projects = Project::GetProjectsByClient($client_id, true);
    $project_id = pintval($_GET["project_id"]);
    
    if($projects[$project_id]){
        $subprojects = Project::GetSubprojectsByProject($project_id);
        $subproject_id = pintval($_GET["subproject_id"]);
        
        if(!$subprojects[$subproject_id]) $subproject_id = 0;
    }
    else{
    	$project_id = 0;
    }
}
else{
	$client_id = 0;
}

$client_arr = array();
foreach($clients as $c_id => $client) {
    $client_arr[$c_id]['name'] = $client->getName();
    $client_arr[$c_id]['id'] = $client->getId();
    $client_arr[$c_id]['id']['projects'] = array();
    $prj = Project::GetProjectsByClient($c_id, true);
    if($prj) {
        foreach($prj as $p_id => $p) {
            $client_arr[$c_id]['projects'][$p_id]['name'] = $p->getName();
            $client_arr[$c_id]['projects'][$p_id]['id'] = $p->getId();
            $subprj =  Project::GetSubprojectsByProject($p_id);
            if($subprj) {
                foreach($subprj as $sp_id => $sp) {
                    $client_arr[$c_id]['projects'][$p_id]['subprojects'][$sp->getId()]['name'] = $sp->getName();
                    $client_arr[$c_id]['projects'][$p_id]['subprojects'][$sp->getId()]['id'] = $sp->getId();
                }
            }
        }
    }
}

sm_assign("client_arr", json_encode($client_arr));

sm_assign("prj_category", $prj_category);
sm_assign("gen_category", $gen_category);

sm_assign("show_file_version", $show_file_version);

sm_assign("clients", $clients);
sm_assign("client_id", $client_id);

sm_assign("projects", $projects);
sm_assign("project_id", $project_id);

sm_assign("subprojects", $subprojects);
sm_assign("subproject_id", $subproject_id);

sm_assign("show_general_doc", $show_general_doc);
sm_assign("show_project_doc", $show_project_doc);

if($show_general_doc) {
    $files = TimesheetFiles::GetAllByDocumentType('GENERAL_DOCUMENT',$show_file_version,true);
}
else if($show_project_doc) {
    $files = TimesheetFiles::GetAllByDocumentType('PROJECT_DOCUMENT',$show_file_version,true);
}
else {
    if($subproject_id){
        $files = TimesheetFiles::GetByProject($subproject_id,false,$show_file_version);
    }
    elseif($project_id){
        $files = TimesheetFiles::GetByProject($project_id,true,$show_file_version);
    }
    elseif($client_id){
        $files = TimesheetFiles::GetByClient($client_id, true,$show_file_version);
    }
    else{
        $files = TimesheetFiles::GetAll($show_file_version);
    }
}
//if($files) uasort($files, "sort_files");
if($files) {
    foreach($files as $k => $v) {
        $v->userFullName = User::getFullNameByEmailId($v->getCreatedBy());
        $v->userId = User::getUserIdByEmailId($v->getCreatedBy());
        $v->createdDate = date("m/d/Y h:i a",$v->getUploadDate());
        if($v->userId == u()->getId()) {
            $v->isOwner=true;
        }
        else {
            $v->isOwner=false;
        }
    }
}
sm_assign("files", $files);
sm_display("file.list.html");

function sort_files($a, $b){
	$k1 = "{$a->getClientName()}.{$a->getParentProjectName()}.{$a->getProjectName()}";
	$k2 = "{$b->getClientName()}.{$b->getParentProjectName()}.{$b->getProjectName()}";
	
	return strnatcasecmp($k1, $k2);
}

