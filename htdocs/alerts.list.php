<?php
    include_once("../config.php");
    require_perms("admin");
    $days = array("Mon" => "Monday","Tue" =>"Tuesday","Wed" => "Wednesday","Thu" => "Thursday", "Fri" => "Friday", "Sat" => "Saturday", "Sun" =>"Sunday");
    $d = date("d");
    $m = date("m");
    $y = date("Y");
    for($h=0; $h < 24; $h++) {
        $min = 0;
        for($i=0; $i < 60; $i++) {
            $ts = mktime($h, $min, 0, $m, $d, $y);
            $tm[date("H:i", $ts)] = date("h:i A", $ts);
            $min += 10;
        }
    }
    $server_current_time = date("l, jS F Y h:i A T");
    sm_assign("server_current_time", $server_current_time);
    sm_assign("server_timezone", date("T"));
    // get the timesheet settings
    $s = $timesheet_settings;
    $alertCount = count($s['reminder_day']);
    sm_assign("alertCount", $alertCount);
    sm_assign("settings", $s);
    sm_assign("tm", $tm);
    sm_assign("days", $days);
    
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("alerts.list.html");
        exit;
    }
    else {
        sm_display("alerts.list.html");
        sm_assign("fetch", 0);
    }
?>
