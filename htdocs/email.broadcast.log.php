<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.pagination.inc.php");
    include_once("../classes/class.emailbroadcast.inc.php");

    $from = isset($_GET['from_date']) ? $_GET['from_date'] : date("m/01/Y");
    $to = isset($_GET['to_date']) ? $_GET['to_date'] : date("m/d/Y");
    $p = (isset($_GET["p"]) && intval($_GET['p']) > 0) ? intval($_GET["p"]) : 1;
    $total_pages = 0;
    $total_results = 0;
    $page_limit = 2;
    $from = ymd(is_date($from));
    $to = ymd(is_date($to));

    if($_GET["excel"]) $p = 0;


    $broadcastlogs = EmailBroadcast::GetAll($from, $to, $p, $page_limit);
    if($broadcastlogs) {
        foreach($broadcastlogs as $v) {
            if($_GET["excel"]){
                $v->bodyText = $v->getBody();
            }
            else {
                $v->bodyText = short_string(strip_tags($v->getBody()),200)." ...";
            }
        }
    }
    if($p > 0) {  
        $total_pages = EmailBroadcast::getTotalPages();
        $total_results = EmailBroadcast::getTotalResults();
        $Paginate = new Pagination();
        $Paginate->setCurrentPage($p);
        $Paginate->setPageLimit($page_limit);
        $Paginate->setTotalPages($total_pages);
        $Paginate->setTotalResult($total_results);
        $Paginate->setPageNumberLimit(5);
        $Paginate->Paginate();
        $pages = $Paginate->getNumberStyleArray();
        $previous_page =$Paginate->getPreviousPage();
        $next_page = $Paginate->getNextPage();
        $current_page = $Paginate->getCurrentPage();

        sm_assign("total_results", $total_results);
        sm_assign("total_pages", $total_pages);
        sm_assign("pages", $pages);
        sm_assign("previous_page", $previous_page);
        sm_assign("next_page", $next_page);
        sm_assign("current_page", $current_page);
        sm_assign("page_end", @end($pages));
    }
    sm_assign("broadcastlogs", $broadcastlogs);
    sm_assign("from_date", $from);
    sm_assign("to_date", $to);
    sm_assign("p", $p);

    if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.email.broadcast.log.html"), "Email Broadcast Logs",null,array());
    	$fileName = date("d-m-Y").'_emailbroadcast_logs.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }

    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("email.broadcast.log.html");
        exit;
    }
    else {
        sm_display("email.broadcast.log.html");
        sm_assign("fetch", 0);
    }
?>
