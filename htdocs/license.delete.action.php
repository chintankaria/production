<?php

include_once("../config.php");
require_perms("admin");
include_once("../classes/class.license.inc.php");

$license = new License($_POST["id"]);
if ($license->getId() > 0) {
	$key = $license->getLicenseKey();
	if($license->delete()) {
		s("License deleted successfully - {$key}");
	} else {
		e("Failed to delete license - {$key}");
	}
}

redirect('license.list.php');
