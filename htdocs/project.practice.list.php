<?php
include_once("../config.php");
require_perms("admin");

include_once("../classes/class.project.practicearea.inc.php");
$project_practice_area = ProjectPracticeArea::GetPracticeArea();

sm_assign("project_practice_area", $project_practice_area);

if ($_GET["excel"]) {
    include_once("../classes/class.excelwriter.inc.php");
    $excel = new ExcelWriter();
    $excel->setHTMLTable(sm_fetch("table.project.practice.list.html"), "Project Practice Area", null, array(3));
    $fileName = date("d-m-Y") . '_practice_area.xls';
    ob_clean();
    $excel->send($fileName);
    $excel->close();
    exit;
}
if ($_REQUEST['fetch'] == 1) {
    sm_assign("fetch", 1);
    print sm_fetch("project.practice.list.html");
    exit;
} else {
    sm_display("project.practice.list.html");
}
