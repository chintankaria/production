<?php
include_once("../config.php");
require_perms("admin");

include_once("../classes/class.pto.details.inc.php");




$pto_detail = PtoPolicy::GetPolicies(null,$_GET['loc_id']);

$loc_name = PtoPolicy::GetLocationByName($_GET['loc_id']);

$loc_name =  $loc_name[$_GET['loc_id']]['name'];


//echo "<pre>";
//print_R($watchdog);

foreach ($pto_detail as $key => $value) {
    $tier_detail[$key] = PtoPolicy::checkTiers($key);
    $i += count( $tier_detail[$key]);
}


//echo "<pre>";
//print_R($tier_detail);


sm_assign("tier_detail", $tier_detail);
sm_assign("dtierCnt",$i);
$tierCnt =  count($pto_detail);

sm_assign("tierCnt", $tierCnt);
sm_assign("polCnt", $tierCnt);
sm_assign("loc_id", $_GET['loc_id']);
sm_assign("loc_name", $loc_name);
sm_assign("pto_detail", $pto_detail);

if ($_REQUEST['fetch'] == 1) {
    sm_assign("fetch", 1);
    print sm_fetch("pto_policy_list.html");
    exit;
} else {
    sm_display("pto_policy_list.html");
}
