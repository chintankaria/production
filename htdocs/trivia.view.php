<?php
    include_once("../config.php");
    include_once("../classes/class.trivia.inc.php");
    include_once("../classes/class.trivia.responses.inc.php");
    include_once("../classes/class.trivia.points.inc.php");

    $trivia = new Trivia($_GET["id"]);
    if($trivia->getId() < 1) redirect("trivia.list.php");
    
    $trivia->create_date = $trivia->getCreateDate('D M j Y h:i A T');
    $trivia->update_date = $trivia->getUpdateDate('D M j Y h:i A T');
    $trivia->close_date_time = $trivia->getCloseDateTime("d M y h:i a");

    $trivi_responses = new TriviaResponses();
    $responses = $trivi_responses->GetResponseList($_GET["id"]);
    if($responses) {
        foreach($responses as $key => $val) {
            $pClass = ($pClass == "comments-even") ? "comments-odd" : "comments-even";

            $val->date_time = $val->getDateTime("d M y h:i a");
            $val->class = $pClass;
            $val->correctAns = false;
            $val->response_pint_id = 0;

            $uPoints = $val->GetResponsePoints();
            $points = $uPoints[$val->getUserId()]; 
            if(is_object($points) && $points->getPoints() > 0) {
                // User Response is correct
                $val->class = "comments-correct";
                $val->correctAns = true;
                $val->response_pint_id = $points->getId();
            }
            else if(is_object($points) && $points->getPoints() < 1) {
                $val->class = "comments-incorrect";
                $val->correctAns = false;
                $val->response_pint_id = $points->getId();
            }
            /*
            if($val->getChildrens()) {
                foreach($val->getChildrens() as $k => $v) {
                    $cClass = ($cClass == "comment-reply-odd") ? "comment-reply-even" : "comment-reply-odd";
                    $v->date_time = date("d M y h:i a",strtotime($v->getDateTime()));
                    $v->class = $cClass;
                    if($v->getReplies()) {
                        foreach($v->getReplies() as $k1 => $v1) {
                            $rClass = ($rClass == "comment-reply-even") ? "comment-reply-odd" : "comment-reply-even";
                            $v1->date_time = date("d M y h:i a",strtotime($v1->getDateTime()));
                            $v1->class = "$rClass comment-children-child";
                        }
                    }
                }
            }
            */
        }
    }
    sm_assign("trivia", $trivia);
    sm_assign("responses", $responses);
    sm_display("trivia.view.html");
?>
