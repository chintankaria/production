<?php
include_once("../config.php");
require_perms("admin", "manager");
$pid = $_GET["id"];
$subProjects = Project::GetSubprojectsByProject($pid); 

$mainProject = new Project($_GET["id"]);

sm_assign("pid", $pid);
sm_assign("subProjects", $subProjects);
sm_assign("mainProject", $mainProject);
sm_display("project.subproject.html");