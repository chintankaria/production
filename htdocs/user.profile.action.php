<?php
    include_once("../config.php");
    include_once("../classes/class.employees.inc.php");

#debug($_POST);
#exit;
    $user = new User($_POST["id"]);
    if($user->getId() > 0){
        
        if(trim($_POST['txtUserName']) != $user->getEmail()) {
            if($user->getUserIdByEmailId(trim($_POST['txtUserName']))) {
                // User Name already exist
                e("<b>{$_POST['txtUserName']}</b> member already exist.");
                redirect("user.profile.php?id={$_POST["id"]}&tab={$_POST['tab']}");
            }
            $user->setEmail(trim($_POST["txtUserName"]));
        }
        
        if($_POST['action'] == 'PWD') {
            // Password Changes
            $user = u();
            if(
                $user->checkPassword($_POST["old_password"])
                && $_POST["new_password_1"]
                && $_POST["new_password_1"] == $_POST["new_password_2"]
                && $user->changePassword($_POST["new_password_1"])
            ){
                $str = "Member password changed";
                watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
                session_destroy();
                redirect("user.login.php");
            }
            else{
                e("Failed to change password");
                $str = "Failed to change password";
                watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
                redirect("user.profile.php?tab={$_POST['tab']}");
            }
        }
        else if($_POST['action'] == 'PROFILE') {
            // Profile Update
            
            $is_timesheet = isset($_POST['is_timesheet']) ? intval($_POST['is_timesheet']) : 1;  // Default is required
            $is_trainer = isset($_POST['is_trainer']) ? intval($_POST['is_trainer']) : 0;  // Default is NO

            $user->setFirstName(trim($_POST["txtFirstName"]));
            $user->setMiddleName(trim($_POST["txtMiddleName"]));
            $user->setLastName(trim($_POST["txtLastName"]));
            #$user->setGender(trim($_POST["rGender"]));
            $user->setDateOfBirth(trim($_POST["txtDOB"]));
            #$user->setRoles(implode(",",$_POST['roles']));
            $status = $user->save();
            if($status) {
                if($_FILES) {
                    // User Pic
                    $file = array();
                    foreach($_FILES['file']['error'] as $k => $v){
                        if(!$v){
                            $ext = getExtension(stripslashes($_FILES['file']['name'][$k]));
                            $imginfo_array = getimagesize($_FILES['file']['tmp_name'][$k]);
                            if ($imginfo_array !== false) {
                                if(intval($_FILES['file']['size'][$k]) > 5120) {
                                    $file = array(
                                        'name' => addslashes($_FILES['file']['name'][$k]),
                                        'type' => $_FILES['file']['type'][$k],
                                        'tmp_name' => $_FILES['file']['tmp_name'][$k],
                                        'size' => $_FILES['file']['size'][$k],
                                    );
                                }
                                else {
                                    e("Image size limit exceeded.");
                                }
                            }
                            else {
                                e("Invalid image file uploaded.");
                            }
                        }
                    }
                    if(is_array($file) && count($file) > 0) {
                        if($user->SaveUserFiles($file, 'PROFILE_PIC')) {
                            $str = "Profile & Picture saved successfully";
                            s("Profile & Picture saved successfully.");
                            watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                        }
                        else {
                            $str = "Failed to change picture";
                            e("Failed to change picture.");
                            watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                        }
                    }

                    // User CV
                    $file = array();
                    foreach($_FILES['file_cv']['error'] as $k => $v){
                        if(!$v){
                            $file = array(
                                'name' => addslashes($_FILES['file_cv']['name'][$k]),
                                'type' => $_FILES['file_cv']['type'][$k],
                                'tmp_name' => $_FILES['file_cv']['tmp_name'][$k],
                                'size' => $_FILES['file_cv']['size'][$k],
                            );
                        }
                    }
                    if(is_array($file) && count($file) > 0) {
                        if($user->SaveUserFiles($file, 'USER_CV')) {
                            $str = "CV updated successfully";
                            s("CV updated  successfully.");
                            watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                        }
                        else {
                            $str = "Failed to update CV";
                            e("Failed to update CV.");
                            watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                        }
                    }
                }

                $employee = new Employee(0, $user->getId());
                $employee->setUserId($user->getId());
                $employee->setNationality(trim($_POST["txtNationality"]));

                if($_POST['txtPersonalEmail'] == '')
                    $_POST['txtPersonalEmail'] = $_POST['txtUserName'];

                $employee->setPersonalEmail($_POST["txtPersonalEmail"]);
                $employee->setMobile1($_POST["txtMobile1"]);
                $employee->setMobile2($_POST["txtMobile2"]);
                $employee->setPhone1($_POST["txtPhone1"]);
                $employee->setPhone2($_POST["txtPhone2"]);
                $employee->setAddress($_POST["txtAddress"]);
                $employee->setCity($_POST["txtCity"]);
                $employee->setZip($_POST["txtZip"]);
                $employee->setState($_POST["txtState"]);
                $employee->setCountryId($_POST["country_id"]);
                $employee->setSkypeId($_POST["txtSkypeId"]);
                $employee->setYahooId($_POST["txtYahooId"]);
                $employee->setMsnId($_POST["txtMsnId"]);
                $employee->setGtalkId($_POST["txtGtalkId"]);

                // Save Employee Details
                $employee->save();

            }
        }
    }
    else $status = false;

    if($status) {
        $str = "Member information updated ";
        s("Member information updated.");
        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
    }
    else {
        $str = "Failed to update member information";
        e("Failed to update member information.");
        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
    }

    redirect("user.profile.php?id={$_POST["id"]}&tab={$_POST['tab']}");
