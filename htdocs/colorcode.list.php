<?php
    include_once("../config.php");
    require_perms("admin");

    if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.category.list.html"), "Color code list",null, array(2));
    	$fileName = date("d-m-Y").'_colorcode_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    $sql = "SELECT * FROM calendar_colorcode ORDER BY name";
    $colorcode = db_get_all($sql);

    sm_assign("colorcode", $colorcode);
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("colorcode.list.html");
        exit;
    }
    else {
        sm_display("colorcode.list.html");
    }
?>
