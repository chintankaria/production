<?php
    include_once("../config.php");
    include_once("../classes/class.user.inc.php");

    $user = u();
    if($user->isGuest()) redirect("user.login.php");
    elseif($user->isAdmin()) redirect("user.list.php");
    else redirect("project.list.php");
