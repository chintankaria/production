<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.technology.skills.inc.php");
    $technology_skills = new TechnologySkills($_POST["id"]);
    $technology_skills->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($technology_skills->delete()) {
            $str = "Technology Skills <i><b>{$_POST['name']}</b></i> deleted.";
            s("Department  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete technology skills.";
            e("Failed to delete technology_skills.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$technology_skills->checkName()) {
            if($technology_skills->save()) {
                $str = "Technology Skills  <i><b>{$technology_skills->getName()}</b></i> saved.";
                s("Technology Skills  <i><b>{$technology_skills->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save technology skills {$_POST["name"]}";
                e("Failed to save technology skills {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Technology Skills <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("technology.skills.list.php");
    }
