<?php

include_once("../config.php");
include_once("../classes/class.calendarevents.inc.php");

$status = false;
$event_data = array();

    if(isset($_POST['event_action']) && $_POST['event_action'] != ''){
        $event = new CalendarEvent($_POST["id"]);
        $event->setTitle($_POST["title"]);
        $event->setDescription($_POST["description"]);
        $event->setStatus($_POST["status"]);
        $event->setClientId($_POST["client_id"]);
        $event->setProjectId($_POST["project_id"]);
        $event->setSubprojectId($_POST["subproject_id"]);
        $event->setUserId($_POST["user_id"]);
        $event->setStartDate($_POST["start_date"]);
        $event->setEndDate($_POST["end_date"]);
        $event->setCreatedBy(u()->getEmail());
        $event->setUpdatedBy(u()->getEmail());

        if(!$event->checkEvent()) {
            if($event->save()){
                $result = "Event saved successfully.";
                $status = true;
                $event_data = CalendarEvents::GetJavascriptEvent($event->getId());
            }
        }
        else {
            $result = "Event already exist!";
        }
    }

    if(u()->isAdmin()){
        if(isset($_POST['holiday_action']) && $_POST['holiday_action'] != ''){
            $blnHoliday = false;
            if($_POST["pto_holiday"] == "HOLIDAY") {
                $blnHoliday = true;
                $_POST["holiday_user_id"] = 0;
                $_POST["selHolidayStatus"] = "CONFIRMED";
            } else if($_POST["pto_holiday"] == "PTO") {
                $blnHoliday = false;
                $_POST['holiday_in'] = '';
            }
            $event = new CalendarEvent($_POST["id"]);
            $event->setTitle($_POST["txtHolidayTitle"]);
            $event->setDescription($_POST["txtHolidayDescription"]);
            $event->setIsHoliday($blnHoliday);
            $event->setStatus($_POST["selHolidayStatus"]);
            $event->setHolidayIn($_POST['holiday_in']);
            $event->setUserId($_POST["holiday_user_id"]);
            $event->setStartDate($_POST["Hstart_date"]);
            $event->setEndDate($_POST["Hend_date"]);
            $event->setCreatedBy(u()->getEmail());
            $event->setUpdatedBy(u()->getEmail());

            if(!$event->checkEvent()) {
                if($event->save()){
                    $status = true;
                    $result = "PTO/Holiday saved successfully.";
                    $event_data = CalendarEvents::GetJavascriptEvent($event->getId());
                }
            }
            else {
                $result = "PTO/Holiday already exist!";
            }
        }
    }

ob_clean();
print json_encode(array('status' => $status, 'result' => $result, 'event_data' => array_shift($event_data)));

