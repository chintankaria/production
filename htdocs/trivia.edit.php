<?php
    include_once("../config.php");
    include_once("../classes/class.trivia.inc.php");
    $user_id = (!u()->isAdmin()) ? u()->getId() : 0; 
    $user_id = 0;
    $trivia = new Trivia($_GET["id"]);
    if($trivia->getId() < 1) redirect("trivia.list.php");
    sm_assign("trivia", $trivia);
    sm_display("trivia.edit.html");
?>
