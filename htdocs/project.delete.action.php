<?php
    include_once("../config.php");
	
	if(!$_GET['close'])
    if($_POST["answer"] != "yes") redirect("project.list.php");

    include_once("../classes/class.project.inc.php");
	
	if($_POST["id"])
	$project_id = $_POST["id"];
	else $project_id = $_GET["id"];
	
    $project = new Project($project_id);
	
	
	if($_GET['close']){
		
		if(!$_GET['op'] && $_POST["end_date"]){
			if($project->getStartdate() > strtotime($_POST["end_date"])+1){
				e("Invalid project start date and end date.");
				redirect("project.open.php?id={$project_id}");
			}
		}
		 
		$end_date = $_POST["end_date"];
		if($project->getId() > 0) $project->close($_GET['op'],$project->getParentId(), $end_date);
		 redirect("project.view.php?id={$project_id}");
	}else{
	    if($project->getId() > 0) $project->delete();
	}
    redirect("project.list.php");
