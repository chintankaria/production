<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.technology.practice.inc.php");
    $technology_practice = new TechnologyPractice($_POST["id"]);
    $technology_practice->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($technology_practice->delete()) {
            $str = "Technology Practice <i><b>{$_POST['name']}</b></i> deleted.";
            s("Department  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete technology practice.";
            e("Failed to delete technology_practice.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$technology_practice->checkName()) {
            if($technology_practice->save()) {
                $str = "Technology Practice  <i><b>{$technology_practice->getName()}</b></i> saved.";
                s("Technology Practice  <i><b>{$technology_practice->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save technology practice {$_POST["name"]}";
                e("Failed to save technology practice {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Technology Practice <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("technology.practice.list.php");
    }
