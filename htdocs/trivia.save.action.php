<?php
include_once("../config.php");
include_once("../classes/class.trivia.inc.php");
include_once("../classes/class.trivia.points.inc.php");

$app_url = "http://influence.globalss.com/htdocs";

    if(isset($_POST["action"]) && $_POST["action"] == "DELETE") {
        $trivia = new Trivia($_POST["id"]);
        if($trivia->getId() < 1) redirect("trivia.list.php");

        if($trivia->Delete()) {
            s("Trivia deleted successlfully.");
            $str = "<i><b>{$trivia->getTitle()}({$trivia->getId()})</b></i> trivia question deleted.";
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            e("Failed to delete trivia");
            $str = "Failed to deleted trivia: <i><b>{$trivia->getTitle()}({$trivia->getId()})</b></i>";
            watchdog('FAILED','DELETE', "$str");
        }
        redirect("trivia.list.php");
    }
    else if(isset($_POST["action"]) && $_POST["action"] == "TRIVIA_POINTS") {
        if($_POST['rating'] > 0) {
            $trivia = new Trivia($_POST["id"]);
            $trivia->setRating($_POST['rating']);
            $trivia->RateTrivia();
            $str = "<i><b>{$trivia->getTitle()}</b></i> trivia question rated with <b>{$_POST['rating']}</b> points.";
            watchdog('SUCCESS','POINTS', "$str");
            //print 'Done!';
        } 
        exit;
    }
    else if(isset($_POST["action"]) && $_POST["action"] == "APPROVE") {
        $trivia = new Trivia($_POST["id"]);
        if($trivia->Approve()) {
            $tPoint = new TriviaPoints();
            $tPoint->setUserId($trivia->getUserId());
            $tPoint->setTriviaId($_POST['id']);
            $tPoint->setPoints(10);
            $tPoint->setDateTime(time());
            $tPoint->save();

            // Set the closing date for the Approved Trivia
            $trivia->setCloseDateTime(strtotime("+30 days",time()));
            $trivia->save();
 
            // Reload the Triva object
            $trivia = new Trivia($_POST["id"]);

            // Send notification to the Owner
            $subject = "Trivia post approved - {$trivia->getTitle()}";
            $trivia_link = "$app_url/trivia.view.php?id={$trivia->getId()}";
            $isOwner = true;
            $u = new User($trivia->getUserId());
            $name = $u->getFullName();
            $full_owner_name = $u->getFullName();
            $email = $u->getEmail();
            //$email = "shekhar@globalss.com"; // Testing Email ID
            
            sm_assign("isOwner", $isOwner);
            sm_assign("trivia", $trivia);
            sm_assign("name", $name);
            sm_assign("trivia_link", $trivia_link);
            _::Mail($from,
                array($email => $name),
                $subject,
                sm_fetch("trivia.post.letter.html")
            );
            $str = "<b>$subject</b> :: notification to Triva Owner($name -> $email) sent....";
            watchdog('SUCCESS','EMAIL', "$str");

            // Send notification to all Registred Members
            $subject = "New Trivia posted - {$trivia->getTitle()}";
            $members = User::GetActiveUsers();
            foreach($members as $k => $v){
                if($v->getId() != $trivia->getUserId()){
                    // if the user is not the owner of trivia
                    $isOwner = false;
                    $name = $v->getFullName();
                    $email = $v->getEmail();
                    //$email = "shekhar@globalss.com"; // Testing Email ID
                    sm_assign("trivia", $trivia);
                    sm_assign("full_owner_name", $full_owner_name);
                    sm_assign("name", $name);
                    sm_assign("isOwner", $isOwner);
                    sm_assign("trivia_link", $trivia_link);

                    _::Mail($from,
                        array($email => $name),
                        $subject,
                        sm_fetch("trivia.post.letter.html")
                    );
                    $str = "<b>$subject</b> :: notification of new Triva post to registred member ($name -> $email) sent....";
                    watchdog('SUCCESS','EMAIL', "$str");
                }
            }

            $str = "<i><b>{$trivia->getTitle()}</b></i> trivia question approved.";
            watchdog('SUCCESS','APPROVED', "$str");
            s("<i><b>{$trivia->getTitle()}</b></i> trivia question approved.");
        } 
        else {
            $str = "Failed to approve trivia question: <i><b>{$trivia->getTitle()}</b></i>";
            watchdog('FAILED','APPROVED', "$str");
            e("Failed to approve trivia question.");
        }
        #sm_display("trivia.post.letter.html");
        redirect("trivia.view.php?id={$_POST['id']}");
    }
    else if(isset($_POST["action"]) && $_POST["action"] == "REJECT") {
        $trivia = new Trivia($_POST["id"]);
        $trivia->setRejectedReason($_POST["reason"]);
        if($trivia->Reject()) {

            // Send notification to the Owner
            $subject = "Trivia post rejected - {$trivia->getTitle()}";
            $trivia_link = "$app_url/trivia.view.php?id={$trivia->getId()}";
            $u = new User($trivia->getUserId());
            $name = $u->getFullName();
            $email = $u->getEmail();
            //$email = "shekhar@globalss.com"; // Testing Email ID
            
            sm_assign("trivia", $trivia);
            sm_assign("name", $name);
            sm_assign("trivia_link", $trivia_link);
            _::Mail($from,
                array($email => $name),
                $subject,
                sm_fetch("trivia.post.letter.html")
            );
            $str = "<b>$subject</b> :: notification to Triva Owner($name -> $email) sent....";
            watchdog('SUCCESS','EMAIL', "$str");

            $str = "<i><b>{$trivia->getTitle()}</b></i> trivia question rejected.";
            watchdog('SUCCESS','REJECTED', "$str");
            s("<i><b>{$trivia->getTitle()}</b></i> trivia question rejected.");
        } 
        else {
            $str = "Failed to reject trivia question: <i><b>{$trivia->getTitle()}</b></i>";
            watchdog('FAILED','REJECTED', "$str");
            e("Failed to reject trivia question.");
        }
        redirect("trivia.view.php?id={$_POST['id']}");
    }
    else {
        $trivia = new Trivia($_POST["id"]);
        $trivia->setTitle($_POST["txtTitle"]);
        $trivia->setDescription($_POST["txtDescription"]);
        $trivia->setStatus($_POST["txtStatus"]);
        if(intval($_POST["id"]) < 1) {
            $trivia->setUserId(u()->getId());
            $trivia->setDateTime(date("Y-m-d H:i:s", time()));
            $trivia->setCloseDateTime(strtotime("+30 days",time()));
        }
        if(!$trivia->checkTitle()) {
            if($trivia->save()) {
                $str = "<i><b>".$_POST['txtTitle']."</b></i> saved successlfully.";
                watchdog('SUCCESS','ADD', "$str");
                s("<i><b>".$_POST['txtTitle']."</b></i> saved successlfully.");
                if(intval($_POST['id']) < 1) {
                    // Sent notification to Business Manager's
                    $nTrivia = new Trivia($trivia->getId());
                    sm_assign("trivia", $nTrivia);
                    $full_name = u()->getFullName();
                    $subject = "New Trivia post - {$_POST['txtTitle']}";
                    $trivia_link = "$app_url/trivia.view.php?id={$trivia->getId()}";
                    $bManager = User::GetBusinessManagers();
                    if($bManager) {
                        foreach($bManager as $u_id => $o_u) {
                            if(in_array("business_manager", $o_u->getRoles())) {
                                $name = $o_u->getFullName();
                                sm_assign("name", $name);
                                sm_assign("trivia_link", $trivia_link);
                                sm_assign("full_name", $full_name);
                                $email = $o_u->getEmail();
                                //$email = "shekhar@globalss.com"; // Testing Email ID
                                _::Mail($from,
                                    array($email => $name),
                                    $subject,
                                    sm_fetch("trivia.post.letter.html")
                                );
                                $str = "<b>$subject</b> :: notification to Business Manager($name -> $email) sent....";
                                watchdog('SUCCESS','EMAIL', "$str");
                            }
                        }
                    }

                }
                redirect("trivia.list.php");
            } 
            else {
                $str = "Failed to add a trivia.";
                watchdog('FAILED','ADD', "$str");
                e("Failed to add a trivia.");
                p($_POST);
                redirect("trivia.list.php");
            }
        } 
        else {
            if($_POST["id"] > 0) {
                $str = "<i><b>{$_POST["txtTitle"]}</b></i> already exist!";
                watchdog('SUCCESS','EDIT', "$str");
                e("<i><b>{$_POST["txtTitle"]}</b></i> already exist!");
                redirect("trivia.edit.php?id={$_POST["id"]}");
            }
            else {
                $str = "<i><b>{$_POST["txtTitle"]}</b></i> already exist!";
                watchdog('FAILED','EDIT', "$str");
                e("<i><b>{$_POST["txtTitle"]}</b></i> already exist!");
                p($_POST);
                redirect("trivia.add.php");
            }
        }
    }
?>
