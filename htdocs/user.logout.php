<?php
    include_once("../config.nologin.php");
    $user = u();
    # Wiki Logout
    require_once('../MySQLActiveUser.php');
    global $MySQLActiveUserData ;
 
    // Add this line AFTER all other cookies
    // have been cleared for your web page
    $MySQLActiveUserData->clear_cookie() ;

    watchdog('SUCCESS','LOGOUT', "{$user->getFullName()}({$user->getEmail()}) logged out successfully.");
    session_destroy();
    redirect("user.login.php");
?>
