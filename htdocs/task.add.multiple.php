<?php
    define("COUNT_TASK_EDIT_ROWS", 8);

    include_once("../config.php");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.timecard.inc.php");
    include_once("../classes/class.project.users.php");
    include_once("../classes/class.employee.project.role.php");    
	
    if($_GET['delete1']){
	  include_once("../classes/class.task.inc.php");
	  $task = new Task($_GET['task_id']); 
	  echo (bool)$task->delete();
	  exit;	
	}
    /*if(!is_date($_GET['date'])) {
        //  Invalid date
        e("Invalid date provided for task view.<br>Please click week number to go to task view.");
        redirect("task.timecard.php"); 
    }
    */
    $days = get_dates_of_week($_GET["date"]);
    $fday = strtotime(date('Y-m-d',$days[0]));
    
    sm_assign("days", $days);
    sm_assign("start_date_ts", $start_date_ts);
    sm_assign("prev_week_ts", $fday - 604800);
    sm_assign("next_week_ts", $fday + 604800);
        
    $week_number= Task::getWeek($_GET["date"]); // date("W",strtotime($_GET["date"]));
   // $week_number += 1;
    sm_assign("week_number", $week_number);

    // Load user tasks
    $user = new User($_GET["user_id"]);
	if($user->getId() < 1) $user = u();
	
    $o_tasks = $user->getTasksByDateRange(ymd($days[0]), ymd($days[6]));
   // echo '<pre>'; print_r($o_tasks); echo '</pre>';
    $tasks = array();
    $no_of_days = array();
    // This is used to track the marked public holiday
    foreach($days as $d) {
        $day = getdate($d);
        $v = date("m-d-Y",$d);
        $dates[$v] = strtolower($day['weekday']);
        $no_of_days[date("m-d-Y",$d)]['days'][date("m-d-Y",$d)] = strtolower($day['weekday']);
        $no_of_days[date("m-d-Y",$d)]['days_ts'][date("m-d-Y",$d)] = strtotime(date("m/d/Y",$d));
    }
    $curr_date[date("m-d-Y")] = strtotime(date("m/d/Y"));
    sm_assign("dates", $dates);
    sm_assign("curr_date_ts", strtotime(date("m/d/Y")));

    $disableAll = false;
    //$timecard = new Timecard(null,$user->getId(),0, $days[0], $days[6]);
    $timecard = new Timecard(null,$user->getId(),date("W",strtotime($_GET['date'])),ymd($days[0]), ymd($days[6]));
    if($timecard->getStatus() == "SUBMITTED")
        $disableAll = true;
    foreach($o_tasks as $o_task){
        if(!isset($no_of_days[date("m-d-Y",$o_task->getTaskDate())]['total_mins']))
            $no_of_days[date("m-d-Y",$o_task->getTaskDate())]['total_mins'] = 0;
        if(!isset($no_of_days[date("m-d-Y",$o_task->getTaskDate())]['pto']))
            $no_of_days[date("m-d-Y",$o_task->getTaskDate())]['pto'] = 0;
        if($o_task->getClientId() != 1000) {
            $a_task = array(
                "id" => $o_task->getId(),
                "detail" => $o_task->getDetail(),
                //"detail" => preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n",$o_task->getDetail()),
                "client_id" => $o_task->getClientId(),
                "task_date" => $o_task->getTaskDate(),
                "duration_mins" => $o_task->getDurationMins(),
                "task_type_id" => $o_task->getTaskTypeId(),
                "task_status" => $o_task->getTaskStatus(), 
                "project_role_id" => $o_task->getProjectRoleId(),
                "billable" => $o_task->getBillable()
            );
            
            $a_task["task_date"] = strtotime(date('Y-m-d', $a_task["task_date"]));
            
            if($o_task->isSubprojectTask()){ 
                $a_task["project_id"] =  $o_task->getMainProjectId();;//$o_task->getProjectParentId();
                $a_task["subproject_id"] = $o_task->getProjectId();
                $a_task["main_project_id"] = $o_task->getMainProjectId();
            }
            else{
                $a_task["project_id"] = $o_task->getProjectId();
                $a_task["subproject_id"] = 0;
            }
        
            $key = "{$a_task["task_type_id"]}.{$a_task["client_id"]}.{$a_task["project_id"]}.{$a_task["subproject_id"]}";
            if(!$tasks[$key]){
                $tasks[$key] = array(
                    "client_id" => $a_task["client_id"],
                    "task_type_id" => $a_task["task_type_id"],
                    "project_id" => $a_task["project_id"],
                    "subproject_id" => $a_task["subproject_id"],
                    "main_project_id" => $a_task["main_project_id"],
                    "billable" => $a_task["billable"],
                    "project_role_id" => $a_task["project_role_id"]
                );
            }
        
            $tasks[$key]["times"][$a_task["task_date"]] = array(
                "id" => $a_task["id"],
                "detail" => $a_task["detail"],
                "task_date" => $a_task["task_date"],
                "task_status" => $a_task["task_status"],
                "duration_mins" => $a_task["duration_mins"]
                );
            $no_of_days[date("m-d-Y",$a_task["task_date"])]['total_mins'] += $a_task["duration_mins"];
        } else { 
            $a_task = array(
                "id" => $o_task->getId(),
                "detail" => $o_task->getDetail(),
                "client_id" => $o_task->getClientId(),
                "task_date" => $o_task->getTaskDate(),
                "duration_mins" => $o_task->getDurationMins(),
                "task_status" => $o_task->getTaskStatus(),
                "task_type_id" => $o_task->getTaskTypeId()
            );
        
            if($o_task->isSubprojectTask()){
                $a_task["project_id"] = $o_task->getProjectParentId();
                $a_task["subproject_id"] = $o_task->getProjectId();
            }
            else{
                $a_task["project_id"] = $o_task->getProjectId();
                $a_task["subproject_id"] = 0;
            }
        
            $key = "{$a_task["task_type_id"]}.{$a_task["client_id"]}.{$a_task["project_id"]}.{$a_task["subproject_id"]}";
            if ($o_task->getProjectId() == 1000) {
                if(!$tasks1[$key]){
                    $tasks1[$key] = array(
                        "client_id" => $a_task["client_id"],
                        "task_type_id" => $a_task["task_type_id"],
                        "project_id" => $a_task["project_id"],
                        "subproject_id" => $a_task["subproject_id"]
                    );
                }
                $a_task["task_date"] = strtotime(date('Y-m-d', $a_task["task_date"]));
                $tasks1[$key]["times"]['pto'][$a_task["task_date"]] = array(
                    "id" => $a_task["id"],
                    "detail" => $a_task["detail"],
                    "task_date" => $a_task["task_date"],
                    "task_status" => $a_task["task_status"],
                    "duration_mins" => $a_task["duration_mins"]
                    );
                $no_of_days[date("m-d-Y",$a_task["task_date"])]['pto'] += $a_task["duration_mins"];
            } else {
                if(!$tasks[$key]){
                    $tasks[$key] = array(
                        "client_id" => $a_task["client_id"],
                        "task_type_id" => $a_task["task_type_id"],
                        "project_id" => $a_task["project_id"],
                        "subproject_id" => $a_task["subproject_id"]
                    );
                }
            
                $tasks[$key]["times"][$a_task["task_date"]] = array(
                    "id" => $a_task["id"],
                    "detail" => $a_task["detail"],
                    "task_date" => $a_task["task_date"],
                    "task_status" => $a_task["task_status"],
                    "duration_mins" => $a_task["duration_mins"]
                    );
                $no_of_days[date("m-d-Y",$a_task["task_date"])]['total_mins'] += $a_task["duration_mins"];
            }
        }
    }
    sm_assign("disableAll", intval($disableAll));
    // Get Timecard details
    $standard_hours = getStandardHours();
    sm_assign("standard_mins", ($standard_hours["standard_hours_in_mins"]/60));
    $timecard_utilization = calculate_weekly_timecard($user->getId(),$days[0], $days[6]);
    sm_assign("utilization", $timecard_utilization['utilization']);
    sm_assign("public_holidays", $timecard_utilization['public_holidays']);
    sm_assign("productivity_factor", $timecard_utilization['productivity_factor']);
    sm_assign("billable_factor", $timecard_utilization['billable_utilization_factor']);
    sm_assign("no_of_days", count($no_of_days));
    sm_assign("timecard_data", json_encode($no_of_days));

    // If adding tasks to a particular project is requested.
    if(isset($_GET["client_id"])){
        $tasks["newtask"] = array(
            "client_id" => intval($_GET["client_id"]),
            "project_id" => intval($_GET["project_id"]),
            "subproject_id" => intval($_GET["subproject_id"])
        );
    }

    if($tasks){
        $total_tasks = count($tasks);
	$tasks = array_pad($tasks, $total_tasks, array());
        /*if($total_tasks > COUNT_TASK_EDIT_ROWS) $tasks = array_pad($tasks, $total_tasks + 1, array());
        else $tasks = array_pad($tasks, COUNT_TASK_EDIT_ROWS, array());*/
    }
    else $tasks = array_fill(0, 1, array());
    
    if($tasks1){
        $total_tasks1 = count($tasks1);
	$tasks1 = array_pad($tasks1, $total_tasks1, array());
    }
    else $tasks1 = array_fill(0, 1, array());
    
    //$tasks = array_fill(0, COUNT_TASK_EDIT_ROWS, array());
   //echo '<pre>'; print_r($tasks); echo '</pre>';
   
    sm_assign("tasks1", $tasks1);
    sm_assign("tasks", $tasks);
    
    // Load clients, projects and subprojects
    $o_clients = Client::GetClients($user->getId());

    $data = array();
    $clients = array();
    $end_date = date("Y-m-d",$days[0]);
    foreach($o_clients as $o_client){
        $a_client = array("id" => $o_client->getId(), "name" => $o_client->getName());
        $o_projects = $o_client->getProjectsByUser($user->getId(), true,$end_date);
        foreach($o_projects as $o_project){
            //if ($o_project->getClosed())
            //    continue;
            $a_project = array(
				"id" => $o_project->getId(), 
				"name" => $o_project->getName(), 
				"closed" => (bool) $o_project->getClosed(),
				"allow_task_types" => (bool)$o_project->getAllowTaskTypes(),
				"start_date" => mdy($o_project->getStartdate()),
				"end_date" => mdy($o_project->getEnddate()),
				"is_billable" => $o_project->getIsBillable(),
				"project_role" => ProjectUsers::GetProjectUserRole($o_project->getId(), $user->getId())
			);

            
            $o_subprojects = Project::SubprojectsByUser($user->getId(), $o_project->getId(), $end_date);
            Project::resetSortSubProjectsByUser();
            Project::SortSubProjectsByUser($o_subprojects);                                         
            $o_subprojects = Project::getSortSubProjectsByUser();
            
             foreach($o_subprojects as $o_subproject){
                 
                $a_subproject = array(
					"id" => $o_subproject->getId(), 
					"name" => $o_subproject->getName(), 
					"closed" => (bool) $o_subproject->getClosed(),
					"allow_task_types" => (bool)$o_subproject->getAllowTaskTypes(),
					"start_date" => mdy($o_subproject->getStartdate()),
					"end_date" => mdy($o_subproject->getEnddate()),
					"is_billable" => $o_subproject->getIsBillable(),
					"project_role" => ProjectUsers::GetProjectUserRole($o_subproject->getId(), $user->getId())
				);
                $a_project["subprojects"][$o_subproject->getId()] = $a_subproject;
            }
            $a_client["projects"][$o_project->getId()] = $a_project;
        }
        $clients[$o_client->getId()] = $a_client;
    }
   // echo '<pre>';print_r($clients); echo '</pre>';
    $data["clients"] = $clients;
    sm_assign("clients", $clients);
    sm_assign("data", json_encode($data));

    $tt = TaskType::GetAll();
    $task_types = array();
    foreach($tt as $t) $task_types[$t->getGroupName()][] = $t;
    sm_assign("task_types", $task_types);

	
	if(!u()->isAdmin()){
	    $usersList = array();
		$usersList = Project::getUsersByManager(u()->getId());
		foreach( $usersList as $list){
		    $userslist[] = array(
				"id" => $list['user_id'],
				"manager_id" => u()->getId(),
				"name" => $list['first_name']." ".$list['middle_name']." ".$list['last_name'],
			);
		}
		sm_assign("userslist", $userslist);
		sm_assign("loggeduser",$user->getId());
	} else {
	
		$usersList = User::GetUsers();
		foreach( $usersList as $list){
		    $userslist_all[] = array(
				"id" => $list->getId(),
				"name" => $list->getName(),
			);
		}
        sm_assign("userslist_all", $userslist_all);
	}
        
        
    $empProjectRole = EmployeeProjectRole::GetEmployeeProjectRole();   
    sm_assign("empProjectRole", $empProjectRole);
    sm_display("task.add.multiple.html");
    
