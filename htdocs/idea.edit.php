<?php
    include_once("../config.php");
    include_once("../classes/class.ideatracker.inc.php");
    $user_id = (!u()->isAdmin()) ? u()->getId() : 0; 
    $user_id = 0;
    $idea = new IdeaTracker($_GET["id"]);
    if($idea->getId() < 1) redirect("ideatracker.list.php");
    sm_assign("idea", $idea);
    sm_display("idea.edit.html");
?>
