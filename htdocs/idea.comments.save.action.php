<?php
include_once("../config.php");
include_once("../classes/class.ideatracker.inc.php");
$idea = new IdeaTracker($_POST["id"]);

if($idea->getId() < 1) redirect("ideatracker.list.php");

include_once("../classes/class.ideacomments.inc.php");
$comments = new IdeaComments();

$comments->setDateTime(time());
$comments->setUserId(u()->getId());
$comments->setTitle($_POST["txtTitle"]);
$comments->setComment($_POST["txtComments"]);
$comments->setIdeaId($_POST["id"]);
$comments->setParentId($_POST["parent_id"]);
$comments->setCreatedBy(u()->getEmail());

if($_POST["txtTitle"] != "" && $_POST["txtComments"] != "") {
    if($comments->save()) {
        s("Comment <i><b>".$_POST['txtTitle']."</b></i> posted successlfully.");
        redirect("idea.view.php?id=".$_POST["id"]."#comment-{$comments->getId()}");
    }
    else {
        e("Failed to add comment.");
        p($_POST);
        redirect("idea.view.php?id=".$_POST["id"]."#frm");
    }
}
else {
    e("Failed to add comment.");
    p($_POST);
}
redirect("idea.view.php?id=".$_POST["id"]."#frm");
?>
