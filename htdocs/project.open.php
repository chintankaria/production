<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.project.inc.php");

    $project = new Project($_GET["id"]);
    if($project->getId() < 1) redirect("project.list.php");
	
	
	$parent_project = new Project($project->getParentId());
    sm_assign("parent_project", $parent_project);
    sm_assign("project", $project);
    sm_display("project.open.html");
