<?php
    include_once("../config.php");

    $user = u();
    if(
        $user->checkPassword($_POST["old_password"])
        && $_POST["new_password_1"]
        && $_POST["new_password_1"] == $_POST["new_password_2"]
        && $user->changePassword($_POST["new_password_1"])
    ){
        session_destroy();
        redirect("user.login.php");
    }
    else{
        e("Failed to change password");
        redirect("user.profile.php");
    }

