<?php

    include_once("../config.php");
    require_perms("manager", "admin");

    include_once("../classes/class.employeeactivity.inc.php");

    if($_POST['emp_id'] && is_array($_POST['id'])) // Re-align activities on drag & drop
    {
        EmployeeActivity::RealignActivities($_POST['emp_id'], $_POST['id']);
        die;
    }

    $employeeActivity = new EmployeeActivity();
    $employeeActivity->setEmployeeId($_POST["employee_id"]);
    $employeeActivity->setTitle($_POST["title"]);
    $employeeActivity->setDescription($_POST["description"]);
    $employeeActivity->setSticky($_POST["sticky"]);
    $employeeActivity->setPriority($_POST["priority"]);
    $employeeActivity->setStartDate($_POST["start_date"]);
    $employeeActivity->setEndDate($_POST["end_date"]);

    if($_POST['employee_id'])
    {
        if($ret = $employeeActivity->save())
        {
            s("Employee Activity <i>$ret</i> saved successfully.");
            watchdog('SUCCESS','ADD', "Employee Activity <i>$ret</i> saved successfully.");
            die($ret);
        }
        else
        {
            watchdog('FAILED','ADD', "Failed to save Employee Activity <i>" . trim($_POST['title']) . "</i>.");
            die("Failed to save Employee Activity.");
        }
    }

