<?php
    include_once("../config.php");
    include_once("../classes/class.user.inc.php");


    $user = new User($_GET["id"]);
    $isBusinessManager = in_array("business_manager" ,u()->getRoles());

    $employee = $user->getEmployee();
    $doj = format_date($employee->getDateOfJoin(),"Y-m-d");    
    $start = strtotime($doj);
    $dor = format_date($employee->getDateOfResign(),"Y-m-d");

    if(isset($dor))
        $end = strtotime($dor);
    else
    {
         $dor = date('Y-m-d');
         $end = strtotime('now');
    }

    $location = $employee->getLocationId();
    //$years_of_service = $employee->getYearsOfService($doj,$dor);


     if($doj == "" || $location == 0)
      $months = "";
    else{
   
        $month = strtotime(date("Y-m",$start));
        $i=0;
        $balance = 0;
        while($month < $end) {
          
          $months[date('Y', $month)][$i]['month'] = date('M', $month);
          $current_date = date('Y', $month)."-".date('M', $month)."-".date('d', $month);
          //$months[date('Y', $month)][$i]['expCo'] =  $employee->getYearsOfService($current_date,$doj);

          $earned = $employee->getEarnedPTO($employee->getYearsOfService($current_date,$doj),$location,$current_date);
          $used = $employee->getUsedPTO(format_date($current_date,"Y-m"),$_GET["id"]);
          $adjusted = $employee->getAdjustment(format_date($current_date,"Y-m"),$_GET["id"]);
          $balance += $earned - $used + $adjusted['adjusted_hours'];


          $months[date('Y', $month)][$i]['earned'] = $earned;
          $months[date('Y', $month)][$i]['used'] = $used;
          $months[date('Y', $month)][$i]['adjusted'] = $adjusted['adjusted_hours'];
          $months[date('Y', $month)][$i]['balance'] = $balance;

          $month = strtotime("+1 month", $month);
          $i++;

          $current_earned = $earned;
          $current_balance = $balance;
        }
  }

  sm_assign("user_id", $_GET["id"]);
  sm_assign("pto_duration", array_reverse($months,true));
  sm_assign("employee", $employee);
  sm_assign("current_earned", $current_earned);
  sm_assign("current_balance", $current_balance);
  sm_assign("user", $user);
  sm_display("salary_details.html");
?>