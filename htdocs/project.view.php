<?php
    include_once("../config.php");
    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.category.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");

    $project = new Project($_GET["id"]);
    if($project->getId() < 1 || $project->getId() == 1000) redirect("project.list.php");
    sm_assign("project", $project);
    $project_users = $project->getUsers();
    $my_role= Project::getUserRoleByProject($project->getId(),u()->getId());
	
  
    $usersProjectByRole = array();
    foreach($project_users as &$u){
        $role = Project::getUserRoleByProject($project->getId(),$u->getId());
        $u->role = $role['role'];
    }

    $category = Category::GetAll('PROJECT_DOCUMENT');
    $files = TimesheetFiles::GetByProject($project->getId(),false,true);
    //$otherfiles = TimesheetFiles::GetByProject($project->getId(),true,false);

    foreach($files as $k => $v) {
        $v->userFullName = User::getFullNameByEmailId($v->getCreatedBy());
        $v->userId = User::getUserIdByEmailId($v->getCreatedBy());
        $v->createdDate = date("m/d/Y h:i a",$v->getUploadDate());
        if($v->getFileHistory()) {
            foreach($v->getFileHistory() as $k1 => $v1) {
                $v1->userFullName = User::getFullNameByEmailId($v1->getCreatedBy());
                $v1->userId = User::getUserIdByEmailId($v1->getCreatedBy());
                $v1->createdDate = date("m/d/Y h:i a",$v1->getUploadDate());
            }
        }
    }
    /*
    foreach($otherfiles as $k => $val) {
        $val->userFullName = User::getFullNameByEmailId($val->getCreatedBy());
        $val->userId = User::getUserIdByEmailId($val->getCreatedBy());
        $val->createdDate = date("m/d/Y h:i a",$val->getUploadDate());
    }
    */

    sm_assign("files", $files);
    //sm_assign("otherfiles", $otherfiles);
    sm_assign("category", $category);

   // debug($project_users);
    sm_assign("my_role", $my_role);
    sm_assign("project_users", $project_users);
    sm_display("project.view.html");
