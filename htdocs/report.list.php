<?php
	include_once("../config.php");
	require_perms("manager","admin");
	include_once("../classes/class.task.inc.php");
	include_once("../classes/class.project.inc.php");
	// include_once("../classes/class.project.inc.php");	
	
	//debug($_GET);
	$user_obj = u();
	$user_id = $_GET["user_id"];
	$report_type = $_GET['report_type'];
	$grp_by = array();
	$grp_by = $_GET['grp_by'];
    if (!isset($_GET) || !count($_GET))
        $grp_by[] = 'showdetails_grp';
	
	if($report_type == 'custom_date'){
		$from =  $_GET["from"];
		if($_GET["to"])
		$to =  $_GET["to"];
		else $to =  ymd(is_date(date("y-m-d")));
		$_GET["month_year"] = '';
		$start_date = ymd(is_date($from));
		$end_date = ymd(is_date($to));
	}else{
		$month_year =  trim($_GET["month_year"]);
		$from =  '';
		$to = '';
		$start_date = array_shift(get_dates_of_month($month_year,true));
		$end_date = array_pop(get_dates_of_month($month_year,true));
	}
	
	$clients = Client::GetClients();
    sm_assign("clients", $clients);

    $client = new Client($_GET["client_id"]);
    if($client->getId() > 0){
	    $client_id = $client->getId();
        sm_assign("client", $client);
        $projects = $client->getProjects();
        sm_assign("projects", $projects);

        $project = $client->getProject($_GET["project_id"]);
		
        if($project && $project->getId() > 0){
			$has_subproject = $project->GetSubprojectsByProject($_GET["project_id"]);
	        sm_assign("has_subproject", $has_subproject);
			$parent_id = '';
			if($has_subproject)
			$parent_id = $_GET["project_id"];
			$project_id = $_GET["project_id"];

            sm_assign("project", $project);
            $subproject = $project->getSubproject($_GET["subproject_id"]);
			$subproject_id = $_GET["subproject_id"];
            sm_assign("subproject", $subproject);

            $subprojects = $project->getSubprojects();
            sm_assign("subprojects", $subprojects);
        }
    }

	
	$params = array();
	$params1 = array();
	if($user_id && !$client_id){

		$projects = Project::GetProjectsByUser($user_id);
		//foreach ($projects as $proj){
		 $params[] = array(
			'client_id' => $client_id,
			'user_id' => $user_id,
			'project_id' => $project_id,
			'parent_id' => $parent_id,
			'subproject_id' => $subproject_id,
			'from' => $from,
			'to' => $to,
			'month_year' => $month_year,
			'grp_by' => $grp_by,
		 );
		//}
		$task_list = array();
		foreach ($params as $in_params ){
		$tasks = Task::GetTasksReports($in_params);
		if($tasks)
		$task_list[] = $tasks;
		}
	}else {
		$params1 =  $_GET;
		$tasks = Task::GetTasksReports($params1);
		if($tasks)
		$task_list[] = $tasks;
		sm_assign("show_all", "1");
		
	}
	
	//debug($task_list);
	if($user_obj->isAdmin()){
        $users = User::GetUsers();
        sm_assign("users", $users);
    } else {
		$users = array();
		$usersList = Project::getUsersByManagerReport($user_obj->getId(), $project_id, $subproject_id);
		if($usersList)
		foreach( $usersList as $list){
		    $userslist[] = array(
				"id" => $list['user_id'],
				"name" => $list['first_name']." ".$list['middle_name']." ".$list['last_name'],
			);
		}
		sm_assign("userslist", $userslist);
	}
	
	//debug($userslist);	
	$form_values = array();
	$form_values['client_id'] = $client_id;
	$form_values['project_id'] = $project_id;
	$form_values['subproject_id'] = $subproject_id;
	if($user_id > 1)
	$form_values['user_id'] = $user_id;
	else $form_values['user_id'] = $user_obj->getId();
	$form_values['from'] = $from;
	$form_values['to'] = $to;
	$form_values['month_year'] = $month_year;
	$form_values['report_type'] = $report_type;
	$form_values['grp_by'] = $grp_by;
	$form_values['count_grp_by'] = count($grp_by);
	sm_assign("form_values", $form_values);

    #print "<pre>"; print_r($_REQUEST); "<pre>";
    //////////////////////////////////////////
    $include_pto = '';
    $where_project = "";
    $where_subproject = "";
    $include_closed_project = "AND p.end_date IS NULL";
    $include_closed_subproject = "AND sp.end_date IS NULL";
    if(isset($_GET['include_closed']) && $_GET['include_closed'] == 1) {
        $include_closed_project = "AND (p.end_date < CURRENT_DATE OR p.end_date IS NULL)";
        $include_closed_subproject = "AND (sp.end_date < CURRENT_DATE OR sp.end_date IS NULL)";
    }
    if(isset($_GET['user_id']) && $_GET['user_id'] > 0)
        $where_user = "AND u.id = {$_GET['user_id']}";
    else if (!$user_obj->isAdmin())
        $where_user = "AND u.id = " . $user_obj->getId();
    if(isset($_GET['client_id']) && $_GET['client_id'] > 0)
        $where_client = "AND c.id = {$_GET['client_id']}";
    if(isset($_GET['project_id']) && $_GET['project_id'] > 0)
        $where_project = "AND p.id = {$_GET['project_id']}";
    if(isset($_GET['subproject_id']) && $_GET['subproject_id'] > 0)
        $where_subproject = "AND sp.id = {$_GET['subproject_id']}";
    if(isset($_GET['include_pto']) && $_GET['include_pto'] == 1) {
        if($_GET['project_id'] > 0) 
            $where_project = "AND p.id IN (1000,{$_GET['project_id']})";
    }
    else {
        if($_GET['project_id'] > 0) 
            $where_project = "AND p.id != 1000 AND p.id = {$_GET['project_id']}";
        else 
            $where_project = "AND p.id != 1000";
    }
    $where = "AND (t.task_date BETWEEN '{$start_date}' AND '{$end_date}')";
   
 	$tbl_tasks = "tmp_" . md5(uniqid(microtime(), true));
	$order_by = "ORDER BY task_date, user_name, client_name, project_name, subproject_name";
    #$qry = "CREATE TABLE {$tbl_tasks} AS
    $qry = "CREATE TEMPORARY TABLE {$tbl_tasks} AS

                SELECT 
                    t.id, t.detail, t.task_type_id, t.task_date as tk_date, UNIX_TIMESTAMP( t.task_date ) AS task_date, t.duration_mins, t.task_status, 
                    ty.name AS task_type_name, u.id AS user_id, CONCAT_WS( ' ', u.first_name, u.middle_name, u.last_name ) AS user_name, 
                    p.id AS project_id, p.name AS project_name, (p.end_date <= CURRENT_DATE) AS closed, NULL AS subproject_id, NULL as subproject_name,
                    c.id AS client_id, c.name AS client_name
                FROM tasks t
                LEFT JOIN task_types ty ON ty.id = t.task_type_id
                INNER JOIN users u ON u.id = t.user_id $where_user
                INNER JOIN projects p ON p.id = t.project_id AND p.id >0 AND p.parent_id IS NULL $where_project
                INNER JOIN clients c ON c.id = p.client_id $where_client
                WHERE t.deleted =0 $where $include_closed_project

             UNION
                
                SELECT 
                    t.id, t.detail, t.task_type_id, t.task_date as tk_date, UNIX_TIMESTAMP( t.task_date ) AS task_date, t.duration_mins, t.task_status, 
                    ty.name AS task_type_name, u.id AS user_id, CONCAT_WS( ' ', u.first_name, u.middle_name, u.last_name ) AS user_name, 
                    sp.parent_id AS project_id, p.name AS project_name, (sp.end_date <= CURRENT_DATE) AS closed, sp.id AS subproject_id,sp.name as subproject_name,
                    c.id AS client_id, c.name AS client_name
                FROM tasks t
                LEFT JOIN task_types ty ON ty.id = t.task_type_id
                INNER JOIN users u ON u.id = t.user_id $where_user
                INNER JOIN projects sp ON sp.id = t.project_id AND sp.id >0 AND sp.parent_id IS NOT NULL $where_subproject
                INNER JOIN projects p ON p.id = sp.parent_id
                INNER JOIN clients c ON c.id = p.client_id $where_client
                WHERE t.deleted =0 $where $include_closed_subproject
                
            "; 
	db_execute($qry);
//debug($qry);
    $group_by_arr = array('users_grp' => 'user_name','client_grp' => 'client_name', 'project_grp' => 'project_name','subproject_grp' => 'subproject_name');
    /*if(isset($_GET['grp_by']) && count($_GET['grp_by']) > 0) {
        $group_by = 'GROUP BY';
        foreach($_GET['grp_by'] as $k) {
            $groupby[] = $group_by_arr[$k];
        }
        $group_by .= implode(",",$groupby);
    }
    */
    //////////////////////////////////////////	
	//debug($task_list); 
	$rpt_array = array();
	
	$user_name = '';
	$client_name = '';
	$proj_name_list = '';
	$subproj_name_list = '';
	
	
	
 	if($grp_by){
		//$duration_mins = "SUM(duration_mins) AS duration_mins";
		$duration_mins = "duration_mins AS duration_mins";
 		$order_group_by = "ORDER BY ";
		$roll_up = " WITH CUBE ";
		
		if(in_array('client_grp',$grp_by)){
			if(in_array('project_grp',$grp_by) || in_array('subproject_grp',$grp_by) || in_array('users_grp',$grp_by)) $comma = ','; else  $comma = '';
			$client_order = "client_name $comma";
		}
		if(in_array('project_grp',$grp_by)){
			if(in_array('subproject_grp',$grp_by) || in_array('users_grp',$grp_by)) { $comma = ',';} else { $comma = '';}
			$project_order = "project_name $comma";
		}

		if(in_array('subproject_grp',$grp_by)){
			if(in_array('users_grp',$grp_by)) $comma = ','; else  $comma = '';
			$subproject_order = "subproject_name $comma";
		}
		if(in_array('users_grp',$grp_by)){
			$users_order = "user_name ";
		}
		
	}else{
		$duration_mins = "duration_mins AS duration_mins";
	 	
	}
	$order_by = "ORDER BY task_date, user_name, subproject_name, project_name, client_name";
	if(!$user_obj->isAdmin()){
		 
		$project_id_report = report_filter("a.project_id","AND");
		$project_id_report .= "  AND a.user_id = {$user_obj->getId()}";
	    $user_clause_mem = "AND user_id = {$user_obj->getId()} ";
		//$project_id_report_notin = report_filter("project_id NOT","AND ");
		//$user_clause_mem .= $project_id_report_notin;	
	}
	

    if(isset($_GET['include_pto']) && $_GET['include_pto'] == 1) {
        if($project_id)
           $project_clause = "AND (project_id = $project_id OR project_id IN (1000,$project_id))"; 
        #else
        #   $project_clause = "AND project_id IN (1000)"; 
    }
    else {
        if($project_id)
           $project_clause = "AND project_id = $project_id"; 
    }
 	if($client_id || $user_id || $user_clause_mem)
	$where_clause = "WHERE user_id > 1 ";
	if($client_id) $client_clause = "AND  client_id = {$client_id} ";
	#if($project_id) $project_clause = "AND project_id IN ($include_pto $project_id)";
	if($subproject_id) $subproject_clause = "AND subproject_id = {$subproject_id}";
		
	if(trim($user_id) > 1 ) $user_clause = "AND user_id = {$user_id}";
	else if(trim($user_id) == 0 ) $user_clause = "";
	else  $user_clause = "AND user_id = {$user_obj->getId()}";

    ###
    $sql = "SELECT * FROM $tbl_tasks WHERE (tk_date BETWEEN '{$start_date}' AND '{$end_date}') 
            $client_clause $project_clause $subproject_clause $user_clause $order_by";	
	$rows = db_get_all($sql);
	foreach($rows as $row){
            $sub_project_name = isset($row["subproject_name"]) ? $row["subproject_name"] : "NULL";
			$rpt_array[$row["user_name"]][$row["client_name"]][$row["project_name"]][$sub_project_name][$row["id"]] = array(
					'id' => $row["id"],
					'date' => mdy($row["task_date"]),
					'name' => $row["user_name"],
					'client' => $row["client_name"],
					'project' => $row["project_name"],
					'subproject' => $row["subproject_name"],
					'closed' => $row["closed"],
					'project_id' => $row["project_id"],
					'subproject_id' => $row["subproject_id"],
					'tasktype' => $row["task_type_name"],
					'totalhours' => $row["duration_mins"],

			);
		    if($grp_by && in_array('exportdetails_grp',$grp_by)){
			    $rpt_array[$row["user_name"]][$row["client_name"]][$row["project_name"]][$sub_project_name][$row["id"]]['detail'] = $row['detail'];
            }
    }
 	$task_list = $rpt_array;
	sm_assign("task_list", $task_list);
	
	//debug($task_list);
	if($_GET["excel"]){
		include_once("../classes/class.excelwriter.inc.php");
		$excel = new ExcelWriter();
        if($grp_by && in_array('exportdetails_grp',$grp_by))
		    $excel->setHTMLTable(sm_fetch("table.report.list.html"), "Report List", null, array(10));
        else
		    $excel->setHTMLTable(sm_fetch("table.report.list.html"), "Report List", null, array(9));
		$fileName = date("d-m-Y").'_report_list.xls';
		ob_clean();
		$excel->send($fileName);
		$excel->close();
		exit;
    }

    $colors = array("#96DD09", "#C5FE23", "#B1E786", "#FFB03B", "#98B038", "#C2A25C", "#876C9B", 
                    "#BF9280", "#E4923C", "#A0F0B0", "#EDCC30", "#F2E751", "#A1F1F5");
    sm_assign("colors", $colors);
    sm_display("report.list.html");
