<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.contactgroups.inc.php");
    $groups = new ContactGroups($_POST["id"]);
    $groups->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($groups->delete()) s("Group  <i><b>{$_POST['name']}</b></i> deleted.");
        else e("Failed to delete group.");
    }
    else {
        if(!$groups->checkName()) {
            if($groups->save()) s("Group  <i><b>{$groups->getName()}</b></i> saved.");
            else e("Failed to save group {$_POST["name"]}");
        }
        else e("Group <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("contactgroups.list.php");
    }
