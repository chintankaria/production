<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.meetingtypes.inc.php");
    $meeting_types = MeetingTypes::GetAll();
    $participants = MeetingTypes::GetStandardParticipants();

    $data = array();
    $usersArr = array();
    foreach($meeting_types as $key => $val) {
        $m_data = array(
                        "id" => $val->getId(),
                        "name" => $val->getName(),
                    );
        if($val->GetParticipants()) {
            foreach($val->GetParticipants() as $k => $v) {
                $a_participants = array(
                                    "id" => $v->getParticipantsId(),
                                    "user_id" => $v->getUserId(),
                                    "email" => $v->getEmail(),
                                );
                $m_data["participants"][$k] = $a_participants;
            }
        }
        $a_data[$key] = $m_data;
    }
    $users = User::GetUsers();
    foreach($users as $o_user) {
        $a_user = array("id" => $o_user->getId(), "name" => $o_user->getName());
        $usersArr[$o_user->getId()] = $a_user;
    }
    $data["meetings"] = $a_data;
    $data["users"] = $usersArr;
    #sm_assign("data", json_encode($data));
    sm_assign("participants", $participants);
    sm_assign("meeting_types", $meeting_types);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.meetingtypes.list.html"), "Meeting Types list",null, array(2));
    	$fileName = date("d-m-Y").'_meetingtypes_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("meetingtypes.list.html");
        exit;
    }
    else {
        sm_display("meetingtypes.list.html");
    }
