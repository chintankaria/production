<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.tasktype.inc.php");

    $task_type = new TaskType($_POST["id"]);
    $task_type->setGroupId($_POST["group_id"]);
    $task_type->setName($_POST["name"]);
    if(!$task_type->checkName($_POST['name'], $_POST['group_id'])) {
        if($task_type->save()) {
            $str = "Task type {$task_type->getName()} saved.";
            s("Task type {$task_type->getName()} saved.");
            if($_POST['id'] > 0) 
                watchdog('SUCCESS','EDIT', "$str");
            else
                watchdog('SUCCESS','ADD', "$str");
        }
        else {
            $str = "Failed to save task type {$_POST["name"]}";
            e("Failed to save task type {$_POST["name"]}");
            if($_POST['id'] > 0) 
                watchdog('FAILED','EDIT', "$str");
            else
                watchdog('FAILED','ADD', "$str");
        }
    }
    else {
        e("Task type <b><i>{$_POST["name"]}</i></b> already exists.");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("tasktype.list.php");
    }
