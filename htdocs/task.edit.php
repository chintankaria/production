<?php
    include_once("../config.php");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.project.inc.php");

    $task = new Task($_GET["id"]);
    if($task->getId() < 1) redirect("task.list.php");
    sm_assign("task", $task);

    // Set GET variables here for task.filter.php
    $p = $task->getProject();
    if($p && $p->getId() > 0){
        $_GET["client_id"] = $p->getClientId();
        if($p->isSubproject()){
            $_GET["project_id"] = $p->getParentId();
            $_GET["subproject_id"] = $p->getId();
        }
        else $_GET["project_id"] = $p->getId();

        include_once("task.filter.php");
    }

    sm_display("task.edit.html");
