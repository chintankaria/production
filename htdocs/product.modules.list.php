<?php

include_once("../config.php");
require_perms("admin");

include_once("../classes/class.products.inc.php");

/* AJAX check.  Below if block is used in populating product modules. */
$modules = array();
foreach(Products::GetProductModules($_GET['id']) as $m) {
	$modules[] = array(
		"id" => $m->getId(),
		"name" => $m->getName(),
	);
}

ob_clean();
print(json_encode($modules));
die;
