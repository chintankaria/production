<?php
    $clients = Client::GetClients();
    sm_assign("clients", $clients);

    $client = new Client($_GET["client_id"]);
    if($client->getId() > 0){
        sm_assign("client", $client);
        $projects = $client->getProjects();
        sm_assign("projects", $projects);

        $project = $client->getProject($_GET["project_id"]);
        if($project && $project->getId() > 0){
            sm_assign("project", $project);
            $subproject = $project->getSubproject($_GET["subproject_id"]);
            sm_assign("subproject", $subproject);

            $subprojects = $project->getSubprojects();
            sm_assign("subprojects", $subprojects);
        }
    }

    $task_types = TaskType::GetAll();
    sm_assign("task_types", $task_types);

