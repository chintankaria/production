<?php
    include_once("../config.php");
    include_once("../classes/class.client.inc.php");

    $allClients = isset($_GET['all']) ? $_GET['all'] : 1;

    $clients = Client::GetClients(0,$allClients);
    sm_assign("clients", $clients);
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.client.list.html"), "Client List", null, array(5));
    	$fileName = date("d-m-Y").'_client_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    sm_assign("allClients", $allClients);
    sm_display("client.list.html");
    
