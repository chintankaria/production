<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.designation.inc.php");
    $designation = Designation::GetAll();

    sm_assign("designation", $designation);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.designation.list.html"), "Designation list",null, array(3));
    	$fileName = date("d-m-Y").'_designation.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("designation.list.html");
        exit;
    }
    else {
        sm_display("designation.list.html");
    }
