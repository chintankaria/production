<?php
    include_once("../config.php");
    require_perms("hr_manager","admin");
    include_once("../classes/class.user.inc.php");

    $user = new User($_GET["id"]);
    if(!$user || $user->getId() == 1 || $user->getId() < 1) redirect("user.list.php");

    sm_assign("user", $user);
    sm_display("user.unblock.html");
