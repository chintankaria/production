<?php

include_once("../config.php");
include_once("../classes/class.colorcode.inc.php");
include_once("../classes/class.calendarevents.inc.php");
include_once("../classes/class.client.inc.php");
include_once("../classes/class.project.inc.php");

if($_GET["excel"]){
    include_once("../classes/class.excelwriter.inc.php");
    $excel = new ExcelWriter();
    
    $start_date = ymd(strtotime(date("m")."/1/".date("Y")));
    $end_date = ymd(strtotime(date("m")."/31/".date("Y")));
    $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
    $client_id = isset($_GET['client_id']) ? $_GET['client_id'] : 0;
    $project_id = isset($_GET['project_id']) ? $_GET['project_id'] : 0;
    $subproject_id = isset($_GET['subproject_id']) ? $_GET['subproject_id'] : 0;

    $param['start_date'] = $start_date;
    $param['end_date'] = $end_date;
    $param['client_id'] = $client_id;
    $param['project_id'] = $project_id;
    $param['subproject_id'] = $subproject_id;
    $param['user_id'] = $user_id;

    #$events = CalendarEvents::GetJavascriptEventsByUser(u(), $param);
    $events = CalendarEvents::GetProjectTimeline($param);
    if($events) {
        foreach($events as $k => $v) {
            $events[$k]["data"]["created_by"] = User::getFullNameByEmailId($v["data"]["created_by"]);
            $events[$k]["data"]["updated_by"] = User::getFullNameByEmailId($v["data"]["updated_by"]);
            $events[$k]["data"]["status"] = ucwords(strtolower($v["data"]["status"]));
        }
    }
    sm_assign("events", $events);
   
    $excel->setHTMLTable(sm_fetch("table.calendar.list.html"), "Calendar Event List", null, array());
    $fileName = date("d-m-Y").'_calendar_event_list.xls';
    ob_clean();
    $excel->send($fileName);
    $excel->close();
    exit;
    
    sm_display("table.calendar.list.html");
}
else {
    $user_id = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
    $chkUserColor = isset($_GET['chkUserColor']) ? $_GET['chkUserColor'] : 0;
    $client_id = pintval($_GET["clientid"]);

    $u = u();
    if($u->isAdmin()) {
        $users1 = $user_arr = User::GetUsers();
    }
    else if($u->isCalendarManager()) {
        $users1 = $user_arr = User::GetProjectUsersByProjectIds($u->getProjectIds());
    }
    else {
        $user_arr = new User($u->getId());
        $user_id = $u->getId();
        if(!is_array($user_arr)) {
            $a = $user_arr;
            $user_arr = array($a);
            $users1 = $user_arr;
        }
    }
    $data = array();
    $usersArr = array();
    foreach($user_arr as $o_user) {
        $a_user = array("id" => $o_user->getId(), "name" => $o_user->getName());
        $o_clients = $o_user->getProjectAssociation();
        foreach($o_clients as $clientId => $o_client){
            $a_client = array(
                "id" => $clientId,
                "name" => $o_client["clientName"],
            );
            $o_projects = $o_client["projects"];
            if(is_array($o_projects) && count($o_projects) > 0) {
                foreach($o_projects as $proj_id => $o_project){
                    $a_project = array(
                        "id" => $proj_id, 
                        "name" => $o_project["projectName"],
                    );

                    if($o_project["subprojects"]){
                        $o_subprojects = $o_project["subprojects"];
                        foreach($o_subprojects as $subproj_id => $o_subproject){
                            $a_subproject = array(
                                "id" => $subproj_id,
                                "name" => $o_subproject["projectName"],
                            );
                            $a_project["subprojects"][$subproj_id] = $a_subproject;
                        }
                    }
                    $a_client["projects"][$proj_id] = $a_project;
                }
            }
            $a_user["clients"][$clientId] = $a_client;
        }
        $usersArr[$o_user->getId()] = $a_user;
    }
    if($u->isAdmin() || $u->isCalendarManager())
        $data["usersArr"] = $usersArr;
    sm_assign("data", json_encode($data));

    $event_colors = ColorCode::getCalendarColorCode();
    $user_colors = array();
    if($chkUserColor) {
        $user_colors = ColorCode::getUserColorCode();
    }

    sm_assign("event_colors", $event_colors);
    sm_assign("user_colors", $user_colors);
    sm_assign("chkUserColor", $chkUserColor);

    $projects = array();
    $subprojects = array();
    $project_members = array();
    if($user_id > 0) { 
        if($usersArr[$user_id]) {
            $clients1 = $usersArr[$user_id]["clients"];
            if($client_id > 0) {
                $project_id = pintval($_GET["projectid"]);
                $projects = $usersArr[$user_id]["clients"][$client_id]["projects"];
                if($project_id > 0) {
                    $subproject_id = pintval($_GET["subprojectid"]);
                    $subprojects = $usersArr[$user_id]["clients"][$client_id]["projects"][$project_id]["subprojects"];
                }
                else {
                    $subproject_id = 0;
                }
            }
            else {
                $project_id = 0;
            }
        }
    }
    else {
        $clients1 = Client::GetClients();
        if($clients1[$client_id]){
            $projects = Project::GetProjectsByClient($client_id, true);
            $project_id = pintval($_GET["projectid"]);

            if($projects[$project_id]){
                $subprojects = Project::GetSubprojectsByProject($project_id);
                $subproject_id = pintval($_GET["subprojectid"]);

                if(!$subprojects[$subproject_id]) $subproject_id = 0;
            }
            else{
                $project_id = 0;
            }
        }
        else{
            $client_id = 0;
        }
    }
    sm_assign("clients1", $clients1);
    sm_assign("client_id", $client_id);

    sm_assign("projects", $projects);
    sm_assign("project_id", $project_id);

    sm_assign("subprojects", $subprojects);
    sm_assign("subproject_id", $subproject_id);

    $users = $project_members;
    sm_assign("users", $users);
    sm_assign("users1", $users1);
    sm_assign("user_id", $user_id);

    sm_display("calendar.view.html");
}
