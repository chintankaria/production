<?php
    include_once("../config.php");
    include_once("../classes/class.task.inc.php");
    $task = new Task();
    $task->setProjectId($_POST["project_id"], $_POST["subproject_id"]);
    $task->setTaskDate($_POST["task_date"]);
    $task->setDetail($_POST["detail"]);
    $task->setDurationMins($_POST["duration"]);
    $task->setTaskTypeId($_POST["task_type_id"]);

    if($task->save()){
        s("Task added successfully.");
        p(array(
            "task_date" => mdy($task->getTaskDate()),
            "task_type_id" => $task->getTaskTypeId()
        ));
    }
    else{
        e("Failed to add task.");
        p(array(
            "task_date" => $_POST["task_date"],
            "task_type_id" => $_POST["task_type_id"],
            "duration" => $_POST["duration"],
            "detail" => $_POST["detail"]
        ));
    }

    redirect("task.add.single.php?client_id={$_POST["client_id"]}&project_id={$_POST["project_id"]}&subproject_id={$_POST["subproject_id"]}");

