<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.communicationtype.inc.php");
    $types = new CommunicationType($_POST["id"]);
    $types->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($types->delete()) s("Communication Type  <i><b>{$_POST['name']}</b></i> deleted.");
        else e("Failed to delete communication type..");
    }
    else {
        if(!$types->checkName()) {
            if($types->save()) s("Communication Type <i><b>{$types->getName()}</b></i> saved.");
            else e("Failed to save communication type {$_POST["name"]}");
        }
        else e("Communication Type <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("communicationtype.list.php");
    }
