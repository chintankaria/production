<?php

include_once("../config.php");
include_once("../classes/class.ideatracker.inc.php");

$id = intval($_GET["id"]);
if($id > 0) {
    $file = new IdeaTracker();
    $f = $file->getFile($id);
    ob_clean();
    header("Content-Type: {$f['mime_type']}");
    header("Content-Length: {$f['file_size']}");
    header("Content-Disposition: filename=\"{$f['file_name']}\"");
    readfile($f['file_path']);
    exit;
}
