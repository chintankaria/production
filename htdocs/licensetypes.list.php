<?php

include_once("../config.php");
require_perms("admin");

require_once('../classes/class.licensetypes.inc.php');

$license_types = LicenseTypes::FindAll();

sm_assign("license_types", $license_types);
sm_display("licensetypes.list.html");