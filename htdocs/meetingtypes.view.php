<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.meetingtypes.inc.php");
    $meeting_type = new MeetingTypes($_GET["id"]);
    if($meeting_type->getId() < 1) redirect("meetingtypes.list.php");
    $arr = array($meeting_type);
    foreach($arr as $key => $val) {
        $val->createdFullName = User::getFullNameByEmailId($val->getCreatedBy());
        $val->updatedFullName = User::getFullNameByEmailId($val->getUpdatedBy());
        $val->createdDate = date("m/d/Y h:i a",strtotime($val->getCreateDate()));
        $val->updatedDate = date("m/d/Y h:i a",strtotime($val->getUpdateDate()));
        $m_data = array(
                        "id" => $val->getId(),
                        "name" => $val->getName(),
                    );
        if($val->GetParticipants()) {
            foreach($val->GetParticipants() as $k => $v) {
                $a_participants = array(
                                    "id" => $v->getParticipantsId(),
                                    "user_id" => $v->getUserId(),
                                    "email" => $v->getEmail(),
                                    "name" => $v->getFullName(),
                                );
                $m_data["participants"][$k] = $a_participants;
            }
        }
        $a_data[$key] = $m_data;
    }

    $users = User::GetUsers();

    foreach($users as $key => $o_user) {
        $a_user = array("id" => $o_user->getId(), "name" => $o_user->getName());
        $usersArr[$o_user->getId()] = $a_user;
        if($meeting_type->GetParticipants()) {
            foreach($meeting_type->GetParticipants() as $v) {
                if($o_user->getId() == $v->getUserId()) {
                    unset($users[$key]);
                }
            }
        }
    }
    $data["meetings"] = $a_data;
    $data["users"] = $usersArr;
    sm_assign("data", json_encode($data));
    sm_assign("meeting_type", $meeting_type);
    sm_assign("users", $users);

    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("meetingtypes.view.html");
        exit;
    }
    else {
        sm_display("meetingtypes.view.html");
    }
?>
