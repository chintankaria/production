<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.contacttype.inc.php");
    $types = ContactType::GetAll();

    sm_assign("types", $types);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.contacttype.list.html"), "Contact Type list",null, array(3));
    	$fileName = date("d-m-Y").'_contacttype.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("contacttype.list.html");
        exit;
    }
    else {
        sm_display("contacttype.list.html");
    }
