<?php
    include_once("../config.php");
    include_once("../classes/class.watchdog.inc.php");
    include_once("../classes/class.pagination.inc.php");
    require_perms("admin");
    
    $actions = array("ADD" => "Items Added", "EDIT" => "Items Edited", "APPROVED" => "Items Approved", "DELETE" => "Items Deleted", 
                    "REJECTED" => "Items Rejected", "POINTS" => "Items Points",
                    "FILE" => "File Uploads", "LOGIN" => "Member Login", "LOGOUT" => "Member Logout", "TIMECARD" => "Timecard Item", 
                    "TIMECARD_REVIEW" => "Timecard Review", "EMAIL" => "Email Notification", "CRON" => "Cron Activity", 
                    );
    $status_arr = array("SUCCESS" => "Success", "FAILED" => "Failed");
    $from = isset($_GET['from_date']) ? $_GET['from_date'] : date("m/01/Y");
    $to = isset($_GET['to_date']) ? $_GET['to_date'] : date("m/d/Y");
    $email = (isset($_GET['email']) && !is_numeric($_GET['email'])) ? $_GET['email'] : null;
    $action = (isset($_GET['action']) && array_key_exists($_GET['action'], $actions))? $_GET['action'] : null;
    $status = (isset($_GET['status']) && array_key_exists($_GET['status'], $status_arr))? $_GET['status'] : null;
    $hide_cron = (isset($_GET['cron']) && is_numeric($_GET['cron'])) ? $_GET['cron'] : 0;
    $p = (isset($_GET["p"]) && intval($_GET['p']) > 0) ? intval($_GET["p"]) : 1;
    $total_pages = 0;
    $total_results = 0;
    $page_limit = 50;

    if(strtoupper($action) == "CRON") $email = null;
    if($action != null) $hide_cron = 0;
    $from = ymd(is_date($from));
    $to = ymd(is_date($to));

    if($_GET["excel"]) $p = 0;

    $auditlogs = Watchdog::GetLogs($email, $from, $to, $action, $status,$hide_cron,$p,$page_limit); 
    if($p > 0) {  
        $total_pages = Watchdog::getTotalPages();
        $total_results = Watchdog::getTotalResults();
        $Paginate = new Pagination();
        $Paginate->setCurrentPage($p);
        $Paginate->setPageLimit($page_limit);
        $Paginate->setTotalPages($total_pages);
        $Paginate->setTotalResult($total_results);
        $Paginate->setPageNumberLimit(5);
        $Paginate->Paginate();
        $pages = $Paginate->getNumberStyleArray();
        $previous_page =$Paginate->getPreviousPage();
        $next_page = $Paginate->getNextPage();
        $current_page = $Paginate->getCurrentPage();

        sm_assign("total_results", $total_results);
        sm_assign("total_pages", $total_pages);
        sm_assign("pages", $pages);
        sm_assign("previous_page", $previous_page);
        sm_assign("next_page", $next_page);
        sm_assign("current_page", $current_page);
        sm_assign("page_end", @end($pages));
    }
    sm_assign("hide_cron", $hide_cron);
    sm_assign("actions", $actions);
    sm_assign("sel_action", $action);
    sm_assign("status_arr", $status_arr);
    sm_assign("status", $status);
    sm_assign("auditlogs", $auditlogs);
    sm_assign("from_date", $from);
    sm_assign("to_date", $to);
    sm_assign("email", $email);
    sm_assign("p", $p);

    $users = User::GetUsers();
    sm_assign("users", $users); 
    if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.auditlog.html"), "Audit Logs",null,array());
    	$fileName = date("d-m-Y").'_auditlogs.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }

    sm_display("auditlog.list.html");
?>
