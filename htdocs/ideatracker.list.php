<?php
    include_once("../config.php");
    include_once("../classes/class.ideatracker.inc.php");
    include_once("../classes/class.pagination.inc.php");


    $from_date = (isset($_GET["from_date"]) && is_date($_GET["from_date"])) ? $_GET["from_date"] : date("m")."/1/".date("Y");
    $to_date = (isset($_GET["to_date"]) && is_date($_GET["to_date"])) ? $_GET["to_date"] : date("m/d/Y",strtotime("tomorrow"));
    $status = isset($_GET["status"]) ? trim($_GET["status"]) : "PUBLISHED";
    $view = isset($_GET["view"]) ? trim($_GET["view"]) : "list";
    $p = (isset($_GET["p"]) && intval($_GET['p']) > 0) ? intval($_GET["p"]) : 1;
    $total_pages = 0;
    $total_results = 0;
    $page_limit = 5;

    $user = u();
    $excel_no_field = 8;
    sm_assign("from_date", $from_date);
    sm_assign("to_date", $to_date);
    sm_assign("status", $status);
    sm_assign("p", $p);

    $ideas = IdeaTracker::GetIdeasList($status, $p, $page_limit);
    if($p > 0) {  
        $total_pages = IdeaTracker::getTotalPages();
        $total_results = IdeaTracker::getTotalResults();
        $Paginate = new Pagination();
        $Paginate->setCurrentPage($p);
        $Paginate->setPageLimit($page_limit);
        $Paginate->setTotalPages($total_pages);
        $Paginate->setTotalResult($total_results);
        $Paginate->setPageNumberLimit(5);
        $Paginate->Paginate();
        $pages = $Paginate->getNumberStyleArray();
        $previous_page =$Paginate->getPreviousPage();
        $next_page = $Paginate->getNextPage();
        $current_page = $Paginate->getCurrentPage();

        sm_assign("total_results", $total_results);
        sm_assign("total_pages", $total_pages);
        sm_assign("pages", $pages);
        sm_assign("previous_page", $previous_page);
        sm_assign("next_page", $next_page);
        sm_assign("current_page", $current_page);
        sm_assign("page_end", @end($pages));
    }
    if($_GET["excel"]){
        $ideas = IdeaTracker::GetIdeasList($status);
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
		$excel->setHTMLTable(sm_fetch("table.meeting.list.html"), "Meeting List", null, array($excel_no_field));    	
    	$fileName = date("d-m-Y").'_meeting_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    if($view == "list") {
        if($ideas) {
            foreach($ideas as $key => $idea) {
                $pClass = ($pClass == "comments-odd") ? "comments-even" : "comments-odd";
                $idea->class = $pClass;
                $idea->create_date = date('d M y h:i a',strtotime($idea->getCreateDate()));
                $idea->update_date = date('d M y h:i a',strtotime($idea->getUpdateDate()));
                $idea->date_time = date("d M y h:i a",strtotime($idea->getDateTime()));
                $idea->description = short_string(strip_tags($idea->getDescription()),500);
            }
        }
    }

    sm_assign("ideas", $ideas);    
    sm_assign("view", $view);
    sm_display("ideatracker.list.html");
?>
