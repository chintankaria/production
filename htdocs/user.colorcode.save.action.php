<?php
    include_once("../config.php");
    include_once("../classes/class.colorcode.inc.php");
    require_perms("admin");
    $color = new ColorCode();
    if($color->saveUserColors($_POST)) {
        s("Members color codes save."); 
    } 
    else {
        e("Failed to save members color codes.");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("user.colorcode.list.php");
    }
?>
