<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.client.contacts.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");


// Below if block is for adding Client Contact from Create License page -> Create New Contact.
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && $_POST["contact_add_from"] == 'AddLicense' )
    {
        foreach($_POST['contact_id'] as $k => $v) {
            if($k == 0) continue;

            $contact = new ClientContact($_POST['contact_id'][$k]);

            $_POST['is_primary'][$k] = isset($_POST['is_primary'][$k]) ? $_POST['is_primary'][$k] : 0;
            $_POST['is_active'][$k] = isset($_POST['is_active'][$k]) ? $_POST['is_active'][$k] : 0;

            $contact->setClientId($_POST['client_id']);
            $contact->setFirstName($_POST['first_name'][$k]);
            $contact->setLastName($_POST['last_name'][$k]);
            $contact->setEmailId($_POST['email'][$k]);
            $contact->setPhone($_POST['phone'][$k]);
            $contact->setJobTitle($_POST['job_title'][$k]);
            $contact->setIsPrimary($_POST['is_primary'][$k]);
            $contact->setIsActive($_POST['is_active'][$k]);

            $contact->save();
        }
        die("Success");
    }


    $client = new Client();
    $_POST["active"] = isset($_POST["active"]) ? $_POST["active"] : 0;
    $_POST["is_product_client"] = isset($_POST["is_product_client"]) ? $_POST["is_product_client"] : 0;
    $client->setName($_POST["name"]);
    $client->setDescription($_POST["description"]);
    $client->setWebsite($_POST["website"]);
    $client->setIsProductClient($_POST["is_product_client"]);
    $client->setIsActive($_POST["active"]);

    $chk_id = $client->checkClient($_POST['name']);
    if(!$chk_id) {
        if($client->save()){
            foreach($_POST['contact_id'] as $k => $v) {
                if($k == 0) continue;
                
                $contact = new ClientContact($_POST['contact_id'][$k]);
                
                $_POST['is_primary'][$k] = isset($_POST['is_primary'][$k]) ? $_POST['is_primary'][$k] : 0;
                $_POST['is_active'][$k] = isset($_POST['is_active'][$k]) ? $_POST['is_active'][$k] : 0;
                
                $contact->setClientId($client->getId());
                $contact->setFirstName($_POST['first_name'][$k]);
                $contact->setLastName($_POST['last_name'][$k]);
                $contact->setEmailId($_POST['email'][$k]);
                $contact->setPhone($_POST['phone'][$k]);
                $contact->setJobTitle($_POST['job_title'][$k]);
                $contact->setIsPrimary($_POST['is_primary'][$k]);
                $contact->setIsActive($_POST['is_active'][$k]);

                $contact->save();
            }

            watchdog('SUCCESS','ADD', "Client <i>{$client->getName()}<i> created successfully.");

            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            {
                die("Success~~{$client->getId()}");
            }
            else
            {
                s("Client added successfully.");
                redirect("client.view.php?id={$client->getId()}");
            }
        }
        else{
            watchdog('FAILED','ADD', "Failed to create <i>{$client->getName()}<i> client.");
            p($_POST);

            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            {
                die("Failed to create <i>{$client->getName()}<i> client.");
            }
            else
            {
                e("Failed to add client.");
                redirect("client.add.php");
            }
        }
    }
    else {
        p($_POST);

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            die("Client already exists.");
        }
        else
        {
            e("Client already exists.<a href='client.view.php?id=$chk_id'>Click here to view the details</a>");
            redirect("client.add.php");
        }
    }
 
