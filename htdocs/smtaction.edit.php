<?php
    include_once("../config.php");
    require_perms("smt_member","admin");
    include_once("../classes/class.smtaction.inc.php");

    $action_status = array("Active", "Pending", "On Hold", "Complete", "Closed");

    if(!u()->isAdmin() && !u()->isSMTMember())
        redirect("smtaction.list.php");

    $actionItem = new SMTAction($_GET["id"]);
    if($actionItem->getId() < 1) redirect("smtaction.list.php");
    if ($actionItem->getAssignedTo())
        $sel_users = explode(",", $actionItem->getAssignedTo());

    $user_id = (!u()->isAdmin()) ? u()->getId() : 0; 
    $users = User::GetUsers();
    foreach($users as $o_user) {
        if ($o_user->isSMTMember()) {
            $a_user = array("id" => $o_user->getId(), "name" => $o_user->getName(), "email" => $o_user->getEmail());
            $usersArr[$o_user->getId()] = $a_user;
            if ($sel_users) {
                if (in_array($o_user->getEmail(), $sel_users))
                    $selusersArr[$o_user->getId()] = $a_user;
            }
        }
    }

    $data["users"] = $selusersArr;
    sm_assign("item", $actionItem);
    sm_assign("users",$usersArr);
    sm_assign("data", json_encode($data));
    sm_assign("status",$action_status);
    sm_display("smtaction.edit.html");
?>
