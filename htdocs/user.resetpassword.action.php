<?php
    include_once("../config.php");
    if($_POST["answer"] != "yes") redirect("user.list.php");

    include_once("../classes/class.user.inc.php");

    $user = new User($_POST["id"]);
    $from = array("admin.timesheet@globalss.com" => "GSS Administrator");
    if($user && $user->getId() > 0){
        $password = $user->changePassword();
        if($password){
            _::Mail($from,
                array($user->getEmail() => $user->getName()),
                "Password reset confirmation.",
                sm_fetch("user.resetpassword.letter.html", array(
                    "name" => $user->getEmail(),
                    "password" => $password
                ))
            );

            s("Password for {$user->getName()} has been reset.");
            $str = "Reset password notification sent to <i>{$user->getName()}({$user->getEmail()})</i>.";
            watchdog('SUCCESS','EMAIL', $str);
        }
        else e("Failed to reset password for {$user->getName()}");
    }


    redirect("user.list.php");
