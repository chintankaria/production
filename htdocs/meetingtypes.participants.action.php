<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.meetingtypes.inc.php");

    $mType = new MeetingTypes($_POST["id"]);
    if($mType->getId() < 1) redirect("meetingtypes.list.php");

    if($_POST["action"] == "ASSIGN_MEMBERS") {
        if($mType->deleteParticipants($mType->getId())) {
            foreach($_POST["participants_id"] as $v) {
                $tmp = explode("|",$v);
                $email_ids[] = $tmp[1];
                $mType->saveParticipants($tmp[0],$tmp[1]);
            }
            $str = "Participants <i><b>".implode(", ",$email_ids)."</b></i> assigned successlfully.";
            s("Participants <i><b>".implode(", ",$email_ids)."</b></i> assigned successlfully.");
            watchdog('SUCCESS','EDIT', "$str");
        }
        else e("Failed to assign participants.");
    }
    else if($_POST["action"] == "REMOVE_MEMBERS") {
        if($_POST["participants"]) {
            foreach($_POST["participants"] as $v) {
                if(!$mType->deleteParticipants($v)) {
                    e("Failed to remove participants.");
                    redirect("meetingtypes.view.php?id=".$mType->getId());
                }
            }
            $str = "Participants removed successfully.";
            s("Participants removed successfully.");
            watchdog('SUCCESS','EDIT', "$str");
        }
        else e("Failed to remove participants.");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("meetingtypes.view.php?id=".$mType->getId());
    }
