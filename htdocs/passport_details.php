<?php

include_once("../config.php");
include_once("../classes/class.user.inc.php");
include_once("../classes/class.passport.inc.php");


if (isset($_POST['action']) && $_POST['action'] == 'AddVisa') {

	$user = new User($_POST["id"]);

		if($user->getId() > 0){
		
			$passport = new Passport($_POST['vpid'], $user->getId());
			$passport->setPassportId($_POST["vpid"]);
			$passport->setVisaType($_POST['txtVisaType']);
			$passport->setVisaCountry($_POST['visa_country_of_residance']);
			$passport->setVisaIssueDate($_POST['txtVisaDateOfIssue']);
			$passport->setVisaExpiryDate($_POST['txtVisaDateOfExpiry']);
			$passport->setVisaPlaceOfIssue($_POST['txtVisaPlaceOfIssue']);
			$status = $passport->saveVisaInfo();
			 if($status) {
			        $str = "New visa added ";
			        s("New visa added .");
			        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
			    }
			    else {
			        $str = "Failed to add visa details";
			        e("Failed to visa details.");
			        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
			        redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
			    }
			    redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
	}

}
if (isset($_POST['action']) && $_POST['action'] == 'Save Details') {


	$user = new User($_POST["id"]);

		if($user->getId() > 0){

		    foreach($_POST['passport_id'] as $k => $v) {
		       
		        $_POST['txtPassportNumberE'][$k] = remove_whitespace($_POST['txtPassportNumberE'][$k]);
		        $_POST['txtDateOfIssueE'][$k] = remove_whitespace($_POST['txtDateOfIssueE'][$k]);
		        $_POST['txtDateOfExpiryE'][$k] = remove_whitespace($_POST['txtDateOfExpiryE'][$k]);
		        $_POST['txtPlaceOfIssueE'][$k] = remove_whitespace($_POST['txtPlaceOfIssueE'][$k]);
		        
				$passport = new Passport($_POST['passport_id'][$k], $user->getId());
				$passport->setPassportId($_POST['passport_id'][$k]);
				$passport->setUserId($user->getId());
		        $passport->setPassportNumber($_POST['txtPassportNumberE'][$k]);
		        $passport->setPassportIssueDate($_POST['txtDateOfIssueE'][$k]);
		        $passport->setPassportExpiryDate($_POST['txtDateOfExpiryE'][$k]);
		        $passport->setPassportPlaceOfIssue($_POST['txtPlaceOfIssueE'][$k]);
		        $passport->setPassportCountryOfResidence($_POST['country_of_residance_e'][$k]);
		        $passport->setVisaId($_POST['visa_id'][$k]);
		        $passport->save($_POST['passport_id'][$k]);


		    }



		    foreach($_POST['visa_id'] as $k => $v) {
                
                $_POST['visa_type'][$k] = remove_whitespace($_POST['visa_type'][$k]);
                $_POST['vcountry'][$k] = remove_whitespace($_POST['vcountry'][$k]);
                $_POST['vissue_date'][$k] = remove_whitespace($_POST['vissue_date'][$k]);
                $_POST['vexpiry_date'][$k] = remove_whitespace($_POST['vexpiry_date'][$k]);
                $_POST['vplace_of_issue'][$k] = remove_whitespace($_POST['vplace_of_issue'][$k]);

                $passport = new Passport(0, $user->getId());
				$passport->setPassportId($_POST['passport_id'][$k]);
				$passport->setUserId($user->getId());
                $passport->setVisaId($_POST['visa_id'][$k]);
                $passport->setVisaType($_POST['visa_type'][$k]);
                $passport->setVisaCountry($_POST['vcountry'][$k]);
                $passport->setVisaIssueDate($_POST['vissue_date'][$k]);
                $passport->setVisaExpiryDate($_POST['vexpiry_date'][$k]);
                $passport->setVisaPlaceOfIssue($_POST['vplace_of_issue'][$k]);
                $status = $passport->getVisaInfo();
               	$status = $passport->updatVisa();
            }

		    if($status) {
		        $str = "Passport Details Update successfully ";
		        s("Passport Details Update successfully .");
		        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
		    }
		    else {
		        $str = "Failed to update passport details";
		        e("Failed to  update passport details.");
		        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
		    }

		    redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
		}

}
if (isset($_POST['action']) && $_POST['action'] == 'Delete Visa') {

	$user = new User($_POST["id"]);

	if($user->getId() > 0){

		$passport = new Passport($_POST['delId'], $user->getId());
		$passport->setPassportId($_POST["delId"]);
		$newstatus = $passport->deletedVisaInfo($_POST['vdelId']);

		if($newstatus) {
		        $str = "Visa Deleted ";
		        s("Visa deleted successfully .");
		        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
		    }
		    else {
		        $str = "Failed to delete Visa";
		        e("Failed to  delete Visa.");
		        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
		        redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
		    }

		    redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
	}

}

if (isset($_POST['action']) && $_POST['action'] == 'Delete') {

		$user = new User($_POST["id"]);


		if($user->getId() > 0){

			$passport = new Passport($_POST['delId'], $user->getId());
			$passport->setPassportId($_POST["delId"]);
			$status = $passport->deletePassport();
			$newstatus = $passport->deletedVisaInfo();

			if($newstatus) {
		        $str = "Passport Deleted ";
		        s("Passport deleted successfully .");
		        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
		    }
		    else {
		        $str = "Failed to delete passport";
		        e("Failed to  delete passport.");
		        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
		        redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
		    }

		    redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
			
		}
}

	   

if (isset($_POST['action']) && $_POST['action'] == 'AddEdit') {

	$user = new User($_POST["id"]);
	
	if($user->getId() > 0){
	       
	            $passport = new Passport(0, $user->getId());
	            $passport->setUserId($user->getId());
	            //$passport->setId($_POST["passport_id"]);
	            $passport->setPassportNumber($_POST["txtPassportNumber"]);
	            $passport->setPassportIssueDate($_POST["txtDateOfIssue"]);
	            $passport->setPassportExpiryDate($_POST["txtDateOfExpiry"]);
	            $passport->setPassportPlaceOfIssue($_POST["txtPlaceOfIssue"]);
	            $passport->setPassportCountryOfResidence($_POST["country_of_residance"]);
	            // Save Passport Details
	            $status = $passport->save();

	             if($status) {
			        $str = "New Passport Added successfully ";
			        s("New Passport Added successfully.");
			        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
			    }
			    else {
			        $str = "Failed to add new passport";
			        e("Failed to add new passport.");
			        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
			    }
	        }
	    
	   redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
}



$user = new User($_GET["id"]);

if(!$user || $user->getId() < 1) redirect("user.list.php");
if($user->getId() == 1) {
    e("Administrator account can not be edited!");
    redirect($_SERVER['HTTP_REFERER']);
    exit;
}



$passport = new Passport(0, $user->getId());

$passports = $passport->_passInfo;
$pcnt = count($passport->_passInfo);

//echo "<pre>";
//print_R($passport);
//die;

$country = getCountryList();


sm_assign("project", $project);
sm_assign("user", $user);
sm_assign("allUsers", $allUsers);
sm_assign("myRole", $myRole);
sm_assign("passports", $passports);
sm_assign("pcnt", $pcnt);
sm_assign("projectUsers", $projectUsers);
sm_assign("projectRoles", $projectRoles);
sm_assign("country", $country);
sm_display("passport_details.html");