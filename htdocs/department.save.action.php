<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.department.inc.php");
    $department = new Department($_POST["id"]);
    $department->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($department->delete()) {
            $str = "Department  <i><b>{$_POST['name']}</b></i> deleted.";
            s("Department  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete department.";
            e("Failed to delete department.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$department->checkName()) {
            if($department->save()) {
                $str = "Department  <i><b>{$department->getName()}</b></i> saved.";
                s("Department  <i><b>{$department->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save department {$_POST["name"]}";
                e("Failed to save department {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Department <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("department.list.php");
    }
