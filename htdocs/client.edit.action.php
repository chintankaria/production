<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.client.contacts.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");

    if($_POST['action'] == 'CONTACT_DELETE') {
        $code = 1;
        $msg = "Failed to delete client contact!";
        if($_POST['cid']) {
            $client = new Client($_POST["id"]);
            $contact = new ClientContact($_POST['cid']);
            if($contact->Delete()) {
                $code = 0;
                $msg = "Client contact deleted.";
                $str = "Client (<i>{$client->getName()}<i>) contact (<i>{$contact->getFirstName()} {$contact->getLastName()}</i>) deleted successfully.";
                watchdog('SUCCESS','EDIT',$str);
            }
            else {
                $str = "Failed to delete client (<i>{$client->getName()}<i>) contact (<i>{$contact->getFirstName()} {$contact->getLastName()}</i>).";
                watchdog('SUCCESS','EDIT',$str);
            }
        }
        $return['code'] = $code;
        $return['msg'] = $msg;
        print json_encode($return);
        exit;
    }
    else {
        $client = new Client($_POST["id"]);
        if($client->getId() < 1) redirect("client.list.php");

        $_POST["active"] = isset($_POST["active"]) ? $_POST["active"] : 0;
        $_POST["is_product_client"] = isset($_POST["is_product_client"]) ? $_POST["is_product_client"] : 0;

        $client->setName($_POST["name"]);
        $client->setDescription($_POST["description"]);
        $client->setWebsite($_POST["website"]);
        $client->setIsProductClient($_POST["is_product_client"]);
        $client->setIsActive($_POST["active"]);

        if($client->save()){
            foreach($_POST['contact_id'] as $k => $v) {
                if($k == 0) continue;
                
                $contact = new ClientContact($_POST['contact_id'][$k]);
                
                $_POST['is_primary'][$k] = isset($_POST['is_primary'][$k]) ? $_POST['is_primary'][$k] : 0;
                $_POST['is_active'][$k] = isset($_POST['is_active'][$k]) ? $_POST['is_active'][$k] : 0;
                
                $contact->setClientId($client->getId());
                $contact->setFirstName($_POST['first_name'][$k]);
                $contact->setLastName($_POST['last_name'][$k]);
                $contact->setEmailId($_POST['email'][$k]);
                $contact->setPhone($_POST['phone'][$k]);
                $contact->setJobTitle($_POST['job_title'][$k]);
                $contact->setIsPrimary($_POST['is_primary'][$k]);
                $contact->setIsActive($_POST['is_active'][$k]);

                $contact->save();
            }
            s("Client details saved successfully.");
            watchdog('SUCCESS','EDIT', "Client <i>{$client->getName()}<i> saved successfully.");
            redirect("client.view.php?id={$client->getId()}");
        }
        else{
            e("Failed to save client details.");
            watchdog('FAILED','EDIT', "Failed to save <i>{$client->getName()}<i> client details.");
            p($_POST);
            redirect("client.edit.php?id={$_POST["id"]}");
        }
    }
