<?php
    include_once("../config.php");
    include_once("../classes/class.pagination.inc.php");
    include_once("../classes/class.contactgroups.inc.php");
    include_once("../classes/class.contact.inc.php");
    $alpha_arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W',
                                'X','Y','Z');
    $group_list = ContactGroups::GetAll();

    $group_id = (isset($_GET["group"]) && intval($_GET['group']) > 0) ? intval($_GET["group"]) : 0;
    $client_id = (isset($_GET["client"]) && intval($_GET['client']) > 0) ? intval($_GET["client"]) : 0;
    $view = isset($_GET["view"]) ? trim($_GET["view"]) : "list";
    $total_pages = 0;
    $total_results = 0;
    $p = 0;
    if(!$_GET["excel"]){
        $p = (isset($_GET["p"]) && intval($_GET['p']) > 0) ? intval($_GET["p"]) : 1;
        $page_limit = 50;
    }

    $contacts = Contact::GetContacts($client_id, $group_id,$p,$page_limit);
    $clients = Contact::GetContactsByClients();
    $groups = Contact::GetContactsByGroups();
    $total_contacts = Contact::getTotalContacts();
    $con_alpha = Contact::GetContactsByAlpha();


    $user = u();
    $excel_no_field = 8;
    sm_assign("p", $p);
    sm_assign("client_id", $client_id);
    sm_assign("group_id", $group_id);
    sm_assign("group_list", $group_list);
    sm_assign("groups", $groups);
    sm_assign("contacts", $contacts);
    sm_assign("clients", $clients);
    sm_assign("total_contacts", $total_contacts);
    sm_assign("con_alpha", $con_alpha);
    sm_assign("alpha_arr", $alpha_arr);
    
    if($p > 0) {
        $total_pages = Contact::getTotalPages();
        $total_results = Contact::getTotalResults();
        $Paginate = new Pagination();
        $Paginate->setCurrentPage($p);
        $Paginate->setPageLimit($page_limit);
        $Paginate->setTotalPages($total_pages);
        $Paginate->setTotalResult($total_results);
        $Paginate->setPageNumberLimit(5);
        $Paginate->Paginate();
        $pages = $Paginate->getNumberStyleArray();
        $previous_page =$Paginate->getPreviousPage();
        $next_page = $Paginate->getNextPage();
        $current_page = $Paginate->getCurrentPage();

        sm_assign("total_results", $total_results);
        sm_assign("total_pages", $total_pages);
        sm_assign("pages", $pages);
        sm_assign("previous_page", $previous_page);
        sm_assign("next_page", $next_page);
        sm_assign("current_page", $current_page);
        sm_assign("page_end", @end($pages));
    }


    if($_GET["excel"]){
        $contacts = Contact::GetContacts();
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
		$excel->setHTMLTable(sm_fetch("table.contact.list.html"), "Contact List", null, array($excel_no_field));    	
    	$fileName = date("d-m-Y").'_contact_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }

    if($view == "list") {
    }
    sm_assign("view", $view);
    sm_display("contact.list.html");

?>
