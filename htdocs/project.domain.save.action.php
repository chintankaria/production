<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.project.domain.inc.php");
    $project_domain = new ProjectDomain($_POST["id"]);
    $project_domain->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($project_domain->delete()) {
            $str = "Project Domain <i><b>{$_POST['name']}</b></i> deleted.";
            s("Domain  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete project domain.";
            e("Failed to delete project domain.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$project_domain->checkName()) {
            $project_domain->setIsActive(1);
            if($project_domain->save()) {
                $str = "Project Domain  <i><b>{$project_domain->getName()}</b></i> saved.";
                s("Project  Domain  <i><b>{$project_domain->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save project domain {$_POST["name"]}";
                e("Failed to save project domain {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Project Domain <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("project.domain.list.php");
    }
