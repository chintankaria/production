<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.client.inc.php");

    $client = new Client($_GET["id"]);
    if($client->getId() < 1) redirect("client.list.php");

    sm_assign("client", $client);
    sm_display("client.delete.html");
