<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.license.inc.php");
    include_once("../classes/class.client.contacts.inc.php");
	
    if($_POST['action'] == 'RESEND-EMAIL')
    {
        if($_POST['license_type'] == 'Evaluation' && strtotime($_POST['license_end_date']) < time())
        {
            e("License expired.  Download Instructions of an active license can only be re-sent.");
        }
        else
        {
			if($_POST['valid_upto']) {
				$_POST['download_valid_upto'] = $_POST['valid_upto'];
                License::UpdateDownloadValidUpto($_POST["id"], $_POST['valid_upto']);
			}
			
            sm_assign("product_name", $_POST['product_name']);
            sm_assign("contact_name", $_POST['contact_name']);
            sm_assign("license_type", $_POST["license_type"]);
            sm_assign("download_key_validity", $_POST['download_valid_upto']);
            sm_assign("license_key", $_POST["license_key"]);
            sm_assign("license_start_date", $_POST["license_start_date"]);
            sm_assign("license_period", $_POST['license_period']);
            sm_assign("client_name", $_POST['client_name']);
			
			//sm_display("product.download.email.html");
			//die;
			
            $from = array('info@globalss.com' => 'GlobalsSoft Support');
            _::Mail($from,
                    array($_POST['contact_email'] => $_POST['contact_name']),
                    "Thank you for downloading " . $_POST['product_name'],
                    sm_fetch("product.download.email.html")
            );

            s("Download instructions emailed to <i>{$_POST['contact_email']}</i>.");
        }

        redirect("license.view.php?id={$_POST["id"]}");

    }
    elseif($_POST['action'] == 'SAVE')
    {
        $license = new License($_POST["id"]);
        if($license->getId() < 1) redirect("license.list.php");

        if( // Log License Edits.
            $license->getAgreementId() != $_POST['agreement_id'] ||
            $license->getClientId() != $_POST['client_id'] ||
            $license->getLocation() != $_POST['location']
          )
        {
            License::LogLicenseEdit($_POST["id"]);
        }

        $license->setAgreementId($_POST["agreement_id"]);
        $license->setClientId($_POST["client_id"]);
        $license->setLocation($_POST["location"]);
        $license->setTechnicalContactId($_POST["technical_contact_id"]);
        $license->setBusinessContactId($_POST["business_contact_id"]);
        $license->setNoticesContactId($_POST["notices_contact_id"]);
        $license->setNotes($_POST["notes"]);
		$license->setLicenseTypeId($_POST['license_type_id']);

        $error_flg = false;
        $file_size_limit = 5242880; // 5 MB
        $path = AppPath::$FILES . '/license/' . $license->getId() . '/';
        $dir_exists = false;
        if(!is_dir($path))
        {
            if(!@mkdir($path, 0755, true))
            {
                e("Unable to create directory: " . $path);
                $error_flg = true;
            }
            else
            {
                $dir_exists = true;
            }
        }
        else
        {
            $dir_exists = true;
        }

        if($dir_exists)
        {
            if($_FILES["sla_doc"]["error"] == 0)
            {
                if($_FILES["sla_doc"]["size"] > $file_size_limit)
                {
                    e("Please upload a file which is less than 5 MB.");
                    $error_flg = true;
                }
//Commented out as suggested by Chetan verbally on 24-08-2012
//                elseif (($_FILES["sla_doc"]["type"] == "application/msword") || ($_FILES["sla_doc"]["type"] == "application/pdf"))
                else
                {
                    if(!move_uploaded_file($_FILES["sla_doc"]["tmp_name"], $path . $_FILES["sla_doc"]["name"]))
                    {
                        e("Failed to upload file <i>{$_FILES["sla_doc"]["name"]}</i>.");
                        $error_flg = true;
                    }
                    else
                    {
                        $license->setSlaDoc($_FILES["sla_doc"]["name"]);
                    }
                }
            }
        }

        $sla_adj_path = $path. 'sla_adjs/'; // Uppend SLA Adjustment folder
 
        // For deleting the SLA Adjustment docs
        if(trim($_POST["sla_adj_del"]) != '') {
            $slaArr = explode(';;', trim($_POST["sla_adj_del"]));
            foreach($slaArr as $sla)
            {
                if(trim($sla) != '')
                {
                    $arr = explode('~~', trim($sla));
                    $id = $arr[0];
                    $filename = $arr[1];
                    @unlink($sla_adj_path . $filename);
                    License::DelSlaAdjustments($id);
                }
            }
        }

        // For saving the SLA Adjustment docs
        if(count($_FILES["sla_adj"]["error"]) > 0) {
            $t = !@mkdir($sla_adj_path, 0755, true);
            foreach ($_FILES["sla_adj"]["error"] as $key => $error) {
                if ($error == UPLOAD_ERR_OK) {
                    if($_FILES["sla_adj"]["size"][$key] > $file_size_limit)
                    {
                        e("Please upload a file which is less than 5 MB.");
                        $error_flg = true;
                    }
//Commented out as suggested by Chetan verbally on 24-08-2012
//                    elseif (($_FILES["sla_adj"]["type"][$key] == "application/msword") || ($_FILES["sla_adj"]["type"][$key] == "application/pdf"))
                    else
                    {
                        if(!move_uploaded_file($_FILES["sla_adj"]["tmp_name"][$key], $sla_adj_path . $_FILES["sla_adj"]["name"][$key]))
                        {
                            e("Failed to upload file <i>{$_FILES["sla_adj"]["name"][$key]}</i>.");
                            $error_flg = true;
                        }
                        else
                        {
                            License::SetSlaAdjustments($_POST["id"], $_FILES["sla_adj"]["name"][$key]);
                        }
                    }
                }
                else
                {
                    e("File error: " . $error);
                    $error_flg = true;
                }
            }
        }

        if(!$error_flg)
        {
            if($_POST['business_contact_id'] > 0) {
                $contact = new ClientContact($_POST['business_contact_id']);
                $contact->setJobTitle($_POST['brole']);
                if($contact->save()) {
                    $str = "Job Title of client contact (<i>{$contact->getFirstName()} {$contact->getLastName()}</i>) updated to (<i>{$_POST['brole']}</i>).";
                    watchdog('SUCCESS','EDIT',$str);
                }
            }

            if($_POST['technical_contact_id'] > 0) {
                $contact = new ClientContact($_POST['technical_contact_id']);
                $contact->setJobTitle($_POST['trole']);
                if($contact->save()) {
                    $str = "Job Title of client contact (<i>{$contact->getFirstName()} {$contact->getLastName()}</i>) updated to (<i>{$_POST['trole']}</i>).";
                    watchdog('SUCCESS','EDIT',$str);
                }
            }

            if($_POST['notices_contact_id'] > 0) {
                $contact = new ClientContact($_POST['notices_contact_id']);
                $contact->setJobTitle($_POST['nrole']);
                if($contact->save()) {
                    $str = "Job Title of client contact (<i>{$contact->getFirstName()} {$contact->getLastName()}</i>) updated to (<i>{$_POST['nrole']}</i>).";
                    watchdog('SUCCESS','EDIT',$str);
                }
            }

            if($_POST['valid_upto']) {
                License::UpdateDownloadValidUpto($_POST["id"], $_POST['valid_upto']);
            }

            if($license->save()){
                s("License details saved successfully.");
                watchdog('SUCCESS','EDIT', "License <i>{$license->getAgreementId()}<i> edited successfully.");
                redirect("license.view.php?id={$license->getId()}");
            }
            else{
                e("Failed to save license details.");
                watchdog('FAILED','EDIT', "Failed to edit license with SLA ID <i>{$license->getAgreementId()}<i>.");
                p($_POST);
                redirect("license.view.php?id={$_POST["id"]}");
            }
        }
    }
