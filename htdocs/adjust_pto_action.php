<?php
    include_once("../config.php");
    

    include_once("../classes/class.user.inc.php");
    include_once("../classes/class_pto_adjustment_inc.php");



    if($_POST['save_adjustment'] == "SAVE"){

        $user = new User($_GET["id"]);
        $employee = $user->getEmployee();
        $adj_date = $_GET["adj_date"];
        $adj_id = $_POST["adj_id"];

        $pto_adjustment = new PtoAdjustment($adj_id);
        
        $pto_adjustment->setUserID($_POST["id"]);
        $pto_adjustment->setAdjustedHours($_POST["adj_hours"]);
        $pto_adjustment->setAdjustedDate($_POST["adj_date"]);
        $pto_adjustment->setReason($_POST["adj_reason"]);
        $status = $pto_adjustment->saveAdjustment();

        if($status) {
                $str = "PTO information updated. ";
                s("PTO information updated.");
                watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
            }
            else {
                $str = "Failed to update pto information";
                e("Failed to update pto information.");
                watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
            }

            redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
        
    }




    
?>