<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.products.inc.php");
    include_once("../classes/class.licenseperiods.inc.php");
	include_once("../classes/class.licensetypes.inc.php");

    $clients = Client::GetProductClients();
    sm_assign("clients", $clients);

    $products = Products::GetAll();
    sm_assign("products", $products);

    $licenseperiods = LicensePeriods::GetAll();
    sm_assign("licenseperiods", $licenseperiods);
	
	$license_types = LicenseTypes::FindAll();
	sm_assign("license_types", $license_types);

    sm_assign("license_start_date", date("m/d/Y"));

    sm_display("license.add.html");
