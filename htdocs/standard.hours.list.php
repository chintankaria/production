<?php
    include_once("../config.php");
    require_perms("admin");
    $hours = getStandardHours();
    $hours['standard_hours_in_mins'] = m2hm($hours['standard_hours_in_mins']);
    sm_assign("hours", $hours); 
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("standard.hours.list.html");
        exit;
    }
    else {
        sm_display("standard.hours.list.html");
    }

?>
