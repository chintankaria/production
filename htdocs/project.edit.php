<?php
    include_once("../config.php");
    require_perms("manager","admin");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.category.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");

    $project = new Project($_GET["id"]);
    if(!$project->getId() || $project->getId() == 1000) redirect("project.list.php");
    //if(!$project->getId()) redirect("project.list.php");
    sm_assign("project", $project);

    $project_users = $project->getUsers();

    if($project->isSubproject()){
        $parent = $project->getParent();
        sm_assign("parent", $parent);
        $all_users = $parent->getUsers();
    }
    else{
        $all_users = User::GetUsers();
    }
    
    $users = array();
    foreach($all_users as $u){
        $role = Project::getUserRoleByProject($project->getId(),$u->getId());
        $users[] = array(
            "id" => $u->getId(),
            "name" => $u->getName(),
            "role" => $role['role'],
            "email" => $u->getEmail(),
            "checked" => $project_users[$u->getId()]
        );
    }
    
    sm_assign("users", $users);

    $client = $project->getClient();
    sm_assign("client", $client);

    if($project->isSubproject()){
        $parent_projects = $client->getProjects();
        sm_assign("parent_projects", $parent_projects);
    }

    $category = Category::GetAll('PROJECT_DOCUMENT');
    $files = TimesheetFiles::GetByProject($project->getId(),false,true);

    foreach($files as $k => $v) {
        $v->userFullName = User::getFullNameByEmailId($v->getCreatedBy());
        $v->userId = User::getUserIdByEmailId($v->getCreatedBy());
        $v->createdDate = date("m/d/Y h:i a",$v->getUploadDate());
        if($v->getFileHistory()) {
            foreach($v->getFileHistory() as $k1 => $v1) {
                $v1->userFullName = User::getFullNameByEmailId($v1->getCreatedBy());
                $v1->userId = User::getUserIdByEmailId($v1->getCreatedBy());
                $v1->createdDate = date("m/d/Y h:i a",$v1->getUploadDate());
            }
        }
    }

    sm_assign("files", $files);
    sm_assign("category", $category);

    sm_display("project.edit.html");
