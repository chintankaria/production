<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.products.inc.php");
	include_once("../classes/class.module.inc.php");

    /* AJAX check.  Below if block is used in populating product versions. */
    if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' && $_GET["ajax"] == 'yes') {
        $versions = Products::GetVersions($_GET['id']);

        $jsonArr = array();
        foreach($versions as $v)
        {
            $jsonArr[] = array('Id' => $v['id'], 'Version' => $v['version'], 'LicenseVersion' => $v['license_version'], 'Build' => $v['build'], 'Path' => $v['path']);
        }
        print_r(json_encode($jsonArr));
        die;
    }

    $products = Products::GetAll();
    sm_assign("products", $products);
	
	$modules = Module::GetAll();
	sm_assign("modules", $modules);
	
	$product_modules = array();
	foreach($products as $p) {
		$mlist = $p->getModules();
		$product_modules[$p->getId()]['modules'] = $mlist;
		
		$codes = array();
		foreach($mlist as $m) {
			$codes[] = $m->getCode();
		}
		
		$product_modules[$p->getId()]['module_codes'] = implode(', ', $codes);
	}
	
	sm_assign("product_modules", $product_modules);
	
	$product_modules_list = implode(", ", $product_modules);
	sm_assign("product_modules_list", $product_modules_list);

	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.products.list.html"), "Products list",null, array(3));
    	$fileName = date("d-m-Y").'_products.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("products.list.html");
        exit;
    }
    else {
        sm_display("products.list.html");
    }
