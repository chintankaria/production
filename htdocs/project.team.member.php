<?php

include_once("../config.php");
include_once("../classes/class.employee.project.role.php");
include_once("../classes/class.user.inc.php");
include_once("../classes/class.project.users.php");

$projectUsers = ProjectUsers::GetProjectUsers($_GET["id"]);
$project = new Project($_GET["id"]);
$allUsers = User::GetUsers();
$projectRoles = EmployeeProjectRole::GetEmployeeProjectRole();


$myRole= Project::getUserRoleByProject($project->getId(),u()->getId());



sm_assign("project", $project);
sm_assign("allUsers", $allUsers);
sm_assign("myRole", $myRole);
sm_assign("projectUsers", $projectUsers);
sm_assign("projectRoles", $projectRoles);
sm_display("project.team.member.html");