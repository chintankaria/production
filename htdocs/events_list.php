<?php

include_once("../config.php");
require_perms("admin");

require_once('../classes/class.licensetypes.inc.php');

$event_types = LicenseTypes::FindAll();

//echo "<pre>";
//print_r($event_types);

sm_assign("event_types", $event_types);
sm_display("events_list.html");