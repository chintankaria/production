<?php
    include_once("../config.php");
    require_perms("hr_manager","admin");

    $errors = false;
    function _set_error($e){
        global $errors;
        $errors = true;
        e($e);
    }

    $post = array(
        "first_name" => trim($_POST["first_name"]),
        "middle_name" => trim($_POST["middle_name"]),
        "last_name" => trim($_POST["last_name"]),
        "txtDOB" => trim($_POST["txtDOB"]),
        "rGender" => trim($_POST["rGender"]),
        "email" => is_email($_POST["email"]),
        "roles" => $_POST["roles"]
    );

    if(!$post["first_name"]) _set_error("Please enter first name.");
    if(!$post["last_name"]) _set_error("Please enter last name.");
    if(!$post["email"]) _set_error("Please enter a valid email address.");
	

    if($errors){
        p($post);
        redirect("user.register.php");
    }

    include_once("../classes/class.user.inc.php");
    $user = new User($post["email"]);
    if($user->getId()){
        e("Provided email address is not available.");
        p($post);
        redirect("user.register.php");
    }

    $password = gen_password();

    $user->setFirstName($post["first_name"]);
    $user->setMiddleName($post["middle_name"]);
    $user->setLastName($post["last_name"]);
    $user->setDateOfBirth($post["txtDOB"]);
    $user->setGender($post["rGender"]);
    $user->setEmail($post["email"]);
    $user->setPassword($password);
    $user->setRoles(implode(",",$_POST['roles']));
    $from = array("admin.timesheet@globalss.com" => "GSS Administrator");

    if($user->save()){
        unset($post["name"]);
        p($post);
        _::Mail($from,
            array($user->getEmail() => $user->getName()),
            "Your GlobalSoft Timesheet System account has been created.",
            sm_fetch("user.register.letter.html", array(
                "name" => $user->getName(),
                "email" => $user->getEmail(),
                "password" => $password
            ))
        );
        $str = "Welcome notification sent to a new member <i>{$user->getName()}({$user->getEmail()})</i>.";
        watchdog('SUCCESS','EMAIL', $str);

        $str = "Account for <i>{$user->getName()}({$user->getEmail()})</i> created.";
        watchdog('SUCCESS','ADD', $str);
        s("Account for {$user->getName()} created.");
        redirect("user.list.php");
    }
    else{
        e("Error occured while trying to create member. Please contact administrator.");
        $str = "Failed to create new user <i>{$post['email']}</i>";
        watchdog('FAILED','ADD', $str);
        p($post);
        redirect("user.register.php");
    }
    
