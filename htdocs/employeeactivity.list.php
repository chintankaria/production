<?php
include_once("../config.php");
require_perms("admin");

include_once("../classes/class.employeeactivity.inc.php");
$employees = EmployeeActivity::GetEmployees();
sm_assign("employees", $employees);

$employee_id = $_GET['employee_id'] ? $_GET['employee_id'] : 9999999;
sm_assign("employee_id", $employee_id);

$employee = EmployeeActivity::GetEmployees($employee_id);
sm_assign("employee_name", $employee[0]['name']);

$employee_activities = EmployeeActivity::GetEmployeeActivities($employee_id);
sm_assign("employee_activities", $employee_activities);

$employee_activity_history = array();
$i = 1;
foreach($employee_activities as $v)
{
    $employee_activity_history[$i++] = EmployeeActivity::GetEmployeeActivityHistory($v->getID());
}
sm_assign("employee_activity_history", $employee_activity_history);

sm_display("employeeactivity.list.html");

