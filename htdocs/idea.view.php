<?php
    include_once("../config.php");
    include_once("../classes/class.ideatracker.inc.php");
    include_once("../classes/class.ideacomments.inc.php");

    $idea = new IdeaTracker($_GET["id"]);
    if($idea->getId() < 1) redirect("ideatracker.list.php");
    
    $idea->create_date = date('D M j Y h:i A T',strtotime($idea->getCreateDate()));
    $idea->update_date = date('D M j Y h:i A T',strtotime($idea->getUpdateDate()));

    $idea_comments = new IdeaComments();
    $comments = $idea_comments->GetCommentList($_GET["id"]);

    if($comments) {
        foreach($comments as $key => $val) {
            $pClass = ($pClass == "comments-even") ? "comments-odd" : "comments-even";
            $val->date_time = date("d M y h:i a",strtotime($val->getDateTime()));
            $val->class = $pClass;
            if($val->getChildrens()) {
                foreach($val->getChildrens() as $k => $v) {
                    $cClass = ($cClass == "comment-reply-odd") ? "comment-reply-even" : "comment-reply-odd";
                    $v->date_time = date("d M y h:i a",strtotime($v->getDateTime()));
                    $v->class = $cClass;
                    if($v->getReplies()) {
                        foreach($v->getReplies() as $k1 => $v1) {
                            $rClass = ($rClass == "comment-reply-even") ? "comment-reply-odd" : "comment-reply-even";
                            $v1->date_time = date("d M y h:i a",strtotime($v1->getDateTime()));
                            $v1->class = "$rClass comment-children-child";
                        }
                    }
                }
            }
        }
    }
    sm_assign("idea", $idea);
    sm_assign("comments", $comments);
    sm_display("idea.view.html");
?>
