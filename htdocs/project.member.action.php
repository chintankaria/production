<?php

include_once("../config.php");
require_perms("admin");
include_once("../classes/class.project.users.php");
include_once("../classes/class.project.inc.php");

$objAddProjectUser = new ProjectUsers();

if ($_POST['project_user_id']) {
    $objAddProjectUser->setId($_POST['project_user_id']);
}

if (isset($_POST['action']) && $_POST['action'] == 'AddEdit') {
    
    $booleanError  = 0;
    $projectDetail = new Project($_POST["project_id"]);
   
    $projectStartDate = strtotime(date('Y-m-d',$projectDetail->getStartDate()));
    $projectEndDate =  strtotime(date('Y-m-d',$projectDetail->getEndDate()));
    
    $startDate = $_POST['start_date'] ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
    $endDate = $_POST['end_date'] ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
    
    $startDate1 = $_POST['start_date'] ?  strtotime($startDate) : '';
    $endDate1 = $_POST['end_date'] ?  strtotime($endDate) : '';
    
    if($startDate1 < $projectStartDate){
        $booleanError = 1;
        $_SESSION['project_setup_message'] = "Error : Employee start date should be greater than or equal to project start date.";
    }
    elseif($endDate1 > $projectEndDate){
        $booleanError =1;
        $_SESSION['project_setup_message'] = "Error : Employee end date should be lesser than or equal to project end date.";
    }
    
    if($booleanError){
        redirect("project.detail.php?id={$_POST['project_id']}");
        die;
    }
    
    
    $objAddProjectUser->setUserId($_POST['user_id']);
    $objAddProjectUser->setProjectId($_POST['project_id']);
    $objAddProjectUser->setProjectRoleId($_POST['project_role_id']);    
    $objAddProjectUser->setStartDate($startDate);    
    $objAddProjectUser->setEndDate($endDate);
    $objAddProjectUser->setBillingRate($_POST['billing_rate']);
    $objAddProjectUser->setAuthHours($_POST['auth_hours']);
    $objAddProjectUser->setPoNumber($_POST['po_number']);

    if ($objAddProjectUser->save()) {
        $_SESSION['project_setup_message'] = "Congratulation! Your record saved successfully";
    } else {
        $_SESSION['project_setup_message'] = "Error : Sorry, your record does not saved. Check duplicate entry";
    }
} else if ($_POST['action'] == 'Delete') {
    $objAddProjectUser->setId($_POST['project_member_id']);
    if ($objAddProjectUser->delete()) {
        $_SESSION['project_setup_message'] = "Congratulation! Your record deleted successfully";
    } else {
        $_SESSION['project_setup_message'] = "Error : Sorry, your record does not deleted.";
    }
}

redirect("project.detail.php?id={$_POST['project_id']}");