<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.contactgroups.inc.php");
    $groups = ContactGroups::GetAll();

    sm_assign("groups", $groups);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.contactgroups.list.html"), "Contact Groups list",null, array(3));
    	$fileName = date("d-m-Y").'_contactgroups.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("contactgroups.list.html");
        exit;
    }
    else {
        sm_display("contactgroups.list.html");
    }
