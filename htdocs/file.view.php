<?php

include_once("../config.php");
include_once("../classes/class.timesheetfile.inc.php");

$id = intval($_GET["id"]);
$file = new TimesheetFile($id);

ob_clean();
header("Content-Type: {$file->getMimeType()}");
header("Content-Length: {$file->getFileSize()}");
header("Content-Disposition: filename=\"{$file->getFileName()}\"");
// header("Content-Disposition: attachment; filename=\"{$file->getFileName()}\"");
readfile($file->getFilePath());
exit;
