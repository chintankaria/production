<?php
include_once("../config.php");
require_perms("admin");

include_once("../classes/class.employee.project.role.php");
$employee_role = EmployeeProjectRole::GetEmployeeProjectRole();

sm_assign("employee_role", $employee_role);

if ($_GET["excel"]) {
    include_once("../classes/class.excelwriter.inc.php");
    $excel = new ExcelWriter();
    $excel->setHTMLTable(sm_fetch("table.employee.project.role.list.html"), "Project Role", null, array(3));
    $fileName = date("d-m-Y") . '_project_role.xls';
    ob_clean();
    $excel->send($fileName);
    $excel->close();
    exit;
}
if ($_REQUEST['fetch'] == 1) {
    sm_assign("fetch", 1);
    print sm_fetch("employee.project.role.list.html");
    exit;
} else {
    sm_display("employee.project.role.list.html");
}
