<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.employee.location.inc.php");
    $employee_location = EmployeeLocation::GetAll();

    sm_assign("employee_location", $employee_location);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.employeelocation.list.html"), "Employee Location list",null, array(3));
    	$fileName = date("d-m-Y").'_employeelocation.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("employeelocation.list.html");
        exit;
    }
    else {
        sm_display("employeelocation.list.html");
    }
