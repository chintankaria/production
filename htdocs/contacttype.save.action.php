<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.contacttype.inc.php");
    $types = new ContactType($_POST["id"]);
    $types->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($types->delete()) s("Contact Type  <i><b>{$_POST['name']}</b></i> deleted.");
        else e("Failed to delete contact type..");
    }
    else {
        if(!$types->checkName()) {
            if($types->save()) s("Contact Type <i><b>{$types->getName()}</b></i> saved.");
            else e("Failed to save contact type {$_POST["name"]}");
        }
        else e("Contact Type <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("contacttype.list.php");
    }
