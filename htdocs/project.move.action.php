<?php
    include_once("../config.php");
    require_perms("manager","admin");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.project.inc.php");

    $old_client = new Client($_POST["old_client_id"]);
    $client_id = $old_client->getId();
    if($old_client->getId() < 1) redirect("project.list.php");

    $old_project = new Project($_POST["old_project_id"]);
    if($old_project->getId() < 1) {
        e("Invalid project!");
        redirect("project.list.php");
    }

    $new_client = new Client($_POST["sel_new_client"]);
    if($new_client->getId() < 1) {
        e("Select a client to move the project!");
        redirect("project.list.php");
    }
    
    // Move the project under another client
    $old_project->setClientId($new_client->getId());

    $parent_id = NULL;
    if(!$_POST['move_subproject'] && $_POST['sel_new_project'] > 0) {
        // Move the project to under targeted project (Subproject)
        $new_project = new Project($_POST['sel_new_project']);
        $parent_id = $new_project->getId();
    }
    $old_project->setParentId($parent_id);
    if($old_project->save()){
        if($_POST['move_subproject'] && $_POST['sel_new_project'] == 0) {
            // Move all subproject to targeted client/project 
            if($old_project->countSubprojects() > 0) {
                foreach($old_project->getSubprojects() as $subprj) {
                    $sub_project = new Project($subprj->getId());
                    $sub_project->setClientId($new_client->getId());
                    $sub_project->save();
                    $str = "<i>{$sub_project->getName()}</i> subproject is moved under <i>{$old_project->getName()}</i> project.";
                    watchdog('SUCCESS','EDIT', $str);
                }
            }
        }
        else {
            // Make all subproject to project
            if($old_project->countSubprojects() > 0) {
                foreach($old_project->getSubprojects() as $subprj) {
                    $sub_project = new Project($subprj->getId());
                    $sub_project->setParentId(0);
                    $sub_project->save();
                    $str = "<i>{$sub_project->getName()}</i> subproject is removed from the <i>{$old_project->getName()}</i> project and marked as project.";
                    watchdog('SUCCESS','EDIT', $str);
                }
            }
        }
        if($parent_id > 0) {
            s("Project <i>{$old_project->getName()}</i> moved under the <i>{$new_project->getName()}</i> project of <i>{$new_client->getName()}</i> client.");
            $str = "Project <i>{$old_project->getName()}</i> moved under the <i>{$new_project->getName()}</i> project of <i>{$new_client->getName()}</i> client.";
            watchdog('SUCCESS','EDIT', $str);
        } else {
            s("Project <i>{$old_project->getName()}</i> moved to <i>{$new_client->getName()}</i> client.");
            $str = "Project <i>{$old_project->getName()}</i> moved to <i>{$new_client->getName()}</i> client.";
            watchdog('SUCCESS','EDIT', $str);
        }
        redirect("project.list.php?client_id=".$client_id);
    }
    else {
        e("Failed to move project <i>{$old_project->getName()}</i>.");
        redirect("project.list.php?client_id=".$client_id);
   } 
?>
