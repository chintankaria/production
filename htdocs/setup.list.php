<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.employee.location.inc.php");
    $employee_location = EmployeeLocation::GetAll();
    

    sm_assign("employee_location", $employee_location);

    sm_display("setup.list.html");
?>
