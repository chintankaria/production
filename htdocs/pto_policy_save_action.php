<?php
    include_once("../config.php");


    require_perms("admin");

    include_once("../classes/class.pto.details.inc.php");
    include_once("../classes/class.pto.tiers.inc.php");


    //echo "<pre>";
    //print_r($_POST);die;


   if( $_POST['action'] == 'EDIT DATES'){

      $cnt = $_POST['polCnt'];

     $status = true;

     for($i=0; $i < $cnt; $i++){

        
        $pto_policy = new PtoPolicy($_POST['txtIDs'][$i], $_POST['loc_id']);
        $txtFromDate = $_POST['txtFromDate'][$i] ? date("Y-m-d", strtotime($_POST['txtFromDate'][$i])) : '';
        $txtToDate = $_POST['txtToDate'][$i] ? date("Y-m-d", strtotime($_POST['txtToDate'][$i])) : '';
        $maxHours = $_POST['txtMaxHours'][$i];

        $pto_policy->setMaxHours($maxHours);
        $pto_policy->setFromDate($txtFromDate);
        $pto_policy->setToDate($txtToDate);
        $pto_policy->setLocation($_POST['loc_id']);
        

        if($pto_policy->savePolicy()) 
        {

            $tcnt = $_POST['dtierCnt'];

            for($j=0; $j < $tcnt; $j++){

                $pto_tiers = new PtoTiers($_POST['txtTierIDs'][$j]);
                $pto_tiers->setTimePeriod($_POST['txtTier'][$j]);
                $pto_tiers->setLeaves($_POST['txtLeaves'][$j]);
                $pto_tiers->setMaxService($_POST['txtService'][$j]);
                $pto_tiers->setLocation($_POST['loc_id']);
                if($pto_tiers->save())
                    $status = true;
                else
                    $status = false;
            }
        }
        else
        {
            $status = false;
        }
     }    

    if($status) {
                $str = "Policy updated successfully ";
                s("Policy updated successfully.");
                watchdog('SUCCESS','EDIT', "$str");
            }
            else {
                $str = "Failed to update policy.";
                e("Failed to update policy tier.");
                watchdog('FAILED','EDIT', "$str");
            }
   }


       if($_POST['action'] == "DELETE TIER") {

        $status = true;

        $pto_tiers = new PtoTiers($_POST["hdn_tier_id"]);


        if($pto_tiers->delete()) {
            $str = "Policy tier deleted successfully.";
            s("Policy tier deleted successfully.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete tier.";
            e("Failed to delete policy tier.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    
    /* if($_POST['tierAction'] == 'SAVE TIERS'){

        $cnt = $_POST['tierCntNew'];
        $status = true;


         for($i=0; $i < $cnt; $i++){

            
            $pto_tiers = new PtoTiers();
            $pto_tiers->setPolID($_POST['pol_id']);
            $pto_tiers->setTimePeriod($_POST['txtTierNew'][$i]);
            $pto_tiers->setLeaves($_POST['txtLeavesNew'][$i]);
            $pto_tiers->setMaxService($_POST['txtServiceNew'][$i]);
            $pto_tiers->setLocation($_POST['loc_id']);
            if($pto_tiers->save())
                $status = true;
            else
                $status = false;

         }

    if($status) {
                $str = "Policy tiers added successfully ";
                s("Policy updated successfully.");
                watchdog('SUCCESS','ADD', "$str");
                redirect('setup.list.php');
            }
            else {
                $str = "Failed to update policy tiers.";
                e("Failed to update policy tier.");
                watchdog('FAILED','ADD', "$str");
            }

     }*/

      
     if($_POST['action'] == 'Save Policy'){

        $cnt = $_POST['polCnt'];
        $status = true;


        for($i=0; $i < $cnt; $i++){
        
         if($_POST['action'] == 'Save Policy')
            $pto_policy = new PtoPolicy(null, $_GET['loc_id']);
        else
             $pto_policy = new PtoPolicy($_POST['txtIDs'][$i], $_POST['loc_id']);


         $txtFromDate = $_POST['txtFromDate'][$i] ? date("Y-m-d", strtotime($_POST['txtFromDate'][$i])) : '';
         $txtToDate = $_POST['txtToDate'][$i] ? date("Y-m-d", strtotime($_POST['txtToDate'][$i])) : '';
         $maxHours = $_POST['txtMaxHours'][$i];

        $pto_policy->setMaxHours($maxHours);
        $pto_policy->setFromDate($txtFromDate);
        $pto_policy->setToDate($txtToDate);
        $pto_policy->setLocation($_POST['loc_id']);
        if($pto_policy->savePolicy()) 
        {
                    $cnt = $_POST['tierCount'];
                    $status = true;

                     for($i=0; $i < $cnt; $i++){
                        
                    $pto_tiers = new PtoTiers();
                    $pto_tiers->setPolID($_POST['pol_id']);
                    $pto_tiers->setTimePeriod($_POST['txtTierNew'][$i]);
                    $pto_tiers->setLeaves($_POST['txtLeavesNew'][$i]);
                    $pto_tiers->setMaxService($_POST['txtServiceNew'][$i]);
                    $pto_tiers->setLocation($_POST['loc_id']);

                    //print_R($pto_tiers);die;
                    if($pto_tiers->save())
                        $status = true;
                    else
                        $status = false;

                     }

        }
        else
        {
             $str = "Policy already exists for this date.";
             e("Policy already exists for this date.");
             watchdog('FAILED','EDIT', "$str");
             exit;
        }

     }

    if($status) {
            $str = "Policy added successfully ";
            s("Policy added successfully.");
            watchdog('SUCCESS','ADD', "$str");
        }
        else {
            $str = "Failed to add policy .";
            e("Failed to update policy tier.");
            watchdog('FAILED','ADD', "$str");
        }
}
    
    
    if($_POST['action'] == "DELETE") {

        $pto_policy = new PtoPolicy($_POST["id"],$_POST['loc_id']);


        if($pto_policy->delete()) {
            $str = "Policy deleted successfully.";
            s("Policy deleted successfully.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete policy.";
            e("Failed to delete policy tier.");
            watchdog('FAILED','DELETE', "$str");
        }

    }
    
    /*if($_POST['action'] == 'Save' || $_POST['action'] == 'EDIT'){

    $cnt = $_POST['tierCnt'];
    $status = true;


     for($i=0; $i < $cnt; $i++){

        if($_POST['action'] == 'Save')
            $pto_policy = new PtoPolicy(null, $_GET['loc_id']);
        else
            $pto_policy = new PtoPolicy($_POST['txtIDs'][$i], $_POST['loc_id']);

        $pto_policy->settime_period($_POST['txtTier'][$i]);
        $pto_policy->setleaves($_POST['txtLeaves'][$i]);
        $pto_policy->setMaxService($_POST['txtService'][$i]);
        $pto_policy->setLocation($_POST['loc_id']);
        if($pto_policy->save()) 
            $status = true;
        else
            $status = false;

     }

if($status) {
            $str = "Policy updated successfully ";
            s("Policy updated successfully.");
            watchdog('SUCCESS','ADD', "$str");
        }
        else {
            $str = "Failed to update policy tier.";
            e("Failed to update policy tier.");
            watchdog('FAILED','ADD', "$str");
        }
         
}*/
