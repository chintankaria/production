<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.training.skills.inc.php");
    $training_skills = TrainingSkills::GetAll();

    sm_assign("training_skills", $training_skills);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.training.skills.list.html"), "Training Skills",null, array(3));
    	$fileName = date("d-m-Y").'_training_skills.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("training.skills.list.html");
        exit;
    }
    else {
        sm_display("training.skills.list.html");
    }
