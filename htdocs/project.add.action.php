<?php
    include_once("../config.php");
    require_perms("admin","manager");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");
	
    $_POST["is_billable"] = isset($_POST["is_billable"]) ? $_POST["is_billable"] : 0;

    $project = new Project();
    $project->setName($_POST["name"]);
    $project->setStatus($_POST["status"]);
    $project->setShowInCalendar($_POST["show_in_calendar"]);
    $project->setDescription($_POST["description"]);
    $project->setClientId($_POST["client_id"]);
    $project->setParentId($_POST["parent_id"]);
    $project->setStartDate($_POST["start_date"]);
    $project->setEndDate($_POST["end_date"]);
    $project->setIsBillable($_POST["is_billable"]);
	$return_val = $project->compareDate($_POST["start_date"],$_POST["end_date"]);
	if($return_val){
		p($_POST);
	    e("Invalid project start date and end date ({$return_val})");
		if($project->getParentId()) $qs = "?parent_id={$project->getParentId()}";
        redirect("project.add.php{$qs}");
	}
    $project->setAllowSubprojects($_POST["allow_subprojects"]);
    $project->setAllowTaskTypes($_POST["allow_task_types"]);
    
    $params = array("users" => $_POST["users"],"manager" => $_POST["manager"]);
     
    if($project->save()){
        $project->setUsers($params);
        TimesheetFiles::SaveUploadedFiles($project->getClientId(), $project->getId());
        s("Project added successfully.");
        $str = "<i>{$_POST['name']}</i> project added successfully.";
        watchdog('SUCCESS','ADD', $str);
        
        if($_POST['referer'] == "calendar.view.php") {
            redirect("calendar.view.php");
        }
        else {
            redirect("project.view.php?id={$project->getId()}");
        }
    }
    else{
        e("Failed to add project.");
        $str = "Failed to add {$_POST['name']} project.";
        watchdog('FAILED','ADD', $str);
        
        p($_POST);
        if($project->getParentId()) $qs = "?parent_id={$project->getParentId()}";
        redirect("project.add.php{$qs}");
    }
