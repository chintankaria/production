<?php
include_once("../config.php");
include_once("../classes/class.ideatracker.inc.php");

    if(isset($_POST["action"]) && $_POST["action"] == "DELETE") {
        $idea = new IdeaTracker($_POST["id"]);
        if($idea->getId() < 1) redirect("ideatracker.list.php");

        if($idea->Delete()) {
            s("An Idea deleted successlfully.");
        }
        else {
            e("Failed to delete an idea");
        }
        redirect("ideatracker.list.php");
    }
    else if(isset($_POST["action"]) && $_POST["action"] == "DELETE_FILE") {
        $idea = new IdeaTracker($_POST["id"]);
        if($idea->getId() < 1) redirect("ideatracker.list.php");
        if($idea->DeleteAttachment($_POST['file_id'])) {
            s("An Idea attachment deleted successlfully.");
        }
        else {
            e("Failed to delete attachement.");
        }
        redirect("idea.edit.php?id={$idea->getId()}");
        
    }
    else {
        $idea = new IdeaTracker($_POST["id"]);
        $idea->setDateTime($_POST["txtDateTime"]);
        $idea->setTitle($_POST["txtTitle"]);
        $idea->setDescription($_POST["txtDescription"]);
        $idea->setStatus($_POST["txtStatus"]);
        if(intval($_POST["id"]) < 1) {
            $idea->setUserId(u()->getId());
        }
        $idea->setCreatedBy(u()->getEmail());
        $idea->setUpdatedBy(u()->getEmail()); 
        $idea->setFiles($_FILES);
        if(!$idea->checkTitle()) {
            if($idea->save()) {
                s("An Idea <i><b>".$_POST['txtTitle']."</b></i> saved successlfully.");
                redirect("ideatracker.list.php");
            } 
            else {
                e("Failed to add an idea.");
                p($_POST);
                redirect("ideatracker.list.php");
            }
        } 
        else {
            if($_POST["id"] > 0) {
                e("An Idea <i><b>{$_POST["txtTitle"]}</b></i> already exist!");
                redirect("idea.edit.php?id={$_POST["id"]}");
            }
            else {
                e("An Idea <i><b>{$_POST["txtTitle"]}</b></i> already exist!");
                p($_POST);
                redirect("idea.add.php");
            }
        }
    }
?>
