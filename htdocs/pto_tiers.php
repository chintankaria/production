<?php
include_once("../config.php");
require_perms("admin");

include_once("../classes/class.pto.details.inc.php");



$tier_detail = PtoPolicy::checkTiers($_GET['id']);
sm_assign("tier_detail", $tier_detail);
sm_assign("dtierCnt", count($tier_detail));
sm_assign("loc_id", $_GET['loc_id']);
sm_assign("pol_id", $_GET['id']);


if ($_REQUEST['fetch'] == 1) {
    sm_assign("fetch", 1);
    print sm_fetch("pto_tiers.html");
    exit;
} else {
    sm_display("pto_tiers.html");
}
