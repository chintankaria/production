<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.technology.practice.inc.php");
    $technology_practice = TechnologyPractice::GetAll();

    sm_assign("technology_practice", $technology_practice);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.technology.practice.list.html"), "Technology Practice",null, array(3));
    	$fileName = date("d-m-Y").'_technology_practice.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("technology.practice.list.html");
        exit;
    }
    else {
        sm_display("technology.practice.list.html");
    }
