<?php
    include_once("../config.php");
    
    include_once("../classes/class.user.inc.php");
    include_once("../classes/class.employees.inc.php");
    include_once("../classes/class.department.inc.php");
    include_once("../classes/class.designation.inc.php");
    include_once("../classes/class.product.knowledge.inc.php");
    include_once("../classes/class.technology.practice.inc.php");
    include_once("../classes/class.technology.skills.inc.php");
    include_once("../classes/class.training.skills.inc.php");
    include_once("../classes/class.employee.types.inc.php");
    include_once("../classes/class.employee.location.inc.php");

     if (trim($_POST['save_per_details']) == "Save"){

         $user = new User($_POST["id"]);

        if($user->getId() > 0){

            if(trim($_POST['txtUserName']) != $user->getEmail()) {
                if($user->getUserIdByEmailId(trim($_POST['txtUserName']))) {
                    // User Name already exist
                    e("<b>{$_POST['txtUserName']}</b> member already exist.");
                    redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
                }
                $user->setEmail(trim($_POST["txtUserName"]));
            }

            $user->setDateOfBirth(trim($_POST["txtDOB"]));
            $status = $user->save();

            if($status) {


            $employee = new Employee(0, $user->getId());
            $employee->setUserId($user->getId());

            if($_POST['txtPersonalEmail'] == '')
                $_POST['txtPersonalEmail'] = $_POST['txtUserName'];

            $employee->setPersonalEmail($_POST["txtPersonalEmail"]);

            $employee->setAddress($_POST["txtAddress"]);
            $employee->setCity($_POST["txtCity"]);
            $employee->setZip($_POST["txtZip"]);
            $employee->setState($_POST["txtState"]);
            $employee->setCountryId($_POST["country_id"]);
            $employee->setPhone1($_POST["txtPhone1"]);
            $employee->setEmergencyCnt($_POST["txtEmergencyCnt"]);
            $employee->setRelationship(trim($_POST["txtRelationship"]));
            $employee->setNationality(trim($_POST["txtNationality"]));
            
            // Save Employee Details
            $employee->save();

            }

        }

        else $status = false;
            if($status) {
                $str = "Member information updated ";
                s("Member information updated.");
                watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
            }
            else {
                $str = "Failed to update member information";
                e("Failed to update member information.");
                watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");
            }

            redirect("user.detail.php?id={$_POST["id"]}&utabId={$_POST['utabId']}");


     }   

    $user = new User($_GET["id"]);
    if(!$user || $user->getId() < 1) redirect("user.list.php");
    if($user->getId() == 1) {
        e("Administrator account can not be edited!");
        redirect($_SERVER['HTTP_REFERER']);
        exit;
    }
    $isBusinessManager = in_array("business_manager" ,u()->getRoles());



    $employee = $user->getEmployee();
    $departments = Department::GetAll();    
    $designations = Designation::GetAll();    
    $prod_knowledge = ProductKnowledge::GetAll();
    $tech_practice = TechnologyPractice::GetAll();
    $tech_skills = TechnologySkills::GetAll();
    $training_skills = TrainingSkills::GetAll();
    $employee_types = EmployeeTypes::GetAll();
    $employee_location = EmployeeLocation::GetAll();

    $country = getCountryList();


    sm_assign("tab", $tab);
    sm_assign("trainer_style", $trainer_style);
    sm_assign("country", $country);
    sm_assign("departments", $departments);
    sm_assign("designations", $designations);
    sm_assign("prod_knowledge",$prod_knowledge);
    sm_assign("tech_practice", $tech_practice);
    sm_assign("tech_skills", $tech_skills);
    sm_assign("training_skills", $training_skills);
    sm_assign("employee_location", $employee_location);
    sm_assign("employee_types", $employee_types);
    sm_assign("employee", $employee);
    sm_assign("isBusinessManager", $isBusinessManager);
    sm_assign("user", $user);
    sm_display("personal_details.html");
