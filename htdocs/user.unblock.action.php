<?php
    include_once("../config.php");
    require_perms("hr_manager","admin");
    if($_POST["answer"] != "yes") redirect("user.list.php");

    include_once("../class.user.inc.php");

    $user = new User($_POST["id"]);
    if($user && $user->getId() > 0) $user->setBlocked(false);

    redirect("user.list.php");
