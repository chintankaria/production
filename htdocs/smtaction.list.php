<?php
    include_once("../config.php");
    require_perms("smt_member","admin");
    include_once("../classes/class.smtaction.inc.php");
    include_once("../classes/class.smtaction.comments.inc.php");

    $action_status = array("Active", "Pending", "On Hold", "Completed", "Closed");

    $from_date = (isset($_GET["from_date"]) && is_date($_GET["from_date"])) ? $_GET["from_date"] : "1/1/".date("Y");
    $to_date = (isset($_GET["to_date"]) && is_date($_GET["to_date"])) ? $_GET["to_date"] : date("m/d/Y",strtotime("tomorrow"));
    //$status = (isset($_GET["status"]) && in_array($_GET['status'], $action_status)) ? $_GET['status'] : "-- All Action Items --";
   
    $blnAll=false; 
    if(strpos($_GET['status'], "|") > 0) {
        $tmp = explode("|",$_GET['status']);
        foreach($tmp as $val){
            if($val != '') 
                $status[] = $val;
        }
    }
    else {
        if($_GET['status'] == "") $_GET['status'] = 'ALL';
        $status = array(strtoupper($_GET['status']));
        if(strtoupper($_GET['status']) == 'ALL')
            $blnAll=true;
    }
    $excel = (isset($_GET['excel']) ? $_GET['excel'] : 0);
    sm_assign("excel", $excel);    
    $publish = (isset($_GET['publish']) ? $_GET['publish'] : 0);
    sm_assign("publish", $publish);    

    $user = u();
    $items = SMTAction::GetActionItems($from_date,$to_date, $status);
    $excel_no_field = 8;
    sm_assign("blnAll", $blnAll);
    sm_assign("user", $user);
    sm_assign("from_date", $from_date);
    sm_assign("to_date", $to_date);
    sm_assign("action_items",$items);
    sm_assign("status",$status);
    sm_assign("status_str",implode("|",$status));
    sm_assign("action_status", $action_status);

    if($excel){
        if(!is_dir(AppPath::$FILES."/excel")) {
            @mkdir(AppPath::$FILES."/excel", 0777);
        }
	    include_once("../classes/class.excelwriter.inc.php");
    	$fileName = AppPath::$FILES."/excel/".date("d-m-Y").'_actionitems_list.xls';
    	$excel = new ExcelWriter($fileName);
		$excel->setHTMLTable(sm_fetch("table.smtaction.list.html"), "Action Items List", null, null);    	
        if($publish) {
    	    $excel->close();
            $users = User::GetUsers();
            $from = array("admin.timesheet@globalss.com" => "GSS Administrator");
            sm_assign("type" ,"PUBLISH");
            foreach($users as $o_user) {
                if ($o_user->isSMTMember()) {
                    $attendees = array();
                    $attendees[$o_user->getEmail()] = $o_user->getFullName();
                    sm_assign("full_name", $o_user->getFullName());
                    _::Mail($from,$attendees,"SMT Meeting - Action Item", sm_fetch("smtaction.letter.html"), $fileName);
                    $str = "Published action items to attendee {$o_user->getFullName()}({$o_user->getEmail()}).";
                    watchdog("SUCCESS", "EMAIL", $str);
                }
            }
    	    $excel->close();
            s("Published action items successlfully sent to attendess.");
            redirect($_SERVER['HTTP_REFERER']);
        }
        else {
    	    ob_clean();
        	$excel->send($fileName);
    	    $excel->close();
        	exit;
        }
    }
    sm_display("smtaction.list.html");
?>
