<?php
    include_once("../config.php");
    require_perms("hr_manager","admin");
    $isBusinessManager = in_array("business_manager" ,u()->getRoles());
    sm_assign("isBusinessManager", $isBusinessManager);
    sm_display("user.register.html");
