<?php
    include_once("../config.php");
    include_once("../classes/class.contactgroups.inc.php");
    include_once("../classes/class.communicationtype.inc.php");
    include_once("../classes/class.contacttype.inc.php");
    $groups = ContactGroups::GetAll();
    $country = getCountryList();
    $comm_types = CommunicationType::GetAll();
    $contact_types = ContactType::GetAll();

    sm_assign("comm_types", $comm_types);
    sm_assign("contact_types", $contact_types);
    sm_assign("groups", $groups);
    sm_assign("country", $country);
    sm_display("contact.add.html");
?>
