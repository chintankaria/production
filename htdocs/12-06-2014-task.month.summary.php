<?php
    include_once("../config.php");
    require_perms("admin");        
    
    $date = ymd(is_date($_GET["date"], true));
    $prev_month = date("m/01/Y", strtotime("{$date} -1 month"));
    $next_month = date("m/01/Y", strtotime("{$date} +1 month"));
    
    $days = get_dates_of_month($date, false, false);    
    $to = date('Y-m-t', strtotime($date));
    $from = date('Y-m-01', strtotime($date));
   
    $sql = "SELECT c.id as client_id, c.name as client_name, p.id as project_id, p.name as project_name, u.id as user_id, CONCAT_WS(' ', u.first_name, u.last_name) AS user_name
            FROM tasks AS t
            INNER JOIN users u ON u.id = t.user_id
            INNER JOIN projects p ON p.id = t.project_id
            INNER JOIN clients c ON c.id = p.client_id
            LEFT JOIN task_types tt ON tt.id = t.task_type_id
            WHERE  t.task_date BETWEEN '{$from}' AND '{$to}' 
            AND NOT t.deleted
            AND p.id > 0
            AND p.id != 1000
            Order by client_name, project_name, user_name";  
            
    $records = array();
    foreach(db_get_all($sql) as $rec){
        $key = $rec['project_id'].'-'.$rec['user_id'];
        $records[$key]['client_name'] = $rec['client_name'];
        $records[$key]['project_name'] = $rec['project_name'];
        $records[$key]['user_name'] = $rec['user_name'];
    }       
   
    $sqlTask = "SELECT id, user_id, project_id, task_type_id, UNIX_TIMESTAMP(task_date) AS task_date_ts, deleted, duration_mins  FROM tasks 
                WHERE task_date BETWEEN '{$from}' AND '{$to}'                
                AND NOT deleted AND project_id != 1000";
                
    foreach(db_get_all($sqlTask) as $task){
        $key = $task['project_id'].'-'.$task['user_id'];
        $roundtime = strtotime(date('Y-m-d', $task['task_date_ts']));
        $records[$key][$roundtime] = $task['duration_mins'];                 
    }
    
    //echo '<pre>'; print_r($records); echo '</pre>';
     
    $tableData = array();   
    $totalWorkHrs = 0;
    foreach($records as $key=>$record){
        $tempArray = array();
        $tempArray[] = $record['client_name'];
        $tempArray[] = $record['project_name'];
        $tempArray[] = $record['user_name'];
        $wrkHrs = 0;
        foreach($days as $day){            
           if(array_key_exists($day, $record)){
               $wrkHrs += $record[$day];//date('G.i',$record[$day]*60);
               $tempArray[] = $record[$day]/60 .'.'.$record[$day]%60;//date('G:i',$record[$day]*60);
               $totalWorkHrs +=$record[$day];
           }
           else{
               $tempArray[] = '';
           }
        }
        
        $tempArray[] = $wrkHrs/60 .'.'.$wrkHrs%60;//number_format($wrkHrs,2,".",".");
        $tableData[] = $tempArray; 
    }
    $tableHeading = array();
    $tableHeading[] = array('title'=> 'Client', 'width' => 150, 'dataType'=> 'string');
    $tableHeading[] = array('title'=> 'Project', 'width' => 150, 'dataType'=> 'string');
    $tableHeading[] = array('title'=> 'Consultant', 'width' => 150, 'dataType'=> 'string');
    
    $exclFirstHeading = array();
    $exclSecondHeading = array();
    
    $exclFirstHeading[] = array('title'=> '');
    $exclFirstHeading[] = array('title'=> '');
    $exclFirstHeading[] = array('title'=> '');
    
    $exclSecondHeading[] = array('title'=> 'Client');
    $exclSecondHeading[] = array('title'=> 'Project');
    $exclSecondHeading[] = array('title'=> 'Consultant');
    
    $lastRow = array();
    $lastRow[] = '';$lastRow[] = '';$lastRow[] = '';
    
    foreach($days as $day){
        $tableHeading[] = array('title'=> substr(date('l', $day),0,1).'<br/>'.date('j', $day), 'width' => 5, 'align' => 'center', 'dataType'=> 'string');
        $exclFirstHeading[] = array('title'=> substr(date('l', $day),0,1));
        $exclSecondHeading[] = array('title'=> date('j', $day));
        $lastRow[] = '';
    }
    $lastRow[] =  $totalWorkHrs/60 .'.'.$totalWorkHrs%60;
    $tableData[] = $lastRow;
    $tableHeading[] = array('title'=> 'Total <br/> Hours', 'width' => 100, 'dataType'=> 'string');    
    
    $exclFirstHeading[] = array('title'=> 'Total'); 
    $exclFirstHeading[] = array('title'=> 'Billable'); 
    $exclSecondHeading[] = array('title'=> 'Hours');
    $exclSecondHeading[] = array('title'=> 'Amount');
    
    sm_assign("tableHeading", json_encode($tableHeading)); 
    sm_assign("tableData", json_encode($tableData));     
    sm_assign("days", $days);
    sm_assign("date", date("F", strtotime($date)));
    sm_assign("prev_month", $prev_month);
    sm_assign("next_month", $next_month);
    
    if($_GET["excel-9876"]){
	sm_assign("exclFirstHeading", json_encode($exclFirstHeading));
        sm_assign("exclSecondHeading", json_encode($exclSecondHeading));
        include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.month.summary.excel.html"), "Monthly Summary of Time Worked", null);
    	$fileName = date("d-m-Y").'_monthy_summary_of_time_worked.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close(); 
    	exit;
    } 
    elseif($_GET["excel"]){
        require_once 'Spreadsheet/Excel/Writer.php';
        
        $workbook = new Spreadsheet_Excel_Writer(); 
        
        $format_center =& $workbook->addFormat();
        $format_center->setAlign('center');
        
        $format_title =& $workbook->addFormat();
        $format_title->setBold();
        $format_title->setColor('white');
        $format_title->setPattern(1);
        $format_title->setFgColor('black');        
        $format_title->setAlign('center');        

        $worksheet =& $workbook->addWorksheet();
        
        $i=0;
        foreach($exclFirstHeading as $key => $val){
            $worksheet->write(1, $i, $val['title'],$format_center);
            $i++;
        }
        
        $i=0;
        foreach($exclSecondHeading as $key => $val){
            $worksheet->write(2, $i, $val['title'], $format_title);
            $i++;
        }
        
        $i=3; 
        foreach($tableData as $outer){
            $j=0;
            foreach($outer as $val){
                $worksheet->write($i, $j, $val);
                $j++;
            }
            $i++;
        }
        $fileName = date("d-m-Y").'_monthy_summary_of_time_worked.xls';
        $workbook->send($fileName);
        $workbook->close();
    }
    
    sm_display("task.month.summary.html");        