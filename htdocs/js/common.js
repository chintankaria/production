   
jQuery(function($){
    $("a.linkbutton").each(function(){
        $this = $(this);
        text = $this.text();
        var href = $this.attr("href");
        var vText = $this.attr("vText");
        if(vText == 'Cancel') 
            $this.replaceWith($("<input type='button' class='buttonCancel' value='" + text + "'>").click(function(){ window.location.href = href; }));
        else
            $this.replaceWith($("<input type='button' class='buttonNew' value='" + text + "'>").click(function(){ window.location.href = href; }));
    });

    $("table[highlight=on] tr").hover(
        function(){ $("td", this).addClass("highlight"); },
        function(){ $("td", this).removeClass("highlight"); }
    );

    /*
    $("table[highlight=on]")
    .listen("mouseover", "td", function(){
        $(this).siblings().andSelf().addClass("highlight");
    })
    .listen("mouseout", "td", function(){
        $(this).siblings().andSelf().removeClass("highlight");
    });
    */


    $("input[type=button], input[type=submit]").each(function() {
        if($(this).attr("vText") == "Cancel") {
            $(this).addClass("buttonCancel");
        } else {
            if(this.id != "btnTimeCardSubmit")
                $(this).addClass("buttonNew");
        }
    });
    $("input[type=text], input[type=password], textarea, select").addClass("ui-widget-content ui-corner-all");
    //$("table:not(.sortable) th").addClass("ui-widget-header");
    //$("th").addClass("ui-widget-header");

    var $sort_icons = $("<span class='asc ui-icon-carat-1-n'></span><span class='desc ui-icon-carat-1-s'></span><span class='sort ui-icon-carat-2-n-s'></span>");
    $sort_icons.addClass("ui-icon").css({float: "right"});
    $("table.sortable").each(function(){
        var $table = $(this);
        $("th", $table).each(function(){
            var $th = $(this);
            if($th.metadata().sorter != false){
                $th.append($sort_icons.clone());
            }
        });
        $table.tablesorter();
    });

    $('div.inlineblockgroup').each(function(){
        var $div = $(this);
        var max_w = 0;
        $('span.inlineblock', $div).each(function(){
            var $span = $(this);
            if($span.width() > max_w) max_w = $span.width();
        });

        $('span.inlineblock', $div).width(max_w);
    });
});
  
function usercheck(str_id,str_val){
   if(document.getElementById(str_id+str_val).checked){
     document.getElementById(str_id+str_val).checked = false;  
   } 
     
}
function onlyNumbers(event) {
    if ( event.keyCode == 46 || event.keyCode == 8 ) { 
    } else { 
        if (event.keyCode < 95) { 
            if (event.keyCode < 48 || event.keyCode > 57 ) { 
                event.preventDefault(); 
            } 
        } else { 
            if (event.keyCode < 96 || event.keyCode > 105 ) { 
                event.preventDefault(); 
            } 
        } 
    } 
}
function checkEmail(id, inputvalue, title){    
    var blnValid = true;
    if(title == '') {
        title = 'User Profile';
    }
    if(inputvalue != "") {
        var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
        if(pattern.test(inputvalue)){         
           blnValid = true; 
        }else{   
            jAlert("Invalid email address",title); 
            $("#"+id).focus();
            blnValid = false;
        }
    }
    return blnValid;
}
function minutes2Time(mins){
    var hours = Math.floor(mins / 60);

    var minutes = mins % 60;
    if(hours < 1)
        hours =0;
    if(minutes < 1) 
        minutes = 0;
    var obj = {
        "hour": hours,
        "min": minutes
    };
    return obj;
}
 function GetQueryStringParams(name)
        {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
function seconds2Time(secs){
    var hours = Math.floor(secs / (60 * 60));

    var divisor_for_minutes = secs % (60 * 60);
    var minutes = Math.floor(divisor_for_minutes / 60);

    var divisor_for_seconds = divisor_for_minutes % 60;
    var seconds = Math.ceil(divisor_for_seconds);
        
    if(hours < 1)
        hours =0;
    if(minutes < 1) 
        minutes = 0;
    if(seconds < 1) 
        seconds = 0;
    var obj = {
        "hour": hours,
        "min": minutes,
        "sec": seconds
    };
    return obj;
}
function roundNumber(rnum, rlength) {
    return Math.round(rnum*Math.pow(10,rlength))/Math.pow(10,rlength);
}
function onlyTime(evt,id) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\:|[0-9]/;
    if ( theEvent.keyCode == 46 || theEvent.keyCode == 8 ) {
         // backspace & delete
    }
    else {
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            theEvent.preventDefault();
        }
    }
}