jQuery(function($){
    $("input.datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat:'dd-M-yy',
        onSelect: function(d){
            target = $(this).attr("target");
            if(target) window.location.href = target + "?date=" + d+"&user_id="+$("select[name=user_id]").val();
        }
    });
});
