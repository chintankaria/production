jQuery(function($){
    $("#filtersubmit").remove();

    /*
    $("#sel_client").change(function(){ $("#sel_project, #sel_subproject").remove(); });
    $("#sel_project").change(function(){ $("#sel_subproject").remove(); });

    $("#sel_client, #sel_project").change(function(){
        window.location.href = this.form.action + "?" + $("#sel_client, #sel_project").serialize();
    });
    */

    var $frm_task = $("#frm_task");
    if($("input[name='project_id']", $frm_task).val() < 1) $("#sel_subproject").hide();
    else if($("#sel_subproject option").not(":first").length < 1) $("#sel_subproject").hide();
    if($("input[name='client_id']", $frm_task).val() < 1) $("#sel_project").hide();

    $("#sel_client").change(function(){
        $this = $(this);
        $("input[name='client_id']", $frm_task).val($this.val());
        $("#sel_project, #sel_subproject").each(function(){
            $(this).hide().find("option").not(":first").remove();
        });
        if($this.val() > 0){
            $.getJSON(
                "ajax.project.list.php",
                { client_id : $this.val() },
                function(data){
                    if(data.length){
                        $.each(data, function(){
                            $("#sel_project").show().append('<option value="' + this.id + '">' + this.name + '</option>');
                        });
                    }
                }
            );
        }
    });

    $("#sel_project").change(function(){
        $this = $(this);
        $("input[name='project_id']", $frm_task).val($this.val());
        $("#sel_subproject").hide().find("option").not(":first").remove();
        if($this.val() > 0){
            $.getJSON(
                "ajax.project.list.php",
                { parent_id : $this.val() },
                function(data){
                    if(data.length){
                        $.each(data, function(){
                            $("#sel_subproject").show().append('<option value="' + this.id + '">' + this.name + '</option>');
                        });
                    }
                }
            );
        }
    });

    $("#sel_subproject").change(function(){
        $this = $(this);
        $("input[name='subproject_id']", $frm_task).val($this.val());
    });

    $("#frm_task").submit(function(){
        if($("input[name=project_id]", this).val() < 1){
            alert("Please select a project.");
            return false;
        }
    });
});
