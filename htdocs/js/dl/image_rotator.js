$(document).ready(function(){
	
        var $ul = $("#sidebar-left ul.menu");

        $.each($ul, function(i, v) {
            var $menu = $(v);
            var l = $menu.parents("ul.menu").length;
            $menu.addClass("level-" + l);
            $menu.attr("id","level-" + l);
        });

		var txts=$("#sidebar-left ul.menu li.active-trail a.active").text();
		//var $me = $("#sidebar-left ul.menu li.active-trail a.active").html('<div id="lselected">'+txts+'</div>');

		$("#sidebar-left ul.menu li.active-trail a.active").parents("li.active-trail").each(function(i, v) {
            var $li = $(v);
            var $p = $li.parent("ul.menu");

            if($p.hasClass("level-0")) {
                //$li.html("0--" + $li.html());
                $li.children("a").html('<div id="lselected">'+$li.children("a").text()+'</div>'); 
            } else if($p.hasClass("level-1")) {
                //$li.html("1--" + $li.html());
                if($li.children("a").hasClass("active")) {
                    $li.children("a").html('<span class="lselected-level-1 active-menu-span">'+$li.children("a").text()+'</span>'); 
                }
                else {
                    $li.children("a").html('<span class="lselected-level-1">'+$li.children("a").text()+'</span>'); 
                }
            } else if($p.hasClass("level-2")) {
                //$li.html("2--" + $li.html());
                if($li.children("a").hasClass("active")) {
                    $li.children("a").html('<span class="lselected-level-2 active-menu-span">'+$li.children("a").text()+'</span>'); 
                }
                else {
                    $li.children("a").html('<span class="lselected-level-2">'+$li.children("a").text()+'</span>'); 
                }
            }
            
        });
        

		setTimeout("changeHome('0')","0");
		//$("div#sidebar-left-inner div.block-menu ").before("<div style='margin-top: 2em;'></div>");
		var baseurl = $("#baseurl").val();
		var userid = $("#userid").val();

        /*
		$("#Map1").click(function(){
			window.location.href=baseurl+"/clients-global2000";
		});
		
		$("#Map2").click(function(){
			window.location.href=baseurl+"/clients-new-ventures";
		});
		
		$("#Map3").click(function(){
			window.location.href=baseurl+"/clients-software-vendors";
		});
		
		$("#left-link1").click(function(){
			window.location.href=baseurl+"/services/consulting";
		});
		
		$("#left-link2").click(function(){
			window.location.href=baseurl+"/services/engineering";
		});
		
		$("#left-link3").click(function(){
			window.location.href=baseurl+"/services/process";
		});
		
		$("#main-link1").click(function(){
			window.location.href=baseurl+"/clients-global2000";
		});
		
		$("#main-link2").click(function(){
			window.location.href=baseurl+"/clients-new-ventures";
		});
		
		$("#main-link3").click(function(){
			window.location.href=baseurl+"/clients-software-vendors";
		});
		
		$("#more1").click(function(){
			window.location.href=baseurl+"/clients-global2000";
		});
		
		$("#more2").click(function(){
			window.location.href=baseurl+"/clients-new-ventures";
		});
		
		$("#more3").click(function(){
			window.location.href=baseurl+"/clients-software-vendors";
		});
		
		$("#terms").click(function(){
			window.location.href=baseurl+"/terms-of-use";
		});
		
		$("#privacy").click(function(){
			window.location.href=baseurl+"/privacy-policy";
		});		
        */
		
/*		if(userid == 1)
		{
			$("#sidebar-left").css("width","190px");
//			$("#content-area").css("float","right");
		}*/
		
		var services = $("#block-menu-menu-services").length;
		//var about = $("#block-menu-menu-about-global-soft").length;
		var services2 = $("#block-menu-menu-ervices2").length;
		var solutions = $("#block-menu-menu-solutions").length;
		var clients = $("#block-menu-menu-menu-clients").length;
		
        /*
		if(about == 0 && services == 0 && services2 == 0 && $("#header-blocks ul.menu li").eq(1).attr("class") != "leaf active-trail")
		{
			$("#header-blocks ul.menu li").eq(3).addClass("leaf active-trail");
		}
        */
		/*
		if(about == 1)
		{
					$("#header-blocks ul.menu li").eq(0).addClass("leaf active-trail");
		}
		*/
		if(services == 1)
		{
					$("#header-blocks ul.menu li").eq(3).addClass("leaf active-trail");
		}
		
		if(solutions == 1)
		{
					$("#header-blocks ul.menu li").eq(2).addClass("leaf active-trail");
		}
		if(clients == 1)
		{
					$("#header-blocks ul.menu li").eq(1).addClass("leaf active-trail");
		}
		if(services2 == 1)
		{
					$("#header-blocks ul.menu li").eq(1).addClass("leaf active-trail");
		}
		
		$("#consulting").mouseover(function(){
			$("#consulting").attr("src","/misc/images/home-consulting-services-box-over.jpg");
		});
		$("#consulting").mouseout(function(){
			$("#consulting").attr("src","/misc/images/home-consulting-services-box.jpg");
		});
		
		$("#engineering").mouseover(function(){
			$("#engineering").attr("src","/misc/images/home-engineering-services-box-over.jpg");
		});
		$("#engineering").mouseout(function(){
			$("#engineering").attr("src","/misc/images/home-engineering-services-box.jpg");
		});
		
		$("#process").mouseover(function(){
			$("#process").attr("src","/misc/images/home-process-services-box-over.jpg");
		});
		$("#process").mouseout(function(){
			$("#process").attr("src","/misc/images/home-process-services-box.jpg");
		});

});
function changeHome(j)
{
	var baseurl = $("#baseurl").val();
	if(j==0)
	{
		$("#main-img-div").animate({"opacity": "show"	}, { "duration": "6500", "easing": "easein" }, function() {
		    // Animation complete.
		  });

		$("#main-image").attr("src",baseurl+"/misc/images/ShinningGlobe_450x225.png");	
		$("#main-menu-bar").attr("src",baseurl+"/misc/images/home-globalsoft-slide-bar.gif");
		$("#image0").fadeIn(3000);
		$("#image1").css("display", "none");
		$("#image2").css("display", "none");
		$("#image3").css("display", "none");
		$("#image4").css("display", "none");
		setTimeout("changeHome('1')","6500");
	}
	else if(j==1)
	{
		
		$("#main-img-div").animate({"opacity": "show"	}, { "duration": "6500", "easing": "easein" }, function() {
		    // Animation complete.
		  });
		$("#main-image").attr("src",baseurl+"/misc/images/Buildings_450x225.png");	
		//$("#main-menu-bar").attr("src",baseurl+"/misc/images/home-G2000-tab-highlighted.gif");
		$("#image0").css("display", "none");
		$("#image1").fadeIn(3000);
		$("#image2").css("display", "none");
		$("#image3").css("display", "none");
		$("#image4").css("display", "none");
		setTimeout("changeHome('2')","6500");
		
	}
	else if(j==2)
	{
		$("#main-img-div").animate({"opacity": "show"	}, { "duration": "6500", "easing": "easein" }, function() {
		    // Animation complete.
		  });
		$("#main-image").attr("src",baseurl+"/misc/images/Flowchart_450x225.png");
		//$("#main-menu-bar").attr("src",baseurl+"/misc/images/home-new-ventures-tab-highlighted.gif");
		$("#image0").css("display", "none");
		$("#image1").css("display", "none");
		$("#image2").fadeIn(3000);
		$("#image3").css("display", "none");
		$("#image4").css("display", "none");
		setTimeout("changeHome('3')","6500");
	}
	else if(j==3)
	{
		$("#main-img-div").animate({"opacity": "show"	}, { "duration": "6500", "easing": "easein" }, function() {
		    // Animation complete.
		  });
		$("#main-image").attr("src",baseurl+"/misc/images/ProcessDiagram_450x225.png");	
		//$("#main-menu-bar").attr("src",baseurl+"/misc/images/home-isvs-tab-highlighted.gif");
		$("#image0").css("display", "none");
		$("#image1").css("display", "none");
		$("#image2").css("display", "none");
		$("#image3").fadeIn(3000);
		$("#image4").css("display", "none");
		setTimeout("changeHome('4')","6500");
	}
    else if(j==4)
	{
		$("#main-img-div").animate({"opacity": "show"	}, { "duration": "6500", "easing": "easein" }, function() {
		    // Animation complete.
		  });
		$("#main-image").attr("src",baseurl+"/misc/images/HubAnalyzer-Throughput-450x225.png");	
		//$("#main-menu-bar").attr("src",baseurl+"/misc/images/home-isvs-tab-highlighted.gif");
		$("#image0").css("display", "none");
		$("#image1").css("display", "none");
		$("#image2").css("display", "none");
		$("#image3").css("display", "none");
		$("#image4").fadeIn(3000);
		setTimeout("changeHome('0')","6500");
	}
}
//$("#sidebar-left ul.menu li.expanded a.active").after("</div>");
