var base_url = '';
var raw_report_html = '';
var raw_report_html_submit = '';
var img_html = '';
var raw_hub_req_demo_html = '';
var raw_hub_req_demo_html_submit = '';

function showHubReqDemoFrm() {
    raw_hub_req_demo_html = $("#d-hdn-hub-req-demo-frm").html();
    $("#d-hdn-hub-req-demo-frm").html('');
    popupUI(raw_hub_req_demo_html,30,650);
}
function showHubReqDemoFrmSubmit(url) {
    url = base_url + url;
    if($("#first_name").val() == '') {
        alert("Please enter first name");
        $("#first_name").focus();
        return false;
    }
    if($("#last_name").val() == '') {
        alert("Please enter last name");
        $("#last_name").focus();
        return false;
    }
    if($("#txtPhone").val() == '') {
        alert("Please enter phone number");
        $("#txtPhone").focus();
        return false;
    }
    if($("#txtEmail").val() == '') {
        alert("Please enter email");
        $("#txtEmail").focus();
        return false;
    }
    if($("#txtCompany").val() == '') {
        alert("Please enter company name");
        $("#txtCompany").focus();
        return false;
    }
    if(validate_email($("#txtEmail").val())) {

        var param = "demoreq=1&company="+$("#txtCompany").val()+"&email="+$("#txtEmail").val()+"&first_name="+$("#first_name").val()+"&last_name="+$("#last_name").val()+"&phone="+$("#txtPhone").val();
        raw_hub_req_demo_html_submit = $("#d-hdn-hub-req-demo-frm-submit").html();
        $("#d-hdn-hub-req-demo-frm-submit").html('');
        popupUI(raw_hub_req_demo_html_submit,50,600);
        showHubReqDemoFrmSubmitAjax(url, param);
    }
}
function showHubReqDemoFrmSubmitAjax(url, param) {
         $.ajax({
                url:url,
                type: 'POST',
                data: param,
                cache: false,
                error: function() {
                            alert("Failed to submit information.");
                        },
                success: function(v) {
                            if(v == "Done") {
                            }
                        }
            });
}
function cancelHubReqDemoFrm() {
    if(raw_hub_req_demo_html != '') {
        $("#d-hdn-hub-req-demo-frm").html(raw_hub_req_demo_html);
    }   
    if(raw_hub_req_demo_html_submit != '') {
        $("#d-hdn-hub-req-demo-frm-submit").html(raw_hub_req_demo_html_submit);
    }
    $.unblockUI();
}
function showPopupImage(title,img,w,h) {
    $("#img-hdn").attr("width", w);
    $("#img-hdn").attr("height", h);
    $("#img-hdn").attr("src", base_url+'/misc/images/'+img);
    if(title != '')
        $("#img-title").html(title);
    img_html = $("#d-hdn-img-box").html();
    //$("#d-hdn-img-box").html('');
    //$("#d-hdn-img-box-inner").css('width', w+'px');
    popupUI(img_html,10,w+100);
}
function closePopupImage(){
    if(img_html != '') {
        $("#d-hdn-img-box").html(img_html);
    }
    $.unblockUI();
}
function showClientFrm() {
    raw_report_html = $("#d-hdn-client-frm").html();
    $("#d-hdn-client-frm").html('');
    popupUI(raw_report_html,50,600);
}
function showClientFrmSubmit(param) {
    raw_report_html_submit = $("#d-hdn-client-frm-submit").html();
    $("#d-hdn-client-frm-submit").html('');
    popupUI(raw_report_html_submit,50,600);
    //window.location.href = param;
    doAjax(param+'&action=ajax&stype=summary&rpt=pdf','mail');
}
function popupUI(html,padding,w) {
    $.blockUI({
                css: { 
                    border: '2px solid #666666', 
                    padding: padding+'px', 
                    backgroundColor: '#F2F2F2', 
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    'border-radius': '10px', 
                    color: '#fff',
                    width: w+'px',
                    cursor:'hand',
                    top:  ($(window).height() - 500) /2 + 'px',
                    left: ($(window).width() - 500) /2 + 'px'
                }, 
                centerY: true,
                centerX: true,
                overlayCSS: { backgroundColor: '#E8E7E7'}, 
                showOverlay: true,
                message:html,
            });
}
function cancel_report(url) {
    if(raw_report_html != '') {
        $("#d-hdn-client-frm").html(raw_report_html);
    }   
    if(raw_report_html_submit != '') {
        $("#d-hdn-client-frm-submit").html(raw_report_html_submit);
    }   
    $.unblockUI();
}
function generate_report(url) {
    if($("#txtCompanyName").val() == '') {
        alert("Please enter company name");
        $("#txtCompanyName").focus();
        return false;
    }
    if($("#txtCompanyEmail").val() == '') {
        alert("Please enter email");
        $("#txtCompanyEmail").focus();
        return false;
    }
    if(validate_email($("#txtCompanyEmail").val())) {
        var param = "&company="+$("#txtCompanyName").val()+"&email="+$("#txtCompanyEmail").val();
        showClientFrmSubmit(param);
        //showClientFrmSubmit(url + param);
        $("#d-hdn-client-frm").html(raw_report_html);
        //$("#loading").text("Generating report...");    
        //$("#loading").show();
        //window.location.href = url;
    }
}

function validate_email(email) {
   var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
   if(reg.test(email) == false) {
      alert('Invalid Email Address');
      return false;
   }
   return true;
}
function onlyNumbers(event) {
    if((event.keyCode >= 48 && event.keyCode <= 57)||(event.keyCode >= 96 && event.keyCode <= 105)||(event.keyCode == 8 ) ||(event.keyCode == 9) || (event.keyCode == 12) || (event.keyCode == 27) || (event.keyCode == 37) || (event.keyCode == 39) || (event.keyCode == 46) ){
        return true;
    }
    else{
        event.preventDefault();
    }
}

function NumberToMoney(amount,id) {
    /*
    if(amount.indexOf(',') > 0) {
        // looks like number is already formated.
        var num = amount;    
    }
    else if(amount != ''){
        var num = numberToCurrency(parseFloat(amount));
    }
    */
    if(amount != ''){
        var num = numberToCurrency(parseFloat(amount.replace(",",'')));
        if(id) {
            $("#"+id).val(num.split(".")[0]);
        }
        else {
            return num;
        }
    }
}
/**
 * Converts number into currency format
 * @param {number} number   Number that should be converted.
 * @param {string} [decimalSeparator]    Decimal separator, defaults to '.'.
 * @param {string} [thousandsSeparator]    Thousands separator, defaults to ','.
 * @param {int} [nDecimalDigits]    Number of decimal digits, defaults to `2`.
 * @return {string} Formatted string (e.g. numberToCurrency(12345.67) returns '12,345.67')
 */

function numberToCurrency(number, decimalSeparator, thousandsSeparator, nDecimalDigits){
    //default values
    decimalSeparator = decimalSeparator || '.';
    thousandsSeparator = thousandsSeparator || ',';
    nDecimalDigits = nDecimalDigits || 2;
    
    var fixed = number.toFixed(nDecimalDigits), //limit/add decimal digits
    parts = RegExp('^(-?\\d{1,3})((\\d{3})+)\\.(\\d{'+ nDecimalDigits +'})$').exec( fixed ); //separate begin [$1], middle [$2] and decimal digits [$4]

    if(parts){ //number >= 1000 || number <= -1000
        return parts[1] + parts[2].replace(/\d{3}/g, thousandsSeparator + '$&') + decimalSeparator + parts[4];
    }else{
        return fixed.replace('.', decimalSeparator);
    }
}
function validate() {
    var blnEmpty=false;
    var blnFound=false;

    $("input[type='text'][qty=1]").each(function(){
        if($(this).val() == '' && !blnFound) {
            blnFound=false;
        }
        else {
            blnFound=true;
        }
    });

    if($('#burden_rate').val() == '') {
        alert($('#burden_rate').attr('title'));
        $('#burden_rate').focus();
        blnEmpty=false;
    }
    if($('#recruting_fee').val() == '') {
        alert($('#recruting_fee').attr('title'));
        $('#recruting_fee').focus();
        blnEmpty=false;
    }
    // Senior Level
    if($('#stQty').val() != '') {
        if($('#stPay').val() == '') {
            alert($('#stPay').attr('title'));
            $('#stPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#stPay').val() != '') {
        if($('#stQty').val() == '') {
            alert($('#stQty').attr('title'));
            $('#stQty').focus();
            blnEmpty=true;
        }
    }
    if($('#sqQty').val() != '') {
        if($('#sqPay').val() == '') {
            alert($('#sqPay').attr('title'));
            $('#sqPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#sqPay').val() != '') {
        if($('#sqQty').val() == '') {
            alert($('#sqQty').attr('title'));
            $('#sqQty').focus();
            blnEmpty=true;
        }
    }
    if($('#spQty').val() != '') {
        if($('#spPay').val() == '') {
            alert($('#spPay').attr('title'));
            $('#spPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#spPay').val() != '') {
        if($('#spQty').val() == '') {
            alert($('#spQty').attr('title'));
            $('#spQty').focus();
            blnEmpty=true;
        }
    }
    // Mid Level
    if($('#mtQty').val() != '') {
        if($('#mtPay').val() == '') {
            alert($('#mtPay').attr('title'));
            $('#mtPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#mtPay').val() != '') {
        if($('#mtQty').val() == '') {
            alert($('#mtQty').attr('title'));
            $('#mtQty').focus();
            blnEmpty=true;
        }
    }
    if($('#mqQty').val() != '') {
        if($('#mqPay').val() == '') {
            alert($('#mqPay').attr('title'));
            $('#mqPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#mqPay').val() != '') {
        if($('#mqQty').val() == '') {
            alert($('#mqQty').attr('title'));
            $('#mqQty').focus();
            blnEmpty=true;
        }
    }
    if($('#mpQty').val() != '') {
        if($('#mpPay').val() == '') {
            alert($('#mpPay').attr('title'));
            $('#mpPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#mpPay').val() != '') {
        if($('#mpQty').val() == '') {
            alert($('#mpQty').attr('title'));
            $('#mpQty').focus();
            blnEmpty=true;
        }
    }
    // Junior Level
    if($('#jtQty').val() != '') {
        if($('#jtPay').val() == '') {
            alert($('#jtPay').attr('title'));
            $('#jtPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#jtPay').val() != '') {
        if($('#jtQty').val() == '') {
            alert($('#jtQty').attr('title'));
            $('#jtQty').focus();
            blnEmpty=true;
        }
    }
    if($('#jqQty').val() != '') {
        if($('#jqPay').val() == '') {
            alert($('#jqPay').attr('title'));
            $('#jqPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#jqPay').val() != '') {
        if($('#jqQty').val() == '') {
            alert($('#jqQty').attr('title'));
            $('#jqQty').focus();
            blnEmpty=true;
        }
    }
    if($('#jpQty').val() != '') {
        if($('#jpPay').val() == '') {
            alert($('#jpPay').attr('title'));
            $('#jpPay').focus();
            blnEmpty=true;
        }
    }
    else if($('#jpPay').val() != '') {
        if($('#jpQty').val() == '') {
            alert($('#jpQty').attr('title'));
            $('#jpQty').focus();
            blnEmpty=true;
        }
    }
   if(!blnEmpty) {
        if(blnFound) {
            doAjax('&action=ajax&stype=summary','');
        }
        else {
            alert('Please enter atleast column data to calculate Cost');
            return false;
        }
    }
}

function doAjax(v,type) {
    if(type != 'mail') { 
        $("#loading").text("Calculating Cost...");    
        $("#loading").show();
        $("#dVOE").html("");
        $("#dVOE").hide();
    }
    var param = $("form").serialize();
    param = param + v;
    $.ajax({
        url:base_url+'/roical/genreport',
        type: 'POST',
        data: param,
        cache: false,
        error: function() {
                    //alert("Failed to calculate Cost");
                    $("#loading").text("");    
                    $("#loading").hide();
                },
        success: function(v) {
                    if(v == "Done") {
                        $("#loading").text("");
                        $("#loading").hide();
                    }
                    else {
                        $("#dVOE").html(v);
                        $("#dVOE").show();
                        $("#loading").text("");    
                        $("#loading").hide();
                    }
                }
    });
}

function sumQty() {
    var Tqty = 0;
    var Qqty = 0;
    var Pqty = 0;

    // Senior Level
    if($('#stQty').val() != '') {
       Tqty = Tqty + parseInt($('#stQty').val());
    }
    if($('#sqQty').val() != '') {
       Qqty = Qqty + parseInt($('#sqQty').val());
    }
    if($('#spQty').val() != '') {
       Pqty = Pqty + parseInt($('#spQty').val());
    }
    // Mid Level 
    if($('#mtQty').val() != '') {
       Tqty = Tqty + parseInt($('#mtQty').val());
    }
    if($('#mqQty').val() != '') {
       Qqty = Qqty + parseInt($('#mqQty').val());
    }
    if($('#mpQty').val() != '') {
       Pqty = Pqty + parseInt($('#mpQty').val());
    }
    // Junior Level   
    if($('#jtQty').val() != '') {
       Tqty = Tqty + parseInt($('#jtQty').val());
    }
    if($('#jqQty').val() != '') {
       Qqty = Qqty + parseInt($('#jqQty').val());
    }
    if($('#jpQty').val() != '') {
       Pqty = Pqty + parseInt($('#jpQty').val());
    }
    if(Tqty > 0) {
        $("#Tqty").text(Tqty);
    }
    else {
        $("#Tqty").text('');
    }
    if(Qqty > 0) {
        $("#Qqty").text(Qqty);
    }
    else {
        $("#Qqty").text('');
    }
    if(Pqty > 0) {
        $("#Pqty").text(Pqty);
    } 
    else {
        $("#Pqty").text('');
    }
}
