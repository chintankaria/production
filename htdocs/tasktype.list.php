<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.tasktype.inc.php");
    $task_types = TaskType::GetAll();
    $groups = TaskType::GetGroups();

    sm_assign("task_types", $task_types);
    sm_assign("groups", $groups);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.tasktype.list.html"), "Task type list",null, array(3));
    	$fileName = date("d-m-Y").'_tasktype_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("tasktype.list.html");
        exit;
    }
    else {
        sm_display("tasktype.list.html");
    }
