<?php
include_once('../config.nologin.php');

$email = trim($_GET['email']);
$key = trim($_GET['license_key']);

if(!$email || !preg_match('/.+@.+\..+/', $email)) {
	return_server_error("Invalid email address.");
}

$sql = "
	SELECT	c.path, b.valid_upto >= CURDATE() AS is_valid, b.id AS download_key_id
    FROM licenses a, download_keys b, product_versions c
    WHERE a.id = b.license_id 
	AND a.product_version_id = c.id 
	AND a.license_key = " . q($key) . " 
	ORDER BY a.id DESC LIMIT 1
";

$row = db_get_row($sql);

if ($row) {
	if ($row['is_valid']) {
		$file = realpath(dirname(__FILE__) . "/../downloads/" . $row["path"]);
		if ($file && file_exists($file)) {
			$sql = "
				INSERT INTO download_logs (download_key_id, email, remote_ip, downloaded_on)
				VALUES (" . q($row["download_key_id"]) . ", " . q($email) . ", " . q($_SERVER["REMOTE_ADDR"]) . ", NOW())
			";
			
			db_execute($sql);

			setcookie('fileDownload', 'true', 0, '/');
			download_file($file);
			exit;
		}
		else {
			return_server_error('File does not exist.');
		}
	} else {
		return_server_error('The download link has expired.');
	}
} else {
	return_server_error('Invalid License.');
}

function return_server_error($message) {
	ob_clean();
	header("X-Error-Message: {$message}", true, 500);
	exit($message);
}
