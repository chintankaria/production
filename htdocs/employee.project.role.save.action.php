<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.employee.project.role.php");
    $employee_project_role = new EmployeeProjectRole($_POST["id"]);
    $employee_project_role->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($employee_project_role->delete()) {
            $str = "Employee Project Role <i><b>{$_POST['name']}</b></i> deleted.";
            s("Employee Project Role  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete Employee Project Role.";
            e("Failed to delete Employee Project Role.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$employee_project_role->checkName()) {
            $employee_project_role->setIsActive(1);
            if($employee_project_role->save()) {
                $str = "Employee Project Role  <i><b>{$employee_project_role->getName()}</b></i> saved.";
                s("Employee Project Role  <i><b>{$employee_project_role->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save Employee Project Role {$_POST["name"]}";
                e("Failed to save Employee Project Role {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Employee Project Role <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("employee.project.role.list.php");
    }
