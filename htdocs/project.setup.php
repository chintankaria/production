<?php

include_once("../config.php");
require_perms("admin", "manager");
include_once("../classes/class.project.servicetype.inc.php");
include_once("../classes/class.project.practicearea.inc.php");
include_once("../classes/class.project.domain.inc.php");

//if (trim($_POST['name']) != '') {
if (trim($_POST['Save']) == "Save"){
    $tab = $_POST["tabId"];
    $id = $_POST["id"] ? $_POST["id"] : null;
    $project = new Project($id);
    $logoName = '';
    $fname = '';
    $extension = '';

    if ($id != null && $project->getId() < 1 && trim($_POST["name"]) != '') {
        $str = "Failed to save {$_POST['name']} project changes.";
        $message['data'] = $str;
        $message['status'] = "FAILED";
    } else {
        $message = array();
        $message['status'] = "";
        if ($_FILES["clientLogo"]["error"] != 4) {
            if ($_FILES["clientLogo"]["error"] > 0) {
                $message['data'] = "Error: " . $_FILES["clientLogo"]["error"] . "<br>";
                $message['status'] = "FAILED";
            } else {
                $allowedExts = array("gif", "jpeg", "jpg", "png");
                $temp = explode(".", $_FILES["clientLogo"]["name"]);
                $extension = end($temp);

                if ((($_FILES["clientLogo"]["type"] == "image/gif") || ($_FILES["clientLogo"]["type"] == "image/jpeg") || ($_FILES["clientLogo"]["type"] == "image/jpg") || ($_FILES["clientLogo"]["type"] == "image/pjpeg") || ($_FILES["clientLogo"]["type"] == "image/x-png") || ($_FILES["clientLogo"]["type"] == "image/png")) && ($_FILES["clientLogo"]["size"] < 2000000) && in_array($extension, $allowedExts)) {
                    if ($_FILES["clientLogo"]["error"] > 0) {
                        $message['data'] = "Error: " . $_FILES["clientLogo"]["error"] . "<br>";
                        $message['status'] = "FAILED";
                    } else {
                        $fname = $_FILES["clientLogo"]["name"];
                        $id = $id ? $id : 'XXXXX';
                        $logoName = "client_logo_{$id}.{$extension}";
                    }
                } else {
                    $message['data'] = "Error: Invalid logo file " . $_FILES["clientLogo"]["error"] . "<br>";
                    $message['status'] = "FAILED";
                }
            }
        }

        if ($_POST["pid"]) {
            $project->setParentId($_POST["pid"]);
        }

        $project->setName($_POST["name"]);
        $project->setDescription($_POST["description"]);
        $project->setIsBillable($_POST["is_billable"]);
        $project->setStatus($_POST["status"]);
        $project->setClientId($_POST["client_id"]);
        $project->setStartDate($_POST["start_date"]);
        $project->setEndDate($_POST["end_date"]);
        $project->setAllowSubprojects($_POST["allow_subprojects"]);
        $project->setAllowTaskTypes($_POST["allow_task_types"]);
        $project->setChannel($_POST["channel"]);
        $project->setClientProjectLead($_POST["client_project_lead"]);
        $project->setPartnerProjectLead($_POST["partner_project_lead"]);
        $project->setDeliveryLocation($_POST["delivery_location"]);
        $project->setServiceType($_POST["service_type"]);
        $project->setPracticeArea($_POST["practice_area"]);
        $project->setDomain($_POST["domain"]);
        if ($logoName != '') {
            $project->setClientLogo($logoName);
        }

        $return_val = $project->compareDate($_POST["start_date"], $_POST["end_date"]);
        if ($return_val) {
            $message['data'] = "Error :: Invalid project start date and end date ({$return_val})";
            $message['status'] = "FAILED";
        } elseif (trim($_POST["name"]) == '') {
            $message['data'] = "Error :: Please specify a project name as well.";
            $message['status'] = "FAILED";
        } elseif (trim($_POST["client_id"]) == 0) {
			sm_assign("name", $_POST['name']);
            $message['data'] = "Error :: Please specify a client name as well.";
            $message['status'] = "FAILED";
        }

        if ($message['status'] != "FAILED") {
			
            if ($project->save()) {

                if ($fname && $id != 'XXXXX') {
                    move_uploaded_file($_FILES["clientLogo"]["tmp_name"], "img/client-logo/" . $logoName);
                } elseif ($fname && $id == 'XXXXX') {
                    $logoName = str_replace('XXXXX', $project->getId(), $logoName);
                    move_uploaded_file($_FILES["clientLogo"]["tmp_name"], "img/client-logo/" . $logoName);
                    $project->setClientLogo($logoName);                    
                    $project->save();
                }

                $str = "'{$_POST['name']}'  project changes saved successfully!";
                $message['data'] = $str;
                $message['status'] = "SUCCESS";
                watchdog('SUCCESS', 'EDIT', $str);
            } else {
				
                $str = "Failed to save '{$_POST['name']}' project changes.";
                $message['data'] = $str;
                $message['status'] = "FAILED";
                watchdog('FAILED', 'EDIT', $str);
            }
        }
    }
    $_SESSION['project_setup_message'] = $message['data'];
    
    if ($message['status'] == "FAILED" && $_POST["pid"]) {
        redirect("project.detail.php?pid={$_POST["pid"]}&tab={$tab}");
    } elseif ($message['status'] == "FAILED" && $_POST["id"]) {
        redirect("project.detail.php?id={$_POST["id"]}&tab={$tab}");
    } else {
        redirect("project.detail.php?id={$project->getId()}&tab={$tab}");
    }

    exit();
} else {

    if ($_GET["pid"]) {
        $parentProject = new Project($_GET["pid"]);
        sm_assign("parentProject", $parentProject);
        sm_assign("isSubProject", 1);
    }

    if (trim($_GET["id"]) != '') {
        $project = new Project($_GET["id"]);
        sm_assign("project", $project);
        sm_assign("isSubProject", 0);
    }
    elseif($_GET["pid"] == ''){
        $project = new Project();
        sm_assign("project", $project);
        sm_assign("isSubProject", 0);
    }

    $clients = Client::GetClients(0, 1);

    $serviceTypes = ProjectServiceTypes::GetService();
    $practiceAreas = ProjectPracticeArea::GetPracticeArea();
    $domains = ProjectDomain::GetDomains();

    sm_assign("clients", $clients);
    sm_assign("serviceTypes", $serviceTypes);
    sm_assign("practiceAreas", $practiceAreas);
    sm_assign("domains", $domains);
    sm_display("project.setup.html");
}