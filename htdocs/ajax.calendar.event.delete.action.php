<?php

include_once("../config.php");
include_once("../classes/class.calendarevent.inc.php");

$status = false;
if(u()->isAdmin()){
    $event = new CalendarEvent($_POST["id"]);
    if($event->delete()) $status = true;
}

ob_clean();
print json_encode(array('status' => $status));

