<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.licenseperiods.inc.php");
    $license_periods = new LicensePeriods($_POST["id"]);
    $license_periods->setDuration($_POST["duration"]);
    $license_periods->setDaymonthyear($_POST["daymonthyear"]);
    if($_POST['action'] == "DELETE") {
        if($license_periods->delete()) {
            s("License Period <i><b>{$_POST['duration']}</b></i> deleted.");
            $str = "License Period <i><b>{$_POST['duration']}</b></i> deleted.";
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            e("Failed to delete license period.");
            $str = "Failed to delete license period.";
            watchdog('FAILED','DELETE', "$str");
        }
    }
    elseif((int)$_POST["duration"] <= 0) {
        e("Invalid License Period <i><b>{$_POST["duration"]} {$_POST["daymonthyear"]}</b></i>.");
    }
    else {
        if(!$license_periods->checkDuration()) {
            if($license_periods->save()) {
                s("License Period <i><b>{$license_periods->getDuration()} {$license_periods->getDaymonthyear()}</b></i> saved.");
                $str = "License Period <i><b>{$license_periods->getDuration()} {$license_periods->getDaymonthyear()}</b></i> saved.";
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                e("Failed to save license period {$_POST["duration"]} {$_POST["daymonthyear"]}");
                $str = "Failed to save license period {$_POST["duration"]} {$_POST["daymonthyear"]}";
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("License Period <i><b>{$_POST["duration"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("licenseperiods.list.php");
    }
