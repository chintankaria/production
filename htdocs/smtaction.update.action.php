<?php
    include_once("../config.php");
    require_perms("smt_member","admin");
    include_once("../classes/class.smtaction.inc.php");

    $id = $_GET['id'];
    $status = $_GET['status'];
    $actionItem = new SMTAction($id);
//debug($actionItem); exit;
    if (!$actionItem || !$actionItem->getId()) {
        e("Unable to locate the action item to update.");
        redirect("smtaction.list.php");
    }
    $actionItem->setStatus($status);

    if ($actionItem->save()) {
        s("Action item  <i><b>".$actionItem->getTitle()."</b></i> updated successlfully.");
    } else {
        e("Failed to update action item.");
    }
    redirect("smtaction.list.php");
?>
