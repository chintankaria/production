<?php
    include_once("../config.php");
    include_once("../classes/class.trivia.inc.php");
    include_once("../classes/class.pagination.inc.php");


    $user = u();
    if(!$_GET && count($_SESSION['trivia']['filters']) > 0) {
        $_GET = $_SESSION['trivia']['filters'];
    }

    $from_date = (isset($_GET["from_date"]) && is_date($_GET["from_date"])) ? $_GET["from_date"] : date("m")."/1/".date("Y");
    $to_date = (isset($_GET["to_date"]) && is_date($_GET["to_date"])) ? $_GET["to_date"] : date("m/d/Y",strtotime("tomorrow"));

    $_GET['from_date'] = $from_date;
    $_GET['to_date'] = $to_date;

    if($_GET['status'] == 'ALL') {
        $status = array('ALL');
        $_GET['status'] = $status;
    }
    else {
        $status = isset($_GET["status"]) ? $_GET["status"] : array("ALL");
        $_GET['status'] = $status;
        if(strpos($status,",")) {
            $status = explode(",", $status);
        }
    }

    if($user->isAdmin()) {
        $view = isset($_GET["view"]) ? trim($_GET["view"]) : "table";
    }
    else {
        $view = isset($_GET["view"]) ? trim($_GET["view"]) : "list";
    }
    $_GET['view'] = $view;

    $p = (isset($_GET["p"]) && intval($_GET['p']) > 0) ? intval($_GET["p"]) : 1;
    $_GET['p'] = $p;

    if($_GET) {
        $_SESSION['trivia']['filters'] = $_GET;
    }
    $total_pages = 0;
    $total_results = 0;
    $page_limit = 5;

    $excel_no_field = 8;
    sm_assign("from_date", $from_date);
    sm_assign("to_date", $to_date);
    sm_assign("status", $status);
    sm_assign("p", $p);

    $strStatus = implode(",",$status);
    sm_assign("strStatus", $strStatus);

    $blnAll=false;
    if(in_array("ALL", $status)) {
        $status = array( 'DRAFT', 'QUEUED', 'REJECTED', 'OPEN', 'CLOSED');
        $blnAll=true;
    }
    sm_assign("blnAll", $blnAll);
    if($user->isAdmin()) {
        $trivias = Trivia::GetTriviaList($status, $from_date, $to_date, $p, $page_limit);
    }
    else {
        $trivias = Trivia::GetUserTriviaList($status, $from_date, $to_date, $p, $page_limit);
    }
    if($p > 0) {  
        $total_pages = Trivia::getTotalPages();
        $total_results = Trivia::getTotalResults();
        $Paginate = new Pagination();
        $Paginate->setCurrentPage($p);
        $Paginate->setPageLimit($page_limit);
        $Paginate->setTotalPages($total_pages);
        $Paginate->setTotalResult($total_results);
        $Paginate->setPageNumberLimit(5);
        $Paginate->Paginate();
        $pages = $Paginate->getNumberStyleArray();
        $previous_page =$Paginate->getPreviousPage();
        $next_page = $Paginate->getNextPage();
        $current_page = $Paginate->getCurrentPage();

        sm_assign("total_results", $total_results);
        sm_assign("total_pages", $total_pages);
        sm_assign("pages", $pages);
        sm_assign("previous_page", $previous_page);
        sm_assign("next_page", $next_page);
        sm_assign("current_page", $current_page);
        sm_assign("page_end", @end($pages));
    }
    if($_GET["excel"]){
        $trivias = Trivia::GetIdeasList($status);
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
		$excel->setHTMLTable(sm_fetch("table.trivia.list.html"), "Trivia List", null, array($excel_no_field));    	
    	$fileName = date("d-m-Y").'_trivia_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    //if($view == "list") {
        if($trivias) {
            foreach($trivias as $key => $trivia) {
                $pClass = ($pClass == "comments-odd") ? "comments-even" : "comments-odd";
                $trivia->class = $pClass;
                //$trivia->date_time = date("d M y h:i a",strtotime($trivia->getDateTime()));
                if($view == "list") {
                    $trivia->description = short_string(strip_tags($trivia->getDescription()),500);
                }
                else {
                    $trivia->description = short_string(strip_tags($trivia->getDescription()),50);
                }

                if(strtoupper($trivia->getStatus()) == 'OPEN' && $trivia->isClosingDate()) {
                    $trivia->closeTrivia($trivia->getId(), $trivia->getCloseDateTime());
                    $trivia->setStatus('Closed');
                }
            }
        }
    //}

    sm_assign("trivias", $trivias);    
    sm_assign("view", $view);
    sm_display("trivia.list.html");
?>
