<?php
include_once("../config.php");
require_perms("admin");

include_once("../classes/class.project.servicetype.inc.php");
$project_service_type = ProjectServiceTypes::GetService();

sm_assign("project_service_type", $project_service_type);

if ($_GET["excel"]) {
    include_once("../classes/class.excelwriter.inc.php");
    $excel = new ExcelWriter();
    $excel->setHTMLTable(sm_fetch("table.project.practice.list.html"), "Project Service Type", null, array(3));
    $fileName = date("d-m-Y") . '_service_type.xls';
    ob_clean();
    $excel->send($fileName);
    $excel->close();
    exit;
}
if ($_REQUEST['fetch'] == 1) {
    sm_assign("fetch", 1);
    print sm_fetch("project.service.list.html");
    exit;
} else {
    sm_display("project.service.list.html");
}
