<?php
include_once("../config.php");
include_once("../classes/class.task.inc.php");
include_once("../classes/class.timecard.inc.php");

//echo '<pre>';print_r($_POST);echo '</pre>';

$clients = array_filter($_POST["client_id"]);
$user_id = $_POST["user_id"];
$total_tasks = 0;
$saved_tasks = 0;
$deleted_tasks = 0;
$failed_tasks = 0;
$user_check_tasks = 0;
$user = u();
if (count($clients) == 1) {
    $pto_duration = array_filter($_POST['durations']['pto']);
    if (false && !count($pto_duration)) {
        $str = "Incomplete information, choose a valid client.";
        watchdog('FAILED', 'WEEKLY_TIMECARD', $str);
        e($str);
        redirect("task.add.multiple.php?date=" . $_POST['entry_date'] . "&user_id=" . $user_id);
    }
}
//$status = (isset($_POST['action']) && $_POST['action'] == 'SUBMIT_TIMECARD') ? "SUBMITTED" : "PENDING"; 
if (isset($_POST['action']) && $_POST['action'] == 'SUBMIT_TIMECARD') {
    $status = 'SUBMITTED';
} else if (isset($_POST['action']) && $_POST['action'] == 'REQUEST_REVIEW') {
    $status = 'REVIEW';
} else if (isset($_POST['action']) && $_POST['action'] == 'MARK_PENDING') {
    $status = 'PENDING';
} else {
    $status = 'PENDING';
}

$from = array("admin.timesheet@globalss.com" => "GSS Administrator");
if ((isset($_POST['action']) && $_POST['action'] == 'MARK_PENDING') || $status == 'REVIEW') {
    // Member requesting for the weekly timecard review.
    $redirect_url = "$timesheet_url/user.login.php?url=" . urlencode("task.add.multiple.php?date={$_POST['entry_date']}&user_id=$user_id");
    $oUser = new User($user_id);
    // $week_number=date("W",strtotime($_POST['entry_date']));
    $week_number = Task::getWeek($_POST['entry_date']);
    sm_assign("full_name", $oUser->getFullName());
    sm_assign("user_id", $oUser->getId());
    sm_assign("date", $_POST['entry_date']);
    sm_assign("week_number", $week_number);
    sm_assign("redirect_url", $redirect_url);
    if (isset($_POST['action']) && $_POST['action'] == 'MARK_PENDING') {
        _::Mail($from, array($oUser->getEmail() => $oUser->getFullName()), "Mr.{$oUser->getFullName()}, time card review granted for the week number #$week_number", sm_fetch("timecard.marked.pending.letter.html")
        );
        $str = "Mr.{$oUser->getFullName()}({$oUser->getEmail()}), time card review granted for the week number #$week_number";
        watchdog('SUCCESS', 'EMAIL', $str);
    } else {
        $bManager = User::GetBusinessManagers();
        if ($bManager) {
            foreach ($bManager as $u_id => $o_u) {
                if (in_array("business_manager", $o_u->getRoles())) {
                    sm_assign("name", $o_u->getFullName());
                    $email = $o_u->getEmail();
                    //$email = "shekhar@globalss.com";
                    _::Mail($from, array($email => $o_u->getFullName()), "Timecard Review Request by Mr.{$oUser->getFullName()}", sm_fetch("timecard.review.letter.html")
                    );
                    $str = "Timecard Review Request by Mr.{$oUser->getFullName()}({$oUser->getEmail()}) sent to Business Manager({$o_u->getFullName()} - {$o_u->getEmail()}) ";
                    watchdog('SUCCESS', 'EMAIL', $str);
                }
            }
        }
    }
    if ($status == "REVIEW") {
        $str = "A request for review the weekly timecard is sent successfully to Mr.{$oUser->getFullName()}.";
        s($str);
        redirect("task.timecard.php?date=" . $_POST['entry_date'] . "&user_id=" . $user_id);
    }
}

$failed_duration = array();
// First lets get the marked public holiday list for each day
foreach ($clients as $k => $v) {
    //$durations = array_filter($_POST["durations"][$k]);
    $durations = $_POST["durations"][$k];
    if (!$durations)
        continue;

    foreach ($durations as $date_ts => $duration) {
        ++$total_tasks;

        if ($_POST["subproject_id"][$k])
            $project_id = $_POST["subproject_id"][$k];
        else
            $project_id = $_POST["project_id"][$k];
        $failed_status = 1;
        $user_check_status = 1;

        $project = new Project($project_id);

        if (hm2m($duration) > 0) {
            if ($project_id != 1000 && !$project->checkUserByProject($project_id, $user_id)) {
                $user_check_status = 0;
                ++$user_check_tasks;
                $prj_name_by_u_check[] = "<i>{$project->getName()}</i>";
                $failed_duration[] = $date_ts;
            }

            if ($project_id != 1000 && $project->getEnddate()) {
                if (strtotime(date('Y-m-d', $project->getStartdate())) > strtotime(date('Y-m-d', $date_ts)) || $project->getEnddate() < $date_ts) {
                    $failed_status = 0;
                    ++$failed_tasks;
                    $prj_name_by_dt[] = "<i>{$project->getName()}</i>";
                    $failed_duration[] = $date_ts;
                }
            } else if ($project_id != 1000 && strtotime(date('Y-m-d', $project->getStartdate())) > strtotime(date('Y-m-d', $date_ts))) {
                $failed_status = 0;
                ++$failed_tasks;
                $prj_name_by_dt[] = "<i>{$project->getName()}</i>";
                $failed_duration[] = $date_ts;
            }
        }
        $projectRoleId = $_POST['project_role_id'][$k] > 0 ? $_POST['project_role_id'][$k] : 0;
        $task = new Task($_POST["id"][$k][$date_ts], $user_id);
        $task->setProjectId($_POST["project_id"][$k], $_POST["subproject_id"][$k]);
        $task->setProjectRoleId($projectRoleId);
        $task->setBillable(isset($_POST['billable'][$k]) ? 1 : 0);
        $task->setTaskDate(mdy($date_ts));
        $task->setTaskTypeId($_POST["task_type_id"][$k]);
        $task->setDetail($_POST["details"][$k][$date_ts]);
        $task->setTaskStatus($status);
        $project = new Project($_GET["id"]);
        if (hm2m($duration) < 1) {
            if ($task->delete($user_id))
                ++$deleted_tasks;
            if ($deleted_tasks < $total_tasks)
                $f = "e";
            else
                $f = "s";

            //$f("Deleted {$deleted_tasks} of {$deleted_tasks} tasks.");
            //redirect("task.timecard.php?date=" . mdy($date_ts)."&user_id=".$user_id);
        }
        else {
            $task->setDurationMins($duration);
            if ($failed_status && $user_check_status) {
                if ($task->save($user_id)) {
                    ++$saved_tasks;
                }
            }
        }
    }
}

if ($_POST['action'] == 'PENDING') {
    if ($saved_tasks < $total_tasks)
        $f = "e";
    else
        $f = "s";

    if ($failed_tasks) {
        //$f("Failed to update  {$failed_tasks} entries because of entered time not within project start and end date.");
        $failed_tasks .= " entries (<b><i>" . implode(", ", $prj_name_by_dt) . "</i></b>)";
        $f("{$failed_tasks} can not be saved because of entered time not within project start and end date.<br>Please contact <i>Administrator</i> for further asistance.");
        $str = "{$failed_tasks} can not be saved because of entered time not within project start and end date.";
        if ($saved_tasks < $total_tasks)
            watchdog('FAILED', 'TIMECARD', $str);
    }
    if ($user_check_tasks) {
        //$f("Failed to update  {$user_check_tasks} entries because the selected user not associated with project / subproject   ");
        $user_check_tasks .= " entries (<b><i>" . implode(", ", $prj_name_by_u_check) . "</i></b>)";
        $f("{$user_check_tasks} can not be saved because the selected user not associated with project / subproject.<br>Please contact <i>Administrator</i> for further asistance.");
        $str = "{$user_check_tasks} can not be saved because the selected user not associated with project / subproject.";
        if ($saved_tasks < $total_tasks)
            watchdog('FAILED', 'TIMECARD', $str);
    }
}

foreach ($_POST['durations'] as $k => $v) {
    foreach ($_POST['durations'][$k] as $date_ts => $duration) {
        if (in_array($date_ts, $failed_duration))
            continue;
        $time_card['week_dt_ts'][date("Y-m-d", $date_ts)] = date("Y-m-d", $date_ts); // date("m-d-Y",$date_ts);
        if (isset($_POST['is_holiday'][date("m-d-Y", $date_ts)])) {
            $time_card['is_holiday'][date("Y-m-d", $date_ts)] = $_POST['is_holiday'][date("m-d-Y", $date_ts)];
        }
        if (isset($_POST['txtTotal'][$date_ts])) {
            $time_card['total_mins'][date("Y-m-d", $date_ts)] = hm2m($_POST['txtTotal'][$date_ts]);
        }
        if (isset($_POST['txtBillable'][$date_ts])) {
            $time_card['billable_mins'][date("Y-m-d", $date_ts)] = hm2m($_POST['txtBillable'][$date_ts]);
        }
        if (isset($_POST['txtUnassigned'][$date_ts])) {
            $time_card['unassigned_mins'][date("Y-m-d", $date_ts)] = hm2m($_POST['txtUnassigned'][$date_ts]);
        }
        if (strtolower($k) == 'pto') {
            $time_card['pto'][date("Y-m-d", $date_ts)] = hm2m($duration);
        }
    }
}

// echo '<pre>'; print_r($time_card); echo '</pre>';
if (is_array($time_card) && count($time_card) > 0) {
    $standard_hours = getStandardHours();
    $standard_hour = $standard_hours["standard_hours_in_mins"];
    foreach ($time_card['week_dt_ts'] as $k => $v) {
        $week_no = date("W", strtotime($k));
        $time_card['is_holiday'][$k] = isset($time_card['is_holiday'][$k]) ? $time_card['is_holiday'][$k] : 0;
        $time_card['pto'][$k] = isset($time_card['pto'][$k]) ? $time_card['pto'][$k] : 0;
        $time_card['unassigned_mins'][$k] = isset($time_card['unassigned_mins'][$k]) ? $time_card['unassigned_mins'][$k] : 0;
        $time_card['billable_mins'][$k] = isset($time_card['billable_mins'][$k]) ? $time_card['billable_mins'][$k] : 0;

        $timecard = new Timecard(0, $user_id, 0, $k);
        $timecard->setUserId($user_id);
        $timecard->setWeekNumber($week_no);
        $timecard->setTimecardDate(strtotime($k));
        $timecard->setUnassignedMinutes($time_card['unassigned_mins'][$k]);
        $timecard->setBillableMinutes($time_card['billable_mins'][$k]);
        $timecard->setPTOMinutes($time_card['pto'][$k]);
        $timecard->setTotalMinutes($time_card['total_mins'][$k]);
        $timecard->setIsHoliday(intval($time_card['is_holiday'][$k]));
        $timecard->setStatus($status);
        $timecard->save();
    }
}
if (isset($_POST['action']) && $_POST['action'] == 'MARK_PENDING') {
    $str = "Timecard marked pending successfully.";
    watchdog('SUCCESS', 'TIMECARD_REVIEW', $str);
    s($str);
} else if (isset($_POST['action']) && $_POST['action'] == 'SUBMIT_TIMECARD') {
    $str = "Timecard submitted successfully for the <b>week# $week_no</b>.";
    watchdog('SUCCESS', 'TIMECARD', $str);
    s($str);
} else {
    $str = "Timecard updated successfully for the <b>week# $week_no</b>.";
    watchdog('SUCCESS', 'TIMECARD', $str);
    s($str);
}

redirect("task.timecard.php?date=" . mdy($date_ts) . "&user_id=" . $user_id);