<?php
    include_once("../config.php");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.contact.inc.php");

    if($_POST["txtClient"]) {
        $client = new Client();
        $client_id = $client->checkClient($_POST["txtClient"]);
        $client_id = (isset($client_id) && intval($client_id) > 0) ? $client_id : 0;
        $active = isset($_POST["contact_active"]) ? 1 : 0; 
        $contact_id = (isset($_POST["id"]) && $_POST["id"] > 0) ? $_POST["id"] : 0; 
        $contact = new Contact($contact_id);
        if($client_id < 1) {
            $client->setName($_POST["txtClient"]);
            $client->setIsClient(0);
            $client->save();
            $client_id = $client->getId();
        }
        $contact->setClientId($client_id);
        $contact->setGroupId($_POST["group_id"]); 
        $contact->setFirstName($_POST["txtFirstName"]);
        $contact->setMiddleName($_POST["txtMiddleName"]);
        $contact->setLastName($_POST["txtLastName"]);
        $contact->setActive($active);
        #if($_POST["contact_exist"] == "validate") {
        #    if($contact->checkContact(trim($_POST["txtFirstName"]), trim($_POST["txtLastName"]))) {
        #        p($_POST);
        #    }
        #}
        if($contact->save()) {
            foreach($_POST["txtCommunication"] as $k => $v) {
                if($_POST["txtCommunication"][$k] != "") {
                    $active = 1;
                    $is_primary = isset($_POST["is_primary_comm"][$k]) ? 1 : 0; 
                    if($_POST["txtCommunicationId"][$k] > 0) {
                        $active = isset($_POST["comm_active"][$k]) ? 1 : 0; 
                        $contact->setCommunicationId($_POST["txtCommunicationId"][$k]);
                    }
                    else
                        $contact->setCommunicationId(0);
                    $contact->setCommunicationDetails(trim($_POST["txtCommunication"][$k]));
                    $contact->setCommunicationTypeId($_POST["comm_type"][$k]);
                    $contact->setIsPrimaryCommunication($is_primary); 
                    $contact->setCommunicationActive($active);
                    $contact->saveCommunication();
                }
            }
            foreach($_POST["address"] as $k => $v) {
                if($_POST["address"][$k] != "") {
                    $active = 1;
                    $is_primary = isset($_POST["is_primary_address"][$k]) ? 1 : 0; 
                    if($_POST["address_id"][$k] > 0) {
                        $active = isset($_POST["address_active"][$k]) ? 1 : 0; 
                        $contact->setAddressId($_POST["address_id"][$k]);
                    }
                    else 
                        $contact->setAddressId(0);
                    
                    $contact->setContactTypeId($_POST["contact_type"][$k]);
                    $contact->setAddress($_POST["address"][$k]);
                    $contact->setCity($_POST["city"][$k]);
                    $contact->setZip($_POST["zip"][$k]);
                    $contact->setState($_POST["state"][$k]);
                    $contact->setCountryId($_POST["country_id"][$k]);
                    $contact->setIsPrimaryAddress($is_primary);
                    $contact->setAddressActive($active);

                    $contact->saveAddressBook();
                    
                }                
            }
            s("Contact <i><b>{$contact->getFirstName} {$contact->getLastName}</i></b> saved."); 
            redirect("contact.view.php?id={$contact->getId()}");
        }
        else {
            e("Failed to save contact.");
        }
    }
#debug($_POST);
    if($_POST["id"])
        redirect("contact.edit.php?id={$_POST['id']}");
    else
        redirect("contact.add.php");
?>
