<?php
set_time_limit(10000);
error_reporting(E_ALL ^ E_NOTICE);

include_once("../config.php");
include_once("../classes/class.resize.image.inc.php");



$resize_image = new Resize_Image;

// Image to resize
$user_id = isset($_GET['u']) ? $_GET['u'] : u()->getId();

if($user_id > 0) {
    $user = new User($user_id);
    $image = $user->getPicture();
}
// Folder where the (original) images are located with trailing slash at the end
if(strtolower($image) == "male.jpeg" || strtolower($image) == "female.jpeg") {
    $images_dir = "img/";
}
else {
    $images_dir = "img/u-$user_id/";
    $images_dir = AppPath::$FILES . "/pic/$user_id/";
}

/* Some validation */
if(!@file_exists($images_dir.$image)) {
    $images_dir = "img/";
    $image = "male.jpeg";
    //exit('The requested image does not exist.');
}

// Get the new with & height
$new_width = (int)$_GET['w'];
$new_height = (int)$_GET['h'];

if($new_width < 1) $new_width = 200;
if($new_height < 1) $new_height = 200;

$resize_image->new_width = $new_width;
$resize_image->new_height = $new_height;

$resize_image->image_to_resize = $images_dir.$image; // Full Path to the file

$resize_image->ratio = true; // Keep aspect ratio

$process = $resize_image->resize(); // Output image
?>
