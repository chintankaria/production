<?php
    include_once("../config.php");
    require_perms("smt_member","admin");
    include_once("../classes/class.smtaction.inc.php");
    include_once("../classes/class.smtaction.comments.inc.php");

    $item = new SMTAction($_GET["id"]);
    if($item->getId() < 1) redirect("smtaction.list.php");
    
    sm_assign("item", $item);
    sm_display("smtaction.view.html");
?>
