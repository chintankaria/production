<?php
    include_once("../config.php");
    if($_POST["answer"] != "yes") redirect("client.list.php");

    include_once("../classes/class.client.inc.php");
    $client = new Client($_POST["id"]);
    if($client->getId() > 0) $client->delete();

    redirect("client.list.php");
