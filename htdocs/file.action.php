<?php
    include_once("../config.php");
    require_perms("manager","admin");
    include_once("../classes/class.category.inc.php");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");

    $user = u();
    $client_id = 0;
    $project_id = 0;
    if(strtoupper($_POST['docType']) == 'GENERAL_DOCUMENT') {
        // General documents are always PUBLIC
        $is_public = 1;
        $_POST['category_id'] = $_POST['gen_category_id'];
    }
    else {
        $_POST['category_id'] = $_POST['prj_cat'];
        
        if($_POST['category_id'] == 1){
            // Other Category
            $category = new Category($_POST["category_id"]);
        }
        else {
            $category = new Category($_POST["category_id"],'PROJECT_DOCUMENT');
        }

        $is_public = 0;
        $is_public = (isset($_POST["is_public"]) && strtoupper($_POST["is_public"]) == 'PUBLIC') ? 1 : 0;

        if($_POST['action'] != 'DELETE') {
            if($_POST['subproject_id'] > 0) {
                $project = new Project($_POST["subproject_id"]);
            }
            else {
                $project = new Project($_POST["project_id"]);
            }
            if($project->getId() < 1) redirect("file.list.php");

            $_POST['client_id'] = $project->getClientId();
            $_POST['project_id'] = $project->getId();
        }
    }
    $notes = $_POST['notes'];

    $file = new TimesheetFile($_POST['file_id']);
    if($file->getId()) {
        if($_POST['action'] == 'DELETE') {
            TimesheetFiles::DeleteFiles($_POST["file_id"],$user->getEmail());
        }
        else {
            $faction = (isset($_POST["efaction"]) && strtoupper($_POST["efaction"]) == 'REPLACE') ? 'REPLACE' : 'REVISION';
            $id = $file->getId();
            
            if($_FILES['file']['error']['0']) { 
                // Update details
                $file = new TimesheetFile($_POST['file_id']);
                $file->setCategoryId($_POST['category_id']);
                $file->setClientId($_POST['client_id']);
                $file->setProjectId($_POST['project_id']);
                $file->setNotes(addslashes($_POST['notes']));
                $file->setIsPublic($is_public);
                $file->setDocType('PROJECT_DOCUMENT');
                $file->save();
                $str = "Project {$file->getFileName()} File details saved.";
            }
            else {
                if($faction == 'REPLACE') {
                    // Replace file
                    if(!$_FILES['file']['error']['0']) { 
                        // File found to upload
                        TimesheetFiles::SaveUploadedFiles($id, $user->getEmail(), $category->getId(), $project->getClientId(), $project->getId(),$notes, $is_public,'file','PROJECT_DOCUMENT', $faction);
                        $str = "Project {$file->getFileName()} File replaced and saved details.";
                    }
                }
                else if($faction == 'REVISION') {
                    // Revision File
                    if(!$_FILES['file']['error']['0']) { 
                        // File found to upload
                        TimesheetFiles::SaveUploadedFiles($id, $user->getEmail(), $category->getId(), $project->getClientId(), $project->getId(),$notes, $is_public,'file','PROJECT_DOCUMENT', $faction);
                        $str = "Project {$file->getFileName()} File details saved with revision.";
                    }
                }
            }
            s("$str");
            watchdog('SUCCESS','FILE', $str);  
        }
    }
    else {
        if(!$_FILES['file']['error']['0']) {
            // File found to upload
            $id = 0;
            TimesheetFiles::SaveUploadedFiles($id, $user->getEmail(), $category->getId(), $project->getClientId(), $project->getId(),$notes, $is_public,'file','PROJECT_DOCUMENT'); 
        }
        else {
            e("No file to upload.");
        }
    }

    redirect("file.list.php?client_id={$_POST['client_id']}&project_id={$_POST['project_id']}&subproject_id={$_POST['subproject_id']}");

?>
