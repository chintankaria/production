<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.category.inc.php");
    $category = Category::GetAll('PROJECT_DOCUMENT');

    $groups = Category::GetGroups();

    sm_assign("category", $category);
    sm_assign("groups", $groups);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.category.list.html"), "Project Document Type list",null, array(3));
    	$fileName = date("d-m-Y").'_project_doc_type_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("category.list.html");
        exit;
    }
    else {
        sm_display("category.list.html");
    }
