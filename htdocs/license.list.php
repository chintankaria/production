<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.license.inc.php");
	include_once("../classes/class.licensetypes.inc.php");
    include_once("../classes/class.client.inc.php");


    $client_id = (pintval($_GET["client_id"])) ? pintval($_GET["client_id"]) : null;
    $product_id = (pintval($_GET["product_id"])) ? pintval($_GET["product_id"]) : null;
    // $license_type = null;
    // if(strtoupper($_GET["license_type"]) == 'COMMERCIAL') $license_type = 'COMMERCIAL';
    // if(strtoupper($_GET["license_type"]) == 'EVALUATION') $license_type = 'EVALUATION';

    $clients = Client::GetProductClients(true); // true is passes so that only clients who has License(s) will be returned.

    $products = Products::GetLicenseProducts();
        
    sm_assign("client_id", $client_id);
    sm_assign("clients", $clients);

    sm_assign("product_id", $product_id);
    sm_assign("products", $products);

	sm_assign("license_types", LicenseTypes::FindAll());
	
	$license_version = intval($_GET['license_version']);
	if($license_version < 1) {
		$license_version = 0;
	}
	sm_assign("license_version", $license_version);

	$license_type_id = intval($_GET['license_type_id']);
	if($license_type_id < 1) {
		$license_type_id = 0;
	}
    sm_assign("license_type_id", $license_type_id);
    $licenses = License::GetLicenses($client_id, $product_id, $license_type_id, $license_version);

    sm_assign("licenses", $licenses);

	if($_GET["excel"]){
        sm_assign("excel", 1);
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.license.list.html"), "Licenses", null, array(10));
    	$fileName = date("d-m-Y").'_license_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	else if($_GET["pdf"]){
        sm_assign("pdf", 1);
        include_once("../html2pdf_report.php");
        // Load CSS
        $handle = fopen(BASEDIR . "/htdocs/css/style-pdf.css","r");
        $css = fread($handle, filesize(BASEDIR . "/htdocs/css/style-pdf.css"));
        fclose($handle);
        
        $html = '<html>
                    <head>
                    <style type="text/css">
                        '.$css.'
                        #tbl_tasks a:hover {
                            color: #0066CB;
                            background-color: #ffffff;
                        }
                        a:hover {
                            color: #0066CB;
                            background-color: #ffffff;
                        }
                    </style>
                    </head>
                    <body>
                        <div id="content" class="d-block-table">
                            <table cellpadding="0" cellspacing="0" border="0" width="100%">
                                <tr>
                                    <td width="20%" class="pt3 pb5 pL10">
                                        <h1>Licenses</h1>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="pR5">
                                        <div class="d-dashed"></div>
                                    </td>
                                </tr>
                            </table>
                            <br>
                 ';
        $html .= sm_fetch("table.license.list.report.html");

        $html .= "
                            </div>
                        </body>
                    </html>
                 ";
        

        $pdf_path = AppPath::$FILES ."/pdf";
        if(!is_dir($pdf_path))
            @mkdir($pdf_path, 0755, true);

    	$sFileName = date("d-m-Y").'_license_list.html';
    	$dFileName = date("d-m-Y").'_license_list.pdf';

        $source_file = "$pdf_path/$sFileName";
        $destination_file = "$pdf_path/$dFileName";

        $handle = fopen($source_file, "w");
        $contents = fwrite($handle, $html);
        fclose($handle);

        // PDF Parameters
        $logo_url = "http://dev.globalss.com/shekhar/globalss/influence-v2/htdocs/img/globalsoft_logo.jpg";
        $logo_url = "http://groovesw.com/timesheet-test/htdocs/img/globalsoft_logo.jpg";

        $header ='
                <table width="100%" align="center">
                    <tr>
                        <td align="left" width="410">
                            <div>
                                <a target="_blank" href="http://globalss.com/">
                                    <img width="215" height="50" border="0" src="'.$logo_url.'"/>
                                </a>
                            </div>
                            <div style="border-bottom: 3px solid #161311;margin-top:15px"></div>
                        </td>
                    </tr>
                </table>
            ';

        $pdf_media_margins = array(
                                'left'   => 10,
                                'right'  => 10,
                                'top'    => 20,
                                'bottom' => 10
                            );
        $pdf_config = array(
                        'pagewidth'          => 1024,
                        'margins'            => array(
                                                     'left'    => 10,
                                                     'right'   => 10,
                                                     'top'     => 20,
                                                     'bottom'  => 12,
                                                ),
                        'media'             => 'Letter',
                        'method'            => 'fpdf',
                        'mode'              => 'html',
                        'cssmedia'          => 'screen',
                        'pdfversion'        => '1.6',
                        'pslevel'           => 3,
                        'scalepoints'       => '1',
                        'renderimages'      => true,
                        'renderlinks'       => true,
                        'renderfields'      => true,
                        'renderforms'       => false,
                        'html2xhtml'        => true, // new param
                        'smartpagebreak'    => true,
                        'encoding'          => false,// ''  
                   );
        $param['config'] = $pdf_config;
        $param['media_margins'] = $pdf_media_margins;
        $param['header'] = $header;

        // Generate PDF 
        generate_pdf_report($source_file, $destination_file, $param);

        download_file($destination_file);

        @unlink($source_file);
        @unlink($destination_file);
        exit;

    }

    sm_display("license.list.html");
    
