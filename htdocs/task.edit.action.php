<?php
    include_once("../config.php");
    include_once("../classes/class.task.inc.php");

    $task = new Task($_POST["id"]);
    if($task->getId() < 1) redirect("task.list.php");

    $task->setProjectId($_POST["project_id"], $_POST["subproject_id"]);
    $task->setTaskDate($_POST["task_date"]);
    $task->setDetail($_POST["detail"]);
    $task->setDurationMins($_POST["duration"]);
    $task->setTaskTypeId($_POST["task_type_id"]);

    if($task->save()){
        s("Task changes saved successfully.");
        redirect("task.view.php?id={$task->getId()}");
    }
    else{
        e("Failed to save changes. Please try again.");
        redirect("task.edit.php?id={$task->getId()}");
    }


