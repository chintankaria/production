<?php
    include_once("../config.php");
    include_once("../classes/class.employees.inc.php");
    require_perms("hr_manager","admin");

#debug($_POST);
#exit;
    $user = new User($_POST["id"]);
    if($user->getId() > 0){
        
        if(trim($_POST['txtUserName']) != $user->getEmail()) {
            if($user->getUserIdByEmailId(trim($_POST['txtUserName']))) {
                // User Name already exist
                e("<b>{$_POST['txtUserName']}</b> member already exist.");
                redirect("user.edit.php?id={$_POST["id"]}&tab={$_POST['tab']}");
            }
            $user->setEmail(trim($_POST["txtUserName"]));
        }
        $is_timesheet = isset($_POST['is_timesheet']) ? intval($_POST['is_timesheet']) : 1;  // Default is required
        $is_trainer = isset($_POST['is_trainer']) ? intval($_POST['is_trainer']) : 0;  // Default is NO

        $user->setFirstName(trim($_POST["txtFirstName"]));
        $user->setMiddleName(trim($_POST["txtMiddleName"]));
        $user->setLastName(trim($_POST["txtLastName"]));
        #$user->setGender(trim($_POST["rGender"]));
        $user->setDateOfBirth(trim($_POST["txtDOB"]));
        $user->setRoles(implode(",",$_POST['roles']));
        $status = $user->save();
        if($status) {
            if($_FILES) {
                // User Pic
                $file = array();
                foreach($_FILES['file']['error'] as $k => $v){
                    if(!$v){
                        $ext = getExtension(stripslashes($_FILES['file']['name'][$k]));
                        $imginfo_array = getimagesize($_FILES['file']['tmp_name'][$k]);
                        if ($imginfo_array !== false) {
                            if(intval($_FILES['file']['size'][$k]) > 5120) {
                                $file = array(
                                    'name' => addslashes($_FILES['file']['name'][$k]),
                                    'type' => $_FILES['file']['type'][$k],
                                    'tmp_name' => $_FILES['file']['tmp_name'][$k],
                                    'size' => $_FILES['file']['size'][$k],
                                );
                            }
                            else {
                                e("Image size limit exceeded.");
                            }
                        }
                        else {
                            e("Invalid image file uploaded.");
                        }
                    }
                }
                if(is_array($file) && count($file) > 0) {
                    if($user->SaveUserFiles($file, 'PROFILE_PIC')) {
                        $str = "Profile & Picture saved successfully";
                        s("Profile & Picture saved successfully.");
                        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                    }
                    else {
                        $str = "Failed to change picture";
                        e("Failed to change picture.");
                        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                    }
                }

                // User CV
                $file = array();
                foreach($_FILES['file_cv']['error'] as $k => $v){
                    if(!$v){
                        $file = array(
                            'name' => addslashes($_FILES['file_cv']['name'][$k]),
                            'type' => $_FILES['file_cv']['type'][$k],
                            'tmp_name' => $_FILES['file_cv']['tmp_name'][$k],
                            'size' => $_FILES['file_cv']['size'][$k],
                        );
                    }
                }
                if(is_array($file) && count($file) > 0) {
                    if($user->SaveUserFiles($file, 'USER_CV')) {
                        $str = "CV updated successfully";
                        s("CV updated  successfully.");
                        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                    }
                    else {
                        $str = "Failed to update CV";
                        e("Failed to update CV.");
                        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
                    }
                }
            }

            $employee = new Employee(0, $user->getId());
            $employee->setUserId($user->getId());
            $employee->setEmployeeCode(trim($_POST["empoyee_code"])); 
            $employee->setEmployeeTypeId(trim($_POST["empoyee_type_id"])); 
            $employee->setDepartmentId($_POST["depatment_id"]);
            $employee->setDesignationId($_POST["designation_id"]);
            $employee->setDateOfJoin(trim($_POST["txtDateOfJoining"]));
            $employee->setDateOfConfirmation(trim($_POST["txtDateOfConfirm"]));
            $employee->setDateOfResign(trim($_POST["txtDateOfResign"]));
            $employee->setNationality(trim($_POST["txtNationality"]));
            $employee->setLocationId(trim($_POST["location_id"]));
            $employee->setIsTimesheet($is_timesheet);
            $employee->setIsTrainer($is_trainer);

            // Rob asked to remove multi-department support as of now (11-June-2012), we will do it later, if required.
            #$employee->setDepartmentIds($_POST["depatment_ids"]); 

            $employee->setTrainingSkillsIds($_POST["training_skill_ids"]);
            $training_skills_level = array();
            if(isset($_POST['training_skill'])) {
                foreach($_POST['training_skill'] as $k => $v) {
                    $training_skills_level[$k] = $v;
                }
            }
            $employee->setTrainingSkillsLevels($training_skills_level);

            $employee->setProductKnowledgeIds($_POST["prod_knowledge_ids"]);
            $prod_level = array();
            if(isset($_POST['prod_level'])) {
                foreach($_POST['prod_level'] as $k => $v) {
                    $prod_level[$k] = $v;
                }
            }
            $employee->setProductKnowledgeLevels($prod_level);

            $employee->setTechPracticeIds($_POST["tech_practice_ids"]);
            // Rob asked to remove Expertise from the Technology Practice Area 20-June-2012
            /*
            $tech_practice_level = array();
            if(isset($_POST['tech_practice'])) {
                foreach($_POST['tech_practice'] as $k => $v) {
                    $tech_practice_level[$k] = $v;
                }
            }
            $employee->setTechPracticeLevels($tech_practice_level);
            */

            $employee->setTechSkillsIds($_POST["tech_skills_ids"]);
            $tech_skill_level = array();
            if(isset($_POST['tech_skills'])) {
                foreach($_POST['tech_skills'] as $k => $v) {
                    $tech_skill_level[$k] = $v;
                }
            }
            $employee->setTechSkillsLevels($tech_skill_level);

            $employee->setPassportNumber($_POST["txtPassportNumber"]);
            $employee->setPassportIssueDate($_POST["txtDateOfIssue"]);
            $employee->setPassportExpiryDate($_POST["txtDateOfExpiry"]);
            $employee->setPassportPlaceOfIssue($_POST["txtPlaceOfIssue"]);
            $employee->setPassportCountryOfResidence($_POST["country_of_residance"]);

            if($_POST['txtPersonalEmail'] == '')
                $_POST['txtPersonalEmail'] = $_POST['txtUserName'];

            $employee->setPersonalEmail($_POST["txtPersonalEmail"]);
            $employee->setMobile1($_POST["txtMobile1"]);
            $employee->setMobile2($_POST["txtMobile2"]);
            $employee->setPhone1($_POST["txtPhone1"]);
            $employee->setPhone2($_POST["txtPhone2"]);
            $employee->setAddress($_POST["txtAddress"]);
            $employee->setCity($_POST["txtCity"]);
            $employee->setZip($_POST["txtZip"]);
            $employee->setState($_POST["txtState"]);
            $employee->setCountryId($_POST["country_id"]);
            $employee->setSkypeId($_POST["txtSkypeId"]);
            $employee->setYahooId($_POST["txtYahooId"]);
            $employee->setMsnId($_POST["txtMsnId"]);
            $employee->setGtalkId($_POST["txtGtalkId"]);

            // Save Employee Details
            $employee->save();

            // Visa Info

            // Delete all visa information
            $employee->deletedVisaInfo();

            foreach($_POST['visa_id'] as $k => $v) {
                if($k == 0) 
                    continue;

                $_POST['visa_type'][$k] = remove_whitespace($_POST['visa_type'][$k]);
                $_POST['vcountry'][$k] = remove_whitespace($_POST['vcountry'][$k]);
                $_POST['vissue_date'][$k] = remove_whitespace($_POST['vissue_date'][$k]);
                $_POST['vexpiry_date'][$k] = remove_whitespace($_POST['vexpiry_date'][$k]);
                $_POST['vplace_of_issue'][$k] = remove_whitespace($_POST['vplace_of_issue'][$k]);

                $employee->setVisaId($_POST['visa_id'][$k]);
                $employee->setVisaType($_POST['visa_type'][$k]);
                $employee->setVisaCountry($_POST['vcountry'][$k]);
                $employee->setVisaIssueDate($_POST['vissue_date'][$k]);
                $employee->setVisaExpiryDate($_POST['vexpiry_date'][$k]);
                $employee->setVisaPlaceOfIssue($_POST['vplace_of_issue'][$k]);
                $employee->saveVisaInfo();
            }
        }
    }
    else $status = false;

    if($status) {
        $str = "Member information updated ";
        s("Member information updated.");
        watchdog('SUCCESS','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})<i>");
    }
    else {
        $str = "Failed to update member information";
        e("Failed to update member information.");
        watchdog('FAILED','EDIT', "$str for <i>{$user->getFullName()}({$user->getEmail()})</i>");
        redirect("user.edit.php?id={$_POST["id"]}&tab={$_POST['tab']}");
    }

    redirect("user.view.php?id={$_POST["id"]}&tab={$_POST['tab']}");
    #redirect("user.list.php");
