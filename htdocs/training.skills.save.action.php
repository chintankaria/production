<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.training.skills.inc.php");
    $training_skills = new TrainingSkills($_POST["id"]);
    $training_skills->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($training_skills->delete()) {
            $str = "Training Skills <i><b>{$_POST['name']}</b></i> deleted.";
            s("Department  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete training skills.";
            e("Failed to delete training_skills.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$training_skills->checkName()) {
            if($training_skills->save()) {
                $str = "Training Skills  <i><b>{$training_skills->getName()}</b></i> saved.";
                s("Training Skills  <i><b>{$training_skills->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save training skills {$_POST["name"]}";
                e("Failed to save training skills {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Training Skills <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("training.skills.list.php");
    }
