<?php
    include_once("../config.php");
    require_perms("manager","admin");
    include_once("../classes/class.category.inc.php");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.timesheetfiles.inc.php");

    $user = u();
    $project = new Project($_POST["id"]);

    if($project->getId() < 1) redirect("project.list.php");

    if(isset($_POST['action']) && $_POST['action'] == 'FILES') {
        $_POST['project_id'] = $_POST['id'];
        $_POST['category_id'] = $_POST['prj_cat'];

        if($_POST['category_id'] == 1){
            // Other Category
            $category = new Category($_POST["category_id"]);
        }
        else {
            $category = new Category($_POST["category_id"],'PROJECT_DOCUMENT');
        }
        $is_public = 0;
        if($category->getId() < 1) redirect("project.view.php?id={$_POST['project_id']}");

        $is_public = (isset($_POST["is_public"]) && strtoupper($_POST["is_public"]) == 'PUBLIC') ? 1 : 0;
        $notes = $_POST['notes'];
       
        $file = new TimesheetFile($_POST['file_id']);
        if($file->getId()) {
            $faction = (isset($_POST["efaction"]) && strtoupper($_POST["efaction"]) == 'REPLACE') ? 'REPLACE' : 'REVISION';
            $id = $file->getId();
            
            if($_FILES['file']['error']['0']) { 
                // Update details
                $file = new TimesheetFile($_POST['file_id']);
                $file->setCategoryId($_POST['category_id']);
                $file->setClientId($_POST['client_id']);
                $file->setProjectId($_POST['project_id']);
                $file->setNotes(addslashes($_POST['notes']));
                $file->setIsPublic($is_public);
                $file->setDocType('PROJECT_DOCUMENT');
                $file->save();
                $str = "Project {$file->getFileName()} File details saved.";
            }
            else {
                if($faction == 'REPLACE') {
                    // Replace file
                    if(!$_FILES['file']['error']['0']) { 
                        // File found to upload
                        TimesheetFiles::SaveUploadedFiles($id, $user->getEmail(), $category->getId(), $project->getClientId(), $project->getId(),$notes, $is_public,'file','PROJECT_DOCUMENT', $faction);
                        $str = "Project {$file->getFileName()} File replaced and saved details.";
                    }
                }
                else if($faction == 'REVISION') {
                    // Revision File
                    if(!$_FILES['file']['error']['0']) { 
                        // File found to upload
                        TimesheetFiles::SaveUploadedFiles($id, $user->getEmail(), $category->getId(), $project->getClientId(), $project->getId(),$notes, $is_public,'file','PROJECT_DOCUMENT', $faction);
                        $str = "Project {$file->getFileName()} File details saved with revision.";
                    }
                }
            }
            s("$str");
            watchdog('SUCCESS','FILE', $str);  
        }
        else {
            if(!$_FILES['file']['error']['0']) { 
                // File found to upload
                $id = 0;
                TimesheetFiles::SaveUploadedFiles($id, $user->getEmail(), $category->getId(), $project->getClientId(), $project->getId(),$notes, $is_public,'file','PROJECT_DOCUMENT');
            }
            else {
                e("No file to upload.");
            }
        }
        redirect("project.edit.php?id={$project->getId()}");
    }
    else if($_POST['action'] == 'DELETE') {
        TimesheetFiles::DeleteFiles($_POST["file_id"],$user->getEmail());
        redirect("project.edit.php?id={$project->getId()}");
    }
    else {
        $project->setName($_POST["name"]);
        $project->setStatus($_POST["status"]);
        $project->setShowInCalendar($_POST["show_in_calendar"]);
        $project->setDescription($_POST["description"]);
        // $project->setClientId($_POST["client_id"]);
        // $project->setParentId($_POST["parent_id"]);
        $project->setStartDate($_POST["start_date"]);
        $project->setEndDate($_POST["end_date"]);
        $project->setIsBillable($_POST["is_billable"]);
        $return_val = $project->compareDate($_POST["start_date"],$_POST["end_date"]);
        if($return_val){
            e("Invalid project start date and end date ({$return_val})");
            p($_POST);
            redirect("project.edit.php?id={$project->getId()}");
        }
        $project->setAllowSubprojects($_POST["allow_subprojects"]);
        $project->setAllowTaskTypes($_POST["allow_task_types"]);

        $params = array(
                        "users" => $_POST["users"],
                        "manager" => $_POST["manager"]
                        );
        if($project->save()){
            $project->setUsers($params);
            s("Project changes saved.");
            $str = "<i>{$_POST['name']}</i> project changes saved.";
            watchdog('SUCCESS','EDIT', $str);
            redirect("project.view.php?id={$project->getId()}");
        }
        else{
            $str = "Failed to save <i>{$_POST['name']}</i> project changes.";
            e("Failed to save project changes.");
            watchdog('FAILED','EDIT', $str);
            p($_POST);
            redirect("project.edit.php?id={$project->getId()}");
        }
    }
