<?php
    include_once("../config.php");
    include_once("../classes/class.task.inc.php");
    include_once("../classes/class.project.inc.php");

    /*
    if($_GET["project_id"]){
        $project = new Project($_GET["project_id"]);

        if($project && $project->getId() > 0){
            sm_assign("project", $project);
            $tasks = $project->getTasks();
        }
    }
    elseif($_GET["user_id"]){
        $user = new User($_GET["user_id"]);
        if($user && $user->getId() > 0){
            $tasks = $user->getTasks();
            sm_assign("user", $user);
        }
        else redirect("task.list.php");
    }
    else $tasks = u()->getTasks();
    // else redirect("task.list.weekly.php");
    */

    $params = $_GET;
    $tasks = Task::GetTasks($params);

    if(u()->isAdmin()){
        $users = User::GetUsers();
        sm_assign("users", $users);
    } else {
		$users_list = array();
		$users = u();
		$users_list['manager_id'] = $users->getid();
		$users_list['member_id'] = $_GET['user_id'];
        sm_assign("users_list", $users_list);
	}
    //debug($users);
    sm_assign("params", $params);
    sm_assign("tasks", $tasks);
    sm_display("task.list.html");
