<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.project.practicearea.inc.php");
    $project_practice_area = new ProjectPracticeArea($_POST["id"]);
    $project_practice_area->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($project_practice_area->delete()) {
            $str = "Project Practice Area <i><b>{$_POST['name']}</b></i> deleted.";
            s("Practice Area  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete project Practice Area.";
            e("Failed to delete project Practice Area.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$project_practice_area->checkName()) {
            $project_practice_area->setIsActive(1);
            if($project_practice_area->save()) {
                $str = "Project Practice Area  <i><b>{$project_practice_area->getName()}</b></i> saved.";
                s("Project  Practice Area  <i><b>{$project_practice_area->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save project Practice Area {$_POST["name"]}";
                e("Failed to save project Practice Area {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Project Practice Area <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("project.practice.list.php");
    }
