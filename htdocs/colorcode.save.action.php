<?php
    include_once("../config.php");
    include_once("../classes/class.colorcode.inc.php");
    require_perms("admin");
    if(ColorCode::saveCalendarColors($_POST)) {
        s("Calendar color codes save."); 
    } 
    else {
        e("Failed to save calendar color codes.");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("colorcode.list.php");
    }
?>
