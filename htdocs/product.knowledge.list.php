<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.product.knowledge.inc.php");
    $product_knowledge = ProductKnowledge::GetAll();

    sm_assign("product_knowledge", $product_knowledge);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.product_knowledge.list.html"), "Product Knowledge list",null, array(3));
    	$fileName = date("d-m-Y").'_product_knowledge.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("product.knowledge.list.html");
        exit;
    }
    else {
        sm_display("product.knowledge.list.html");
    }
