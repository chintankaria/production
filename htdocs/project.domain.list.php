<?php
include_once("../config.php");
require_perms("admin");

include_once("../classes/class.project.domain.inc.php");
$project_domain = ProjectDomain::GetDomains();

sm_assign("project_domain", $project_domain);

if ($_GET["excel"]) {
    include_once("../classes/class.excelwriter.inc.php");
    $excel = new ExcelWriter();
    $excel->setHTMLTable(sm_fetch("table.project.domain.list.html"), "Project Domain", null, array(3));
    $fileName = date("d-m-Y") . '_project_domain.xls';
    ob_clean();
    $excel->send($fileName);
    $excel->close();
    exit;
}
if ($_REQUEST['fetch'] == 1) {
    sm_assign("fetch", 1);
    print sm_fetch("project.domain.list.html");
    exit;
} else {
    sm_display("project.domain.list.html");
}
