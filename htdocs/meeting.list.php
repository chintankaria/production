<?php
    include_once("../config.php");
    include_once("../classes/class.meeting.inc.php");

    include_once("../classes/class.meetingtypes.inc.php");
    $meeting_types = MeetingTypes::GetAll();
    $durations = Meeting::$MeetingDurations;

    $meeting_type_id = isset($_GET["meeting_type_id"]) ? $_GET["meeting_type_id"] : 0;
    $revision = isset($_GET["revision"]) ? $_GET["revision"] : 0;
    $from_date = (isset($_GET["from_date"]) && is_date($_GET["from_date"])) ? $_GET["from_date"] : date("m")."/1/".date("Y");
    $to_date = (isset($_GET["to_date"]) && is_date($_GET["to_date"])) ? $_GET["to_date"] : date("m/d/Y",strtotime("tomorrow"));

    $user = u();
    $meetings = Meeting::GetMeetings($meeting_type_id,$from_date,$to_date,$revision);
    if($meetings) {
        foreach($meetings as $k => $v) {
            $v->duration = $durations[$v->getMeetingDuration()];
        }
    }
    $excel_no_field = 8;
    sm_assign("revision", $revision);
    sm_assign("from_date", $from_date);
    sm_assign("to_date", $to_date);
    sm_assign("meetings",$meetings);
    sm_assign("meeting_types", $meeting_types);
    sm_assign("meeting_type_id", $meeting_type_id);

    if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
		$excel->setHTMLTable(sm_fetch("table.meeting.list.html"), "Meeting List", null, array($excel_no_field));    	
    	$fileName = date("d-m-Y").'_meeting_list.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    
    sm_display("meeting.list.html");
?>
