<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.employee.types.inc.php");
    $employee_types = new EmployeeTypes($_POST["id"]);
    $employee_types->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($employee_types->delete()) {
            s("Employee Type  <i><b>{$_POST['name']}</b></i> deleted.");
            $str = "Employee Type  <i><b>{$_POST['name']}</b></i> deleted.";
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            e("Failed to delete employee type.");
            $str = "Failed to delete employee type.";
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$employee_types->checkName()) {
            if($employee_types->save()) {
                s("Employee Type <i><b>{$employee_types->getName()}</b></i> saved.");
                $str = "Employee Type <i><b>{$employee_types->getName()}</b></i> saved.";
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                e("Failed to save employee type {$_POST["name"]}");
                $str = "Failed to save employee type {$_POST["name"]}";
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Employee Type <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("employeetypes.list.php");
    }
