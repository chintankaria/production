<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.licenseperiods.inc.php");
    $licenseperiods = LicensePeriods::GetAll();

    sm_assign("licenseperiods", $licenseperiods);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.licenseperiods.list.html"), "License periods",null, array(3));
    	$fileName = date("d-m-Y").'_licenseperiods.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
	
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("licenseperiods.list.html");
        exit;
    }
    else {
        sm_display("licenseperiods.list.html");
    }
