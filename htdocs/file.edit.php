<?php

include_once("../config.php");
include_once("../classes/class.timesheetfile.inc.php");
include_once("../classes/class.client.inc.php");
include_once("../classes/class.project.inc.php");
include_once("../classes/class.category.inc.php");


$id = intval($_GET["id"]);
$file = new TimesheetFile($id);
if($file->getId() < 1) redirect("file.list.php");

$project_id = 0;
$subproject_id = 0;

$client_id = pintval($_GET["client_id"]);
$project_id = pintval($_GET["project_id"]);
$subproject_id = pintval($_GET["subproject_id"]);


$clients = Client::GetClients();
$projects = array();
$subprojects = array();

$category = array();
$category = Category::GetAll();
if($client_id < 1)
    $client_id = $file->getClientId();

if($file->getParentProjectId()) {
    if($project_id < 1) {
        $project_id = $file->getParentProjectId();
    }
    if($subproject_id < 1) {
        $subproject_id = $file->getProjectId();
    }
}
else {
    if($project_id < 1) {
        $project_id = $file->getProjectId();
    }
}

if($clients[$client_id]){
    $projects = Project::GetProjectsByClient($client_id, true);
    
    if($projects[$project_id]){
        $subprojects = Project::GetSubprojectsByProject($project_id);
        
        if(!$subprojects[$subproject_id]) $subproject_id = 0;
    }
    else{
    	$project_id = 0;
    }
}
else{
	$client_id = 0;
}

sm_assign("category", $category);

sm_assign("clients", $clients);
sm_assign("client_id", $client_id);

sm_assign("projects", $projects);
sm_assign("project_id", $project_id);

sm_assign("subprojects", $subprojects);
sm_assign("subproject_id", $subproject_id);
sm_assign("action", "UPDATE");
sm_assign("file", $file);

sm_display("file.add.html");

?>
