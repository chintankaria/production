<?php
    include_once("../config.php");
    include_once("../classes/class.meeting.inc.php");

    $durations = Meeting::$MeetingDurations;
    $meeting = new Meeting($_GET["id"]);
    if($meeting->getId() < 1) redirect("meeting.list.php");
    
    $meeting->duration = $durations[$meeting->getMeetingDuration()];
    sm_assign("meeting", $meeting);
    sm_display("meeting.view.html");
?>
