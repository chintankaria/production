<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.product.knowledge.inc.php");
    $product_knowledge = new ProductKnowledge($_POST["id"]);
    $product_knowledge->setName($_POST["name"]);
    if($_POST['action'] == "DELETE") {
        if($product_knowledge->delete()) {
            $str = "Product knowledge  <i><b>{$_POST['name']}</b></i> deleted.";
            s("Product knowledge  <i><b>{$_POST['name']}</b></i> deleted.");
            watchdog('SUCCESS','DELETE', "$str");
        }
        else {
            $str = "Failed to delete product knowledge.";
            e("Failed to delete product knowledge.");
            watchdog('FAILED','DELETE', "$str");
        }
    }
    else {
        if(!$product_knowledge->checkName()) {
            if($product_knowledge->save()) {
                $str = "Product knowledge  <i><b>{$product_knowledge->getName()}</b></i> saved.";
                s("Product knowledge  <i><b>{$product_knowledge->getName()}</b></i> saved.");
                if($_POST['id'] > 0) 
                    watchdog('SUCCESS','EDIT', "$str");
                else
                    watchdog('SUCCESS','ADD', "$str");
            }
            else {
                $str = "Failed to save product knowledge {$_POST["name"]}";
                e("Failed to save product knowledge {$_POST["name"]}");
                if($_POST['id'] > 0) 
                    watchdog('FAILED','EDIT', "$str");
                else
                    watchdog('FAILED','ADD', "$str");
            }
        }
        else e("Product knowledge <i><b>{$_POST["name"]}</b></i> already exist!");
    }
    if($_POST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print "Done";
        exit;
    }
    else {
        redirect("product.knowledge.list.php");
    }
