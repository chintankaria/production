<?php
    include_once("../config.php");
    require_perms("admin");

    include_once("../classes/class.technology.skills.inc.php");
    $technology_skills = TechnologySkills::GetAll();

    sm_assign("technology_skills", $technology_skills);
	
	if($_GET["excel"]){
	    include_once("../classes/class.excelwriter.inc.php");
    	$excel = new ExcelWriter();
    	$excel->setHTMLTable(sm_fetch("table.technology.skills.list.html"), "Technology Skills",null, array(3));
    	$fileName = date("d-m-Y").'_technology_skills.xls';
    	ob_clean();
    	$excel->send($fileName);
    	$excel->close();
    	exit;
    }
    if($_REQUEST['fetch'] == 1) {
        sm_assign("fetch", 1);
        print sm_fetch("technology.skills.list.html");
        exit;
    }
    else {
        sm_display("technology.skills.list.html");
    }
