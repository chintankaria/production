<?php
    include_once("../config.php");
    require_perms("admin");
    include_once("../classes/class.client.inc.php");
    include_once("../classes/class.client.contacts.inc.php");
    include_once("../classes/class.products.inc.php");
    include_once("../classes/class.licenseperiods.inc.php");
    include_once("../classes/class.license.inc.php");
	include_once("../classes/class.licensetypes.inc.php");

    $license = new License($_GET["id"]);
    if($license->getId() < 1) redirect("license.list.php");
	
    if($license->getLicenseType() == 'Evaluation' && strtotime($license->getLicenseEndDate()) < time())
    {
        sm_assign("license_expired", 1);
    }
    else
    {
        sm_assign("license_expired", 0);
    }

    $clients = Client::GetProductClients();
    sm_assign("clients", $clients);

    $client_contacts = ClientContact::GetContacts($license->getClientId());
    sm_assign("client_contacts", $client_contacts);

    $products = Products::GetAll();
    sm_assign("products", $products);

    $licenseperiods = LicensePeriods::GetAll();
    sm_assign("licenseperiods", $licenseperiods);

    $sla_adjustments = License::GetSlaAdjustments($_GET["id"]);
    sm_assign("sla_adjustments", $sla_adjustments);
	
	$modules = $license->getModules();
	$module_names = array();
	if(is_array($modules)) {
		foreach($modules as $module) {
			$module_names[] = $module->getName();
		}
	}
	sm_assign("module_names", implode(', ', $module_names));
	
	$license_types = LicenseTypes::FindAll();
	sm_assign("license_types", $license_types);

    $downloads = License::GetDownloads($_GET["id"]);
    if($downloads)
    {
        foreach($downloads as $k=>$v)
        {
            $dateTime = new DateTime($downloads[$k]['downloaded_on']);
            $dateTime->setTimeZone(new DateTimeZone('PST'));
            $downloads[$k]['downloaded_on'] = $dateTime->format('m/d/Y H:i:s T');
        }
    }
    sm_assign("downloads", $downloads);

    sm_assign("license", $license);

	if($_GET["sla"])
    {
        $file = AppPath::$FILES . '/license/' . $license->getId() . '/' . $license->getSlaDoc();
        download_file($file);
    	exit;
    }
	elseif($_GET["sla_adj"])
    {
        $file = AppPath::$FILES . '/license/' . $license->getId() . '/sla_adjs/' . $_GET["sla_adjustment_doc"];
        download_file($file);
    	exit;
    }
    sm_display("license.view.html");
