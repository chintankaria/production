<?php
    include_once("../config.ajax.php");
    include_once("../classes/class.project.inc.php");
    include_once("../classes/class.client.inc.php");

    if($_GET["client_id"]){
        $client = new Client($_GET["client_id"]);
        if($client && $client->getId() > 0) $projects = $client->getProjects();
        else error404();
    }
    elseif($_GET["parent_id"]){
        $parent = new Project($_GET["parent_id"]);
        if($parent && $parent->getId() > 0) $projects = $parent->getSubprojects();
        else error404();
    }
    else $projects = Project::GetProjects();

    if($projects){
        $json = array();
        foreach($projects as $project){
            $o = new stdClass();
            $o->id = $project->getId();
            $o->name = $project->getName();
            $json[] = $o;
        }
        ob_clean();
        print json_encode($json);
    }
    else error404();
