<?php
class TriviaResponses {
    private static $_Responses = null;

    private $_id = null;
    private $_parentId = null;
    private $_triviaId = null;
    private $_userId = null;
    private $_dateTime = null;
    private $_title = null;
    private $_response = null;
    private $_rating = null;
    private $_createDate = null;
    private $_createdBy = null;
    private $_deleted = null;
    private $_deleteDate = null;
    private $_deletedBy = null;

    private $_childrens = null;
    private $_points = null;
    private $_replies = null;
    private $_owner = null;
    private $_ownerPic = null;

    public function __construct($id = null) {
        $id = pintval($id);
        if($id > 0) {
            $sql = "SELECT a.*
                      CONCAT(b.first_name,' ',b.last_name) as owner,b.gender
                    FROM trivia_responses a
                    INNER JOIN users b ON b.id = a.user_id
                    WHERE a.id = $id
                    ";
            $row = db_get_row($sql);
            if($row) self::_Init($row, $this);
        }
    }
    private static function _Init($array, $object = null){
        if(!$object instanceof self) $object = new self;

        $object->_id = $array["id"];
        $object->_parentId = $array["parent_id"];
        $object->_triviaId = $array["trivia_id"];
        $object->_userId = $array["user_id"];
        $object->_dateTime = $array["date_time"];
        $object->_title = $array["title"];
        $object->_response = $array["response"];
        $object->_rating = $array["rating"]; 
        $object->_createDate = $array["create_date"];
        $object->_createdBy = $array["created_by"];
        $object->_deleted = $array["deleted"];
        $object->_deleteDate = $array["deleted_date"];
        $object->_deletedBy = $array["deleted_by"];

        $object->_owner = $array["owner"];
        $sql = "SELECT * FROM users_pic WHERE user_id = {$array["user_id"]}";
        $result = db_get_row($sql);
        if($result) {
            $pic = "img/u-{$array["id"]}/".$result["md5_name"];
            $pic = $result["md5_name"];
        }
        else {
            $pic = strtolower($array["gender"]).".jpeg";
        }
        $object->_ownerPic = $pic; 


        return $object;
    }
    public function getId() {
        return $this->_id;
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getOwner() {
        return $this->_owner;
    }
    public function getOwnerPic() {
        return $this->_ownerPic;
    }
    public function getDateTime($format = 'm/d/Y H:i') {
        return date($format, strtotime($this->_dateTime));
    }
    public function getTitle() {
        return $this->_title;
    }
    public function getResponse() {
        return nl2br($this->_response);
    }
    public function getCreateDate($format = 'm/d/Y H:i') {
        return date($format, strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }
    public function getDeleted() {
        return $this->_deleted;
    }
    public function getDeletedDate($format = 'm/d/Y H:i') {
        return date($format, strtotime($this->_deleteDate));
    }
    public function getDeletedBy() {
        return $this->_deletedBy;
    }

    public function setTriviaId($trivia_id) {
        $this->_triviaId = $trivia_id;
    }
    public function setParentId($parent_id) {
        $this->_parentId = $parent_id;
    }
    public function setUserId($user_id) {
        $this->_userId = $user_id;
    }
    public function setDateTime($date_time) {
        $this->_dateTime = ymdhms(is_date($date_time));
    }
    public function setTitle($title) {
        $this->_title = $title;
    }
    public function setResponse($response) {
        $this->_response = $response;
    }
    public function setCreatedBy($created_by){
        $this->_createdBy = trim($created_by);
        if(!$this->_createdBy) e("Missing or invalid idea creator.");
    }
    public function setDeletedBy($deleted_by){
        $this->_deletedBy = trim($deleted_by);
        if(!$this->_deletedBy) e("Missing or invalid idea deletor.");
    }
    public function save(){
        #if($this->_id) return $this->_update();
        #else return $this->_add();
        return $this->_add();
    }

    private function _add() {
        if($this->_response){
            $sql = "INSERT INTO trivia_responses (user_id,trivia_id, parent_id, date_time, response, create_date, created_by)
                        VALUES(
                            '". n($this->_userId) ."',
                            '". $this->_triviaId ."',
                            '". $this->_parentId ."',
                            '". $this->_dateTime ."',
                            ". q($this->_response) . ",
                            NOW(),
                            '". u()->getEmail() ."'
                        )
                    ";
            if(db_execute($sql)){
                $this->_id = db_insert_id();
                return true;
            }
        }
        return false;
    }
    private function _update() {
        if($this->_id) {
            $sql = "UPDATE trivia_responses SET
                    WHERE id = $this->_id
                    ";
            return db_execute($sql);
        }
        return false;
    }

    public function Delete() {
        if($this->_id) {
            $sql = "UPDATE trivia_responses SET
                        deleted = 1,
                        deleted_date = NOW(),
                        deleted_by = '".u()->getEmail()."'
                        WHERE id = {$this->_id}
                    ";
            return db_execute($sql);
        }
        return false;
    }
    
    public function getReplies() {
        $this->_loadReplies;
        return $this->_replies;
    }
    public function getChildrens() {
        $this->_loadChildrens();
        return $this->_childrens;
    }
    private function _loadReplies() {
        if(is_null($this->_replies)) {
            $sql = "SELECT a.*,
                        CONCAT(b.first_name,' ',b.last_name) as owner,a.gender
                    FROM trivia_responses a
                    INNER JOIN users b ON b.id = a.user_id
                    WHERE a.parent_id > 0 AND a.parent_id = {$this->_id}
                    ORDER BY a.date_time ASC
                    ";
            $rRows = db_get_all($sql);
            if($rRows) {
                foreach($rRows as $rRow) {
                    $this->_replies[$rRow["id"]] = self::_Init($rRow);
                }
            }
        }
    }
    private function _loadChildrens() {
        if(is_null($this->_childrens)) {
            $sql = "SELECT a.*,
                            CONCAT(b.first_name,' ',b.last_name) as owner,b.gender
                        FROM trivia_responses a
                        INNER JOIN users b ON b.id = a.user_id
                        WHERE a.parent_id > 0 AND a.parent_id = {$this->_id}
                        ORDER BY a.date_time ASC
                        ";
            $cRows = db_get_all($sql);
            if($cRows) {
                foreach($cRows as $cRow) {
                    $this->_childrens[$cRow["id"]] = self::_Init($cRow);
                    $sql = "SELECT a.*,
                                    CONCAT(b.first_name,' ',b.last_name) as owner,b.gender
                                FROM trivia_responses a
                                INNER JOIN users b ON b.id = a.user_id
                                WHERE a.parent_id > 0 AND a.parent_id = {$cRow["id"]}
                                ORDER BY a.date_time ASC
                                ";
                        $rRows = db_get_all($sql);
                        if($rRows) {
                            foreach($rRows as $rRow) { 
                                $this->_childrens[$cRow["id"]]->_replies[$rRow["id"]] = self::_Init($rRow);
                            }
                        }
                }
            }
        }
    } 
    public function GetResponsePoints() {
        if(is_null($this->_points)) {
            $this->_points[$this->_userId] = TriviaPoints::GetTriviaPoints($this->_userId,$this->_triviaId,$this->_id); 
        } 
        return $this->_points;
    }
    public function GetResponseList($trivia_id = 0) {
        if($trivia_id > 0) {
            $sql = "SELECT a.*,
                        CONCAT(b.first_name,' ',b.last_name) as owner, b.gender
                    FROM trivia_responses a
                    INNER JOIN users b ON b.id = a.user_id
                    WHERE a.parent_id = 0 AND a.trivia_id = $trivia_id
                    ORDER BY a.date_time ASC
                    ";
            $rows = db_get_all($sql);
            foreach($rows as $row){
                self::$_Responses[$row["id"]] = self::_Init($row);

                // get points for each responses
                self::$_Responses[$row["id"]]->_points[$row["user_id"]] = TriviaPoints::GetTriviaPoints($row['user_id'],$trivia_id,$row['id']);
                
                // Get responses reply's
                $sql = "SELECT a.*,
                            CONCAT(b.first_name,' ',b.last_name) as owner, b.gender
                        FROM trivia_responses a
                        INNER JOIN users b ON b.id = a.user_id
                        WHERE a.parent_id > 0 AND a.parent_id = {$row["id"]}
                        ORDER BY a.date_time ASC
                        ";
                $cRows = db_get_all($sql);
                if($cRows) {
                    foreach($cRows as $cRow) {  
                        self::$_Responses[$row["id"]]->_childrens[$cRow["id"]] = self::_Init($cRow);
                        $sql = "SELECT a.*,
                                    CONCAT(b.first_name,' ',b.last_name) as owner, b.gender
                                FROM trivia_responses a
                                INNER JOIN users b ON b.id = a.user_id
                                WHERE a.parent_id > 0 AND a.parent_id = {$cRow["id"]}
                                ORDER BY a.date_time ASC
                                ";
                        $rRows = db_get_all($sql);
                        if($rRows) {
                            foreach($rRows as $rRow) {  
                                self::$_Responses[$row["id"]]->_childrens[$cRow["id"]]->_replies[$rRow["id"]] = self::_Init($rRow);
                            }
                        }

                    }
                }
            }
            return self::$_Responses;
        }
    }
    public function getTotalResponses($trivia_id) {
        $total = 0;
        if($trivia_id > 0) {
            $sql = "SELECT count(id) as total FROM trivia_responses WHERE trivia_id = $trivia_id";
            $row = db_get_row($sql);
            if($row) 
                $total = $row["total"];
        }
        return $total;
    } 

}
?>
