<?php
include_once("class.calendarevent.inc.php");
include_once("class.colorcode.inc.php");

class CalendarEvents {
    
    public static function GetJavascriptEventsByUser($user, $param){
        return self::_GetJavascriptEvents('user', $user, $param);
    }

    public static function GetJavascriptEvent($id){
        return self::_GetJavascriptEvents('event', $id);
    }

    private static function _GetJavascriptEvents($type, $type_value, $param){
        $holidayin = array('INDIA' => 'India', 'USA' => 'USA');
        $conditions = array();
        $Hconditions = array();

        if($type == 'user'){
            $user = $type_value;

            if($user->isAdmin()){
                $user_id = intval($param["user_id"]);
                if($user_id > 0){
                    #$conditions[] = "a.user_id IN (0, {$user_id})";
                    #$Hconditions[] = "a.user_id IN (0, {$user_id})";
                }
            }
            else if($user->isCalendarManager()){
                $user_id = intval($param["user_id"]);
                if($user_id > 0){
                    #$conditions[] = "a.user_id IN (0, {$user_id})";
                    #$Hconditions[] = "a.user_id IN (0, {$user_id})";
                }
                else {
                    #$project_ids = $user->getProjectIds();
                    $project_ids = array();
                    $sql = "SELECT a.id
                            FROM users a
                            INNER JOIN project_users b ON b.user_id = a.id
                            WHERE b.project_id IN (".implode(", ", $project_ids).")
                            AND NOT a.blocked AND a.id >1 GROUP BY b.user_id";
                    #$rows = db_get_all($sql);
                    #$user_ids = array();
                    #foreach($rows as $row) $user_ids[] = $row["id"];

                    #$conditions[] = 'a.user_id IN (0,' . implode(', ', $user_ids) . ')';
                    #$Hconditions[] = 'a.user_id IN (0,' . implode(', ', $user_ids) . ')';
                }
            }
            else{
                $user_id = u()->getId();
                #if($user_id > 0) $user_ids[] = $user_id;
                $user_ids = array();

                #$conditions[] = 'a.user_id IN (0,' . implode(', ', $user_ids) . ')';
                #$Hconditions[] = 'a.user_id IN (0,' . implode(', ', $user_ids) . ')';
            }
            if(isset($param['client_id']) && $param['client_id'] > 0) {
                $conditions[] = "a.client_id = '{$param['client_id']}'";
                $Hconditions[] = "a.client_id = '0'";
            }
            if(isset($param['project_id']) && $param['project_id'] > 0) {
                $conditions[] = "a.project_id = '{$param['project_id']}'";
                $Hconditions[] = "a.project_id  = '0'";
            }
            if(isset($param['subproject_id']) && $param['subproject_id'] > 0) {
                $conditions[] = "a.subproject_id = '{$param['subproject_id']}'";
                $Hconditions[] = "a.subproject_id = '0'";
            }
            $start_date = $param['start_date'];
            $end_date = $param['end_date'];
            $conditions[] = "(a.start_date BETWEEN '{$start_date}' AND '{$end_date}')";
            $Hconditions[] = "(a.start_date BETWEEN '{$start_date}' AND '{$end_date}')";
        }
        elseif($type == 'event'){
            $event_id = intval($type_value);
            $conditions[] = "a.id = '{$event_id}'";
        }
        else{
            $type = false;
        }

        $events = array();

        if($type){
            if($conditions){
                $conditions = 'WHERE ' . implode(' AND ', $conditions);
            }
            if($Hconditions){
                $Hconditions = 'WHERE ' . implode(' AND ', $Hconditions);
            }

            $sql = "
                (SELECT 
                    a.id, a.user_id, a.title, a.description, a.is_holiday, a.holiday_in, a.status, 
                    UNIX_TIMESTAMP(a.start_date) AS start_date, UNIX_TIMESTAMP(a.end_date) AS end_date,
                    UNIX_TIMESTAMP(a.create_date) as create_date, a.created_by, UNIX_TIMESTAMP(a.update_date) as update_date, a.updated_by,
                    a.client_id, a.project_id, a.subproject_id, IFNULL(b.id, 0) AS individual_event, b.first_name, b.middle_name, b.last_name,
                    c.name AS client_name, d.name AS project_name, e.name as subproject_name
                FROM events a
                LEFT JOIN users b ON b.id = a.user_id
                LEFT JOIN clients c ON c.id = a.client_id 
                LEFT JOIN projects d ON d.id = a.project_id 
                LEFT JOIN projects e ON e.id = a.subproject_id 
                {$conditions}
                )
                UNION
                (
                SELECT 
                    a.id, a.user_id, a.title, a.description, a.is_holiday, a.holiday_in, a.status, 
                    UNIX_TIMESTAMP(a.start_date) AS start_date, UNIX_TIMESTAMP(a.end_date) AS end_date,
                    UNIX_TIMESTAMP(a.create_date) as create_date, a.created_by, UNIX_TIMESTAMP(a.update_date) as update_date, a.updated_by,
                    a.client_id, a.project_id, a.subproject_id, IFNULL(b.id, 0) AS individual_event, b.first_name, b.middle_name, b.last_name,
                    c.name AS client_name, d.name AS project_name, e.name as subproject_name
                FROM events a
                LEFT JOIN users b ON b.id = a.user_id
                LEFT JOIN clients c ON c.id = a.client_id 
                LEFT JOIN projects d ON d.id = a.project_id 
                LEFT JOIN projects e ON e.id = a.subproject_id 
                {$Hconditions}
                )
                ORDER BY b.first_name, b.middle_name, b.last_name
            ";

            $now = time();
            
            $user_colors = ColorCode::getUserColors();
            foreach(db_get_all($sql) as $v){
                $v["start_date_mdy"] = mdy($v["start_date"]);
                $v["end_date_mdy"] = mdy($v["end_date"]);
                $v["create_date"] = mdyhm($v["create_date"]);
                $v["update_date"] = mdyhm($v["update_date"]);
                $v["is_holiday"] = (bool) $v["is_holiday"];
                $v["individual_event"] = (bool) $v["individual_event"];

                if($v["individual_event"]) $user_name = preg_replace("/\s{2,}/", " ", trim("{$v["first_name"]} {$v["middle_name"]} {$v["last_name"]}"));
                else $user_name = "Everyone";

                $v["user_name"] = $user_name;

                if($v["is_holiday"]) $class_name = "holiday";
                else $class_name = strtolower($v['status']);

                /*
                elseif($v["start_date"] < $now && $v["end_date"] > $now) $class_name = "current";
                elseif($v["start_date"] > $now) $class_name = "upcoming";
                elseif($v["end_date"] < $now) $class_name = "completed";
                */
                
                $v["type"] = "EVENT";
                if($v["client_id"] == 0 && $v["project_id"] == 0 && $v["subproject_id"] == 0) {
                    if($v["is_holiday"]) $class_name = "holiday";
                    else $class_name = "pto";
                    $v["type"] = "PTO_HOLIDAY";
                }
                else if($param['chkUserColor'] && $user_colors[$v["user_id"]] && $user_colors[$v["user_id"]]["cal_text_color"] != "") {
                    $class_name = "u-".$v["user_id"];
                } 
                $evt_tooltip = '';
                if($v['type'] == "EVENT") {
                    $evt_tooltip = "
                           <tr><td class='fbold'>Client: {$v['client_name']}</td></tr>
                           <tr><td class='fbold'>Project: {$v['project_name']}</td></tr>";
                    if($v['subproject_name'] != '') {
                        $evt_tooltip .= "<tr><td class='fbold'>Subproject: {$v['subproject_name']}</td></tr>";
                    }
                    
                }
                $holiday_in = '';
                if($v['holiday_in'] != '') {
                    $holiday_in = "<tr><td>Holiday In:".$holidayin[$v['holiday_in']]."</td></tr>";
                }
                $description = "
                   <table cellspacing='0' cellpadding='3' border='0' width='300'>
                       <tr><td class='fbold'>{$user_name}</td></tr>
                       <tr><td class='fbold'>{$v['title']}</td></tr>
                        $holiday_in
                       <tr><td>" . nl2br($v["description"]) . "</td></tr>
                        $evt_tooltip
                       <tr><td class='fbold'>{$v['start_date_mdy']} to {$v['end_date_mdy']}</td></tr>
                   </table>
                ";
                $events[] = array(
                   "id" => $v["id"],
                   "title" => "{$user_name} - {$v["title"]} - {$v["start_date_mdy"]} to {$v["end_date_mdy"]}",
                   "start" => date("c", $v["start_date"]),
                   "end" => date("c", $v["end_date"]),
                   "description" => $description,
                   "allDay" => true,
                   "className" => $class_name,
                   "data" => $v,
                );
            }
        }

        return $events;
    }

    public static function GetProjectTimeline($param){
        $start_date = $param["start_date"];
        $end_date = $param["end_date"];
        $user_id = $param["user_id"];
        $client_id = $param["client_id"];
        $project_id = $param["project_id"];
        $subproject_id = $param["subproject_id"];

        #$where = " AND (a.start_date >= '$start_date' AND a.end_date <= '$end_date')";
        $where = " AND (a.start_date <= '$end_date' AND a.end_date >= '$start_date')";
        $user_ids = array();
        if(u()->isAdmin()) {
            if($user_id > 0) 
                $user_ids[] = $user_id;
        }
        else if(u()->isCalendarManager()) {
            if($user_id > 0) {
                $user_ids[] = $user_id;
            }
            else {
                $project_ids = array();
                $project_ids = u()->getProjectIds();
                $sql = "SELECT a.id
                        FROM users a
                        INNER JOIN project_users b ON b.user_id = a.id
                        WHERE b.project_id IN (".implode(", ", $project_ids).")
                        AND NOT a.blocked AND a.id >1 GROUP BY b.user_id";
                $rows = db_get_all($sql);
                $user_ids = array();
                foreach($rows as $row) $user_ids[] = $row["id"];
            }
        }
        else {
            $user_ids[] = u()->getId();
        }
        if(is_array($user_ids) && count($user_ids) > 0)
            $where .= " AND d.user_id IN (".implode(", ", $user_ids).")";
        
        if($client_id > 0)
            $where .= " AND a.client_id = $client_id";
        if($subproject_id > 0)
            $where .= " AND a.id = $subproject_id";
        if(u()->isCalendarManager()) {
            $project_ids = u()->getProjectIds();
            if($project_id > 0) {
                if($project_ids[$project_id])
                    $where .= " AND (a.id = $project_id OR a.parent_id = $project_id)";
            }
            else {
                $where .= " AND (a.id IN (".implode(",",$project_ids).") OR a.parent_id IN (".implode(",",$project_ids)."))";
            }
        }
        else if($project_id > 0)
            $where .= " AND (a.id = $project_id OR a.parent_id = $project_id)";

        $projects = array();
        $sql = "SELECT a.id, a.name as project_name, a.client_id, b.name as client_name 
                FROM projects a
                INNER JOIN clients b on b.id = a.client_id
                ";
        foreach(db_get_all($sql) as $v) {
            $projects[$v["id"]] = $v;
        }
        #if($user_id > 0) {
            #(a.end_date < CURRENT_DATE) AS closed , d.user_id, CONCAT( u.first_name, ' ', u.last_name ) AS user_name
        $sql = "
               SELECT a.id, a.name as project_name, a.description, a.client_id, a.parent_id,a.status,
                    UNIX_TIMESTAMP( a.start_date ) AS start_date, UNIX_TIMESTAMP( a.end_date ) AS end_date, b.name AS client_name, 
                    (a.end_date < CURRENT_DATE) AS closed , d.user_id, CONCAT( u.first_name, ' ', u.last_name ) AS user_name
                FROM projects a
                INNER JOIN clients b ON b.id = a.client_id
                INNER JOIN project_users d ON d.project_id = a.id
                INNER JOIN users u on u.id = d.user_id AND NOT u.blocked
                WHERE a.id >0 AND a.id !=1000 AND a.end_date IS NOT NULL AND a.show_in_calendar $where
                ";
        /*}
        else {
        $sql = "
               SELECT a.id, a.name as project_name, a.description, a.client_id, a.parent_id,a.status,
                    UNIX_TIMESTAMP( a.start_date ) AS start_date, UNIX_TIMESTAMP( a.end_date ) AS end_date, b.name AS client_name, 
                    (a.end_date < CURRENT_DATE) AS closed 
                FROM projects a
                INNER JOIN clients b ON b.id = a.client_id
                WHERE a.id >0 AND a.id !=1000 AND a.end_date IS NOT NULL $where
                ";
        }*/
        $events = array(); 
        $user_colors = ColorCode::getUserColors();
        foreach(db_get_all($sql) as $v){
            $v["start_date_mdy"] = mdy($v["start_date"]);
            if($v["end_date"] == ""){
                $v["end_date_mdy"] = (date("m")+1)."/".date("t", strtotime(date("Y") . "-" . (date("m")+1) . "-01"))."/".date("Y");  
                $v["end_date"] = strtotime($v["end_date_mdy"]);
            } else {
                $v["end_date_mdy"] = mdy($v["end_date"]);
            }
            if($v["parent_id"] > 0) {
                if(!isset($project_list[$v["user_id"]][$v["parent_id"]]["id"])) {
                    $project_list[$v["user_id"]][$v["parent_id"]] = $projects[$v["parent_id"]];
                }
                $project_list[$v["user_id"]][$v["parent_id"]]["subprojects"][$v["id"]] = $v;
                /*$project_list[$v["parent_id"]]["subprojects"][$v["id"]] = $v;
                $subsql = "SELECT  d.user_id, CONCAT( u.first_name, ' ', u.last_name ) AS user_name
                        FROM project_users d
                        INNER JOIN users u on u.id = d.user_id AND NOT u.blocked
                        WHERE d.project_id = {$v["id"]} $and
                        ";
                $rows = db_get_all($subsql);
                foreach($rows as $row) {
                    $project_list[$v["parent_id"]]["subprojects"][$v["id"]]["users"][$row["user_id"]] = $row;
                }
                */
            }
            else {
                if(!isset($v["start_date"])) continue;
                $project_list[$v["user_id"]][$v["id"]] = $v;
                /*
                $project_list[$v["id"]] = $v;
                $subsql = "SELECT  d.user_id, CONCAT( u.first_name, ' ', u.last_name ) AS user_name
                        FROM project_users d
                        INNER JOIN users u on u.id = d.user_id AND NOT u.blocked
                        WHERE d.project_id = {$v["id"]} $and
                        ";
                $rows = db_get_all($subsql);
                foreach($rows as $row) {
                    $project_list[$v["id"]]["users"][$row["user_id"]] = $row;
                }
                */
            }
        }

        /*
        if($project_list) {
            foreach($project_list as $proj_id => $val) {
                $evt_tooltip = '';
                if($val["subprojects"]) {
                    foreach($val["subprojects"] as $k => $v) {
                        $members = "";
                        $project_name = $project_list[$v["parent_id"]]["project_name"];
                        $evt_tooltip = "
                               <tr><td class='fbold'>Client: {$v['client_name']}</td></tr>
                               <tr><td class='fbold'>Project: $project_name</td></tr>
                               <tr><td class='fbold'>Subproject: {$v['project_name']}</td></tr>
                               <tr><td class='fbold'>{$v['start_date_mdy']} to {$v['end_date_mdy']}</td></tr>";
                               #<tr><td>" . nl2br($v["description"]) . "</td></tr>";
                        if($v["users"]) {
                            $members = " <tr><td>
                                        <table cellspacing='0' cellpadding='3' border='0' width='300'>
                                        <tr><td class='fbold'>Members</td></tr>"
                            foreach($v["users"] as $uid => $users) {
                                $members .= "<tr><td class=''>{$users["user_name"]}</td></tr>";
                            }
                            $members .= "</table></td></tr>";
                        }
                        $description = "
                               <table cellspacing='0' cellpadding='3' border='0' width='300'>
                                    $evt_tooltip
                                    $members
                               </table>
                            ";
                            $v["type"] = "PROJECTS";
                            $events[] = array(
                               "id" => $v["id"],
                               "title" => "{$v["project_name"]} - {$v["start_date_mdy"]} to {$v["end_date_mdy"]}",
                               "start" => date("c", $v["start_date"]),
                               "end" => date("c", $v["end_date"]),
                               "description" => $description,
                               "allDay" => true,
                               "className" => $class_name,
                               "data" => $v,
                            );

                    }
                }
                if(!isset($val["start_date"])) continue;

            }
        }
        */ 
        if($project_list) {
            foreach($project_list as $uid => $proj) {
                foreach($proj as $proj_id => $val) {
                    $evt_tooltip = '';
                    if($val["subprojects"]) {
                        foreach($val["subprojects"] as $k => $v) {
                            #if($v["closed"]) continue;
                            $class_name = strtolower($v['status']);
                            if($param['chkUserColor'] && $user_colors[$v["user_id"]] && $user_colors[$v["user_id"]]["cal_text_color"] != "") {
                                $class_name = "u-".$v["user_id"];
                            } 
                            $project_name = $project_list[$uid][$v["parent_id"]]["project_name"];
                            $project_desc = nl2br($project_list[$uid][$v["parent_id"]]["description"]);
                            $evt_tooltip = "
                               <tr><td class='fbold'>Client: {$v['client_name']}</td></tr>
                               <tr><td class='fbold'>Project: $project_name</td></tr>
                               <tr><td class='fbold'>Subproject: {$v['project_name']}</td></tr>
                               <tr><td class='fbold'>{$v['start_date_mdy']} to {$v['end_date_mdy']}</td></tr>
                               <tr><td>" . nl2br($v["description"]) . "</td></tr>";

                            $description = "
                               <table cellspacing='0' cellpadding='3' border='0' width='300'>
                                   <tr><td class='fbold'>{$v['user_name']}</td></tr>
                                    $evt_tooltip
                               </table>
                            ";
                            $v["type"] = "PROJECTS";
                            $events[] = array(
                               "id" => $v["id"],
                               "title" => "{$v["user_name"]} - {$v["project_name"]} - {$v["start_date_mdy"]} to {$v["end_date_mdy"]}",
                               "start" => date("c", $v["start_date"]),
                               "end" => date("c", $v["end_date"]),
                               "description" => $description,
                               "allDay" => true,
                               "className" => $class_name,
                               "data" => $v,
                            );
                        }
                    }
                    #else {
                        $v = $val;
                        #if($v["closed"]) continue;
                        $class_name = strtolower($val['status']);
                        if($param['chkUserColor'] && $user_colors[$v["user_id"]] && $user_colors[$v["user_id"]]["cal_text_color"] != "") {
                            $class_name = "u-".$v["user_id"];
                        } 
                        $evt_tooltip = "
                               <tr><td class='fbold'>Client: {$v['client_name']}</td></tr>
                               <tr><td class='fbold'>Project: {$v['project_name']}</td></tr>";

                        $description = "
                           <table cellspacing='0' cellpadding='3' border='0' width='300'>
                               <tr><td class='fbold'>{$v['user_name']}</td></tr>
                                $evt_tooltip
                               <tr><td class='fbold'>{$v['start_date_mdy']} to {$v['end_date_mdy']}</td></tr>
                               <tr><td>" . nl2br($v["description"]) . "</td></tr>
                           </table>
                        ";
                        $v["type"] = "PROJECTS";
                        $events[] = array(
                           "id" => $v["id"],
                           "title" => "{$v["user_name"]} - {$v["project_name"]} - {$v["start_date_mdy"]} to {$v["end_date_mdy"]}",
                           "start" => date("c", $v["start_date"]),
                           "end" => date("c", $v["end_date"]),
                           "description" => $description,
                           "allDay" => true,
                           "className" => $class_name,
                           "data" => $v,
                        );
                    #}
                }
            }
        }
        $projectArr = $events;
        $pto_holidayArr = CalendarEvents::GetPtoHolidayList($param);
        $events = "";
        $events = array_merge($projectArr, $pto_holidayArr);
        return $events; 
    } 

    public static function GetPtoHolidayList($param) {

        $Hconditions[] = "a.client_id = '0'";
        $Hconditions[] = "a.project_id  = '0'";
        $Hconditions[] = "a.subproject_id = '0'";

        $start_date = $param['start_date'];
        $end_date = $param['end_date'];
        $Hconditions[] = "(a.start_date BETWEEN '{$start_date}' AND '{$end_date}')";
        if($Hconditions){
            $Hconditions = 'WHERE ' . implode(' AND ', $Hconditions);
        }

        $sql = "
                SELECT 
                    a.id, a.user_id, a.title, a.description, a.is_holiday, a.holiday_in, a.status, 
                    UNIX_TIMESTAMP(a.start_date) AS start_date, UNIX_TIMESTAMP(a.end_date) AS end_date,
                    UNIX_TIMESTAMP(a.create_date) as create_date, a.created_by, UNIX_TIMESTAMP(a.update_date) as update_date, a.updated_by,
                    a.client_id, a.project_id, a.subproject_id, IFNULL(b.id, 0) AS individual_event, b.first_name, b.middle_name, b.last_name
                FROM events a
                LEFT JOIN users b ON b.id = a.user_id
                $Hconditions
                ";
        $events = array();
        foreach(db_get_all($sql) as $v){
            $v["start_date_mdy"] = mdy($v["start_date"]);
            $v["end_date_mdy"] = mdy($v["end_date"]);
            $v["create_date"] = mdyhm($v["create_date"]);
            $v["update_date"] = mdyhm($v["update_date"]);
            $v["is_holiday"] = (bool) $v["is_holiday"];
            $v["individual_event"] = (bool) $v["individual_event"];

            if($v["individual_event"]) $user_name = preg_replace("/\s{2,}/", " ", trim("{$v["first_name"]} {$v["middle_name"]} {$v["last_name"]}"));
            else $user_name = "Everyone";

            $v["user_name"] = $user_name;

            if($v["is_holiday"]) $class_name = "holiday";
            else $class_name = strtolower($v['status']);
            
            if($v["client_id"] == 0 && $v["project_id"] == 0 && $v["subproject_id"] == 0) {
                if($v["is_holiday"]) $class_name = "holiday";
                else $class_name = "pto";
                $v["type"] = "PTO_HOLIDAY";
            }

            $holiday_in = '';
            if($v['holiday_in'] != '') {
                $holiday_in = "<tr><td>Holiday In:".$holidayin[$v['holiday_in']]."</td></tr>";
            }
            $description = "
               <table cellspacing='0' cellpadding='3' border='0' width='300'>
                   <tr><td class='fbold'>{$user_name}</td></tr>
                   <tr><td class='fbold'>{$v['title']}</td></tr>
                    $holiday_in
                   <tr><td>" . nl2br($v["description"]) . "</td></tr>
                   <tr><td class='fbold'>{$v['start_date_mdy']} to {$v['end_date_mdy']}</td></tr>
               </table>
            ";
            $events[] = array(
               "id" => $v["id"],
               "title" => "{$user_name} - {$v["title"]} - {$v["start_date_mdy"]} to {$v["end_date_mdy"]}",
               "start" => date("c", $v["start_date"]),
               "end" => date("c", $v["end_date"]),
               "description" => $description,
               "allDay" => true,
               "className" => $class_name,
               "data" => $v,
            );
        }
        return $events;
    }
}

