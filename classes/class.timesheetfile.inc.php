<?php

class TimesheetFile {
    private $_id = null;
    private $_clientId = null;
    private $_categoryId = null;
    private $_otherCategoryName = null;
    private $_oldCategoryId = null;
    private $_projectId = null;
    private $_oldProjectId = null;
    private $_md5Name = null;
    private $_fileName = null;
    private $_fileSize = null;
    private $_fileVersion = null;
    private $_isPublic = null;
    private $_mimeType = null;
    private $_uploadDate = null;
    private $_docType = null;
    private $_deletedDate = null;

    private $_file = null;
    private $_notes = null;
    private $_fileSizeH = null;
    private $_categoryName = null;
    private $_clientName = null;
    private $_projectName = null;
    private $_parentProjectId = null;
    private $_parentProjectName = null;
    private $_createdBy = null;
    private $_deletedBy = null;

    private $_fileAction = null;
    private $_history = null;

    public function __construct($id = null,$deleted = null){
        $id = pintval($id);
        $deleted = pintval($deleted);
        if($id){
            $sql = "
                SELECT 
                    a.id, a.category_id,a.other_category_name, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version,a.doc_type, 
                    a.mime_type,a.notes,a.created_by,a.deleted_by, a.is_public, UNIX_TIMESTAMP(deleted_date) as deleted_date , 
                    UNIX_TIMESTAMP(a.upload_date) AS upload_date, b.name AS client_name, c.name AS project_name, 
                    d.id AS parent_project_id, d.name AS parent_project_name,e.name as category_name 
                FROM files a
                INNER JOIN category e ON e.id = a.category_id
                LEFT JOIN clients b ON b.id = a.client_id
                LEFT JOIN projects c ON c.id = a.project_id
                LEFT JOIN projects d ON d.id = c.parent_id
                WHERE a.id = '{$id}' AND a.deleted = $deleted
            ";
            if($rs = db_get_row($sql)){
                self::Create($rs, $this);
            }
        }
    }

    public static function Create($array, $object = null, $file_history=false){
        if(!$object instanceof self) $object = new self;

        $object->_id = $array['id'];
        $object->_categoryId = $array['category_id'];
        $object->_otherCategoryName = $array['other_category_name'];
        $object->_oldCategoryId = $array['category_id'];
        $object->_clientId = $array['client_id'];
        $object->_projectId = $array['project_id'];
        $object->_oldProjectId = $array['project_id'];
        $object->_md5Name = $array['md5_name'];
        $object->_fileName = $array['file_name'];
        $object->_fileSize = $array['file_size'];
        $object->_fileVersion = $array['file_version'];
        $object->_isPublic = $array['is_public'];
        $object->_mimeType = $array['mime_type'];
        $object->_uploadDate = $array['upload_date'];
        $object->_docType = $array['doc_type'];
        $object->_deletedDate = $array['deleted_date'];
        
        $object->_notes = $array["notes"];
        $object->_categoryName = empty($array["category_name"]) ? "Others" : $array["category_name"];
        $object->_clientName = $array["client_name"];
        $object->_projectName = $array["project_name"];
        $object->_parentProjectId = $array["parent_project_id"];
        $object->_parentProjectName = $array["parent_project_name"];
        $object->_createdBy = $array["created_by"];
        $object->_deletedBy = $array["deleted_by"];

        if($file_history) {
            $object->getFileHistory();
        }
        return $object;
    }
    public function getFileHistory() {
        $this->_loadFileHistory();
        return $this->_history;
    }
    private function _loadFileHistory() {
        if($this->_id && is_null($this->_history)) {
            $sql = "
                 SELECT 
                    a.id, a.category_id, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version, a.mime_type,a.notes,
                    a.is_public, a.created_by,a.deleted_by,a.parent_id,
                    UNIX_TIMESTAMP( a.upload_date ) AS upload_date, c.name AS category_name, d.name AS client_name, 
                    e.name AS project_name, f.id AS parent_project_id, f.name AS parent_project_name 
                FROM files a
                LEFT JOIN category c ON c.id = a.category_id
                INNER JOIN clients d ON d.id = a.client_id
                LEFT JOIN projects e ON e.id = a.project_id
                LEFT JOIN projects f ON f.id = e.parent_id
                WHERE a.deleted = 0 AND a.parent_id = {$this->_id}
                ORDER BY a.file_version DESC
                ";
            foreach(db_get_all($sql) as $rfile){
                $this->_history[$rfile['id']] = self::Create($rfile);
            }
        }
    }

    public function getId(){
        return $this->_id;
    }

    public function getCategoryId(){
        return $this->_categoryId;
    }
    
    public function getOtherCategoryName(){
        return $this->_otherCategoryName;
    }
    
    public function getOldCategoryId(){
        return $this->_oldCategoryId;
    }

    public function getClientId(){
        return $this->_clientId;
    }

    public function getOldProjectId(){
        return $this->_oldProjectId;
    }
    public function getProjectId(){
        return $this->_projectId;
    }
    public function getProjectName(){
    	return $this->_projectName;
    }

    public function getMD5Name(){
        return $this->_md5Name;
    }
    public function getIsPublic(){
        return $this->_isPublic;
    }

    public function getFileName(){
        return $this->_fileName;
    }
    public function getCreatedBy(){
        return $this->_createdBy;
    }

    public function getDeletedBy(){
        return $this->_deletedBy;
    }

    public function getDeletedDate(){
        return $this->_deletedDate;
    }

    public function getUploadedDate(){
        return $this->_uploadDate;
    }

    public function getDocType(){
        return $this->_docType;
    }

    public function getFileSize($human_readable = false){
        if($human_readable){
            if($this->_fileSizeH === null){
                $mod = 1024;
                $size = $this->_fileSize;
                $units = explode(' ','B KB MB GB TB PB');
                for($i = 0; $size > $mod; $i++){
                    $size /= $mod;
                }

                $this->_fileSizeH = round($size, 2) . ' ' . $units[$i];
            }

            return $this->_fileSizeH;
        }
        else{
            return $this->_fileSize;
        }
    }

    public function getFileVersion(){
        return $this->_fileVersion;
    }

    public function getMimeType(){
        return $this->_mimeType;
    }

    public function getFilePath(){
        if($this->_id) {
            
            if($this->_docType == "GENERAL_DOCUMENT") {
                // All un-related to project documents are save in GENERAL_DOCUMENT folder
                return AppPath::$FILES . "/repository/GENERAL_DOCUMENTS/{$this->_md5Name}";
            }
            else {
                return AppPath::$FILES . "/repository/{$this->_clientId}/{$this->_md5Name}";
            }
        }
        else return false;
    }

    public function getUploadDate($format = null){
        if($format) return date($format, $this->_uploadDate);
        else return $this->_uploadDate;
    }
    
    public function getCategoryName(){
    	return $this->_categoryName;
    }

    public function getNotes(){
    	return $this->_notes;
    }

    public function getClientName(){
    	return $this->_clientName;
    }
    
    public function getParentProjectId(){
    	return $this->_parentProjectId;
    }
    
    public function getParentProjectName(){
    	return $this->_parentProjectName;
    }
    
    public function setCategoryId($category_id){
        $this->_categoryId = pintval($category_id);
    }
    
    public function setOtherCategoryName($other_category_name){
        $this->_otherCategoryName = $other_category_name;
    }

    public function setClientId($client_id){
        $this->_clientId = pintval($client_id);
    }

    public function setProjectId($project_id){
        $this->_projectId = pintval($project_id);
    }

    public function setFile($file){
        $this->_file = $file;
    }

    public function setDocType($doc_type){
        $this->_docType = strtoupper($doc_type);
    }
    public function setNotes($notes){
        $this->_notes = $notes;
    }
    public function setIsPublic($is_public){
        $this->_isPublic = intval($is_public);
    }

    public function setCreatedBy($created_by){
        $this->_createdBy = $created_by;
    }

    public function setDeletedBy($deleted_by){
        $this->_deletedBy = $deleted_by;
    }

    public function setFileAction($action) {
        $this->_fileAction = strtoupper(trim($action));
    }

    private function _saveFile(){
        $file_dir = AppPath::$FILES . "/repository";
        if(!is_dir($file_dir))
            mkdir($file_dir, 0755, true);

        if($this->_docType == "GENERAL_DOCUMENT") {
            // All un-related to project documents are save in GENERAL_DOCUMENT folder
            $file_dir = AppPath::$FILES . "/repository/GENERAL_DOCUMENTS";
        }
        else {
            $file_dir = AppPath::$FILES . "/repository/{$this->_clientId}";
        }
        if(is_dir($file_dir) || mkdir($file_dir, 0755, true)){
            do{
                $md5_name = md5(microtime() . rand(1, 999));
            } while(is_file("{$file_dir}/{$file_name}"));

            if(move_uploaded_file($this->_file['tmp_name'], "{$file_dir}/{$md5_name}")){
                $this->_md5Name = $md5_name;
                $this->_fileName = $this->_file['name'];
                $this->_mimeType = $this->_file['type'];
                $this->_fileSize = intval($this->_file['size']);

                return true;
            }
        }

        return false;
    }

    public function save(){
        if($this->_id) return $this->_update();
        else return $this->_add();
    }

    private function _add() {
        
        if($this->_file){
            if($this->_saveFile()){
                $file_version = 1;
                if($this->_fileAction == 'REVISION') 
                    $file_version = $this->fileVersion();

                $sql = "
                    INSERT INTO files (category_id, other_category_name, client_id, project_id, md5_name, file_name, file_size, mime_type, file_version, notes, is_public,doc_type,
                            upload_date, created_by, update_date, updated_by)
                    VALUES (
                        '{$this->_categoryId}', '{$this->_otherCategoryName}', '{$this->_clientId}', '{$this->_projectId}', '{$this->_md5Name}',
                        " . q($this->_fileName, true, false) . ", '{$this->_fileSize}',
                        " . q($this->_mimeType, true, false) . ",'{$file_version}', '{$this->_notes}','{$this->_isPublic}','{$this->_docType}',
                         NOW(), '{$this->_createdBy}',NOW(),'".u()->getEmail()."'
                    )
                ";

                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }
        }

        return false;
    }

    private function _update() {
        if($this->_id) {
            
            if($this->_oldCategoryId != $this->_categoryId) {
                $file_version = $this->fileVersion();
                $this->_fileVersion = $file_version;
            }
            if($this->_fileAction == "REPLACE" && $this->_saveFile()){
                $sql = "UPDATE files SET
                        category_id = '{$this->_categoryId}',
                        other_category_name = '{$this->_otherCategoryName}',    
                        client_id = '{$this->_clientId}',
                        project_id = '{$this->_projectId}',
                        is_public = '{$this->_isPublic}',
                        doc_type = '{$this->_docType}',
                        notes = '{$this->_notes}',
                        md5_name = '{$this->_md5Name}',
                        file_name = " . q($this->_fileName, true, false) . ",
                        file_size = '{$this->_fileSize}',
                        file_version = '{$this->_fileVersion}',
                        mime_type = " . q($this->_mimeType, true, false) . ",
                        update_date = NOW(),
                        updated_by = '".u()->getEmail()."' 
                    WHERE id = {$this->_id}
                    ";
                return db_execute($sql);
            }
            else if($this->_fileAction == "REVISION"){
                $old_id = $this->_id;
                if($this->_add()) {
                    if($this->_oldProjectId == $this->_projectId) {
                        // Update all file parent_id with newly uploaded file
                        $sql = "UPDATE files SET parent_id = {$this->_id} WHERE id = $old_id OR parent_id = $old_id";
                        return db_execute($sql);
                    }
                    else {
                        // mark the previous file as Actice file
                        $sql = "UPDATE files SET parent_id = 0 WHERE id = '$old_id'";
                        return db_execute($sql);
                    }
                }
            }
            else {
                $sql = "UPDATE files SET
                        category_id = '{$this->_categoryId}',
                        other_category_name = '{$this->_otherCategoryName}', 
                        client_id = '{$this->_clientId}',
                        project_id = '{$this->_projectId}',
                        is_public = '{$this->_isPublic}',
                        doc_type = '{$this->_docType}',
                        notes = '{$this->_notes}',
                        file_version = '{$this->_fileVersion}',
                        update_date = NOW(),
                        updated_by = '".u()->getEmail()."' 
                    WHERE id = {$this->_id}
                    ";
                return db_execute($sql);
            }
        }
        return false;
    }

    public function delete(){
        if($this->_id){
            $file_path = $this->getFilePath();
            //if(db_execute("DELETE FROM files WHERE id = '{$this->_id}'")){
            
            // Deleting a current revision should promote the NEXT available revision as current
            // Check the previous active file(History), if any
            $previous_id = $this->previousActiveFile();
            if($previous_id) {
                // mark the previous file as Actice file
                $sql = "UPDATE files SET parent_id = 0 WHERE id = '$previous_id'";
                db_execute($sql);

                // Update all history file with previous active file id
                $sql = "UPDATE files SET parent_id = $previous_id WHERE parent_id = '{$this->_id}'";
                db_execute($sql);
            } 
            // Now delete the file
            $sql = "UPDATE files SET deleted = 1, deleted_by = '{$this->_deletedBy}', deleted_date=NOW()  WHERE id = '{$this->_id}'";
            if(db_execute($sql)){
                # Dont delete the file physically, may be we can have trash option going further!!
                //unlink($file_path);
                return true;
            }
        }

        return false;
    }
    public function previousActiveFile($id = 0) {
        if($id > 0) {
            $sql = "SELECT id FROM files WHERE parent_id = '$id' ORDER BY id DESC";
            if($rs = db_get_row($sql)){
                return intval($rs['id']);
            }
        }
        else if($this->_id) {
            $sql = "SELECT id FROM files WHERE parent_id = '{$this->_id}' ORDER BY id DESC";
            if($rs = db_get_row($sql)){
                return intval($rs['id']);
            }
        }
        return false;
    } 
    public function fileVersion() {
        $file_version = 1;
        $sql = "SELECT max(file_version) as file_version,category_id FROM files 
                WHERE client_id = '{$this->_clientId}' AND project_id = '{$this->_projectId}' 
                AND doc_type = '{$this->_docType}' ";
        if($this->_docType == 'GENERAL_DOCUMENT') {
            $sql .= " AND file_name='{$this->_fileName}' ";
        }
        else {
            if($this->_id && $this->_fileAction == 'REVISION')
                $sql .= " AND id = '{$this->_id}' ";
        }
        $sql .= " AND deleted = 0 ORDER BY id DESC LIMIT 1";
        if($rs = db_get_row($sql)){
            if($this->_categoryId == $rs['category_id']) {
                // If the category is same then only increment it
                $file_version = intval($rs['file_version']) + 1;
            }
            else {
                // The Category is changed, let check in different category
                $sql = "SELECT max(file_version) as file_version,category_id FROM files 
                            WHERE category_id = '{$this->_categoryId}' client_id = '{$this->_clientId}' AND project_id = '{$this->_projectId}' 
                        AND doc_type = '{$this->_docType}' AND deleted = 0 ORDER BY id DESC LIMIT 1";
                if($rs1 = db_get_row($sql)){
                    $file_version = intval($rs['file_version']) + 1;
                }

                // Now current revision should promote the NEXT available revision as current
                $previous_id = $this->previousActiveFile();
                if($previous_id) {
                    // mark the previous file as file
                    $sql = "UPDATE files SET parent_id = 0 WHERE id = '$previous_id'";
                    db_execute($sql);

                    // Update all history file with previous active file id
                    $sql = "UPDATE files SET parent_id = $previous_id WHERE parent_id = '{$this->_id}'";
                    db_execute($sql);
                }
                
            }
        }
        return $file_version;
    }

    public function isFileFound() {
        $sql = "SELECT id FROM files 
                WHERE category_id = '{$this->_categoryId}' AND client_id = '{$this->_clientId}' AND project_id = '{$this->_projectId}' AND doc_type = '{$this->_docType}'";
        if($rs = db_get_row($sql)){
            return $rs['id'];
        }
        return false;
    }
}

