<?php
    include_once("class.project.inc.php");
    include_once("class.task.inc.php");
    include_once("class.employees.inc.php");

    class User {
        private $_id = null;
        private $_email = null;
        private $_password = null;
        private $_name = null;
        private $_firstName = null;
        private $_middleName = null;
        private $_lastName = null;
        private $_gender = null;
        private $_dob = null;
        private $_registerDate = null;
        private $_projects = null;
        private $_projectIds = null;
        private $_tasks = null;
        private $_projectTasks = null;
        private $_blocked = null;
        private $_roles = null;

        private $_perms = null;
        private $_picture = null;
        private $_cv = null;
        private $_employee = null;

        private static $_Sql = "
            SELECT 
                a.id, a.email, a.password, a.first_name, a.middle_name, a.last_name, a.blocked, UNIX_TIMESTAMP(a.register_date) AS register_date, 
                a.roles,a.gender, DATE_FORMAT(a.date_of_birth, '%d-%M-%Y') as date_of_birth
            FROM users a
        ";

        public function __construct($id = null){
            $this->_projectTasks[0] = array();

            $id = trim($id);
            if($id){
                if(is_numeric($id)) $where = "WHERE a.id = {$id}";
                else $where = "WHERE a.email = " . q(strtolower($id));

                $sql = self::$_Sql . "
                    {$where}
                ";

                $row = db_get_row($sql);
                if($row){
                    self::_Init($row, $this);

                    // For now, a static list is sufficient.
                    // Later on we will need to load permissions from db
                    $this->_perms = array("authuser","manager");
                }
            }
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_email = $array["email"];
            $object->_password = $array["password"];
            $object->_blocked = (bool) $array["blocked"];
            $object->_firstName = $array["first_name"];
            $object->_middleName = $array["middle_name"];
            $object->_lastName = $array["last_name"];
            $object->_gender = $array["gender"];
            $object->_dob = $array["date_of_birth"];
            $object->_registerDate = $array["register_date"];
            $object->_roles = $array["roles"];

            $sql = "SELECT * FROM user_files WHERE user_id = {$array["id"]} AND file_type='PROFILE_PIC'";
            $result = db_get_row($sql);
            if($result) {
                #$pic = AppPath::$FILES . "/pic/{$array["id"]}/".$result["md5_name"];
                $pic = $result["md5_name"];
            }
            else {
                $pic = strtolower($array["gender"]).".jpeg";
            }
            $object->_picture = $pic; 
            
            $sql = "SELECT * FROM user_files WHERE user_id = {$array["id"]} AND file_type='USER_CV'";
            $result = db_get_row($sql);
            if($result) {
                $result['is_cv'] = true;
                $result['cv_file'] = $result["md5_name"];
                $result['file_path'] = AppPath::$FILES."/user-cv/{$array['id']}";
            }
            else {
                $result['is_cv'] = false;
            }
            $object->_cv = $result;
            $object->_employee = new Employee(0, $array["id"]);

            return $object;
        }

        public function getId(){ return $this->_id; }
        public function getEmail(){ return $this->_email; }
        public function getFullName(){ 
            if($this->_middleName)
                return $this->_firstName .' '.$this->_middleName .' '.$this->_lastName; 
            else 
            return $this->_firstName .' '.$this->_lastName; 
        }
        public function getGivenName(){ 
            if($this->_middleName)
                return $this->_firstName .' '.$this->_middleName; 
            else 
            return $this->_firstName; 
        }
        public function getFirstName(){ return $this->_firstName; }
        public function getMiddleName(){ return $this->_middleName; }
        public function getLastName(){ return $this->_lastName; }
        public function getGender(){ return $this->_gender; }
        public function getDateOfBirth(){ return isset($this->_dob) ? format_date($this->_dob,'d-M-Y') : ''; }
        public function getRegisterDate(){ return $this->_registerDate; }
        public function isAdmin(){ return (1 == $this->_id || in_array("business_manager" ,$this->getRoles())); }
        public function isSMTMember(){ return (1 == $this->_id || in_array("smt_member" ,$this->getRoles())); }
        public function isCalendarManager(){ return in_array("calendar_manager" ,$this->getRoles()); }
        public function isMeetinMinutesManager(){ return in_array("minutes_manager" ,$this->getRoles()); }
        public function isReportManager(){ return in_array("report_manager" ,$this->getRoles()); }
        public function isHRManager(){ return in_array("hr_manager" ,$this->getRoles()); }
        public function isGuest(){ return !$this->_id; }
        public function isLogin(){ return $this->_id; }
        public function isBlocked(){ return $this->_blocked; }
		
        public function getRoles(){
            #if($this->_roles == "") $this->_roles = "member";
 			$roles = explode(",", $this->_roles);
			return $roles; 
		}
        public function getPicture() {
            return $this->_picture;
        }
        public function getCV() {
            return $this->_cv;
        }
        public function getEmployee() {
            if(is_null($this->_employee))
                $this->_loadEmployee();
            return $this->_employee;
        }
	    public function _loadEmployee(){
            $this->_employee = new Employee(0, $this->_id);
        }	
        public function getName(){
            if(!$this->_name) $this->_name = concat(" ", $this->_firstName, $this->_middleName, $this->_lastName);
            return $this->_name;
        }

        public function checkPassword($password){
            return (md5($password) == $this->_password);
        }

        public function _loadProjects($include_children = false,$user=false){
            #if(is_null($this->_projects)){
                if($user) {
                    $this->_projects = Project::GetProjectsByUser($this);
                } else if($this->isAdmin()) {
                    $this->_projects = Project::GetProjects($include_children);
                } else { 
                    $this->_projects = Project::GetProjectsByUser($this);
                }
            #}
        }

        public function getProjects($include_children=false, $user = false){
            $this->_loadProjects($include_children, $user);
            return $this->_projects;
        }

        public function getProjectAssociation() {
            $projects = $this->getProjects(true, $this);
            if (!count($projects))
                return array();
            $userprojects = array();
            $now = time();
            foreach ($projects as $project) {
                $p['closed'] = false;
                if ($project->getClosed() || ($project->getEndDate() > 0 && $project->getEndDate() < $now)) {
                    $p['closed'] = true;
                }
                $p['isBillable'] = false;
                if($project->getIsBillable()) $p['isBillable'] = true;
                $p['clientName'] = $project->getClientName();
                $p['projectName'] = $project->getName();
                $p['description'] = $project->getDescription();
                $p['role'] = $project->getRole();
                $p['startDate'] = date('m-d-Y', $project->getStartDate());
                $p['endDate'] = ($project->getEndDate() > 0) ? date('m-d-Y', $project->getEndDate()) : '-'; 
                $p['parentId'] = $project->getParentId();
                #$userprojects[$project->getClientId()]['clientName'] = $project->getClientName();
                #$userprojects[$project->getClientId()]['projects'][$project->getId()] = $p;
                $userprojects[$project->getId()] = $p;
            }
            
            foreach ($userprojects as $pid => $p) {
                if ($p['parentId']) {
                    $userprojects[$pid]['parentProjectName'] = $userprojects[$p['parentId']]['projectName'];
                }
            }
            /*
            foreach ($userprojects as $cid => $c) {
                foreach ($c['projects'] as $pid => $p) {
                    if ($p['parentId']) {
                        $userprojects[$cid]['projects'][$p['parentId']]['subprojects'][$pid] = $p;
                        unset($userprojects[$cid]['projects'][$pid]);
                    }
                }
            }
            */
            return $userprojects;
        }

        public function getProjectIds(){
            if(is_null($this->_projectIds)){
                $this->_loadProjects();
                foreach($this->_projects as $p) $this->_projectIds[] = $p->getId();
            }
            return $this->_projectIds;
        }

        public function isMemberOfProject($project){
            if($project instanceof Project) $project_id = $project->getId();
            else $project_id = pintval($project);

            if($project_id > 0){
                $this->_loadProjects();
                return array_key_exists($project_id, $this->_projects);
            }

            return false;
        }

        public function getProject($project){
            if($project instanceof Project) $project_id = $project->getId();
            else $project_id = pintval($project);

            if($project_id > 0){
                $this->_loadProjects();
                if($this->_projects[$project_id]) return $this->_projects[$project_id];
            }

            return false;
        }

        public function getTasks(){
            if(is_null($this->_tasks)){
                if($this->isAdmin()) $this->_tasks = Task::GetTasks();
                else $this->_tasks = Task::GetTasksByUser($this);
            }

            return $this->_tasks;
        }

        public function getTasksByDateRange($start_date, $end_date){
            return Task::GetTasksByUser($this, null, $start_date, $end_date);
        }

        public function getProjectTasks($project){
            $project = $this->getProject($project);

            if($project && $project->getId() > 0) $project_id = $project->getId();
            else $project_id = 0;

            if(!is_array($this->_projectTasks[$project_id])) $this->_projectTasks[$project_id] = Task::GetTasksByUserAndProject($this, $project);

            return $this->_projectTasks[$project_id];
        }

        public function getTimecard($date = null,$manager_id = null){
            return Task::GetTimecardByUser($this, $date, $manager_id);
        }

        public function getClients(){
            return Client::GetClients();
        }

        public function setEmail($email){
            $email = is_email($email);
            #if($email && !$this->_email) $this->_email = strtolower($email);
            if($email) $this->_email = strtolower($email);
        }

        public function setFirstName($first_name){
            $first_name = trim($first_name);
            if($first_name) $this->_firstName = $first_name;
        }

        public function setMiddleName($middle_name){
            $middle_name = trim($middle_name);
            #if($middle_name) 
            $this->_middleName = $middle_name;
        }

        public function setLastName($last_name){
            $last_name = trim($last_name);
            if($last_name) $this->_lastName = $last_name;
        }
        public function setGender($gender){
            if($gender)
                $this->_gender = trim($gender);
            else
                $this->_gender = "MALE";
        }
        public function setDateOfBirth($dob){
            if($dob)
                $this->_dob = ymd(is_date($dob));
            else
                $this->_dob = date("Y-m-d");
        }
        public function setPassword($password){
            $this->_password = md5($password);
        }
        public function setRoles($roles){
            $this->_roles = $roles;
        }
        public function changePassword($password = null){
            $loginuser = u();
            if($this->_id && ($loginuser->isAdmin() || $loginuser->getId() == $this->_id)){
                if(!$password) $password = gen_password();
                $sql = "
                    UPDATE users SET
                    password = MD5('{$password}')
                    WHERE id = {$this->_id}
                ";

                if(db_execute($sql)) return $password;
            }

            return false;
        }

        public function hasPerms($perms){
            if($this->isAdmin()) return true;
            if($this->isHRManager() && in_array($perms[0] ,$this->getRoles()))  return true;
            if($this->isSMTMember() && in_array($perms[0] ,$this->getRoles()))  return true;
            
			$roles_list = array_merge($this->_perms,$this->getRoles());
		    return array_intersect($roles_list, $perms);
        }

        public function setBlocked($state){
            if($this->_id > 0 && $this->getId() != 1 ){
                if($state) {
                    $state = 1;
                    $str= "Blocked";
                }
                else {
                    $state = 0;
                    $str = "Unblocked";
                }

                $sql = "
                    UPDATE users SET
                    blocked = {$state}
                    WHERE id = {$this->_id}
                ";
                watchdog('SUCCESS','EDIT', "$str member <i>{$this->getName()}({$this->_email})</i>");

                return db_execute($sql);
            }

            return false;
        }

        public function is($user){
            if($user instanceof self) $user_id = $user->getId();
            else $user_id = intval($user);

            return $user_id && $user_id = $this->_id;
        }

        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        private function checkPassport(){
            
        }

        private function _add(){
            if($this->_email && $this->_password && $this->_firstName){
                $sql = "
                    INSERT INTO users (email, password, first_name, middle_name, last_name, gender, date_of_birth, register_date, roles)
                    VALUES (
                        " . q($this->_email) . ",
                        '{$this->_password}',
                        " . q($this->_firstName) . ",
                        " . q($this->_middleName, true, true) . ",
                        " . q($this->_lastName) . ",
                        '". $this->_gender ."',
                        '". $this->_dob ."',  
                        NOW(),
                        '" . $this->_roles . "'
                    )
                ";

                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
		    
            if($this->_password && $this->_firstName){
                $sql = "
                    UPDATE users SET
                        email = ".q($this->_email).",
                        password = '{$this->_password}',
                        first_name = " . q($this->_firstName) . ",
                        middle_name = " . q($this->_middleName, true, true) . ",
                        last_name = " . q($this->_lastName) . ",
                        gender = '" . $this->_gender ."',
                        date_of_birth = '" . $this->_dob ."',
						roles = '" . $this->_roles . "'
                    WHERE id = {$this->_id}
                        
                ";
                return db_execute($sql);
            }

            return false;
        }

        public function SaveUserFiles($file,$type = 'PROFILE_PIC') {
            if(is_array($file) && count($file) > 0) {
                $type = strtoupper($type);
                if($type == 'PROFILE_PIC') {
                    $file_dir = AppPath::$FILES . "/pic";
                }
                else if($type == 'USER_CV') {
                    $file_dir = AppPath::$FILES . "/user-cv";
                }
                if(!is_dir($file_dir)) {
                    mkdir($file_dir, 0755, true);
                }

                if($type == 'PROFILE_PIC') {
                    $file_dir = AppPath::$FILES . "/pic/{$this->_id}";
                }
                else if($type == 'USER_CV') {
                    $file_dir = AppPath::$FILES."/user-cv/{$this->_id}"; 
                }
                if(is_dir($file_dir) || mkdir($file_dir, 0755, true)){
                   do{
                        $md5_name = md5(microtime() . rand(1, 999));
                    } while(is_file("{$file_dir}/{$file_name}"));
                    $ext = getExtension(stripslashes($file["name"]));
                    if($ext)
                        $file_name = $md5_name.".".$ext;
                    else 
                        $file_name = $md5_name;
                    if(move_uploaded_file($file['tmp_name'], "{$file_dir}/{$file_name}")){
                        $sql = "SELECT id FROM user_files WHERE user_id = {$this->_id} AND file_type='$type'";
                        if($row = db_get_row($sql)) {
                           $id = $row["id"];
                            $sql = "UPDATE user_files SET
                                        user_id = '".$this->_id."',
                                        file_type = '$type',
                                        md5_name = '$file_name',
                                        file_name = '{$file["name"]}',
                                        file_size = '{$file["size"]}',
                                        mime_type = '{$file["type"]}',
                                        upload_date = NOW(),
                                        created_by = '".u()->getEmail()."'
                                    WHERE id = $id
                                    "; 
                            return db_execute($sql);
                        }
                        else {
                            $sql = "INSERT INTO user_files (user_id, file_type, md5_name, file_name, file_size, mime_type,  upload_date, created_by)
                                    VALUES (
                                            '".$this->_id."',
                                            '$type',
                                            '$file_name',
                                            '{$file["name"]}',
                                            '{$file["size"]}',
                                            '{$file["type"]}',
                                            NOW(),
                                            '".u()->getEmail()."'
                                        )
                                    "; 
                            return db_execute($sql);
                        }
                    }
                }
            }
            return false;
        }

        public static function GetUsers($all = false, $include_admin = false, $is_timsheet = true){
            $return = array();

            if($include_admin) $where = "WHERE a.id > 0";
            else $where = "WHERE a.id > 1";

            if(!$all) $and = "AND NOT a.blocked";
            else $and = "";

            if($is_timsheet) {
                $and .= " AND b.is_timesheet";
            }
            $sql = self::$_Sql . "
                LEFT JOIN employees b ON b.user_id = a.id
                WHERE a.id > 1
                {$and}
                ORDER BY a.first_name,  a.last_name
            ";

            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }
        public static function GetActiveUsers(){
            $return = array();

            $sql = self::$_Sql . "
                WHERE a.id > 1 AND NOT a.blocked
                ORDER BY a.first_name,  a.last_name
            ";

            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }
        public static function GetTotalActiveUsers(){
            $total = 0;
            $sql = "SELECT count(id) as total FROM users WHERE id > 1 AND NOT blocked";
            $row = db_get_row($sql);
            if($row) {
               $total = $row['total'];
            }
            
            return $total; 
        }
        public static function GetBlockedUsers(){
            $return = array();

            $sql = self::$_Sql . "
                WHERE a.id > 1 AND a.blocked
                ORDER BY a.first_name,  a.last_name
            ";

            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }
        public static function GetTotalBlockedUsers(){
            $total = 0;
            $sql = "SELECT count(id) as total FROM users WHERE id > 1 AND blocked";
            $row = db_get_row($sql);
            if($row) {
               $total = $row['total'];
            }
            
            return $total; 
        }
        public static function GetBusinessManagers(){
            $return = array();

            $sql = self::$_Sql . "
                WHERE a.id > 1 AND a.roles LIKE '%business_manager%' AND NOT a.blocked
                ORDER BY a.first_name,  a.last_name
            ";

            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;

        }

        public static function GetProjectUsersByProjectIds($project_ids = array()) {
            $return = array();
            if(is_array($project_ids) && count($project_ids) > 0) {
                $sql = self::$_Sql . "
                    INNER JOIN project_users b ON b.user_id = a.id
                    WHERE b.project_id IN (".implode(", ", $project_ids).")
                    AND NOT a.blocked
                    AND a.id > 1
                    GROUP BY b.user_id
                    ORDER BY a.last_name, a.first_name
                ";
                $rows = db_get_all($sql);
                foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            }
            return $return;
        }
        public static function GetUsersByProject($project){
            if($project instanceof Project) $project_id = $project->getId();
            else $project_id = pintval($project);

            $return = array();

            if($project_id > 0){
                $sql = self::$_Sql . "
                    INNER JOIN project_users b ON b.user_id = a.id
                    WHERE b.project_id = {$project_id}
                    AND NOT a.blocked
                    AND a.id > 1
                    ORDER BY a.last_name, a.first_name
                ";

                $rows = db_get_all($sql);
                foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
                return $return;
            }

            return $array;
        }

        public static function getFullNameByEmailId($email) {
            if($email != "") {
                $sql = "SELECT first_name,last_name FROM users WHERE email = '{$email}' LIMIT 1";
                if($rs = db_get_row($sql)){
                    return $rs['first_name'].' '.$rs['last_name'];
                }
                return '';
            }
        }

        public static function getUserIdByEmailId($email) {
            if($email != "") {
                $sql = "SELECT id FROM users WHERE email = '{$email}' LIMIT 1";
                if($rs = db_get_row($sql)){
                    return $rs['id'];
                }
                return 0;
            }
        }
    }
