<?php
    class TaskType {
        private static $_DefaultTaskType = null;
        private static $_TaskTypes = null;
        private static $_TaskTypeGroups = null;

        private $_id = null;
        private $_groupId = null;
        private $_groupName = null;
        private $_name = null;

        public function __construct($id = null){
            if(!is_null($id)){
                $t = self::Get($id);
                $this->_id = $t->getId();
                $this->_groupId = $t->getGroupId();
                $this->_groupName = $t->getGroupName();
                $this->_name = $t->getName();
            }
        }

        public function getId(){ return $this->_id; }
        public function getGroupId(){ return $this->_groupId; }

        public function getGroupName($italicize_default = false){
            if($italicize_default && $this->_groupId < 1) return "<i>{$this->_groupName}</i>";
            else return $this->_groupName;
        }

        public function getName(){ return $this->_name; }

        public function setGroupId($id){
            if(self::$_TaskTypeGroups[$id]){
                $this->_groupId = $id;
                $this->_groupName = self::$_TaskTypeGroups[$id];
            }
        }

        public function setName($name){
            $this->_name = trim($name);
        }

        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        private function _add(){
            if($this->_name){
                $sql = "
                    INSERT INTO task_types (group_id, name)
                    VALUES(
                        " . n($this->_groupId) . ",
                        " . q($this->_name) . "
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_name){
                $sql = "
                    UPDATE task_types SET
                    group_id = " . n($this->_groupId) . ",
                    name = " . q($this->_name) . "
                    WHERE id = {$this->_id}
                ";
                return db_execute($sql);
            }

            return false;
        }

        public function checkName($name, $grp_id) {
            $name = trim($name);
            if(intval($grp_id) > 0) 
                $sql = "SELECT id FROM task_types WHERE name='$name' AND group_id = $grp_id";
            else
                $sql = "SELECT id FROM task_types WHERE name='$name' AND group_id IS NULL";
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }
        public static function Get($id){
            if(self::$_TaskTypes[$id]) return self::$_TaskTypes[$id];
            else return self::$_DefaultTaskType;
        }

        public static function GetAll(){
            return self::$_TaskTypes;
        }

        public static function GetGroups(){
            return self::$_TaskTypeGroups;
        }

        public static function Init(){
            self::$_DefaultTaskType = self::_Init(array("id" => 0, "group_id" => 0, "group_name" => "", "name" => "Miscellaneous"));

            $sql = "
                SELECT a.id, a.group_id, COALESCE(b.name, 'Ungrouped') AS group_name, a.name FROM task_types a
                LEFT JOIN department b ON b.id = a.group_id
                WHERE a.id > 0
                ORDER BY b.name, a.name
            ";

            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_TaskTypes[$row["id"]] = self::_Init($row);

            $sql = "
                SELECT id, name FROM department
                ORDER BY name
                ";

            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_TaskTypeGroups[$row["id"]] = $row["name"];
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_groupId = $array["group_id"];
            $object->_groupName = $array["group_name"];
            $object->_name = $array["name"];
            return $object;
        }
    }

    TaskType::Init();
