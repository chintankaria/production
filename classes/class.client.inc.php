<?php
    include_once("class.project.inc.php");
    include_once("class.user.inc.php");

    class Client {
        private $_id = null;
        private $_name = null;
        private $_description = null;
        private $_website = null;
        private $_isProductClient = null;
        private $_active = null;
        private $_createDate = null;

        private $_countProjects = null;
        private $_projects = null;
        private $_projectUsers = null;
        private $_subprojects = null;
        private $_subprojectUsers = null;
        private $_contacts = null;

        private static $_Sql = "
            SELECT a.id, a.name, a.description, a.website, a.active, COUNT(b.id) AS count_projects, a.is_product_client, 
                UNIX_TIMESTAMP(a.create_date) AS create_date
            FROM clients a
            LEFT JOIN projects b ON b.client_id = a.id AND b.id > 0 AND b.id != 1000
        ";

        public function __construct($id = null){
		 
            $id = pintval($id);
			
            if($id > 0){
		        if(stristr($_SERVER['SCRIPT_NAME'], 'report.list.php') != 'report.list.php' && u()->isAdmin()) 
                    $and = get_project_ids_clause("AND", "b.id");

                $sql = self::$_Sql . "
                    WHERE a.id = {$id}
                    {$and}
                    GROUP BY a.id
                ";
                $row = db_get_row($sql);
                if($row) self::_Init($row, $this);
            }
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_name = $array["name"];
            $object->_description = $array["description"];
            $object->_website = $array["website"];
            $object->_isProductClient = $array["is_product_client"];
            $object->_active = $array["active"];
            $object->_countProjects = $array["count_projects"];
            $object->_createDate = $array["create_date"];
            return $object;
        }

        public function getId(){ return $this->_id; }
        public function getName(){ return $this->_name; }
        public function getDescription(){ return $this->_description; }
        public function getWebsite(){ return $this->_website; }
        public function getIsProductClient(){ return $this->_isProductClient; }
        public function getIsActive(){ return $this->_active; }
        public function countProjects(){ return $this->_countProjects; }
        public function getCreateDate(){ return $this->_createDate; }

        public function setName($name){
            $this->_name = trim($name);
            if(!$this->_name) e("Missing or invalid client name.");
        }
        public function setDescription($description){
            $this->_description = addslashes($description);
        }
        public function setWebsite($website) {
            $this->_website = trim($website);
        }
        public function setIsProductClient($is_product_client) {
            $this->_isProductClient = intval($is_product_client);
        }
        public function setIsActive($active) {
            $this->_active = intval($active);
        }

        public function save(){
            if(u()->isAdmin()){
                if($this->_id) return $this->_update();
                else return $this->_add();
            }
            else return false;
        }
        public function checkClient($name) {
            if($name) {
                $name = addslashes($name);
                $sql ="SELECT id FROM clients WHERE name='$name'";
                $row = db_get_row($sql);
                if($row) 
                    return $row["id"];
            }
            return false;
        }

        private function _add(){
            if($this->_name){
                $sql = "
                    INSERT INTO clients (name,description, website, is_product_client, active, create_date)
                    VALUES (" . q($this->_name) . ", ".q($this->_description).", ".q($this->_website).",'".$this->_isProductClient."','".$this->_active."',NOW())
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_id && $this->_name){
                $sql = "
                    UPDATE clients SET
                        name = " . q($this->_name) . ",
                        description = " . q($this->_description) .",
                        website = " . q($this->_website) .",
                        is_product_client = '" . $this->_isProductClient ."',
                        active = '" . $this->_active ."'
                    WHERE id = {$this->_id}
                ";

                return db_execute($sql);
            }

            return false;
        }

        public function delete(){
            $return = false;

            if($this->_id > 0 && u()->isAdmin()){
                // Delete tasks, projects/subprojects and client
                $sql = "
                    UPDATE tasks SET
                        id = -id,
                        project_id = -project_id
                    WHERE project_id IN (
                        SELECT id FROM projects
                        WHERE client_id = {$this->_id}
                        AND id > 0
                    )
                    AND id > 0
                ";
                if(db_execute($sql)){
                    s("Deleted tasks under projects by client {$this->_name}");
                    $str = "Deleted tasks under project by client <i>{$this->_name}({$this->_id})</i>";
                    watchdog("SUCCESS", "DELETE", $str);
                    $sql = "
                        UPDATE projects SET
                            id = -id,
                            client_id = -client_id
                        WHERE client_id = {$this->_id}
                        AND id > 0
                    ";
                    if(db_execute($sql)){
                        s("Deleted projects by client {$this->_name}");
                        $str = "Deleted projects by client <i>{$this->_name}({$this->_id})</i>";
                        watchdog("SUCCESS", "DELETE", $str);
                        $sql = "
                            UPDATE clients SET
                                id = -id
                            WHERE id = {$this->_id}
                        ";

                        if(db_execute($sql)){
                            $str = "Deleted client <i>{$this->_name}({$this->_id})</i>";
                            watchdog('SUCCESS','DELETE', $str);
                            $str = "Deleted client <i>{$this->_name}</i>";
                            s($str);
                            $return = true;
                        }
                        else {
                            $str = "Failed to delete client <i>{$this->_name}</i>";
                            watchdog('FAILED','DELETE', $str);
                            e($str);
                        }
                    }
                    else {
                        $str = "Failed to delete projects by client <i>{$this->_name}</i>";
                        watchdog('FAILED','DELETE', $str);
                        e($str);
                    }
                }
                else {
                    $str = "Failed to delete tasks under projects by client <i>{$this->_name}</i>";
                    watchdog('FAILED','DELETE', $str);
                    e($str);
                }
            }

            return $return;
        }

        private function _loadSubprojects($user_id = 0, $project_id = 0, $end_date = null){
            if (!$user_id) 
                $this->_subprojects = Project::GetSubprojectsByProject($project_id);
            else {
                $user = new User($user_id);
                if($end_date) {
                    $this->_subprojects = Project::GetSubprojectsFilterByUser($project_id, $user, $this,$end_date);
                } else {
                    $this->_subprojects = Project::GetSubprojectsByUser($project_id, $user, $this);
                }
            }
        }

        public function getSubprojectsByProject($project_id = 0){
            $this->_subprojects = Project::GetSubprojectsByProject($project_id);
            return $this->_subprojects;
        }

        public function getSubprojectsByUser($user_id, $project_id = 0, $end_date = null){
            $this->_loadSubprojects($user_id, $project_id, $end_date);
            return $this->_subprojects;
        }

        private function _loadProjects($user_id = 0, $only_parents = false, $end_date = null){
            if (!$user_id) 
                $this->_projects = Project::GetProjectsByClient($this, true);
            else {
                $user = new User($user_id);
                if($end_date) {
                    $this->_projects = Project::GetProjectsFilterByUser($user, $this, true, $end_date);
                } else {
                    $this->_projects = Project::GetProjectsByUser($user, $this, true);
                }
            }
        }

        public function getProjectsByClient($client_id,$only_parents) {
            $this->_projects = Project::GetProjectsByClient($client_id, $only_parents);
            return $this->_projects;
        }
        public function getProjectsByUser($user_id, $only_parents = false, $end_date = null){
            $this->_loadProjects($user_id, $only_parents, $end_date);
            return $this->_projects;
        }

        public function getProjects($user_id = 0, $only_parents = false){
            $this->_loadProjects($user_id, $only_parents);
            return $this->_projects;
        }

        public function getUsersByProject($project_id) {
            if($project_id > 0) {
                $project = new Project($project_id);
                $this->_projectUsers = $project->getUsers();
                #$this->_projectUsers = User::GetUsersByProject($project_id);
                return $this->_projectUsers;
            }           
        }

        public function getUsersBySubprojects($subproject_id) {
            if($project_id > 0) {
                $this->_subprojectUsers = User::GetUsersByProject($subproject_id);
            }           
        }

        public function getProject($project){
            if($project instanceof Project) $project_id = $project->getId();
            else $project_id = intval($project);

            if($project_id > 0){
                $this->_loadProjects();
                if($this->_projects[$project_id]) return $this->_projects[$project_id];
            }

            return false;
        }

        public static function GetClients($user_id = 0, $all_clients = 0){
            $return = array();
			$user = u();
            if ($user_id && $user->getId() != $user_id) {
                $user = new User($_GET["user_id"]);
            }
			if(stristr($_SERVER['SCRIPT_NAME'], 'report.list.php') == 'report.list.php' && !$user->isAdmin()) {
				$sql_member = " SELECT a.id, a.name, COUNT(b.id) AS count_projects, UNIX_TIMESTAMP(a.create_date) AS create_date 
								FROM clients a 
								LEFT JOIN projects b ON b.client_id = a.id 
								LEFT JOIN tasks t ON t.project_id = b.id
								WHERE a.id > 0 
								AND t.user_id = ".$user->getId()."
								GROUP BY a.id ORDER BY a.name  
							";
				$rows_member = db_get_all($sql_member);
	            foreach($rows_member as $row_member) $return[$row_member["id"]] = self::_Init($row_member);
				
			}
            if($user_id > 0) {
                $user = new User($user_id);
                $and = get_project_ids_clause("AND", "b.id", $user->getId());
            }
            else if($user->isAdmin()) {
                if(!$all_clients)
                    $and = get_project_ids_clause("AND", "b.id", $user->getId());
            }
            else {
                $and = get_project_ids_clause("AND", "b.id", $user->getId());
            }
            if(!$all_clients) {
                $and .= " AND active";
            }
            $sql = self::$_Sql . "
                WHERE a.id > 0 
                {$and}
                GROUP BY a.id
                ORDER BY a.name
            ";
            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }

        public static function GetProductClients($hasLicense = false){
            $return = array();
            $sql = "
                 SELECT a.id, a.name, a.description, a.website, a.active, a.is_product_client, UNIX_TIMESTAMP(a.create_date) AS create_date
                 FROM clients a
                 ";
            if($hasLicense) {
                $sql .= ", licenses b WHERE a.id = b.client_id ";
            }
            else {
                $sql .= " WHERE a.id > 0 AND a.is_product_client = 1 ";
            }
            $sql .= "ORDER BY a.name";

            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }

        public static function GetClientListByName($name = '') {
            $and = '';
            if($name)
                $and = "WHERE name LIKE '$name%'";
            $sql = "SELECT id, name FROM clients $and ORDER By name ASC";
            $rows = db_get_all($sql);
            if($rows) return $rows;
            return false;
        }

        public function getContacts() {
            if(is_null($this->_contacts)) {
                $this->_loadContacts();
            }

            return $this->_contacts;
        }

        private function _loadContacts() {
            $sql = "SELECT id FROM client_contacts WHERE client_id = $this->_id";
            $this->_contacts = ClientContact::GetContacts($this->_id);
        }
    }
