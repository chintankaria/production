<?php
## sk(shekhar koli) < skonealone@hotmail.com http://skonealone.110mb.com/ http://skonealone.co.nr/ >

class Pagination{
	var $SelectBoxStyleArray;
	var $NumberStyleArray;
	var $PageLimit=10;
	var $TotalPages=0;
	var $TotalResult=0;
	var $CurrentPage=1;
	var $PreviousPage=0;
	var $NextPage=0;
	var $FromPage=0;		// from page number
	var $ToPage=0;			// To page number
	var $PageNumberLimit=5; // limit the show of page number in a page
	var $Result;
	var $SelectBoxStyle=false;	// page number can be put in HTML select boxes
	var $NumberStyle=true;		// page number can be shown in the page itself
	
	##########################

	function getNumberStyle(){
		return $this->NumberStyle;
	}
	function setNumberStyle($number_style=true){
		$this->NumberStyle=$number_style;
	}
	function getSelectBoxStyle(){
		return $this->SelectBoxStyle;
	}
	function setSelectBoxStyle($select_box_style=false){
		$this->SelectBoxStyle=$select_box_style;
	}
	function getPageNumberLimit(){
		return $this->PageNumberLimit;
	}
	function setPageNumberLimit($page_number_limit=5){
		$this->PageNumberLimit=$page_number_limit;
	}
	function getToPage(){
		return $this->ToPage;
	}
	function setToPage($to_page=1){
		$this->ToPage=$to_page;
	}
	function getFromPage(){
		return $this->FromPage;
	}
	function setFromPage($from_page=1){
		$this->FromPage=$from_page;
	}
	function getNextPage(){
		return $this->NextPage;
	}
	function setNextPage($next_page=0){
		$this->NextPage=$next_page;
	}
	function getPreviousPage(){
		return $this->PreviousPage;
	}
	function setPreviousPage($previous_page=0){
		$this->PreviousPage=$previous_page;
	}
	function getCurrentPage(){
		return $this->CurrentPage;
	}
	function setCurrentPage($current_page=1){
		$this->CurrentPage=$current_page;
	}
	function getPageLimit(){
		return $this->PageLimit;
	}
	function setPageLimit($page_limit=10){
		$this->PageLimit=$page_limit;
	}
	function getTotalResult(){
		return $this->TotalResult;
	}
	function setTotalResult($total_results){
		$this->TotalResult=$total_results;
	}
	function getTotalPages(){
		return $this->TotalPages;
	}

	function getNumberStyleArray(){
		return $this->NumberStyleArray;
	}
	function getSelectBoxStyleArray(){
		return $this->SelectBoxStyleArray;
	}
	/**
     * assign setTotalPages var values
     *
     * @param  integer  $total_pages  the total pages
     **/
	function setTotalPages($total_pages){
		$this->TotalPages=$total_pages;
	}

	/**
     * Generate a pagination
     *
     * @param
     **/
	function Paginate(){

		## HTML SelectBox Style Pagination ##########
		for($k = 1; $k <= $this->TotalPages; $k++){
			$ctr[]=$k;
		}
		$this->SelectBoxStyleArray=$ctr;
		## NumberStyle Pagination ###################
		$b=$this->CurrentPage;
		$c=0;
		if($this->TotalPages >0){
			for($k = 1; $k <=$this->PageNumberLimit; $k++){

				if($this->CurrentPage >1){
					if($b >2){
						$c = $b -2;
						if($c <= $this->TotalPages) $ctr2[]=($b-2);
					}
				}
				else{
					if($b <= $this->TotalPages) $ctr2[]=($b);
				}
				if($this->TotalPages == $k){
					break;
				}
				$b++;
			}
		}
		$this->NumberStyleArray=$ctr2;
		#print "<pre>"; print_r($ctr2);
		#print_r($this->NumberStyleArray);
		if($this->TotalPages == 1){
			$c_page=$this->CurrentPage;
			$this->setFromPage($this->CurrentPage);
			$this->setToPage($this->TotalResult);
		}
		elseif($this->TotalPages > 1){
			$this->setPreviousPage($this->CurrentPage-1);
			$this->setNextPage($this->CurrentPage+1);
			$c_page=$this->CurrentPage;
			$this->setFromPage((($this->CurrentPage-1)*$this->PageLimit)+1);

			$this->setToPage($this->FromPage + $this->PageLimit-1);
			if($this->ToPage > $this->TotalResult){
				$this->setToPage($this->TotalResult);
			}
		}

		#######################################
	}
}
?>
