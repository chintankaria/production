<?php
class TriviaPoints {
    private static $_Points = null;

    private $_id = null;
    private $_triviaId = null;
    private $_triviaResponseId = null;
    private $_userId = null;
    private $_dateTime = null;
    private $_points = null;
    private $_createDate = null;
    private $_createdBy = null;
    private $_updateDate = null;
    private $_updatedBy = null;
    private $_deleted = null;
    private $_deleteDate = null;
    private $_deletedBy = null;

    public function __construct($id = null, $deleted = 0) {
        if($id > 0) {
           $sql = "SELECT a.*,CONCAT(b.first_name,' ',b.last_name) as created_by, 
                        CONCAT(d.first_name,' ',d.last_name) as updated_by,
                        CONCAT(c.first_name,' ',c.last_name) as deleted_by
                    FROM trivia_user_points a
                    INNER JOIN users b on b.email = a.created_by
                    LEFT JOIN users d on d.email = a.updated_by
                    LEFT JOIN users c on c.email = a.deleted_by
                    WHERE a.deleted = $deleted AND a.id = $id
                    "; 
            $row = db_get_row($sql);
            if($row) {
                self::_Init($row, $this);
            }
        }
    }
    private static function _Init ($array, $object = null) {
        if(!$object instanceof self) $object = new self;

        $object->_id = $array['id'];
        $object->_userId = $array["user_id"];
        $object->_triviaId = $array["trivia_id"];
        $object->_triviaResponseId = $array["trivia_response_id"];
        $object->_dateTime = $array["date_time"];
        $object->_points = $array["points"]; 
        $object->_createDate = $array["create_date"];
        $object->_createdBy = $array["created_by"];
        $object->_updateDate = $array["update_date"];
        $object->_updatedBy = $array["updated_by"];
        $object->_deleted = $array["deleted"];
        $object->_deleteDate = $array["deleted_date"];
        $object->_deletedBy = $array["deleted_by"];
        return $object;
    }
    
    public function getId() {
        return $this->_id;
    }
    public function getTriviaId() {
        return $this->_triviaId; 
    }
    public function getTriviaResponseId() {
        return $this->_triviaResponseId;
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getPoints() {
        return $this->_points;
    }
    public function getDateTime($format = 'm/d/Y H:i') {
        return date($format, strtotime($this->_dateTime));
    }
    public function getCreateDate($format = 'm/d/Y H:i') {
        return date($format, strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }
    public function getUpdateDate($format = 'm/d/Y H:i') {
        return date($format, strtotime($this->_updateDate));
    }
    public function getUpdatedBy() {
        return $this->_updatedBy;
    }
    public function getDeleted() {
        return $this->_deleted;
    }
    public function getDeletedDate($format = 'm/d/Y H:i') {
        return date($format, strtotime($this->_deleteDate));
    }
    public function getDeletedBy() {
        return $this->_deletedBy;
    }

    public function setTriviaId($trivia_id) {
        $this->_triviaId = $trivia_id;
    }
    public function setTriviaResponseId($trivia_response_id) {
        $this->_triviaResponseId = $trivia_response_id;
    }
    public function setUserId($user_id) {
        $this->_userId = $user_id;
    }
    public function setPoints($points) {
        $this->_points = intval($points);
    }
    public function setDateTime($date_time) {
        $this->_dateTime = ymdhms(is_date($date_time));
    }

    public function save(){
        if($this->_id) return $this->_update();
        else return $this->_add();
    }

    private function _add() {
        if($this->_triviaId > 0 && $this->_userId > 0) {
            $sql = "INSERT INTO trivia_user_points (user_id,trivia_id,trivia_response_id, date_time, points, create_date, created_by,update_date,updated_by)
                        VALUES(
                            '". n($this->_userId) ."',
                            '". $this->_triviaId ."',
                            '". $this->_triviaResponseId ."',
                            '". $this->_dateTime ."',
                            '". $this->_points . "',
                            NOW(),
                            '". u()->getEmail() ."',
                            NOW(),
                            '". u()->getEmail() ."'
                        )
                    ";
            if(db_execute($sql)){
                $this->_id = db_insert_id();
                return true;
            }
        }
        return false;
    }

    private function _update() {
        if($this->_id) {
            $sql = "UPDATE trivia_user_points SET 
                            points = '". $this->_points . "',
                            update_date = NOW(),
                            updated_by = '". u()->getEmail() ."'
                        WHERE id = {$this->_id}                
                    ";
            return db_execute($sql);
        }
        return false;
    }
    public function Delete() {
        if($this->_id) {
            $sql = "UPDATE trivia_user_points SET
                        deleted = 1,
                        deleted_date = NOW(),
                        deleted_by = '".u()->getEmail()."'
                        WHERE id = {$this->_id}
                    ";
            return db_execute($sql);
        }
        return false;
    }
    
    public static function GetTriviaPoints($user_id = 0, $trivia_id = 0, $trivia_response_id = 0) {
        $and = "";
        if($user_id > 0) {
            $and = " AND a.user_id = $user_id";
        } 
        if($trivia_id > 0) {
            $and .= " AND a.trivia_id = $trivia_id";
        } 
        if($trivia_response_id > 0) {
            $and .= " AND a.trivia_response_id = $trivia_response_id";
        } 

        $sql = "SELECT a.*, c.title,d.response,
                    CONCAT(b.first_name,' ',b.last_name) as full_name, b.gender
                FROM trivia_user_points a
                INNER JOIN users b ON b.id = a.user_id
                INNER JOIN trivia c ON c.id = a.trivia_id
                LEFT JOIN trivia_responses d ON d.id = a.trivia_response_id
                WHERE a.deleted = 0 $and
                ";

        $rows = db_get_all($sql);
        foreach($rows as $row){
            self::$_Points[$row['trivia_response_id']] = self::_Init($row); 
        }
        if($trivia_response_id > 0) {
            return self::$_Points[$trivia_response_id];
        }
        else {
            return self::$_Points;
        }
    }
}
?>
