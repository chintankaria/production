<?php
class EmailBroadcast {
    private static $_Broadcast = null;
    private $_broadcastLogs = null;
    private static $_totalResults = null;
    private static $_totalPages = null;

    private $_id = null;
    private $_dateTime = null;
    private $_subject = null;
    private $_body = null;
    private $_createDate = null;
    private $_createdBy = null;
    

    private $_broadcastId = null;
    private $_broadcastLogId = null;
    private $_userId = null;
    private $_recipientName = null;
    private $_recipientEmail = null;
    private $_broadcastDate = null;


    public function __construct ($id = null) {
        $id = pintval($id);
        if($id > 0) {
            $sql = "SELECT a.*, CONCAT(b.first_name,' ',b.last_name) as created_by
                    FROM email_broadcast a
                    INNER JOIN users b ON b.email = a.created_by
                    WHERE a.id={$id}
                    ";
            $row = db_get_row($sql);
            if($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null){
        if(!$object instanceof self) $object = new self;

        $object->_id = $array["id"];
        $object->_dateTime = $array['date_time'];
        $object->_subject = $array['subject'];
        $object->_body = $array['body'];
        $object->_createDate = $array["create_date"];
        $object->_createdBy = $array["created_by"];

        return $object;
    } 
    
    private static function _InitBroadcastLogs($array, $object = null) {
        if(!$object instanceof self) $object = new self;

        $object->_broadcastId = $array['broadcast_id'];
        $object->_broadcastLogId = $array['id'];
        $object->_userId = $array['user_id'];
        $object->_recipientName = $array['recipient'];
        $object->_recipientEmail = $array['email'];
        $object->_broadcastDate = $array['date_time'];

        return $object;
    }

    public function getId() {
        return $this->_id;
    }
    public function getDateTime($format = "m/d/Y") {
        return date($format, strtotime($this->_dateTime));
    }
    public function getSubject() {
        return $this->_subject;
    }
    public function getBody() {
        return $this->_body;
    }
    public function getCreateDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }

    public function setSubject($subject) {
        $this->_subject = trim($subject);
    }
    public function setBody($body) {
        $this->_body = trim($body);
    }

    public function getBroadcastLogId() {
        return $this->_broadcastLogId;
    }
    public function getBroadcastLogDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_broadcastDate));
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getRecipientName() {
        return $this->_recipientName;
    }
    public function getRecipientEmail() {
        return $this->_recipientEmail;
    }
    public function GetBroadcastLogs() {
        if(is_null($this->_broadcastLogs)) $this->_loadBroadcastLogs();
    
        return $this->_broadcastLogs;
    }

    public function getTotalResults() {
        return self::$_totalResults;
    }
    public function getTotalPages() {
        return self::$_totalPages;
    }
    public function setUserId($uid) {
        $this->_userId = intval($uid);
    }

    public function save(){
        if($this->_id) return $this->_update();
        else return $this->_add();
    }
    public function saveBroadcastLogs(){
        return $this->_addBroadcastLogs();
    }

    private function _add() {
        if($this->_subject) {
            $sql = "INSERT INTO email_broadcast (date_time, subject, body, create_date, created_by)
                    VALUES(
                        NOW(),
                        " . q($this->_subject) . ",
                        " . q($this->_body) . ",
                        NOW(),
                        ". q(u()->getEmail())."
                    );
                ";
            if(db_execute($sql)){
                $this->_id = db_insert_id();
                return true;
            }
        }
        return false;
    }
    private function _addBroadcastLogs() {
        if($this->_id) {
            $sql = "INSERT INTO email_broadcast_log (broadcast_id, date_time, user_id) VALUES({$this->_id},NOW(),{$this->_userId})";
            if(db_execute($sql)) {
                return true;
            }
        }
        return false;
    }
    private function _update() {
        if($this->_id) {
        }
        return false;
    }
    private function _loadBroadcastLogs() {
        if($this->_id) {
            $sql = "SELECT a.*, CONCAT(b.first_name,' ',b.last_name) as recipient, b.email
                    FROM email_broadcast_log a
                    INNER JOIN users b ON b.id = a.user_id            
                    WHERE a.broadcast_id = {$this->_id}";

            $pRows = db_get_all($sql);
            if($pRows) {
                foreach($pRows as $pRow) $this->_broadcastLogs[$pRow["id"]]=self::_InitBroadcastLogs($pRow);
            }
        }
    }

    public static function GetAll($from = null, $to = null, $p = 0, $page_limit = 10 ) {
        $return = array();
        $and = "";
        
        $total_pages=0;
        $total_results=0;

        if($from != null && $to != null) {
            $from = ymd($from);
            $to = ymd($to);
            if($and == "")
                $and = "WHERE (DATE_FORMAT(a.date_time, '%Y-%m-%d') BETWEEN '$from' AND '$to')";
            else
                $and .= " AND (DATE_FORMAT(a.date_time, '%Y-%m-%d') BETWEEN '$from' AND '$to')";
        }

        $sql = "SELECT SQL_CALC_FOUND_ROWS a.*, CONCAT(b.first_name,' ',b.last_name) as created_by
                    FROM email_broadcast a
                    INNER JOIN users b ON b.email = a.created_by
                    $and
                    ORDER BY a.date_time DESC
                    ";
        if($p > 0) {
            // Pagination Query
            $sql .= " LIMIT ".(($p-1)*$page_limit).",".$page_limit;
        }

        $rows = db_get_all($sql);
        if($rows) {
            if($p > 0) {
                $sql = "SELECT FOUND_ROWS() AS total";
                $ttl = db_get_row($sql);
                $total_results = $ttl["total"];
                if($total_results > 0) {
                    self::$_totalResults = $total_results;
                    $total_pages = ceil($total_results/$page_limit);
                    self::$_totalPages = $total_pages;
                }
            }

            foreach($rows as $row){
                self::$_Broadcast[$row['id']] = self::_Init($row);
                $sql = "SELECT a.*, CONCAT(b.first_name,' ',b.last_name) as recipient, b.email
                    FROM email_broadcast_log a
                    INNER JOIN users b ON b.id = a.user_id
                    WHERE a.broadcast_id = {$row['id']}";

                $pRows = db_get_all($sql);
                if($pRows) {
                    foreach($pRows as $pRow) {
                        self::$_Broadcast[$row['id']]->_broadcastLogs[$pRow["id"]]=self::_InitBroadcastLogs($pRow);
                    }
                }
            } 
        }
        return self::$_Broadcast;
    }
    
}
?>
