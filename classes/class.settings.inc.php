<?php
    class Settings {
        private static $_Default = null;
        private static $_Settings = null;

        private $_id = null;
        private $_settingsKey = null;
        private $_settingsValue = null;
        private $_createdBy = null;
        private $_createDate = null;
        private $_updatedBy = null;
        private $_updateDate = null;

        public function __construct($key = null){
            if(!is_null($key)){
                $t = self::Get($key);
                if($t instanceof self) {
                    $this->_id = $t->getId();
                    $this->_settingsKey = $t->getSettingsKey();
                    $this->_settingsValue = $t->getSettingsValue();
                    $this->_createdBy = $t->getCreatedBy();
                    $this->_createDate = $t->getCreateDate();
                    $this->_updatedBy = $t->getUpdatedBy();
                    $this->_updateDate = $t->getUpdateDate();
                }
            }
        }

        public function getId() { return $this->_id; }
        public function getSettingsKey(){ 
            return $this->_settingsKey; 
        }
        public function getSettingsValue(){ 
            return $this->_settingsValue;
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getCreateDate(){
            return date("m/d/Y H:i",$this->_createDate);
        }
        public function getUpdatedBy() {
            return $this->_updatedBy;
        }
        public function getUpdateDate() {
            return date("m/d/Y H:i", $this->_updateDate);
        }

        public function setSettingsKey($key){
            $this->_settingsKey = $key;
        }
        public function setSettingsValue($value){
            if(is_array($value) && count($value) > 0) {
                // setting value is an arry, so let's store in serialized format
                $this->_settingsValue = serialize($value);
            }
            else {
                $this->_settingsValue = $value;
            }
        }
        
        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function checkSettingsKey() {
            $and = '';
            if($this->_id) $and = " AND id != {$this->_id}";
            $sql = "SELECT id FROM timesheet_settings
                    WHERE settings_key = " . q($this->_settingsKey) . " $and
                    ";
            
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        private function _add(){
            if($this->_settingsKey){
                $sql = "
                    INSERT INTO timesheet_settings ( settings_key,settings_value, created_by, create_date, updated_by, update_date)
                    VALUES(
                        " . q($this->_settingsKey) . ",
                        " . q($this->_settingsValue) . ",
                        '" . u()->getEmail() ."',
                        NOW(),
                        '" . u()->getEmail() ."',
                        NOW()
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    self::Init();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_id && $this->_settingsKey){
                $sql = "
                    UPDATE timesheet_settings SET
                    settings_key = " . q($this->_settingsKey) . ",
                    settings_value = " . q($this->_settingsValue) . ",
                    updated_by = '" . u()->getEmail() ."',
                    update_date = NOW()
                    WHERE id = {$this->_id}
                ";
                self::Init();
                return db_execute($sql);
            }

            return false;
        }

        public static function Get($key){
            if(self::$_Settings[$key]) return self::$_Settings[$key];
            else return self::$_Default;
        }

        public static function GetAll(){
            return self::$_Settings;
        }

        public static function Init(){

            $sql = "
                SELECT a.id, a.settings_key, a.settings_value, UNIX_TIMESTAMP(a.create_date) as create_date, UNIX_TIMESTAMP(a.update_date) as update_date,
                CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                FROM timesheet_settings a
                INNER JOIN users b on b.email = a.created_by
                INNER JOIN users c on c.email = a.updated_by
                WHERE a.id > 0
                ORDER BY a.settings_key
            ";

            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_Settings[$row["settings_key"]] = self::_Init($row);

        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_settingsKey = $array["settings_key"];
            $value = @unserialize($array['settings_value']);
            if($array['settings_value'] === 'b:0;' || $value !== false) {
                // Value stored in serialized format, let's unserialize it
                $value = unserialize($array['settings_value']);
            }
            else {
                // Value is not stored in serialized format
                $value = $array['settings_value'];
            }
            $object->_settingsValue = $value;
            $object->_createdBy = $array["created_by"];
            $object->_createDate = $array["create_date"];
            $object->_updatedBy = $array["updated_by"];
            $object->_updateDate = $array["update_date"];

            return $object;
        }
    }
    Settings::Init();

