<?php
class Passport {

    private $_id = null;
    private $_passportId = null;
    
    // Passport Info
    private $_passportNumber = null;
    private $_passportIssueDate = null;
    private $_passportExpiryDate = null;
    private $_passportPlaceOfIssue = null;
    private $_passportCountryOfResidence = null;
    public $_passInfo = null;
    public $_visaInfo = null;

    // Visa Info
    private $_visaIds = null;
    private $_visaId = null;
    private $_visaCountry = null;
    private $_visaType = null;
    private $_visaIssueDate = null;
    private $_visaExpiryDate = null;
    private $_visaPlaceOfIssue = null;

    
    public function __construct($id = null, $user_id = null){

         $return = array();

        if($id > 0 || $user_id > 0) {
            if($id > 0 && $user_id > 0) {
                $where = " WHERE id = $id AND user_id = $user_id";
            }
            else if ($id > 0) {
                $where = " WHERE id = $id";
            }
            else if ($user_id > 0) {
                $where = " WHERE user_id = $user_id";
            }
            $sql = "SELECT *
                    FROM passport   
                    $where";

            $rows = db_get_all($sql);
            if($rows) {
                foreach($rows as $row) {
                    $this->_passInfo[$row['id']] = self::_Init($row);  
                    self::getVisaInfo();
                }
            }
        }        
    }

    private static function _Init($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_id = $array["id"];
        $object->_passportId = $array["passport_id"];
        $object->_passportNumber = $array["passport_number"];
        $object->_passportIssueDate = $array["issue_date"];
        $object->_passportExpiryDate = $array["expiry_date"];
        $object->_passportPlaceOfIssue = $array["place_of_issue"];
        $object->_passportCountryOfResidence = $array["country_of_residence"];
        
        return $object;
    }
    
    private static function _InitVisaInfo($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_visaId = $array["id"];
        $object->_visaType = $array["visa_type"];
        $object->_visaCountry = $array["country"];
        $object->_visaIssueDate = $array["issue_date"];
        $object->_visaExpiryDate = $array["expiry_date"];
        $object->_visaPlaceOfIssue = $array["place_of_issue"];

        return $object;
    }
    

    public function getId() {
        return $this->_id;
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getPassportId() {
        return $this->_passportId;
    }
    public function getPassportNumber() {
        return $this->_passportNumber;
    }
    public function getPassportIssueDate($format = 'd-M-Y') {
        return format_date($this->_passportIssueDate,$format);
    }
    public function getPassportExpiryDate($format = 'd-M-Y') {
        return format_date($this->_passportExpiryDate,$format);
    }
    public function getPassportPlaceOfIssue() {
        return $this->_passportPlaceOfIssue;
    }
    public function getPassportCountryOfResidence() {
        return $this->_passportCountryOfResidence;
    }

    
    // Visa Info
    public function getVisaId() {
        return $this->_visaId;
    }
    public function getVisaType() {
        return $this->_visaType;
    }
    public function getVisaCountry() {
        return $this->_visaCountry;
    }
    public function getVisaIssueDate($format='d-M-Y') {
        return format_date($this->_visaIssueDate,$format);
    }
    public function getVisaExpiryDate($format='d-M-Y') {
        return format_date($this->_visaExpiryDate,$format);
    }
    public function getVisaPlaceOfIssue() {
        return $this->_visaPlaceOfIssue;
    }

    public function setPassportId($passport_id) {
        $this->_passportId = $passport_id;
    }
    public function setPassportNumber($passport_number) {
        $this->_passportNumber = $passport_number;
    }
    public function setPassportIssueDate($issue_date) {
        if($issue_date)
            $this->_passportIssueDate = ymd(is_date($issue_date));
    }
    public function setPassportExpiryDate($expiry_date) {
        if($expiry_date)
        $this->_passportExpiryDate = ymd(is_date($expiry_date));
    }
    public function setPassportPlaceOfIssue($place_of_issue) {
        $this->_passportPlaceOfIssue = addslashes($place_of_issue);
    }
    public function setPassportCountryOfResidence($country_of_residence) {
        $this->_passportCountryOfResidence = $country_of_residence;
    }
    public function setUserId($user_id) {
        $this->_userId = $user_id;
    }
   
    // Visa Info
    public function setVisaId($id) {
        $this->_visaId = $id;
    }
    public function setVisaType($visa_type) {
        $this->_visaType = addslashes(trim($visa_type));
    }
    public function setVisaCountry($country) {
        $this->_visaCountry = addslashes(trim($country));
    }
    public function setVisaIssueDate($idate) {
        if($idate)
            $this->_visaIssueDate = ymd(is_date($idate));
    }
    public function setVisaExpiryDate($edate) {
        if($edate) 
            $this->_visaExpiryDate = ymd(is_date($edate));
    }
    public function setVisaPlaceOfIssue($place_of_issue) {
        $this->_visaPlaceOfIssue = addslashes(trim($place_of_issue));
    }
    
    private function _loadVisaInfo() {
         $sql = "SELECT a.*
                FROM passport_visa_info a
                INNER JOIN passport b ON b.id = a.passport_id
                WHERE a.passport_id = {$this->_id}
                ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $this->_visaInfo[$row['id']] = self::_InitVisaInfo($row);        
                $this->_visaIds[$row['id']] = $row['id'];
            }
        }
    }
    public function getVisaInfo() {
        if(is_null($this->_visaInfo))
            $this->_loadVisaInfo();
        return $this->_visaInfo;
    }

    
    public function save($id = null) {

        if($id) return $this->_update();
        else return $this->_addPassport();
    }

    
    private function _addPassport() {
        if($this->_passportNumber) {
            $sql = "INSERT INTO passport (user_id, passport_number, issue_date, expiry_date, place_of_issue, country_of_residence)
                    VALUES (
                            '". $this->_userId."',
                            '" . $this->_passportNumber ."',
                            '" . $this->_passportIssueDate ."',
                            '" . $this->_passportExpiryDate ."',
                            '" . $this->_passportPlaceOfIssue ."',
                            '" . $this->_passportCountryOfResidence ."'
                        );
               ";
            if(db_execute($sql)){
                $this->_passportId = db_insert_id();
                return true;
            }
        }
         return false;
    }
    public function saveVisaInfo() {

        
        if($this->_passportId) {

            // Now insert visa info
            $sql = "INSERT INTO passport_visa_info (passport_id, visa_type, country, issue_date, expiry_date, place_of_issue)
                    VALUES (
                            '" . $this->_passportId ."',
                            '" . $this->_visaType ."',
                            '" . $this->_visaCountry ."',
                            '" . $this->_visaIssueDate ."',
                            '" . $this->_visaExpiryDate ."',
                            '" . $this->_visaPlaceOfIssue ."'
                        );
               ";

            if(db_execute($sql)){
                $this->_visaId = db_insert_id();
                return true;
            }
        }
        return false;
    }

    public function deletePassport($id = 0) {

        if($this->_passportId) {

              $sql = "DELETE FROM passport 
                    WHERE id = {$this->_passportId}"; 

            if (db_execute($sql)) {
                return true;
            }
        }
        return false;
    }
    public function deletedVisaInfo($id = 0) {
        $and = '';
        
        if($this->_passportId) {
            // Delete visa info
            $sql = "DELETE FROM passport_visa_info 
                    WHERE passport_id = {$this->_passportId}"; 

         if($id) {
                $and = " AND id = $id";
                $sql .= $and;
            }
          
             if (db_execute($sql)) {
                return true;
            }
        }
        return false;
    }
    public function checkVisaInfo($passport_id, $visa_type,$country,$issue_date,$expiry_date,$place_of_issue ) {
        $issue_date = ymd(is_date($issue_date));
        $expiry_date = ymd(is_date($expiry_date));

        $sql = "SELECT id FROM passport_visa_info WHERE passport_id = '$passport_id' AND visa_type = '$visa_type' AND country = '$country' AND
                issue_date = '$issue_date' AND expiry_date = '$expiry_date' AND place_of_issue = '$place_of_issue'";
        $row = db_get_row($sql);
        if($row) {
            return $row['id'];
        }
        return false;
    }
       
    private function _update() {

        if($this->_passportId < 1) {
            $this->_addPassport();
        }
        else return $this->_updatePassport();

    }
    
    private function _updatePassport() {
        if($this->_passportId) {
            $sql = "UPDATE passport SET
                        user_id = '". $this->_userId."',
                        passport_number = '" . $this->_passportNumber ."',
                        issue_date = '" . $this->_passportIssueDate ."',
                        expiry_date = '" . $this->_passportExpiryDate ."',
                        place_of_issue = '" . $this->_passportPlaceOfIssue ."',
                        country_of_residence = '" . $this->_passportCountryOfResidence ."'
                    WHERE id = {$this->_passportId}
                    ";

            if (db_execute($sql)) {
                return true;
            }
        }
        return false;
    }

        public function updatVisa() {

        if($this->_visaId) {
            $sql = "UPDATE passport_visa_info SET

                        visa_type = '" . $this->_visaType ."',
                        country = '" . $this->_visaCountry ."',
                        issue_date = '" . $this->_visaIssueDate ."',
                        expiry_date = '" . $this->_visaExpiryDate ."',
                        place_of_issue = '" . $this->_visaPlaceOfIssue ."'
                    WHERE id = {$this->_visaId}
                    ";

           if (db_execute($sql)) {
                return true;
            }
            
        }
        return false;
    }
    
}
?>
