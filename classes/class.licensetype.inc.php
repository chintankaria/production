<?php

class LicenseType {
	
	private $_id;
	private $_name;
	private $_activeLicenseCount;
	private $_editable;
	private $_description;
	
	public function __construct($array = array()) {
		if($array) {
			$this->_id = $array['id'];
			$this->_name = $array['name'];
			$this->_activeLicenseCount = $array['active_license_count'];
			$this->_editable = (bool) $array['editable'];
			$this->_description = $array['description'];
		}
	}
	
	public function getId() {
		return $this->_id;
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function setName($name) {
		$this->_name = $name;
	}
	
	public function getActiveLicenseCount() {
		return $this->_activeLicenseCount;
	}
	
	public function isEditable() {
		return $this->_editable;
	}
	
	public function getDescription() {
		return $this->_description;
	}
	
	public function setDescription($description) {
		$this->_description = $description;
	}
}