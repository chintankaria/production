<?php
    class Contact {

        private static $_totalResults = null;
        private static $_totalPages = null;
        private static $_Contacts = null;
        
        private $_id = null;
        private $_firstName = null;
        private $_middleName = null;
        private $_lastName = null;
        private $_active = null;
        private $_groupId = null;
        private $_groupName = null;
        private $_clientId = null;
        private $_clientName = null;
        private $_isClient = null;
        
        private $_createDate = null;
        private $_createdBy = null;
        private $_updateDate = null;
        private $_updatedBy = null;
        private $_deleted = null;
        private $_deleteDate = null;
        private $_deletedBy = null;

        private $_communication = null;
        private $_communicationId = null;
        private $_communicationTypeId = null;
        private $_communicationType = null;
        private $_details = null;
        private $_extension = null;
        private $_communicationActive = null;
        private $_isPrimaryCommunication = null;

        private $_addressBook = null;
        private $_addressId = null;
        private $_contactTypeId = null;
        private $_contactType = null;
        private $_address = null;
        private $_city = null;
        private $_zip = null;
        private $_state = null;
        private $_countryId = null;
        private $_countryName = null;
        private $_addressActive = null;
        private $_isPrimaryAddress = null;

    
        public function __construct ($id = null) {
            $id = pintval($id);
            if($id > 0) {
                $sql = "SELECT a.*, d.name as group_name,e.name as client_name, e.is_client,
                            CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by 
                        FROM contacts a
                        INNER JOIN users b ON b.email = a.created_by
                        INNER JOIN users c ON c.email = a.updated_by
                        INNER JOIN clients e on e.id = a.client_id
                        LEFT JOIN contact_groups d ON d.id = a.group_id
                        WHERE a.id={$id}
                        ";
                $row = db_get_row($sql);
                if($row) {
                    self::_Init($row, $this);
                }
            }
        }
        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;

            $object->_id = $array["id"];
            $object->_firstName = $array["first_name"];
            $object->_middleName = $array["middle_name"];
            $object->_lastName = $array["last_name"];
            $object->_active = $array["active"];
            $object->_groupId = $array["group_id"];
            $object->_groupName = $array["group_name"];
            $object->_clientId = $array["client_id"];
            $object->_clientName = $array["client_name"];
            $object->_isClient = $array["is_client"];
            $object->_createDate = $array["create_date"];
            $object->_createdBy = $array["created_by"];
            $object->_updateDate = $array["update_date"];
            $object->_updatedBy = $array["updated_by"];
            $object->_deleted = $array["deleted"];
            $object->_deleteDate = $array["deleted_date"];
            $object->_deletedBy = $array["deleted_by"];
            
            return $object;
        }
        private static function _InitCommunication($array, $object = null){
            if(!$object instanceof self) $object = new self;
            
            $object->_communicationId = $array["communication_id"];
            $object->_communicationTypeId = $array["communication_type_id"];
            $object->_communicationType = $array["communication_type"];
            $object->_details = $array["details"];
            $object->_extension = $array["extension"];
            $object->_communicationActive = $array["active"];
            $object->_isPrimaryCommunication = $array["is_primary"];
            
            return $object;
            
        }
        private static function _InitAddress($array, $object = null){
            if(!$object instanceof self) $object = new self;

            $object->_addressId = $array["address_id"];
            $object->_contactTypeId = $array["contact_type_id"];
            $object->_contactType = $array["contact_type"];
            $object->_address = $array["address"];
            $object->_city = $array["city"];
            $object->_zip = $array["zip"];
            $object->_state = $array["state"];
            $object->_countryId = $array["country_id"];
            $object->_countryName = $array["country_name"];
            $object->_addressActive = $array["active"];
            $object->_isPrimaryAddress = $array["is_primary"];

            return $object;

        }

        public function getTotalResults() {
            return self::$_totalResults;
        }
        public function getTotalPages() {
            return self::$_totalPages;
        }
        public function getId() {
            return $this->_id;
        }
        public function getClientId() {
            return $this->_clientId;
        }
        public function getClientName() {
            return $this->_clientName;
        }
        public function getIsClient(){ 
            return $this->_isClient; 
        }
        public function getGroupId() {
            return $this->_groupId;
        }
        public function getGroupName() {
            return $this->_groupName;
        }
        public function getFullName() {
            return $this->_firstName . ' '. $this->_middleName . ' '. $this->_lastName;
        }
        public function getFirstName() {
            return $this->_firstName;
        }
        public function getMiddleName() {
            return $this->_middleName;
        }
        public function getLastName() {
            return $this->_lastName;
        }
        public function getActive() {
            return $this->_active;
        }

        public function getCreateDate() {
            return date("m/d/Y H:i", strtotime($this->_createDate));
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getUpdateDate() {
            return date("m/d/Y H:i", strtotime($this->_updateDate));
        }
        public function getUpdatedBy() {
            return $this->_updatedBy;
        }
        public function getDeleted() {
            return $this->_deleted;
        }
        public function getDeletedDate() {
            return date("m/d/Y H:i", strtotime($this->_deleteDate));
        }
        public function getDeletedBy() {
            return $this->_deletedBy;
        }

        public function setTotalResults($total_results) {
            $this->_totalResults = $total_results;
        }
        public function setTotalPages($total_pages) {
            $this->_totalPages = $total_pages;
        }
        public function setClientId($client_id) {
            $this->_clientId = $client_id;
        }
        public function setGroupId($group_id) {
            $this->_groupId = $group_id;
        }
        public function setFirstName($first_name) {
            $this->_firstName = trim($first_name);
        }
        public function setMiddleName($middle_name) {
            $this->_middleName = trim($middle_name);
        }
        public function setLastName($last_name) {
            $this->_lastName = trim($last_name);
        }
        public function setActive($active) {
            $this->_active = intval($active);
        }

        public function getCommunication() {
            if(is_null($this->_communication))
                $this->_loadCommunication();
            return $this->_communication;
        }
        private function _loadCommunication() {
            if($this->_id) {
                $sql = "SELECT a.id as communication_id, a.communication_type_id, a.details, a.extension, a.active, 
                            a.is_primary, b.name as communication_type 
                        FROM  contact_communication a
                        INNER JOIN communication_type b ON b.id = a.communication_type_id
                        WHERE contact_id = '{$this->_id}'";
                $pRows = db_get_all($sql);
                if($pRows) {
                    foreach($pRows as $pRow) $this->_communication[$pRow["communication_id"]]=self::_InitCommunication($pRow);
                }
            }
        }
        public function getCommunicationId() {
            return $this->_communicationId;
        }        
        public function getCommunicationTypeId() {
            return $this->_communicationTypeId;
        }
        public function getCommunicationType() {
            return $this->_communicationType;
        }
        public function getCommunicationDetails(){
            return $this->_details;
        }
        public function getCommunicationExtension(){
            return $this->_extension;
        }
        public function getCommunicationActive(){
            return $this->_communicationActive;
        }
        public function getIsPrimaryCommunication(){
            return $this->_isPrimaryCommunication;
        }

        public function setCommunicationId($communication_id) {
            $this->_communicationId = $communication_id;
        }        
        public function setCommunicationTypeId($communication_type_id) {
            $this->_communicationTypeId = intval($communication_type_id);
        }
        public function setCommunicationDetails($details){
            $this->_details = addslashes($details);
        }
        public function setCommunicationExtension($extension){
            $this->_extension = trim($extension);
        }
        public function setCommunicationActive($active){
            $this->_communicationActive = intval($active);
        }
        public function setIsPrimaryCommunication($is_primary){
            $this->_isPrimaryCommunication = intval($is_primary);
        }

        public function getAddressBook() {
            if(is_null($this->_addressBook))
                $this->_loadAddressBook();
            return $this->_addressBook;    
        }
        private function _loadAddressBook() {
            if($this->_id) {
                $sql = "SELECT a.id as address_id, a.contact_type_id, a.address, a.city, a.zip, a.state, a.country_id, 
                            a.active, a.is_primary, b.name as contact_type, c.country_name 
                        FROM  contact_address_book a
                        INNER JOIN contact_type b ON b.id = a.contact_type_id
                        INNER JOIN country c ON c.country_id = a.country_id 
                        WHERE contact_id = '{$this->_id}'";
                $pRows = db_get_all($sql);
                if($pRows) {
                    foreach($pRows as $pRow) $this->_addressBook[$pRow["address_id"]]=self::_InitAddress($pRow);
                }
            }
        }
        public function getAddressId() {
            return $this->_addressId;
        }
        public function getContactTypeId() {
            return $this->_contactTypeId;
        }
        public function getContactType() {
            return $this->_contactType;
        }
        public function getAddress() {
            return $this->_address;
        }
        public function getCity() {
            return $this->_city;
        }
        public function getZip() {
            return $this->_zip;
        }
        public function getState() {
            return $this->_state;
        }
        public function getCountryId() {
            return $this->_countryId;
        }
        public function getCountry() {
            return $this->_countryName;
        }
        public function getAddressActive() {
            return $this->_addressActive;
        }
        public function getIsPrimaryAddress() {
            return $this->_isPrimaryAddress;
        }

        public function setAddressId($address_id) {
            $this->_addressId = $address_id;
        }
        public function setContactTypeId($contact_type_id) {
            $this->_contactTypeId = intval($contact_type_id);
        }
        public function setAddress($address) {
            $this->_address = addslashes($address);
        }
        public function setCity($city) {
            $this->_city = addslashes(trim($city));
        }
        public function setZip($zip) {
            $this->_zip = intval($zip);
        }
        public function setState($state) {
            $this->_state = addslashes(trim($state));
        }
        public function setCountryId($country_id) {
            $this->_countryId = intval($country_id);
        }
        public function setAddressActive($active) {
            $this->_addressActive = intval($active);
        }
        public function setIsPrimaryAddress($is_primary) {
            $this->_isPrimaryAddress = intval($is_primary);
        }

        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }
        public function saveCommunication() {
            if($this->_communicationId) return $this->_updateCommunication();
            else return $this->_addCommunication();
        }
        public function saveAddressBook() {
            if($this->_addressId) return $this->_updateAddressBook();
            else return $this->_addAddressBook();
        }
        public function checkContact($client_id, $first_name, $last_name) {
            if($client_id && $first_name && $last_name) {
                $sql = "SELECT id FROM contacts WHERE client_id = $client_id AND first_name = '$first_name' AND last_name = '$last_name'";
                $row = db_get_row($sql);
                if($row)
                    return $row["id"];
            }
            return false;
        }
        private function _add() {
            if($this->_firstName && $this->_lastName) {
                $sql = " INSERT INTO contacts (client_id, group_id, first_name, middle_name, last_name, active, created_by, create_date, updated_by, update_date)
                         VALUES (
                                '".$this->_clientId."',
                                '".$this->_groupId."',
                                '".$this->_firstName."',
                                '".$this->_middleName."',
                                '".$this->_lastName."',
                                '".$this->_active."',
                                '".u()->getEmail()."',
                                NOW(),
                                '".u()->getEmail()."',
                                NOW()
                         );
                        ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }
            return false;
        }
        private function _addCommunication() {
            if($this->_id && $this->_details) {
                $sql = "INSERT INTO contact_communication 
                            (contact_id, communication_type_id, details, extension, active, is_primary, created_by, create_date, updated_by, update_date)
                            VALUES (
                                '".$this->_id."',
                                '".$this->_communicationTypeId."',
                                '".$this->_details."',
                                '".$this->_extension."',
                                '".$this->_communicationActive."',
                                '".$this->_isPrimaryCommunication."',
                                '".u()->getEmail()."',
                                NOW(),
                                '".u()->getEmail()."',
                                NOW()
                            );
                        ";
                if(db_execute($sql)){
                    #$this->_communicationId = db_insert_id();
                    return true;
                }
            }
            return false;
        }
        private function _addAddressBook() {
            if($this->_id && $this->_address) {
                $sql = "INSERT INTO contact_address_book 
                            (contact_id, contact_type_id, address, city, zip, state, country_id, active, is_primary, created_by, create_date, updated_by, update_date)
                            VALUES (
                                '".$this->_id."',
                                '".$this->_contactTypeId."',
                                '".$this->_address."',
                                '".$this->_city."',
                                '".$this->_zip."',
                                '".$this->_state."',
                                '".$this->_countryId."',
                                '".$this->_addressActive."',
                                '".$this->_isPrimaryAddress."',
                                '".u()->getEmail()."',
                                NOW(),
                                '".u()->getEmail()."',
                                NOW()
                            );
                        ";
                if(db_execute($sql)){
                    #$this->_addressId = db_insert_id();
                    return true;
                }
            }
            return false;
        }
        private function _update() {
            if($this->_id) {
                $sql = "UPDATE contacts SET
                            client_id = '".$this->_clientId."',
                            group_id = '".$this->_groupId."',
                            first_name = '".$this->_firstName."',
                            middle_name = '".$this->_middleName."',
                            last_name ='".$this->_lastName."',
                            active = '".$this->_active."',
                            updated_by =  '".u()->getEmail()."',
                            update_date =  NOW()
                        WHERE id = {$this->_id}
                        ";
                
                if(db_execute($sql)){
                    return true;
                }
            }
        }
        private function _updateCommunication() {
            if($this->_communicationId) {
                $sql = "UPDATE contact_communication SET 
                            contact_id = '".$this->_id."',
                            communication_type_id = '".$this->_communicationTypeId."',
                            details = '".$this->_details."',
                            active = '".$this->_communicationActive."',
                            is_primary = '".$this->_isPrimaryCommunication."',
                            updated_by =  '".u()->getEmail()."',
                            update_date =  NOW()
                        WHERE id = {$this->_communicationId}
                        ";
                return db_execute($sql);
            }
            return false;
        }
        private function _updateAddressBook() {
            if($this->_addressId) {
                $sql = "UPDATE contact_address_book SET 
                            contact_id = '".$this->_id."',
                            contact_type_id = '".$this->_contactTypeId."',
                            address = '".$this->_address."',
                            city = '".$this->_city."',
                            zip = '".$this->_zip."',
                            state = '".$this->_state."',
                            country_id = '".$this->_countryId."',
                            active = '".$this->_addressActive."',
                            is_primary = '".$this->_isPrimaryAddress."',
                            updated_by =  '".u()->getEmail()."',
                            update_date =  NOW()
                        WHERE id = {$this->_addressId}
                        ";
                return db_execute($sql);
            }
            return false;
        }

        public static function GetContacts($client_id = 0, $group_id = 0, $p = 0, $page_limit = 10) {
            $return = array();
            $and = "";
            
            $total_pages=0;
            $total_results=0;

            if(u()->isAdmin() || u()->isHRManager()) {
            }

            if($client_id > 0) 
                $and .= " AND a.client_id = $client_id";
            if($group_id > 0) 
                $and .= " AND a.group_id = $group_id";

            $sql = "SELECT SQL_CALC_FOUND_ROWS a.*, d.name as group_name,e.name as client_name, e.is_client, 
                        CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by 
                    FROM contacts a
                    INNER JOIN users b ON b.email = a.created_by
                    INNER JOIN users c ON c.email = a.updated_by
                    INNER JOIN clients e ON e.id = a.client_id
                    LEFT JOIN contact_groups d ON d.id = a.group_id
                    WHERE deleted = 0 $and
                    ORDER BY a.first_name, a.last_name ASC ";
            if($p > 0 ) {
                // Pagination Query
                $sql .= " LIMIT ".(($p-1)*$page_limit).",".$page_limit;
            }
            $rows = db_get_all($sql);
            if($rows) {
                if($p > 0) {
                    $sql = "SELECT FOUND_ROWS() AS total";
                    $ttl = db_get_row($sql);
                    $total_results = $ttl["total"];
                    if($total_results > 0) { 
                        self::$_totalResults = $total_results; 
                        $total_pages = ceil($total_results/$page_limit);
                        self::$_totalPages = $total_pages; 
                    }
                }
                foreach($rows as $row){
                    self::$_Contacts[$row["id"]] = self::_Init($row);
                }
            }
            return self::$_Contacts;
        }
        
        public static function GetContactsByGroups() {
            $result = array();
            $sql = "SELECT a.*, d.name as group_name,e.name as client_name, e.is_client, 
                        CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by 
                    FROM contacts a
                    INNER JOIN users b ON b.email = a.created_by
                    INNER JOIN users c ON c.email = a.updated_by
                    INNER JOIN clients e ON e.id = a.client_id
                    LEFT JOIN contact_groups d ON d.id = a.group_id
                    WHERE deleted = 0 
                    ORDER BY d.name ASC ";
            $rows = db_get_all($sql);
            if($rows) {
                foreach($rows as $row){
                    if($row["group_id"] < 1) {
                        $row["group_id"] = -1;
                        $row["group_name"] = "Ungrouped";
                    }
                    $result[$row["group_id"]]["group_id"] = $row["group_id"];
                    $result[$row["group_id"]]["group_name"] = $row["group_name"];
                    $result[$row["group_id"]]["total"] = intval($result[$row["group_id"]]["total"]) + 1;
                    $result[$row["group_id"]]["contacts"][$row["id"]] = self::_Init($row);
                }
            }
            return $result;
        } 

        public static function GetContactsByClients() {
            $result = array();
            $sql = "SELECT a.*, d.name as group_name,e.name as client_name, e.is_client, 
                        CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by 
                    FROM contacts a
                    INNER JOIN users b ON b.email = a.created_by
                    INNER JOIN users c ON c.email = a.updated_by
                    INNER JOIN clients e ON e.id = a.client_id
                    LEFT JOIN contact_groups d ON d.id = a.group_id
                    WHERE deleted = 0 
                    ORDER BY e.name ASC ";
            $rows = db_get_all($sql);
            if($rows) {
                foreach($rows as $row){
                    $result[$row["client_id"]]["client_id"] = $row["client_id"];
                    $result[$row["client_id"]]["client_name"] = $row["client_name"];
                    $result[$row["client_id"]]["total"] = intval($result[$row["client_id"]]["total"]) + 1;
                    $result[$row["client_id"]]["contacts"][$row["id"]] = self::_Init($row);
                }
            }
            return $result;
        }

        public static function GetContactsByAlpha($alpha = '', $client_id = 0, $group_id = 0) {
            $result = array();
            $and = '';
            $alpha_arr = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W',
                                'X','Y','Z');

            if($alpha != '') $alpha = strtoupper($alpha);
            if($alpha == '' || $alpha == 'ALL') {
                foreach($alpha_arr as $v) {
                    $and = " AND a.first_name LIKE '$v%'";
                    if($client_id > 0) 
                        $and .= " AND a.client_id = $client_id";
                    if($group_id > 0){ 
                        $and .= " AND a.group_id = $group_id";
                    }
                    else if($group_id == -1) {
                        $and .= " AND a.group_id = 0";
                    }
                    $sql = "SELECT a.*, d.name as group_name,e.name as client_name, e.is_client, 
                                CONCAT(b.first_name,' ',b.last_name) as created_by, 
                                CONCAT(c.first_name,' ',c.last_name) as updated_by 
                            FROM contacts a
                            INNER JOIN users b ON b.email = a.created_by
                            INNER JOIN users c ON c.email = a.updated_by
                            INNER JOIN clients e ON e.id = a.client_id
                            LEFT JOIN contact_groups d ON d.id = a.group_id
                            WHERE deleted = 0 $and
                            ORDER BY a.first_name ASC ";
                    $rows = db_get_all($sql);
                    if($rows) {
                        foreach($rows as $row){
                            $result[$v]["name"] = $v;
                            $result[$v]["total"] = intval($result[$v]["total"]) + 1;
                            $result[$v]["contacts"][$row["id"]] = self::_Init($row);
                        }
                    }
                }
                return $result;
    
            }
            else if($alpha != '' && in_array($alpha, $alpha_arr)) {
                    $and = " AND a.first_name LIKE '$alpha%'";
                    if($client_id > 0) 
                        $and .= " AND a.client_id = $client_id";
                    if($group_id > 0){ 
                        $and .= " AND a.group_id = $group_id";
                    }
                    else if($group_id == -1) {
                        $and .= " AND a.group_id = 0";
                    }
                    $sql = "SELECT a.*, d.name as group_name,e.name as client_name, e.is_client, 
                                CONCAT(b.first_name,' ',b.last_name) as created_by, 
                                CONCAT(c.first_name,' ',c.last_name) as updated_by 
                            FROM contacts a
                            INNER JOIN users b ON b.email = a.created_by
                            INNER JOIN users c ON c.email = a.updated_by
                            INNER JOIN clients e ON e.id = a.client_id
                            LEFT JOIN contact_groups d ON d.id = a.group_id
                            WHERE deleted = 0 $and
                            ORDER BY a.first_name ASC ";
                $rows = db_get_all($sql);
                if($rows) {
                    foreach($rows as $row){
                        $result[$alpha]["name"] = $alpha;
                        $result[$alpha]["total"] = intval($result[$alpha]["total"]) + 1;
                        $result[$alpha]["contacts"][$row["id"]] = self::_Init($row);
                    }
                }
                return $result;
            }
        }

        public function getTotalContacts() {
            $sql = "SELECT count(id) as total FROM contacts";
            $row = db_get_row($sql);
            if($row) {
                return $row["total"];
            }
            return false;
        }

    }
?>
