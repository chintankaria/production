<?php
    include_once("class.productmodule.inc.php");
	include_once("class.module.inc.php");

    class Products {
        private static $_Default = null;
        private static $_Products = null;

        private $_id = null;
        private $_code = null;
        private $_name = null;
        private $_version = null;
		private $_licenseVersion = null;
        private $_build = null;
        private $_path = null;
        private $_createdBy = null;
        private $_createDate = null;

        private $_modules = null;

        public function __construct($id = null){
            if(!is_null($id)){
                $t = self::Get($id);
                $this->_id = $t->getId();
                $this->_code = $t->getCode();
                $this->_name = $t->getName();
            }
        }

        public function getId() { return $this->_id; }
        public function getCode(){ 
            return $this->_code;
        }
        public function getName(){ 
            return (empty($this->_name) ? "Others" : $this->_name); 
        }
        public function getVersion(){ 
            return $this->_version;
        }
		public function getLicenseVersion() {
			return $this->_licenseVersion;
		}
        public function getBuild(){ 
            return $this->_build;
        }
        public function getPath(){ 
            return $this->_path;
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getCreateDate(){
            return $this->_createDate;
        }

        public function setCode($code){
            $this->_code = trim($code);
        }
        public function setName($name){
            $this->_name = trim($name);
        }

        public function getModules() {
            if(is_null($this->_modules)) {
                $this->_modules = self::GetProductModules($this->_id);
            }

            return $this->_modules;
        }
		
        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function delete(){
            if($this->_id) {
                $sql = "DELETE FROM products WHERE id='{$this->_id}' LIMIT 1";
                if(db_execute($sql)){
                    $sql = "DELETE FROM product_versions WHERE product_id='{$this->_id}'";
                    db_execute($sql);
                    return true;
                }
            }
            return false;
        }
		
        public function checkName() {
            $and = '';
            if($this->_id) $and = " AND id != {$this->_id}";
            $sql = "SELECT id FROM products
                    WHERE name = " . q($this->_name) . "
                        $and
                    ";

            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        private function _add(){
            if($this->_name){
                $sql = "
                    INSERT INTO products ( name, code, created_by, create_date)
                    VALUES(
                        " . q($this->_name) . ",
                        " . q($this->_code) . ",
                        '" . u()->getEmail() ."',
                        NOW()
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return $this->_id;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_name){
                $sql = "
                    UPDATE products SET
                    name = " . q($this->_name) . ",
                    code = " . q($this->_code) . "
                    WHERE id = {$this->_id}
                ";
                db_execute($sql);
                return $this->_id;
            }

            return false;
        }

        public static function Get($id){
            if(self::$_Products[$id]) return self::$_Products[$id];
            else return self::$_Default;
        }

        public static function GetAll(){
            return self::$_Products;
        }

        public static function GetLicenseProducts(){
            $return = array();
            $sql = "
                SELECT a.id, a.name, a.code, a.created_by, a.create_date
                FROM products a, licenses b WHERE a.id = b.product_id
                AND a.id > 0
                ORDER BY a.name
            ";

            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }

        public static function Init(){
            self::$_Default= self::_Init(array("id" => 0, "name" => "Others"));

            $sql = "
                SELECT a.id, a.name, a.code, a.created_by, a.create_date
                FROM products a
                WHERE a.id > 0
                ORDER BY a.name
            ";

            $rows = db_get_all($sql);
            if($rows)
            {
                foreach($rows as $row)
                {
                    $ver = self::GetVersions($row["id"]);
                    $row['version'] = $ver[0]['version'];
					$row['license_version'] = $ver[0]['license_version'];
                    $row['build'] = $ver[0]['build'];
                    $row['path'] = $ver[0]['path'];

                    self::$_Products[$row["id"]] = self::_Init($row);
                }
            }
        }

        public static function AddVersion($product_id, $version, $license_version, $build, $path){
            $sql = "SELECT id FROM product_versions WHERE version = " . q($version) . " AND build = " . q($build) . " AND product_id = " . q($product_id);

            if($rs = db_get_row($sql)){
                return false;
            }

            $sql = "
                INSERT INTO product_versions ( product_id, version, license_version, build, path, created_by, created_on)
                VALUES(
                    " . q($product_id) . ",
                    " . q($version) . ",
					" . q($license_version) . ",
                    " . q($build) . ",
                    " . q($path) . ",
                    '" . u()->getEmail() ."',
                    NOW()
                )
            ";

            if(db_execute($sql)){
                return true;
            }
        }

        public static function EditVersion($product_id, $id, $version, $license_version, $build, $path){
            $sql = "SELECT id FROM product_versions WHERE version = " . q($version) . " AND build = " . q($build) . " AND product_id = " . q($product_id) . " AND id != " . q($id);

            if($rs = db_get_row($sql)){
                return false;
            }

            $sql = "
                UPDATE product_versions SET
                    version = " . q($version) . ",
					license_version = " . q($license_version) . ",
                    build = " . q($build) . ",
                    path = " . q($path) . "
                    WHERE id = {$id}
            ";

            if(db_execute($sql)){
                return true;
            }
        }

        public static function GetVersions($product_id){
            $sql = "SELECT * FROM product_versions WHERE product_id = " . q($product_id) . " ORDER BY id DESC";
            $rows = db_get_all($sql);
            if($rows) return $rows;
        }

        public static function GetProductModules($product_id){
            return ProductModule::GetModulesByProductId($product_id);
        }
		
		public static function SaveModules($product_id, $modules) {
			$product_id = intval($product_id);
			if($product_id > 0) {
				foreach($modules as $m) {
					if($m instanceof Module) {
						$name = q($m->getName());
						$sql = "
							INSERT INTO product_modules (product_id, code, name)
							VALUES ('{$product_id}', '{$m->getCode()}', {$name})
							ON DUPLICATE KEY UPDATE
							name = {$name}
						";
							
						db_execute($sql);
					}
				}
			}
		}

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_code = $array["code"];
            $object->_name = $array["name"];
            $object->_version = $array["version"];
			$object->_licenseVersion = $array["license_version"];
            $object->_build = $array["build"];
            $object->_path = $array["path"];
            $object->_createdBy = $array["created_by"];
            $object->_createDate = $array["create_date"];

            return $object;
        }
    }

    Products::Init();
