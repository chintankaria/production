<?php
    class Watchdog {
        private $_id = null; 
        private $_userId = null; 
        private $_action = null;
        private $_status = null;
        private $_message = null;
        private $_hostName = null;
        private $_createdBy = null;
        private $_createDate = null;
        private $_createdByEmail = null;

        private static $_totalResults = null;
        private static $_totalPages = null;

        public function __construct ($id = null) {
            if($id > 0) {
                $sql = "SELECT a.id, a.action, a.message, a.status, a.hostname,a.created_by as created_by_email, CONCAT(b.first_name,' ',b.last_name) as created_by, 
                    b.id as user_id, a.create_date
                    FROM watchdog a
                    INNER JOIN users b ON b.email = a.created_by
                    WHERE a.id = $id
                    ORDER BY a.create_date DESC
                    ";
                $row = db_get_row($sql);
                if($row) self::_Init($row, $this);

            }
        }
        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;

            $object->_id = $array['id'];
            $object->_userId = $array['user_id'];
            $object->_action = $array['action'];
            $object->_message = $array['message'];
            $object->_status = $array['status'];
            $object->_hostName = $array['hostname'];
            $object->_createdBy = $array['created_by'];
            $object->_createDate = $array['create_date'];
            $object->_createdByEmail = $array['created_by_email'];
            
            return $object;
        }

        public function getId() {
            return $this->_id;
        }
        public function getUserId() {
            return $this->_userId;
        }
        public function getAction($formated=true) {
            $actions = array(
                                "ADD" => "Items Added", "EDIT" => "Items Edited", "APPROVED" => "Items Approved", "DELETE" => "Items Deleted", 
                                "REJECTED" => "Items Rejected", "POINTS" => "Items Points",
                                "FILE" => "File Uploads", "LOGIN" => "Member Login", "LOGOUT" => "Member Logout", "TIMECARD" => "Timecard Item", 
                                "TIMECARD_REVIEW" => "Timecard Review", "EMAIL" => "Email Notification", "CRON" => "Cron Activity", 
                            );
            if($formated)
                return $actions[$this->_action];
            else
                return $this->_action;
        }
        public function setAction($action) {
            $this->_action = trim($action);
        }
        public function getMessage($length=0) {
            if ($length > 0) {
                $msg = substr($this->_message, 0, $length);
                if (strlen($this->_message) > $length)
                    $msg .= " ...";
            } else {
                $msg = nl2br($this->_message);
            }
            return $msg;
        }
        public function setMessage($msg) {
            $this->_message = trim($msg);
        }
        public function getStatus() {
            return $this->_status;
        }
        public function setStatus($status) {
            $this->_status = trim($status);
        }
        public function getHostName() {
            return $this->_hostName;
        }
        public function setHostName($host) {
            $this->_hostName = trim($host);
        }
        public function getCreatedByEmail() {
            return $this->_createdByEmail;
        }
        public function setCreadedBy($creaded_by) {
            $this->_createdBy = trim($creaded_by);
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getCreateDate($format = "m/d/Y") {
           return date($format, strtotime($this->_createDate));
        }  
        public function getTotalResults() {
            return self::$_totalResults;
        }
        public function getTotalPages() {
            return self::$_totalPages;
        }
        public function log() {
            return $this->_add();
        }

        private function _add() {
            if($this->_action) {
                $sql = " INSERT INTO watchdog (action, message, status, hostname, created_by, create_date)
                            VALUES (
                                ".q($this->_action).",
                                ".q($this->_message).",
                                ".q($this->_status).",
                                ".q($this->_hostName).",
                                ".q($this->_createdBy).",
                                NOW()
                            );
                        ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }
            return false;
        }

        public static function GetLogs($email=null,$from = null, $to=null, $action = null, $status = null, $hide_cron=false, $p =0, $page_limit = 20) {
            $result = array();
            $and = '';
            $total_pages=0;
            $total_results=0;
            if($email != null && $from != null && $to != null) {
                $from = ymd($from);
                $to = ymd($to);
                $and = "WHERE a.created_by = '$email' AND (DATE_FORMAT(a.create_date, '%Y-%m-%d') BETWEEN '$from' AND '$to')";
            } 
            else {
                if($email != null) {
                    $and = "WHERE a.created_by = '$email'";
                } 
                if($from != null && $to != null) {
                    $from = ymd($from);
                    $to = ymd($to);
                    if($and == "")
                        $and = "WHERE (DATE_FORMAT(a.create_date, '%Y-%m-%d') BETWEEN '$from' AND '$to')";
                    else 
                        $and .= " AND (DATE_FORMAT(a.create_date, '%Y-%m-%d') BETWEEN '$from' AND '$to')";
                }
            }
            if($action != null) {
                if($and == "") { 
                    $and = "WHERE a.action = '".strtoupper($action)."'";
                 } else {
                    $and .= " AND a.action = '".strtoupper($action)."'";
                }
            }
            else {
                if($hide_cron && $and == "") { 
                    $and = "WHERE a.action != 'CRON'";
                } else if($hide_cron) { 
                    $and .= " AND a.action != 'CRON'"; 
                }
            }
            if($status != null) {
                if($and == "") 
                    $and = "WHERE a.status = '".strtoupper($status)."'";
                else
                    $and .= " AND a.status = '".strtoupper($status)."'";
            }
            $limit = "";
            $sql_cal = "";
            if($p > 0) {
                $sql_cal = "SQL_CALC_FOUND_ROWS";
                $limit = "LIMIT ".(($p-1)*$page_limit).",".$page_limit;
            }
            $sql = "SELECT $sql_cal a.id, a.action, a.message, a.status, a.hostname,a.created_by as created_by_email,a.created_by, a.create_date
                    FROM watchdog a
                    $and 
                    ORDER BY a.create_date DESC $limit 
                    ";

            $rows = db_get_all($sql);
            if($rows) {
                if($p > 0) {
                    $sql = "SELECT FOUND_ROWS() AS total";
                    $ttl = db_get_row($sql);
                    $total_results = $ttl["total"];
                    if($total_results > 0) { 
                        self::$_totalResults = $total_results;
                        $total_pages = ceil($total_results/$page_limit);
                        self::$_totalPages = $total_pages;
                    }
                }
            }
            if($rows) {
                foreach($rows as $row) {
                    if($row['action'] == 'CRON') {
                        $row['hostname'] = "System";
                        $row['user_id'] = 0;
                    }
                    else {
                        $row['user_id'] = User::getUserIdByEmailId($row['created_by_email']);
                        $row['created_by'] = User::getFullNameByEmailId($row['created_by_email']);
                    }
                    $result[$row['id']] = self::_Init($row);
                }
            }
            return $result;
        }
    }
?>
