<?php
class TimeCard {
    private $_id = null;
    private $_userId = null;
    private $_userName = null;
    private $_weekNumber = null;
    private $_timecardDate = null;
    private $_billableMin = null;
    private $_ptoMin = null;
    private $_totalMin = null;
    private $_unassignedMin = null;
    private $_isHoliday = null;
    private $_status = null;
    private $_createDate = null;
    private $_createdBy = null;
    private $_updateDate = null;
    private $_updatedBy = null;

    public function __construct($id = null, $uid = null, $week_number= null, $timecard_date1 = null, $timecard_date2 = null) {
        $and = '';
        if($id > 0 || $uid > 0 ) {
            if($id > 0 && $uid > 0 ) {
                $and = "WHERE a.id = $id AND user_id = $uid";
            }
            else {
                if($id > 0) $and = "WHERE a.id = $id";
                if($uid > 0) {
                    $and = "WHERE a.user_id = $uid";
                    if($week_number > 0) {
                        $year = date("Y",strtotime($timecard_date1));
                        $and .= " AND a.week_number = $week_number ";
                        if($timecard_date1 != '' && $timecard_date2 != '') {
                            $timecard_date1 = ymd($timecard_date1);
                            $timecard_date2 = ymd($timecard_date2);
                            $and .= " AND (a.timecard_date BETWEEN '$timecard_date1' AND '$timecard_date2')";
                        }
                        else {
                            $and .= " AND YEAR(a.timecard_date) = '$year'";
                        }
                        $and .= " GROUP BY a.week_number";
                    }
                    else if($timecard_date1 != '' && $timecard_date2 != '') {
                        $timecard_date1 = ymd($timecard_date1);
                        $timecard_date2 = ymd($timecard_date2);
                        $and .= " AND (a.timecard_date BETWEEN '$timecard_date1' AND '$timecard_date2')";
                    }
                    else if($timecard_date1 != '') {
                        $timecard_date1 = ymd($timecard_date1);
                        $and .= " AND a.timecard_date = '$timecard_date1'";
                    }
                }
            }
            $sql = "SELECT a.id, a.user_id, UNIX_TIMESTAMP(a.timecard_date) as timecard_date, a.billable_minutes, a.pto_minutes, a.total_minutes,a.status, 
                        a.unassigned_minutes, a.is_holiday, UNIX_TIMESTAMP(a.create_date) as create_date, UNIX_TIMESTAMP(a.update_date) as update_date,
                        a.week_number, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                    FROM timecard_data a
                    INNER JOIN users b on b.email = a.created_by
                    INNER JOIN users c on c.email = a.updated_by
                    $and
                    ";
            $row = db_get_row($sql);
            if($row) {
                self::_Init($row, $this);
            }
        }
    } 
    private static function _Init($array, $object = null){
        if(!$object instanceof self) $object = new self;

        $object->_id = $array["id"];
        $object->_userId = $array["user_id"];
        $object->_weekNumber = $array["week_number"];
        $object->_timecardDate = $array["timecard_date"];
        $object->_billableMin = $array["billable_minutes"];
        $object->_ptoMin = $array["pto_minutes"];
        $object->_totalMin = $array["total_minutes"];
        $object->_unassignedMin = $array["unassigned_minutes"];
        $object->_isHoliday = intval($array["is_holiday"]);
        $object->_status = $array["status"];
        $object->_createDate = $array["create_date"];
        $object->_createdBy = $array["created_by"];
        $object->_updateDate = $array["update_date"];
        $object->_updatedBy = $array["updated_by"];

        return $object;
    }

    public function getId() {
        return $this->_id;
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getWeekNumber() {
        return $this->_weekNumber;
    }
    public function getTimecardDate() {
        return $this->_timecardDate;
    }
    public function getBillableMinutes() {
        return $this->_billableMin;
    }
    public function getPTOMinutes() {
        return $this->_ptoMin;
    }
    public function getTotalMinutes() {
        return $this->_totalMin;
    }
    public function getUnassignedMinutes() {
        return $this->_unassignedMin;
    }
    public function getIsHoliday() {
        return intval($this->_isHoliday);
    }
    public function getStatus() {
        return $this->_status;
    }
    public function getCreateDate() {
        return date("m/d/Y H:i", strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }
    public function getUpdateDate() {
        return date("m/d/Y H:i", strtotime($this->_updateDate));
    }
    public function getUpdatedBy() {
        return $this->_updatedBy;
    }

    public function setUserId($user_id) {
        $this->_userId = $user_id;
    }
    public function setWeekNumber($week_number) {
       $this->_weekNumber = intval($week_number);
    }
    public function setTimecardDate($timecard_date) {
        $this->_timecardDate = ymd($timecard_date);
    }
    public function setBillableMinutes($billable_minutes) {
        $this->_billableMin = $billable_minutes;
    }
    public function setPTOMinutes($pto_minutes) {
        $this->_ptoMin = $pto_minutes;
    }
    public function setTotalMinutes($total_minutes) {
        $this->_totalMin = $total_minutes;
    }
    public function setUnassignedMinutes($unassigned_minutes) {
        $this->_unassignedMin = $unassigned_minutes;
    }
    public function setIsHoliday($is_holiday) {
        $this->_isHoliday = $is_holiday;
    }
    public function setStatus($status) {
        $this->_status = strtoupper(trim($status));
    }

    public function checkTimecard() {
        if($this->_userId && $this->_timecardDate) {
            $sql = "SELECT id FROM timecard_date WHERE user_id = $this->_userId AND timecard_date = '$this->_timecardDate'";
            $row = db_get_row($sql);
            if($row) {
                return $row["id"];
            }
        }
        return false;
    }
    public function save() {
        if($this->_id) return $this->_update();
        else return $this->_add();
    } 

    private function _add() {
        if($this->_userId && $this->_timecardDate) {
            $sql = "INSERT INTO timecard_data
                        (
                            user_id,
                            week_number,
                            timecard_date,
                            billable_minutes,
                            pto_minutes,
                            total_minutes,
                            unassigned_minutes,
                            is_holiday,
                            status,
                            create_date,
                            created_by,
                            update_date,
                            updated_by
                        )
                        VALUES (
                            {$this->_userId},
                            {$this->_weekNumber},
                            '".$this->_timecardDate."',
                            {$this->_billableMin},
                            {$this->_ptoMin},
                            {$this->_totalMin},
                            {$this->_unassignedMin},
                            {$this->_isHoliday},
                            '".$this->_status."',
                            NOW(),
                            '".u()->getEmail()."',
                            NOW(),
                            '".u()->getEmail()."'
                        );
                    ";
            if(db_execute($sql)) {
                $this->_id = db_insert_id();
                return true;
            }
        } 
        return false;
    }
    private function _update() {
        if($this->_id) {
            $sql = "UPDATE timecard_data SET
                        user_id = {$this->_userId},
                        week_number = {$this->_weekNumber},
                        timecard_date = '".$this->_timecardDate."',
                        billable_minutes = {$this->_billableMin},
                        pto_minutes = {$this->_ptoMin},
                        total_minutes = {$this->_totalMin},
                        unassigned_minutes = {$this->_unassignedMin},
                        is_holiday = {$this->_isHoliday},
                        status = '".$this->_status."',
                        update_date = NOW(),
                        updated_by = '".u()->getEmail()."'
                    WHERE id = {$this->_id}
                    ";
            return db_execute($sql);
        }
        return false;
    }
    
    public static function GetTimecard($user_id = null, $timecard_date1 = null, $timecard_date2 = null) {
        $result = array();
        $and = '';
        if(!is_array($user_id) && $user_id > 0 && $timecard_date1 != null && $timecard_date2 != null) {
            $timecard_date1 = ymd($timecard_date1);
            $timecard_date2 = ymd($timecard_date2);
            $and = "WHERE a.user_id=$user_id AND (a.timecard_date BETWEEN '$timecard_date1' AND '$timecard_date2')";
        }
        else if(is_array($user_id) && count($user_id) > 0 && $timecard_date1 != null && $timecard_date2 != null) {
            $timecard_date1 = ymd($timecard_date1);
            $timecard_date2 = ymd($timecard_date2);
            $and = "WHERE a.user_id IN (" . implode(', ', $user_id).") AND (a.timecard_date BETWEEN '$timecard_date1' AND '$timecard_date2')";
        }
        else {
            if(!is_array($user_id) && $user_id > 0) {
                $and = "WHERE a.user_id = $user_id";
            }
            else if(is_array($user_id) && count($user_id) > 0){
                $and = "WHERE a.user_id IN (" . implode(', ', $user_id).")";
            }
            if($timecard_date1 != '' && $timecard_date2 != '') {
                $timecard_date1 = ymd($timecard_date1);
                $timecard_date2 = ymd($timecard_date2);
                if($and == "")
                    $and = " WHERE (a.timecard_date BETWEEN '$timecard_date1' AND '$timecard_date2')";
                else
                    $and .= " AND (a.timecard_date BETWEEN '$timecard_date1' AND '$timecard_date2')";
            }
            else if($timecard_date1) {
                $timecard_date1 = ymd($timecard_date1);
                if($and == "") 
                    $and = " WHERE a.timecard_date = '$timecard_date1'";
                else 
                    $and .= " AND a.timecard_date = '$timecard_date1'";
            }
            else if($timecard_date2) {
                $timecard_date2 = ymd($timecard_date2);
                if($and == "") 
                    $and = " WHERE a.timecard_date = '$timecard_date2'";
                else 
                    $and .= " AND a.timecard_date = '$timecard_date2'";
            }
        }
        $sql = "SELECT a.id, a.user_id, UNIX_TIMESTAMP(a.timecard_date) as timecard_date, a.billable_minutes, a.pto_minutes, a.total_minutes,a.status, 
                        a.unassigned_minutes, a.is_holiday, UNIX_TIMESTAMP(a.create_date) as create_date, UNIX_TIMESTAMP(a.update_date) as update_date,
                        a.week_number, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                    FROM timecard_data a
                    INNER JOIN users b on b.email = a.created_by
                    INNER JOIN users c on c.email = a.updated_by
                    $and
                    ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row)
                $result[$row['user_id']][$row['id']] = self::_Init($row);
        }
        return $result;
    }
}
?>
