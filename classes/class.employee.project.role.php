<?php

class EmployeeProjectRole {

    private $_id = null;
    private $_name = null;  
    private $_active = 1;  
    private $_table = 'employee_project_role';

    public function __construct($id = null) {
        $id = pintval($id);
        if ($id > 0) {
            $sql = "SELECT * FROM {$this->_table} WHERE id={$id} ";
            $row = db_get_row($sql);
            if ($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null) {
        if (!$object instanceof self)
            $object = new self;

        $object->_id = $array["id"];
        $object->_name = $array["name"];
        $object->_active = $array["active"];
        $object->_recDate = $array["rec_date"];
        return $object;
    }

    public function getId() {
        return $this->_id;
    }

    public function getName() {
        return $this->_name;
    }

    public function getIsActive() {
        return $this->_active;
    }

    public function getRecordDate() {
        return $this->_recDate;
    }

    public function setName($name) {
        $this->_name = trim($name);
    }

    public function setIsActive($active) {
        $this->_active = $active;
    }

    public function save() {
        if ($this->_id)
            return $this->_update();
        else
            return $this->_add();
    }

    public function delete() {
        if ($this->_id) {
            $sql = "DELETE FROM {$this->_table} WHERE id='{$this->_id}' LIMIT 1";
            if (db_execute($sql)) {
                return true;
            }
        }
        return false;
    }

    private function _add() {
        if ($this->_name) {
            $sql = "INSERT INTO {$this->_table} (name, active)
                    VALUES(" . q($this->_name) . ", " . q($this->_active) . ")";
            if (db_execute($sql)) {
                $this->_id = db_insert_id();
                return true;
            }
        }

        return false;
    }

    private function _update() {
        if ($this->_id) {
            $sql = "
                    UPDATE {$this->_table} SET                    
                    name = " . q($this->_name) . ",
                    active = " . q($this->_active) . "
                    WHERE id = {$this->_id}
                ";
            return db_execute($sql);
        }

        return false;
    }
    
    public function checkName() {
        $and = '';
        if($this->_id) $and = " AND id != {$this->_id}";
        $sql = "SELECT id FROM {$this->_table}
                WHERE name = " . q($this->_name) . " $and ";

        if($rs = db_get_row($sql)){
            return $rs['id'];
        }
        return false;
    }

    public static function GetEmployeeProjectRole($type_id = 0) {
        if ($type_id) {
            $sql = "SELECT * FROM employee_project_role WHERE id={$type_id} ";
        } else {
            $sql = "SELECT * FROM employee_project_role WHERE  active=1";
        }

        $rows = db_get_all($sql);

        foreach ($rows as $row) {
            $return[$row["id"]] = self::_Init($row);
        }
        return $return;
    }
}