<?php
    include_once("class.client.inc.php");
    include_once("class.products.inc.php");
    include_once("class.productmodule.inc.php");
    include_once("class.licenseperiods.inc.php");

    class License {
        private $_id = null;
		private $_versionNumber = null;

        private $_productId = null;
        private $_productName = null;
        private $_product = null;
        private $_productVersionId = null;
        private $_productVersion = null;

        private $_modules = null;

        private $_clientId = null;
        private $_clientName = null;
        private $_client = null;

        private $_licensePeriodId = null;
        private $_licensePeriodName = null;
        private $_licenseperiod = null;

        private $_technicalContactId = null;
        private $_businessContactId = null;
        private $_noticesContactId = null;
        private $_technicalContact = null;
        private $_businessContact = null;
        private $_noticesContact = null;

		private $_licenseTypeId= null;
        private $_licenseType= null;
        private $_licensePeriod = null;
        private $_licenseStartDate = null;
        private $_licenseEndDate = null;
        private $_downloadValidUpto = null;
        private $_licenseKey = null;
        private $_agreementId = null;
        private $_location = null;
        private $_gs_rm = null;
        private $_notes = null;
        private $_slaDoc = null;
        private $_active = null;
        private $_createdBy = null;
        private $_createdByName = null;
        private $_createdOn = null;
        private $_updatedBy = null;
        private $_updatedOn = null;

        private $_moduleIds = array();

        private static $_Sql = "
            SELECT a.id, license_version_number, a.product_id, a.client_id, a.license_period_id, a.license_type_id, lt.name AS license_type, a.license_start_date, a.license_end_date, a.license_key,
                a.agreement_id, a.location, a.gs_rm, a.notes, a.sla_doc, a.active, a.created_by, UNIX_TIMESTAMP(a.created_on) AS created_on, a.updated_by,
                UNIX_TIMESTAMP(a.updated_on) AS updated_on, b.name AS client_name, c.name AS product_name, g.version,
                DATE_FORMAT(f.valid_upto, '%m/%d/%Y') AS valid_upto,
                CONCAT(d.duration, ' ', d.daymonthyear) as license_period, CONCAT(e.first_name, ' ', e.last_name) AS created_by_name,
                a.technical_contact_id, CONCAT(cct.first_name, ' ', cct.last_name) AS technical_contact,
                a.business_contact_id, CONCAT(ccb.first_name, ' ', ccb.last_name) AS business_contact,
                a.notices_contact_id, CONCAT(ccn.first_name, ' ', ccn.last_name) AS notices_contact
            FROM licenses a
            INNER JOIN clients b ON b.id = a.client_id
            INNER JOIN products c ON c.id = a.product_id
            INNER JOIN users e ON e.id = a.created_by
            INNER JOIN download_keys f ON f.license_id = a.id
            LEFT JOIN product_versions g ON g.id = a.product_version_id
            LEFT JOIN client_contacts cct ON cct.id = a.technical_contact_id
            LEFT JOIN client_contacts ccb ON ccb.id = a.business_contact_id
            LEFT JOIN client_contacts ccn ON ccn.id = a.notices_contact_id
            LEFT JOIN license_periods d ON d.id = a.license_period_id
			LEFT JOIN license_types lt ON a.license_type_id = lt.id
        ";

        public function __construct($id = null, $client_id = null, $product_id = null, $license_period_id = null, $created_by = null){
		 
            $id = pintval($id);
			
            if($id > 0){
                $sql = self::$_Sql . "
                    WHERE a.id = {$id}
                ";

                if(!is_null($client_id)){
                    $client_id = pintval($client_id);
                    $sql .= "
                        AND a.client_id = {$client_id}
                    ";
                }

                if(!is_null($product_id)){
                    $product_id = pintval($product_id);
                    $sql .= "
                        AND a.product_id = {$product_id}
                    ";
                }

                if(!is_null($license_period_id)){
                    $license_period_id = pintval($license_period_id);
                    $sql .= "
                        AND a.license_period_id = {$license_period_id}
                    ";
                }

                if(!is_null($created_by)){
                    $created_by = pintval($created_by);
                    $sql .= "
                        AND a.created_by = {$created_by}
                    ";
                }

                $row = db_get_row($sql);
                if($row) self::_Init($row, $this);
            }
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
			$object->_versionNumber = $array['license_version_number'];
            $object->_productId = $array["product_id"];
            $object->_productName = $array["product_name"];
            $object->_productVersion = $array["version"];
            $object->_clientId = $array["client_id"];
            $object->_clientName = $array["client_name"];
			$object->_licenseTypeId = $array['license_type_id'];
            $object->_licenseType = ucfirst(strtolower($array["license_type"]));
            $object->_licensePeriodId = $array["license_period_id"];
            $object->_licensePeriodName = $array["license_period"];
            $object->_licenseStartDate = $array["license_start_date"];
            $object->_licenseEndDate = $array["license_end_date"];
            $object->_downloadValidUpto = $array["valid_upto"];
            $object->_licenseKey = $array["license_key"];
            $object->_agreementId = $array["agreement_id"];
            $object->_location = $array["location"];
            $object->_gs_rm = $array["gs_rm"];
            $object->_technicalContactId = $array["technical_contact_id"];
            $object->_businessContactId = $array["business_contact_id"];
            $object->_noticesContactId = $array["notices_contact_id"];
            $object->_technicalContact = $array["technical_contact"];
            $object->_businessContact = $array["business_contact"];
            $object->_noticesContact = $array["notices_contact"];
            $object->_notes = $array["notes"];
            $object->_slaDoc = $array["sla_doc"];
            $object->_active = $array["active"];
            $object->_createdBy = $array["created_by"];
            $object->_createdByName = $array["created_by_name"];
            $object->_createdOn = $array["created_on"];
            $object->_updatedBy = $array["updated_by"];
            $object->_updatedOn = $array["updated_on"];
            return $object;
        }

        public function getId(){ return $this->_id; }
		public function getLicenseVersionNumber() { return $this->_versionNumber ? $this->_versionNumber : 1; }
        public function getClientId(){ return $this->_clientId; }
        public function getClientName(){ return $this->_clientName; }
        public function getProductId(){ return $this->_productId; }
        public function getProductName(){ return $this->_productName; }
        public function getProductVersion(){ return $this->_productVersion; }
		public function getLicenseTypeId() { return $this->_licenseTypeId; }
        public function getLicenseType(){ return $this->_licenseType; }
        public function getLicensePeriodId(){ return $this->_licensePeriodId; }
        public function getLicensePeriodName(){
            if($this->_licensePeriodName) {
                return $this->_licensePeriodName;
            } else if(strtoupper($this->_licenseType) == 'EVALUATION') {
                $days = ceil((strtotime($this->_licenseEndDate) - strtotime($this->_licenseStartDate)) / 86400) - 1;
				if($days < 0) {
					$days = 0;
				}
				
                return "{$days} Day(s)";
            } else {
                return '';
            }
        }
        public function getLicenseStartDate(){ return $this->_licenseStartDate; }
        public function getLicenseEndDate(){ return $this->_licenseEndDate; }
        public function getDownloadValidUpto(){ return $this->_downloadValidUpto; }
        public function getLicenseKey(){ return $this->_licenseKey; }
        public function getAgreementId(){ return $this->_agreementId; }
        public function getLocation(){ return $this->_location; }
        public function getGsRm(){ return $this->_gs_rm; }
        public function getTechnicalContactId(){ return $this->_technicalContactId; }
        public function getBusinessContactId(){ return $this->_businessContactId; }
        public function getNoticesContactId(){ return $this->_noticesContactId; }
        public function getTechnicalContact(){ return $this->_technicalContact; }
        public function getBusinessContact(){ return $this->_businessContact; }
        public function getNoticesContact(){ return $this->_noticesContact; }
        public function getNotes(){ return $this->_notes; }
        public function getSlaDoc(){ return $this->_slaDoc; }
        public function getIsActive(){ return $this->_active; }
        public function getCreatedBy(){ return $this->_createdBy; }
        public function getCreatedByName(){ return $this->_createdByName; }
        public function getCreatedOn(){ return $this->_createdOn; }

        public function getModules() {
            if(is_null($this->_modules)) {
                $this->_modules = ProductModule::GetModulesByLicenseId($this->_id);
            }

            return $this->_modules;
        }
		
        public function getClient(){
            if(is_null($this->_client)) $this->_client = new Client($this->_clientId);
            return $this->_client;
        }

        public function getProduct(){
            if(is_null($this->_product)) $this->_product = new Product($this->_productId);
            return $this->_product;
        }

        public function getLicensePeriod(){
            if(is_null($this->_licensePeriod)) $this->_licensePeriod = new LicensePeriods($this->_licensePeriodId);
            return $this->_licensePeriod;
        }
		
		public function setLicenseVersionNumber($versionNumber) {
			$this->_versionNumber = $versionNumber;
		}


        public function setClientId($client){
            if($client instanceof Client) $client_id = $client->getId();
            else $client_id = pintval($client);

            if($client_id > 0) $this->_clientId = $client_id;
            else e("Missing or invalid client name.");
        }

        public function setProductId($product){
            if($product instanceof Product) $product_id = $product->getId();
            else $product_id = pintval($product);

            if($product_id > 0) $this->_productId = $product_id;
            else e("Missing or invalid product name.");
        }
        public function setProductVersionId($product_version_id){
            $this->_productVersionId = intval($product_version_id);
        }

        public function setLicensePeriodId($licenseperiod){
            if($licenseperiod instanceof LicensePeriods) $licenseperiod_id = $licenseperiod->getId();
            else $licenseperiod_id = pintval($licenseperiod);

            $this->_licensePeriodId = $licenseperiod_id;
        }

		public function setLicenseTypeId($licenseTypeId) {
			$this->_licenseTypeId = intval($licenseTypeId);
		}
        public function setLicenseType($licenseType){
			throw new Exception("DO NOT USE THIS");
			/*
            $this->_licenseType = trim($licenseType);
            if(!$this->_licenseType) e("Missing or invalid license type.");
			 * 
			 */
        }
        public function setLicensePeriod($licensePeriod){
            $this->_licensePeriod = intval($licensePeriod);
        }
        public function setLicenseStartDate($licenseStartDate) {
            $this->_licenseStartDate = trim($licenseStartDate);
        }
        public function setLicenseEndDate($licenseEndDate) {
            $this->_licenseEndDate = trim($licenseEndDate);
        }
        public function setDownloadValidUpto($validUpto) {
            $this->_downloadValidUpto = trim($validUpto);
        }
        public function setLicenseKey($licenseKey) {
            $this->_licenseKey = trim($licenseKey);
        }
        public function setAgreementId($agreementId) {
            $this->_agreementId = trim($agreementId);
        }
        public function setLocation($location) {
            $this->_location = trim($location);
        }
        public function setGsRm($gs_rm) {
            $this->_gs_rm = trim($gs_rm);
        }
        public function setTechnicalContactId($technicalContactId) {
            $this->_technicalContactId = intval($technicalContactId);
        }
        public function setBusinessContactId($businessContactId) {
            $this->_businessContactId = intval($businessContactId);
        }
        public function setNoticesContactId($noticesContactId) {
            $this->_noticesContactId = intval($noticesContactId);
        }
        public function setNotes($notes) {
            $this->_notes = trim($notes);
        }
        public function setSlaDoc($sla_doc) {
            $this->_slaDoc = trim($sla_doc);
        }
        public function setActive($active) {
            $this->_active = intval($active);
        }
        public function setCreatedOn($createdOn) {
            $this->_createdOn = intval($createdOn);
        }

        public function setModuleIds($moduleIds) {
            if(is_array($moduleIds)) {
                $this->_moduleIds = array_filter($moduleIds, 'intval');
            }
        }

        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function checkLicense($product_id, $client_id, $license_type, $license_period_id = null, $license_start_date = null) {
            if($product_id) {
                if(strtoupper($license_type) == "COMMERCIAL") {
                    $sql ="SELECT id, license_end_date, license_key, agreement_id FROM licenses WHERE product_id='$product_id' AND client_id='$client_id' AND license_type='COMMERCIAL'";
                }
                else {
                    $sql ="SELECT id, license_end_date, license_key, agreement_id FROM licenses WHERE product_id='$product_id' AND client_id='$client_id' AND license_type='EVALUATION' AND license_period_id='$license_period_id' AND DATE_FORMAT(license_start_date, '%m/%d/%Y')='$license_start_date'";
                }
                $row = db_get_row($sql);
                if($row) 
                    return $row["id"] . "~~" . $row["license_end_date"] . "~~" . $row["license_key"] . "~~" . $row["agreement_id"];
            }
            return false;
        }

        private function _add(){
            if($this->_licenseTypeId){
                // $licensePeriodId = ($this->_licenseType == 'COMMERCIAL') ? '"0"' : q($this->_licensePeriodId);
                $sql = "
                    INSERT INTO licenses (license_version_number, product_id, product_version_id, client_id, license_type_id, license_start_date, license_end_date, license_key,
                        agreement_id, location, gs_rm, technical_contact_id, business_contact_id, notices_contact_id, notes, sla_doc, created_by, created_on)
                    VALUES (" . q($this->_versionNumber) . ", " . q($this->_productId) . ", " . q($this->_productVersionId) . ", " . q($this->_clientId) . ", ".q($this->_licenseTypeId).",
                            " . q(ymdhms(strtotime($this->_licenseStartDate))) . ", " . q(ymdhms(strtotime($this->_licenseEndDate))) . ",
                            " . q($this->_licenseKey) . ", " . q($this->_agreementId) . ", " . q($this->_location) . ",
                            " . q($this->_gs_rm) . ", " . q($this->_technicalContactId) . ", " . q($this->_businessContactId) . ",
                            " . q($this->_noticesContactId) . ", " . q($this->_notes) . ", " . q($this->_slaDoc) . ", " . u()->getID() . ", NOW())
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();

                    // Save modules
                    foreach($this->_moduleIds as $m) {
                        $sql = "
                            INSERT INTO license_modules (license_id, module_id)
                            VALUE ('{$this->_id}', '{$m}')
                        ";
                        db_execute($sql);
                    }

                    $download_key = $this->generate_file_token();

                    // Insert download key into DB
                    $sql = "INSERT INTO download_keys (license_id, download_key, valid_upto, created_by, created_on)
                            VALUES (" . $this->_id . ", " . q($download_key) . ", DATE_ADD(NOW(), INTERVAL 6 DAY), " . u()->getID() . ", NOW())";
                    if(db_execute($sql)){
                        return $download_key . "~~" . $this->_id;
                    }
                }
            }

            return false;
        }

        private function _update(){
            if($this->_id && $this->_agreementId){
                $sql = "
                    UPDATE licenses SET
                        agreement_id = " . q($this->_agreementId) . ",
                        client_id = " . q($this->_clientId) .",
                        location = " . q($this->_location) .",
                        technical_contact_id = " . q($this->_technicalContactId) .",
                        business_contact_id = " . q($this->_businessContactId) .",
                        notices_contact_id = " . q($this->_noticesContactId) .",
                        notes = " . q($this->_notes) .",
                        sla_doc = " . q($this->_slaDoc) .",
						license_type_id = " . q($this->_licenseTypeId) . ",
                        updated_by = '". u()->getID() ."',
                        updated_on = NOW()
                    WHERE id = {$this->_id}
                ";

                return db_execute($sql);
            }

            return false;
        }

		public function delete() {
			if($this->_id) {
				$id = $this->_id;
				
				if(db_execute("DELETE FROM licenses WHERE id = '{$id}'")) {
					db_execute("DELETE FROM licenses_log WHERE license_id = '{$id}'");
					db_execute("DELETE FROM license_modules WHERE license_id = '{$id}'");
					
					return true;
				}
			}
			
			return false;
		}

        public static function GetSlaAdjustments($license_id){
            $sql = "SELECT a.* FROM sla_adjustments a, licenses b
                    WHERE a.license_id = b.id AND a.active AND a.license_id = " . q($license_id);
            $rows = db_get_all($sql);
            if($rows) return $rows;
        }

        public static function SetSlaAdjustments($license_id, $filename) {
            $sql = "SELECT id FROM sla_adjustments WHERE license_id = " . q($license_id) . " AND sla_adjustment_doc = " . q($filename);
            $row = db_get_row($sql);
            if($row)
            {
                $sql = "UPDATE sla_adjustments SET created_by = " . u()->getID() . ", created_on = NOW(), active = 1 WHERE id = " . $row["id"];
            }
            else
            {
                $sql = "INSERT INTO sla_adjustments (license_id, sla_adjustment_doc, created_by, created_on)
                        VALUES (" . q($license_id) . ", " . q($filename) . ", " . u()->getID() . ", NOW())";
            }
            if(db_execute($sql)) return true;
            return false;
        }

        public static function DelSlaAdjustments($id){
            $sql = "UPDATE sla_adjustments SET active = 0 WHERE id = $id";
            if(db_execute($sql)) return true;
            return false;
        }

        public static function UpdateDownloadValidUpto($license_id, $valid_upto) {
            $tmp = explode('/', $valid_upto);
            $valid_upto = $tmp[2] . '-' . $tmp[0] . '-' . $tmp[1]; 

            $sql = "UPDATE download_keys SET valid_upto = " . q($valid_upto) . " WHERE license_id = '$license_id'";

            if(db_execute($sql)) return true;
            return false;
        }

        public static function GetDownloads($license_id) {
            $sql = "SELECT c.email, c.remote_ip, downloaded_on
                    FROM licenses a, download_keys b, download_logs c
                    WHERE a.id = b.license_id AND b.id = c.download_key_id AND a.id = '$license_id'";
            $rows = db_get_all($sql);
            if($rows) return $rows;
            return false;
        }

        public static function GetLicenses($client_id = 0, $product_id = 0, $license_type_id = 0, $license_version = 0){
            $return = array();

            if($client_id) {
                $and .= " AND a.client_id = {$client_id} ";
            }

            if($product_id) {
                $and .= " AND a.product_id = {$product_id} ";
            }

            if($license_type_id) {
                $and .= " AND a.license_type_id = '{$license_type_id}' ";
            }
			
			if($license_version) {
				$and .= " AND a.license_version_number = '{$license_version}' ";
			}

            $sql = self::$_Sql . "
                WHERE a.id > 0
                {$and}
                ORDER BY a.id DESC
            ";
            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }

        public static function LogLicenseEdit($license_id) {
            $sql = "INSERT INTO licenses_log (license_id, agreement_id, client_id, location, created_by, created_on)
                    SELECT id, agreement_id, client_id, location, '" . u()->getID() . "', NOW() FROM licenses WHERE id = $license_id";
            if(db_execute($sql)) return true;
            return false;
        }

        public function generate_file_token($length=7) {
            $s = strtoupper(uniqid(mt_rand(0,rand()),true));
            $num_token = substr($s,0,$length);
            return $num_token;
        }


    }
