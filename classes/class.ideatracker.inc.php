<?php
    class IdeaTracker {
        
        private static $_Ideas = null;

        private $_id = null;
        private $_userId = null;
        private $_dateTime = null;
        private $_title = null;
        private $_description = null;
        private $_status = null;
        private $_createDate = null;
        private $_createdBy = null;
        private $_updateDate = null;
        private $_updatedBy = null;
        private $_deleted = null;
        private $_deleteDate = null;
        private $_deletedBy = null;

        private $_ideaFiles = null;
        private $_files = null;

        private static $_totalResults = null;
        private static $_totalPages = null;

        public function __construct ($id = null) {
            $id = pintval($id);
            if($id > 0) {
                $sql = "SELECT a.id, a.user_id, a.title, a.description, a.status, UNIX_TIMESTAMP(a.date_time) as date_time, a.create_date, a.update_date,
                        a.deleted, a.deleted_date,
                        CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                    FROM idea_tracker a
                    INNER JOIN users b on b.email = a.created_by
                    INNER JOIN users c on c.email = a.updated_by
                    WHERE a.deleted = 0 AND a.id = $id 
                    ";

                $row = db_get_row($sql);
                if($row) {
                    self::_Init($row, $this);
                }
            }
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;

            $object->_id = $array["id"];
            $object->_userId = $array["user_id"];
            $object->_dateTime = $array["date_time"];
            $object->_title = $array["title"];
            $object->_description = $array["description"];
            $object->_status = $array["status"];
            $object->_createDate = $array["create_date"];
            $object->_createdBy = $array["created_by"];
            $object->_updateDate = $array["update_date"];
            $object->_updatedBy = $array["updated_by"];
            $object->_deleted = $array["deleted"];
            $object->_deleteDate = $array["deleted_date"];
            $object->_deletedBy = $array["deleted_by"];

            $sql = "SELECT * FROM idea_tracker_files WHERE idea_tracker_id = '{$array['id']}' AND deleted=0";
            $pRows = db_get_all($sql);
            if($pRows) {
                foreach($pRows as $pRow)
                    $object->_files[$pRow["id"]] = $pRow;
            }
            
            return $object;
        }

        public function isOwner(){
            return ($this->_userId == u()->getId()) ? $this->_id : false;
        }
        public function getId() {
            return $this->_id;
        }
        public function getUserId() {
            return $this->_userId;
        }
        public function getDateTime() {
            return date("m/d/Y H:i", $this->_dateTime);
        }
        public function getTitle() {
            return $this->_title;
        }
        public function getDescription() {
            return $this->_description;
        }
        public function getStatus() {
            return ucwords(strtolower(str_replace("_", " ",$this->_status)));
        }
        public function getCreateDate() {
            return date("m/d/Y H:i", strtotime($this->_createDate));
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getUpdateDate() {
            return date("m/d/Y H:i", strtotime($this->_updateDate));
        }
        public function getUpdatedBy() {
            return $this->_updatedBy;
        }
        public function getDeleted() {
            return $this->_deleted;
        }
        public function getDeletedDate() {
            return date("m/d/Y H:i", strtotime($this->_deleteDate));
        }
        public function getDeletedBy() {
            return $this->_deletedBy;
        }
        public function getTotalFiles() {
            return count($this->_files);
        }
        public function getFiles() {
            return $this->_files;
        }
        public function getFile($id){
            if($id > 0) {
                $sql = "SELECT * FROM idea_tracker_files WHERE id=$id AND deleted=0";
                $pRow = db_get_row($sql);
                if($pRow){
                    $file_path = AppPath::$FILES ."/idea/{$pRow['idea_tracker_id']}/{$pRow['md5_name']}";
                    $pRow['file_path'] = $file_path;
                    return $pRow;
                }
            }
            return false;
        }
        public function getTotalResults() {
            return self::$_totalResults;
        }
        public function getTotalPages() {
            return self::$_totalPages;
        }

        public function setUserId($user_id) {
            $this->_userId = $user_id;
        }
        public function setDateTime($date_time) {
            $this->_dateTime = ymdhms(is_date($date_time));
        }
        public function setTitle($title) {
            $this->_title = $title;
        }
        public function setDescription($description) {
            $this->_description = $description;
        }
        public function setStatus($status) {
            $this->_status = strtoupper($status);
        }
        public function setCreatedBy($created_by){
            $this->_createdBy = trim($created_by);
            if(!$this->_createdBy) e("Missing or invalid idea creator.");
        }
        public function setUpdatedBy($updated_by){
            $this->_updatedBy = trim($updated_by);
            if(!$this->_updatedBy) e("Missing or invalid idea updator.");
        }
        public function setDeletedBy($deleted_by){
            $this->_deletedBy = trim($deleted_by);
            if(!$this->_deletedBy) e("Missing or invalid idea deletor.");
        }
        public function setFiles($files) {
            $this->_files = $files;
        }
        public function setTotalResults($total_results) {
            $this->_totalResults = $total_results;
        }
        public function setTotalPages($total_pages) {
            $this->_totalPages = $total_pages;
        }

        public function checkTitle() {
            $and = '';
            if($this->_id) $and = " AND id != {$this->_id} ";
            $sql = "SELECT id FROM idea_tracker
                    WHERE title = " . q($this->_title) . " $and
                    ";
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        private function _saveFile(){
            $file_dir = AppPath::$FILES . "/idea";
            if(!is_dir($file_dir))
                mkdir($file_dir, 0755, true);
            $file_dir = AppPath::$FILES . "/idea/{$this->_id}";

            if(is_dir($file_dir) || mkdir($file_dir, 0755, true)){
                if(is_array($this->_files) && count($this->_files) > 0) {
                    foreach($this->_files['file']['error'] as $k => $v) {
                        if(!$v){
                            do{
                                $md5_name = md5(microtime() . rand(1, 999));
                            } while(is_file("{$file_dir}/{$this->_files['files']["name"][$k]}"));

                            if(move_uploaded_file($this->_files['file']['tmp_name'][$k], "{$file_dir}/{$md5_name}")){
                                $sql = "INSERT INTO idea_tracker_files (idea_tracker_id, md5_name, file_name, file_size, mime_type, upload_date, created_by)
                                        VALUES (
                                            '{$this->_id}', '$md5_name',
                                            " . q($this->_files['file']["name"][$k], true, false) . ", '".intval($this->_files['file']['size'][$k])."',
                                            " . q($this->_files['file']["type"][$k], true, false) . ", NOW(), '{$this->_createdBy}'
                                        )
                                     ";
                                db_execute($sql);
                            }
                        }
                    }
                }
            }
            return false;
        }

        private function _add() {
            if($this->_title && $this->_dateTime){
                $sql = "INSERT INTO idea_tracker (user_id, date_time, title, description, status, create_date, created_by, update_date, updated_by)
                            VALUES (
                                '". n($this->_userId) ."',
                                '". $this->_dateTime ."',
                                ". q($this->_title) . ",
                                ". q($this->_description) . ",
                                '". $this->_status ."',
                                NOW(),
                                '". $this->_createdBy ."',
                                NOW(),
                                '". $this->_updatedBy ."'
                            )                        
                        "; 
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    $this->_saveFile();
                    return true;
                }
            }
            return false;
        }

        private function _update() {
            if($this->_id) {
                $sql = "UPDATE idea_tracker SET
                            date_time = '". $this->_dateTime ."',
                            title = ". q($this->_title) . ",
                            description = ". q($this->_description) . ",
                            status = '". $this->_status ."',
                            update_date =  NOW(),
                            updated_by = '". $this->_updatedBy ."'
                        WHERE id = $this->_id
                        ";
                if(db_execute($sql)) {
                    $this->_saveFile();
                    return true;
                }
            }
            return false;
        }
        public function Delete() {
            if($this->_id) {
                $sql = "UPDATE idea_tracker SET
                            deleted = 1,
                            deleted_date = NOW(),
                            deleted_by = '".u()->getEmail()."'
                            WHERE id = {$this->_id}
                        ";
                if(db_execute($sql)) {
                    $sql = "UPDATE idea_tracker_files SET 
                                deleted = 1,
                                deleted_date = NOW(),
                                deleted_by = '".u()->getEmail()."'
                            WHERE idea_tracker_id = {$this->_id}
                        ";
                    db_execute($sql);
                    return true;
                }
            }
            return false;
        }
        public function DeleteAttachment($id) {
            if($id > 0) {
                $sql = "UPDATE idea_tracker_files SET 
                            deleted = 1,
                            deleted_date = NOW(),
                            deleted_by = '".u()->getEmail()."'
                        WHERE id = $id
                    ";
                db_execute($sql);
                return true;
            }
            return false;
        }

        public static function GetIdeasList($status = 'ALL', $p = 0, $page_limit = 10) {
            $return = array();
            $and = "";
            
            $total_pages=0;
            $total_results=0;
            $status = strtoupper($status);
            if(u()->isAdmin()) {
                $and = "";
            }
            else {
                $and .= " AND a.user_id =". u()->getId();
            }
            if(in_array($status, array('DRAFT','PUBLISHED'))){
                $and .= " AND a.status = '$status'";
            }
            if($p > 0) {
                $sql = "SELECT SQL_CALC_FOUND_ROWS a.id, a.user_id, a.title, a.description, a.status, UNIX_TIMESTAMP(a.date_time) as date_time, a.create_date, a.update_date,
                        a.deleted, a.deleted_date, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                        FROM idea_tracker a
                        INNER JOIN users b on b.email = a.created_by
                        INNER JOIN users c on c.email = a.updated_by
                        WHERE a.deleted = 0 $and
                        ORDER BY a.date_time DESC
                        LIMIT ".(($p-1)*$page_limit).",".$page_limit;
            }
            else {
                $sql = "SELECT a.id, a.user_id, a.title, a.description, a.status, UNIX_TIMESTAMP(a.date_time) as date_time, a.create_date, a.update_date,
                        a.deleted, a.deleted_date, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                        FROM idea_tracker a
                        INNER JOIN users b on b.email = a.created_by
                        INNER JOIN users c on c.email = a.updated_by
                        WHERE a.deleted = 0 $and
                        ORDER BY a.date_time DESC
                        ";
            }
            $rows = db_get_all($sql);
            if($rows) {
                if($p > 0) {
                    $sql = "SELECT FOUND_ROWS() AS total";
                    $ttl = db_get_row($sql);
                    $total_results = $ttl["total"];
                    if($total_results > 0) { 
                        self::$_totalResults = $total_results; 
                        $total_pages = ceil($total_results/$page_limit);
                        self::$_totalPages = $total_pages; 
                    }
                }
            }
            foreach($rows as $row){
                self::$_Ideas[$row["id"]] = self::_Init($row);
                $sql = "SELECT * FROM idea_tracker_files WHERE idea_tracker_id = '{$row['id']}' AND deleted=0";
                $pRows = db_get_all($sql);
                if($pRows) {
                    foreach($pRows as $pRow) {
                        self::$_Ideas[$row["id"]]->_files[$pRow["id"]] = $pRow;
                    }
                }
            }
            if(!u()->isAdmin() && $status == "ALL") {
                #self::_userDrafts();
            } 
            return self::$_Ideas;
        }

        public function getUserDrafts() {
            $this->_userDrafts();
            return $this->_Ideas;
        }
        private function _userDrafts() {
            $and = "";
            if(!u()->isAdmin()) {
                $and .= " AND a.user_id =".u()->getId();
            } 
            $sql = "SELECT a.id, a.user_id, a.title, a.description, a.status, UNIX_TIMESTAMP(a.date_time) as date_time, a.create_date, a.update_date,a.deleted, a.deleted_date,
                    CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                    FROM idea_tracker a
                    INNER JOIN users b on b.email = a.created_by
                    INNER JOIN users c on c.email = a.updated_by
                    WHERE a.deleted = 0 AND a.status = 'DRAFT' $and
                    ORDER BY a.date_time DESC
                    ";
            $rows = db_get_all($sql);
            foreach($rows as $row){
                self::$_Ideas[$row["id"]] = self::_Init($row);
            }
        }
       
        public function getTotalComments() {
            $total = 0;
            if($this->_id) {
                $sql = "SELECT count(id) as total FROM idea_tracker_comments WHERE parent_id = 0 AND idea_tracker_id = {$this->_id}";
                $row = db_get_row($sql);
                if($row)
                    $total = $row["total"];
            }
            return $total;
        }
        public function getTotalCommentsReply() {
            $total = 0;
            if($this->_id) {
                $sql = "SELECT count(id) as total FROM idea_tracker_comments WHERE parent_id > 0 AND idea_tracker_id = {$this->_id}";
                $row = db_get_row($sql);
                if($row)
                    $total = $row["total"];
            }
            return $total;
        }
 
    }
?>
