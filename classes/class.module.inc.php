<?php

class Module {
	
	private $_name;
	private $_code;
	
	public function __construct() {
		
	}
	
	public function getName() {
		return $this->_name;
	}
	
	public function getCode() {
		return $this->_code;
	}
	
	private static function Init($array, $object = null) {
		if(!$object instanceof self) {
			$object = new self;
		}
		
		$object->_name = $array['name'];
		$object->_code = $array['code'];
		
		return $object;
	}
	
	public static function GetAll() {
		$return = array();
		$sql = "SELECT name, code FROM modules";
		foreach(db_get_all($sql) as $row) {
			$return[] = self::Init($row);
		}
		
		return $return;
	}
	
	public static function Get($code) {
		$code = intval($code);
		$sql = "SELECT name, code FROM modules WHERE code = '{$code}'";
		$row = db_get_row($sql);
		if($row) {
			return self::Init($row);
		}

		return false;
	}
}