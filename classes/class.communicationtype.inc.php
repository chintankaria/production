<?php
    class CommunicationType {
        private static $_Default = null;
        private static $_CommunicationType = null;

        private $_id = null;
        private $_name = null;
        private $_createdBy = null;
        private $_createDate = null;
        private $_updateDate = null;
        private $_updatedBy = null;

        public function __construct($id = null){
            if(!is_null($id)){
                $t = self::Get($id);
                $this->_id = $t->getId();
                $this->_name = $t->getName();
                $this->_createdBy = $t->getCreatedBy();
                $this->_createDate = $t->getCreateDate();
                $this->_updateDate = $t->getUpdateDate();
                $this->_updatedBy = $t->getUpdatedBy();
            }
        }

        public function getId() { return $this->_id; }
        public function getName(){ 
            return (empty($this->_name) ? "Others" : $this->_name); 
        }
        public function getCreatedBy() {
            return date("m/dY H:i", strtotime($this->_createdBy));
        }
        public function getCreateDate(){
            return $this->_createDate;
        }
        public function getUpdateDate() {
            return date("m/d/Y H:i", strtotime($this->_updateDate));
        }
        public function getUpdatedBy() {
            return $this->_updatedBy;
        }

        public function setName($name){
            $this->_name = trim($name);
        }
        
        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function delete(){
            if($this->_id) {
                $sql_update = "UPDATE contact_communication set communication_type_id = 1 WHERE communication_type_id='{$this->_id}'";
                $sql = "DELETE FROM communication_type WHERE id='{$this->_id}' LIMIT 1";
                if(db_execute($sql)){
                    db_execute($sql_update);
                    return true;
                }
            }
            return false;
        }
        public function checkName() {
            $and = '';
            if($this->_id) $and = " AND id != {$this->_id}";
            $sql = "SELECT id FROM communication_type
                    WHERE name = " . q($this->_name) . " $and
                    ";
            
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        private function _add(){
            if($this->_name){
                $sql = "
                    INSERT INTO communication_type ( name, created_by, create_date,updated_by,update_date)
                    VALUES(
                        " . q($this->_name) . ",
                        '" . u()->getEmail() ."',
                        NOW(),
                        '" . u()->getEmail() ."',
                        NOW()
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_name){
                $sql = "
                    UPDATE communication_type SET
                    name = " . q($this->_name) . ",
                    updated_by = '" . u()->getEmail() ."',
                    update_date =  NOW()
                    WHERE id = {$this->_id}
                ";
                return db_execute($sql);
            }

            return false;
        }

        public static function Get($id){
            if(self::$_CommunicationType[$id]) return self::$_CommunicationType[$id];
            else return self::$_Default;
        }

        public static function GetAll(){
            return self::$_CommunicationType;
        }

        public static function Init(){
            self::$_Default= self::_Init(array("id" => 0, "name" => "Others"));

            $sql = "
                SELECT a.id, a.name, a.create_date, a.update_date,
                    CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                FROM communication_type a
                INNER JOIN users b on b.email = a.created_by
                INNER JOIN users c on c.email = a.updated_by
                WHERE a.id > 0
                ORDER BY a.name
            ";

            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_CommunicationType[$row["id"]] = self::_Init($row);

        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_name = $array["name"];
            $object->_createdBy = $array["created_by"];
            $object->_createDate = $array["create_date"];
            $object->_updatedBy = $array["updated_by"];
            $object->_updateDate = $array["update_date"];

            return $object;
        }
    }

    CommunicationType::Init();
