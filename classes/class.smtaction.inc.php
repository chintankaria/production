<?php
    class SMTAction {
        private $_id = null;
        private $_title = null;
        private $_description = null;
        private $_assignedTo = null;
        private $_dueDate = null;
        private $_status = null;
        private $_createDate = null;
        private $_createdBy = null;
        private $_updateDate = null;
        private $_updatedBy = null;

        private static $_SMTActions = null;
        private $_comments = null;

        public function __construct ($id = null) {
            $id = pintval($id);
            if($id > 0) {
                $sql = "SELECT 
                            a.id, a.title, a.description, 
                            a.assigned_to as assignedTo,
                            a.due_date as dueDate, 
                            a.item_status as status, 
                            a.create_date as createDate, 
                            a.update_date as updateDate, 
                            CONCAT(c.first_name,' ',c.last_name) as createdBy,
                            CONCAT(b.first_name,' ',b.last_name) as updatedBy
                        FROM smtaction a
                        LEFT JOIN users b on b.email = a.updated_by
                        INNER JOIN users c on c.email = a.created_by
                        WHERE a.id = {$id}
                        ";
                $row = db_get_row($sql);
                if($row) self::_Init($row, $this);
            } else {
                self::_Init(array(), $this);
            }
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;

            $object->_id = $array["id"];
            $object->_title = $array["title"];
            $object->_description = $array["description"];
            $object->_assignedTo = $array["assignedTo"];
            $object->_dueDate = $array["dueDate"];
            $object->_status = $array["status"];
            $object->_createDate = $array["createDate"];
            $object->_createdBy = $array["createdBy"];
            $object->_updateDate = $array["updateDate"];
            $object->_updatedBy = $array["updatedBy"];
            return $object;
        }

        public function getId() {
            return $this->_id;
        }

        public function getTitle(){
            return $this->_title;
        }
        public function setTitle($title){
            $this->_title = trim($title);
            if(!$this->_title) e("Missing or invalid title.");
        }

        public function getDescription($length = 0){
            if ($length > 0) {
                $ret = substr($this->_description, 0, $length);
                if (strlen($this->_description) > $length)
                    $ret .= " ...";
            } else {
                $ret = nl2br($this->_description);
            }

            return $ret;
        }
        public function setDescription($description){
            $this->_description = trim($description);
        }

        public function getAssignedToForDisplay($sep = ","){
            $mails = explode(",", $this->_assignedTo);
            $sql = "SELECT CONCAT(b.first_name,' ',b.last_name) as assignedTo FROM users b WHERE b.email IN ";
            foreach ($mails as $mail) {
                if ($in_str) 
                    $in_str .= ", '" . $mail . "'";
                else
                    $in_str = "'" . $mail . "'";
            }
            $sql .= "($in_str)";

            $rows = db_get_all($sql);
            foreach($rows as $row)
                $names[] = $row['assignedTo'];
            if($names) return implode($sep, $names);
        }
        public function getAssignedTo(){
            return $this->_assignedTo;
        }
        public function setAssignedTo($assignedTo){
            $this->_assignedTo = trim($assignedTo);
            if(!$this->_assignedTo) e("Missing or invalid owner assignment");
        }
        public function getDueDate() {
            return date("m/d/Y", strtotime($this->_dueDate));
        }
        public function setDueDate($dueDate){
            $this->_dueDate = trim($dueDate);
            if(!$this->_dueDate) e("Missing or invalid due date");
        }

        public function getStatus(){
            return $this->_status;
        }
        public function setStatus($status){
            $this->_status = trim($status);
            if(!$this->_status) e("Missing or invalid status.");
        }

        public function getUpdateDate() {
            if ($this->_updateDate)
                return date("m/d/Y", strtotime($this->_updateDate));
            return "";
        }
        public function getUpdatedBy() {
            return $this->_updatedBy;
        }
        public function setUpdatedBy($updatedBy){
            $this->_updatedBy = trim($updatedBy);
        }
        public function getCreateDate() {
            return date("m/d/Y", strtotime($this->_createDate));
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function setCreatedBy($createdBy){
            $this->_createdBy = trim($createdBy);
        }


        public function save(){
            return $this->_add();
        }

        private function _add(){
            if (is_Null($this->_id) || $this->_id == 0) {
                $sql = "
                INSERT INTO smtaction (
                        title, description, assigned_to, due_date,
                        item_status, create_date, created_by)
                VALUES (
                    " . q($this->_title) . ",
                    " . q($this->_description) . ",
                    " . q($this->_assignedTo) . ",
                    " . q(ymd($this->_dueDate)) . ",
                    " . q($this->_status) . ",
                    NOW(),
                    " . q($this->_createdBy) . "
                ) ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }

            } else {
                $sql = "
                UPDATE smtaction SET
                    title = " . q($this->_title) . ",
                    description = " . q($this->_description) . ",
                    assigned_to = " . q($this->_assignedTo) . ",
                    due_date = " . q(ymd($this->_dueDate)) . ",
                    item_status = " . q($this->_status) . ",
                    update_date = NOW(),
                    updated_by = '".u()->getEmail()."'
                WHERE id = " . $this->_id;
                if(db_execute($sql))
                    return true;
            }

            return false;
        }
        public function Delete() {
            if($this->_id) {
                $sql = "UPDATE smtaction SET
                            deleted = 1,
                            deleted_date = NOW(),
                            deleted_by = '".u()->getEmail()."'
                            WHERE id = {$this->_id}
                        ";
                return db_execute($sql);
            }
            return false;
        }
        public static function GetActionItems($from_date = '', $to_date = '', $status = ''){
            $return = array();
            
            if($from_date && $to_date) {
                $from_date = strtotime($from_date);
                $to_date = strtotime($to_date);
                $and .= " AND (UNIX_TIMESTAMP(create_date) BETWEEN $from_date AND $to_date)";
            }
            if(!in_array("ALL",$status)) {
                $and .= " AND a.item_status IN ('". implode("','", $status) ."')";
            } 
            $sql = "SELECT 
                    a.id, a.title, a.description, 
                    a.assigned_to as assignedTo,
                    a.due_date as dueDate, 
                    a.item_status as status, 
                    a.create_date as createDate,
                    a.update_date as updateDate,
                    CONCAT(c.first_name,' ',c.last_name) as createdBy,
                    CONCAT(b.first_name,' ',b.last_name) as updatedBy
                FROM smtaction a
                LEFT JOIN users b on b.email = a.updated_by
                INNER JOIN users c on c.email = a.created_by
                WHERE a.deleted = 0 $and
                ORDER BY a.create_date DESC
                ";
            $pRows = db_get_all($sql);
            foreach($pRows as $pRow) {
                self::$_SMTActions[$pRow["id"]] = self::_Init($pRow);
                self::$_SMTActions[$pRow["id"]]->_comments = SMTComments::GetComments($pRow['id']) ;
            }

            return self::$_SMTActions;
        }
        
        public function getComments() {
            $this->_loadComments();
            return $this->_comments;
        }
        
        private function _loadComments() {
            if($this->_id) {
                $this->_comments = SMTComments::GetComments($this->_id) ;
            }
        }

        public function getTotalComments() {
            if($this->_id) {
                $sql = "SELECT count(id) as total FROM smtaction_comments WHERE smtaction_id={$this->_id} AND deleted = 0";
                $row = db_get_row($sql);
                return $row['total']; 
            }
        }
        
    }
?>
