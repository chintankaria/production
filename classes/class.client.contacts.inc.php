<?php
class ClientContact {
    private $_id = null;
    private $_clientId = null;
    private $_firstName = null;
    private $_lastName = null;
    private $_email = null;
    private $_phone = null;
    private $_jobTitle = null;
    private $_isPrimary = null;
    private $_isActive = null;

    private $_createDate = null;
    private $_createdBy = null;
    private $_updatedBy = null;
    private $_updateDate = null;

    public function __construct($id = null){
        $id = pintval($id);

        if($id > 0) {
            $sql = "SELECT a.*, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                     FROM client_contacts a
                     INNER JOIN users b on b.email = a.created_by
                     INNER JOIN users c on c.email = a.updated_by
                     WHERE a.id = '$id'";
            
            $row = db_get_row($sql);
            if($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null){
        if(!$object instanceof self) $object = new self;

        $object->_id = $array["id"];
        $object->_clientId = $array["client_id"];
        $object->_firstName = $array["first_name"];
        $object->_lastName = $array["last_name"];
        $object->_email = $array["email"];
        $object->_phone = $array["phone"];
        $object->_jobTitle = $array["job_title"];
        $object->_isPrimary = $array["is_primary"];
        $object->_isActive = $array["is_active"];
        $object->_createDate = $array["create_date"];
        $object->_createdBy = $array["created_by"];
        $object->_updateDate = $array["update_date"];
        $object->_updatedBy = $array["updated_by"];

        return $object;
    }
    public function getId() {
        return $this->_id;
    }
    public function getClientId() {
        return $this->_clientId;
    }
    public function getFirstName() {
        return $this->_firstName;
    }
    public function getLastName() {
        return $this->_lastName;
    }
    public function getFullName() {
        return trim("{$this->_firstName} {$this->_lastName}");
    }
    public function getEmailId() {
        return $this->_email;
    }
    public function getPhone() {
        return $this->_phone;
    }
    public function getJobTitle() {
        return $this->_jobTitle;
    }
    public function getIsPrimary() {
        return $this->_isPrimary;
    }
    public function getIsActive() {
        return $this->_isActive;
    }
    public function getCreateDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }
    public function getUpdateDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_updateDate));
    }
    public function getUpdatedBy() {
        return $this->_updatedBy;
    }

    public function setClientId($client_id) {
        $this->_clientId = intval($client_id);
    }
    public function setFirstName($first_name) {
        $this->_firstName = trim($first_name);
    }
    public function setLastName($last_name) {
        $this->_lastName = trim($last_name);
    }
    public function setEmailId($email) {
        $this->_email = trim($email);
    }
    public function setPhone($phone) {
        $this->_phone = trim($phone);
    }
    public function setJobTitle($job_title) {
        $this->_jobTitle = trim($job_title);
    }
    public function setIsPrimary($is_primary) {
        $this->_isPrimary = intval($is_primary);
    }
    public function setIsActive($is_active) {
        $this->_isActive = intval($is_active);
    }

    public function save(){
        if($this->_id) return $this->_update();
        else return $this->_add();
    }

    private function _add() {
        if($this->_email && $this->_clientId) {
            $sql = "INSERT INTO client_contacts ( client_id, first_name, last_name, email, phone, job_title, is_primary, is_active, 
                                                    create_date, created_by, update_date, updated_by
                                                )
                                        VALUES ( 
                                                ". n($this->_clientId) .",
                                                ". q($this->_firstName) .",
                                                ". q($this->_lastName) .",
                                                ". q($this->_email) .",
                                                ". q($this->_phone) .",
                                                ". q($this->_jobTitle) .",
                                                ". $this->_isPrimary .",
                                                ". $this->_isActive .",
                                                NOW(),
                                                '". u()->getEmail()."',
                                                NOW(),
                                                '". u()->getEmail() ."'
                                        );
                    ";
            if(db_execute($sql)){
                $this->_id = db_insert_id();
                return true;
            }
        }

        return false;
    }

    private function _update() {
        if($this->_id) {
            $sql = "UPDATE client_contacts SET
                        client_id = " . n($this->_clientId) . ",
                        first_name = ". q($this->_firstName) .",
                        last_name = ". q($this->_lastName) .",
                        email = ". q($this->_email) .",
                        phone = ". q($this->_phone) .",
                        job_title = ". q($this->_jobTitle) .",
                        is_primary = ". $this->_isPrimary .",
                        is_active = ". $this->_isActive .",
                        updated_by = '". u()->getEmail()."',
                        update_date = NOW()
                    WHERE id = $this->_id
                    ";
            if(db_execute($sql)) {
                return true;
            }
        }
        return false;
    } 

    public function Delete() {
        if($this->_id > 0) {
            $sql = "DELETE FROM client_contacts WHERE id = $this->_id LIMIT 1";
            
            if(db_execute($sql)) {
                return true;
            }
        }
        return false;
    }

    public static function GetAll() {
        return self::GetContacts();
    }
    public static function GetContacts($client_id = null) {
        $return = array();
        $and = "";

        if($client_id > 0) {
            $and = " AND client_id = $client_id";
        }

        $sql = "SELECT a.*, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by
                 FROM client_contacts a
                 INNER JOIN users b on b.email = a.created_by
                 INNER JOIN users c on c.email = a.updated_by
                 WHERE a.id > 0 $and ORDER BY a.first_name";
        $rows = db_get_all($sql);
        foreach($rows as $row) {
            $return[$row['id']] = self::_Init($row);
        }
        
        return $return;
    }
}
?>
