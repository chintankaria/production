<?php
    class _ {
        public static $Smarty = null;
        public static $Db = null;
        public static $User = null;

        private static $_Mailer = null;
        private static $_Mailer_Transport = null;

        public static function Mail($from = null, $to, $subject, $body, $attachment=null){
            include_once(AppPath::$LIBS . "/swift/swift_required.php");
            if(is_null(self::$_Mailer)){
                self::$_Mailer_Transport = Swift_SmtpTransport::newInstance("127.0.0.1", 25)
                    ->setUsername('admin.timesheet@globalss.com')
                    ->setPassword('globalss')
                ;
                self::$_Mailer = Swift_Mailer::newInstance(self::$_Mailer_Transport);
            }

            if($to instanceof User) $to = array($to->getEmail() => $to->getName());

            if(!is_array($from) && count($from) < 1) {
                $from = array("admin.timesheet@globalss.com" => "GSS Administrator");
            }
            if($attachment != null) {
                $message = Swift_Message::newInstance($subject)
                    ->setFrom($from)
                    ->setTo($to)
                    ->setBody($body,'text/html')
                    ->attach(Swift_Attachment::fromPath($attachment))
                ;
            }
            else {
                $message = Swift_Message::newInstance($subject)
                    ->setFrom($from)
                    ->setTo($to)
                    ->setBody($body,'text/html')
                ;
            }
            return self::$_Mailer->send($message);
        }
    }
?>
