<?php

require_once('class.licensetype.inc.php');

class LicenseTypes {
	
	public static function CreateNew() {
		return new LicenseType();
	}
	
	public static function FindAll() {
		$sql = "
			SELECT a.id, a.name, a.editable, a.description, COUNT(b.id) AS active_license_count
			FROM license_types a
			LEFT JOIN licenses b ON (b.license_type_id = a.id AND (b.license_end_date IS NULL OR b.license_end_date > CURDATE()))
			GROUP BY a.id
			ORDER BY a.name
		";
		
		$return = array();
		foreach(db_get_all($sql) as $row) {
			$return[] = new LicenseType($row);
		}
		
		return $return;
	}
	
	public static function Find($id) {
		$id = intval($id);
		$sql = "
			SELECT a.id, a.name, a.editable, a.description, COUNT(b.id) AS active_license_count
			FROM license_types a
			LEFT JOIN licenses b ON (b.license_type_id = a.id AND (b.license_end_date IS NULL OR b.license_end_date > CURDATE()))
			WHERE a.id = '{$id}'
			GROUP BY a.id
		";
			
		$row = db_get_row($sql);
		if($row) {
			return new LicenseType($row);
		} else {
			return null;
		}
	}
	
	public static function Save(LicenseType $license_type) {
		$id = intval($license_type->getId());
		$name = q($license_type->getName());
		$description = q($license_type->getDescription());
		if($id < 1) {
			$sql = "
				INSERT INTO license_types (name, description)
				VALUES ({$name}, {$description})
			";
					
			if(db_execute($sql)) {
				return self::Find(db_insert_id());
			}
		} else {
			$sql = "
				UPDATE license_types SET
					name = {$name},
					description = {$description}
				WHERE id = {$id}
				AND editable
			";
				
			if(db_execute($sql)) {
				return self::Find($id);
			}
		}
		
		return false;
	}
	
	public static function Delete($arg) {
		if($arg instanceof LicenseType) {
			$id = intval($arg->getId());
		} else {
			$id = intval($arg);
		}
		
		$sql = "
			DELETE FROM license_types
			WHERE id = {$id}
			AND editable
		";
			
		return (boolean) db_execute($sql);
	}
}
