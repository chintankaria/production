<?php

class ProductModule {
	private $_id;
	private $_productId;
	private $_code;
	private $_name;

	public function __construct($id, $productId, $code, $name) {
		$this->_id = $id;
		$this->_productId = $productId;
		$this->_code = $code;
		$this->_name = $name;
	}

	public function getId() {
		return $this->_id;
	}

	public function getProductId() {
		return $this->_productId;
	}

	public function getCode() {
		return $this->_code;
	}

	public function getName() {
		return $this->_name;
	}
	
	public static function GetModulesByProductId($productId, $codes = array()) {
		$productId = intval($productId);
		$codes = array_filter($codes, 'pintval');

		if($codes) {
			$and = 'AND a.code IN (' . implode(', ', $codes) . ')';
		} else {
			$and = '';
		}

		$sql = "
			SELECT a.id, a.product_id, a.code, a.name
			FROM product_modules a
			WHERE a.product_id = '{$productId}'
			{$and}
		";

		return self::CreateModulesFromSql($sql);
	}

	public static function GetModulesByLicenseId($licenseId) {
		$licenseId = intval($licenseId);

		$sql = "
			SELECT a.id, a.product_id, a.code, a.name
			FROM product_modules a
			INNER JOIN license_modules b ON b.module_id = a.id
			WHERE b.license_id = '{$licenseId}'
		";

		return self::CreateModulesFromSql($sql);
	}

	private static function CreateModulesFromArray($array) {
		$modules = array();
		foreach($array as $row) {
			$modules[$row['id']] = new self($row['id'], $row['product_id'], $row['code'], $row['name']);
		}

		return $modules;
	}

	private static function CreateModulesFromSql($sql) {
		return self::CreateModulesFromArray(db_get_all($sql));
	}
}
