<?php
    class ColorCode {
        private static $_DefaultTypes = null;
        private static $_EventColors = null;
        private static $_UserColors = null;

        private $_id = null;
        private $_name = null;
        private $_bgColor = null;
        private $_textColor = null;
        private $_createDate = null;
        private $_createdBy = null;
        private $_updateDate = null;
        private $_updatedBy = null;

        private $_userId = null;
        private $_userBgColor = null;
        private $_userTextColor = null;

        public function getCalendarColorCode() {
            $EventColors = array(
                'default' => array(
                    'class_name' => '',
                    'bgcolor' => '#0078AE',
                    'color' => '#FFF',
                ),
                'holiday' => array(
                    'class_name' => 'holiday',
                    'bgcolor' => '#C30',
                    'color' => '#FFF',
                ),
                'confirmed' => array(
                    'class_name' => 'confirmed',
                    'bgcolor' => '#096',
                    'color' => '#FFF',
                ),
                'pending' => array(
                    'class_name' => 'pending',
                    'bgcolor' => '#FF6',
                    'color' => '#333',
                ),
                'other' => array(
                    'class_name' => 'other',
                    'bgcolor' => '#09C',
                    'color' => '#FFF',
                ),
            );

            $sql = "SELECT * FROM calendar_colorcode ORDER BY name";
            $rows = db_get_all($sql);

            foreach($rows as $row) {
                $arr = array( 
                            "class_name" => strtolower($row["name"]),
                            "bgcolor" => "#" . $row["background_color"],
                            "color" => "#" . $row["text_color"],
                        );
                $EventColors[strtolower($row["name"])] = $arr;
            }
            return $EventColors;
        }

        public static function saveCalendarColors($values) {
            if(is_array($values["ids"]) && count($values["ids"]) > 0) {
                for($i=0; $i < count($values["ids"]); $i++) {
                    $id = $values["ids"][$i];
                    if($id > 0) {
                        $sql = "UPDATE calendar_colorcode SET
                                    text_color = '".$values["txtText-$id"]."',
                                    background_color = '".$values["txtBackground-$id"]."',
                                    update_date = NOW(),
                                    updated_by = '". u()->getEmail()."'
                                WHERE id=$id
                                ";
                        db_execute($sql);
                    }
                }
                return true;
            }
            return false;
        }

        public function saveUserColors($values) {
            if(is_array($values["user_ids"]) && count($values["user_ids"]) > 0) {
                for($i=0; $i < count($values["user_ids"]); $i++) {
                    $user_id = $values["user_ids"][$i];
                    $id = $this->checkUserColor($values["user_ids"][$i]);
                    if($id > 0 && $values["txtCalText-$user_id"] != "" && $values["txtCalBackground-$user_id"] != "") {
                        $sql = "UPDATE users_colorcode SET
                                    cal_text_color = '".$values["txtCalText-$user_id"]."',
                                    cal_background_color = '".$values["txtCalBackground-$user_id"]."',
                                    update_date = NOW(),
                                    updated_by = '". u()->getEmail()."'
                                WHERE id=$id
                                ";
                        db_execute($sql);
                    }
                    else if($values["txtCalText-$user_id"] != "" && $values["txtCalBackground-$user_id"] != "") {
                        $sql = "INSERT INTO users_colorcode (user_id, cal_text_color, cal_background_color, create_date, created_by, update_date, updated_by)
                                    VALUES(
                                        '$user_id',
                                        '".$values["txtCalText-$user_id"]."',
                                        '".$values["txtCalBackground-$user_id"]."',
                                        NOW(),
                                        '".u()->getEmail()."',
                                        NOW(),
                                        '".u()->getEmail()."'
                                    )
                                ";
                        db_execute($sql);
                    }
                    else {
                        $sql = "DELETE FROM users_colorcode WHERE user_id = $user_id LIMIT 1";
                        db_execute($sql);
                    }
                }
                return true;
            }
            return false;
        }
        public function checkUserColor($user_id) {
            if($user_id > 0) {
                $sql = "SELECT id FROM users_colorcode WHERE user_id = $user_id";
                
                if($rs = db_get_row($sql)){
                    return $rs['id'];
                }
            }
            return false;
        }
        public function getUserColors($id = 0) {
            $result = "";
            $and = '';
            if($id > 0) 
                $and = "AND a.id=$id";

            $sql = "SELECT a.id, CONCAT(a.first_name, ' ', a.last_name) as full_name, a.email, 
                    b.id as colorcode_id, b.user_id, b.cal_background_color, b.cal_text_color
                    FROM users a
                    LEFT JOIN users_colorcode b on b.user_id = a.id
                    WHERE NOT a.blocked $and
                    ORDER BY full_name
                    ";
            $rows = db_get_all($sql);
            foreach($rows as $row) {
                if($row["id"] == 1) continue;
                $result[$row["id"]] = $row;
            }
            return $result;
        }
        public function getUserColorCode() {
            
            $sql = "SELECT * FROM users_colorcode";
            $rows = db_get_all($sql);

            foreach($rows as $row) {
                $arr = array( 
                            "class_name" => "u-".$row["user_id"],
                            "bgcolor" => "#" . $row["cal_background_color"],
                            "color" => "#" . $row["cal_text_color"],
                        );
                $UserColors["u-".$row["user_id"]] = $arr;
            }
            return $UserColors;
        }
    }

