<?php

class CalendarEvent {
    private $_id = null;
    private $_userId = null;
    private $_title = null;
    private $_description = null;
    private $_isHoliday = null;
    private $_holidayIn = null;
    private $_status = null;
    private $_startDate = null;
    private $_endDate = null;
    private $_clientId = null;
    private $_projectId = null;
    private $_subprojectId = null;
    private $_createdBy = null;
    private $_updatedBy = null;
    private $_createDate = null;
    private $_updateDate = null;
    

    private static $_statuses = array('CONFIRMED', 'PENDING', 'OTHER');

    public function __construct($id = null){
        if($id !== null){
            $id = intval($id);
            $sql = "
                SELECT id, user_id, title, description, is_holiday, holiday_in, status,client_id, project_id,subproject_id,
                 UNIX_TIMESTAMP(start_date) AS start_date, UNIX_TIMESTAMP(end_date) AS end_date,
                UNIX_TIMESTAMP(create_date) AS create_date, UNIX_TIMESTAMP(update_date) AS update_date, created_by, updated_by
                FROM events
                WHERE id = '{$id}'
            ";
            if($rs = db_get_row($sql)){
                self::Create($rs, $this);
            }
        }
    }

    public static function Create($array, $object = null){
        if(!$object instanceof self) $object = new self;

        $object->_id = $array['id'];
        $object->_userId = $array['user_id'];
        $object->_title = $array['title'];
        $object->_description = $array['description'];
        $object->_isHoliday = (bool) $array['is_holiday'];
        $object->_holidayIn = $arr['holiday_in'];
        $object->_status = $array['status'];
        $object->_startDate = $array['start_date'];
        $object->_endDate = $array['end_date'];
        $object->_clientId = $array['client_id'];
        $object->_projectId = $array['project_id'];
        $object->_subprojectId = $array['subproject_id'];
        $object->_createDate = $array['create_date'];
        $object->_createdBy = $array['created_by'];
        $object->_updateDate = $array['update_date'];
        $object->_updatedBy = $array['updated_by'];

        return $object;
    }

    public function getId(){
        return $this->_id;
    }

    public function getUserId(){
        return $this->_userId;
    }

    public function getClientId(){
        return $this->_clientId;
    }

    public function getProjectId(){
        return $this->_projectId;
    }

    public function getSubprojectId(){
        return $this->_subprojectId;
    }

    public function getTitle(){
        return $this->_title;
    }

    public function getDescription(){
        return $this->_description;
    }

    public function getIsHoliday(){
        return $this->_isHoliday;
    }

    public function getHolidayIn(){
        $holidayin = array('INDIA' => 'India', 'USA' => 'USA');
        return $holidayin[$this->_holidayIn];
    }

    public function getStatus(){
        return $this->_isHoliday ? '' : $this->_status;
    }

    public function getStartDate($format = null){
        if($format) return date($format, $this->_startDate);
        else return $this->_startDate;
    }

    public function getEndDate($format = null){
        if($format) return date($format, $this->_endDate);
        else return $this->_endDate;
    }

    public function getCreateDate($format = null){
        if($format) return date($format, $this->_createDate);
        else return $this->_createDate;
    }

    public function getUpdateDate($format = null){
        if($format) return date($format, $this->_updateDate);
        else return $this->_updateDate;
    }

    public function getCreatedBy(){
        return $this->_createdBy;
    }

    public function getUpdatedBy(){
        return $this->_updatedBy;
    }

    public function setUserId($user){
        if($user instanceof User) $user = $user->getId();
        $this->_userId = pintval($user);
    }

    public function setTitle($title){
        $title = trim($title);
        if($title) $this->_title = $title;
    }

    public function setDescription($description){
        $this->_description = $description;
    }

    public function setIsHoliday($bool){
        $this->_isHoliday = (bool) $bool;
    }

    public function setHolidayIn($holiday_in){
        $this->_holidayIn = $holiday_in;
    }

    public function setClientId($client_id){
        $this->_clientId = $client_id;
    }

    public function setProjectId($project_id){
        $this->_projectId = $project_id;
    }

    public function setSubprojectId($subproject_id){
        $this->_subprojectId = $subproject_id;
    }

    public function setCreatedBy($created_by){
        $this->_createdBy = $created_by;
    }

    public function setUpdatedBy($updated_by){
        $this->_updatedBy = $updated_by;
    }

    public function setStatus($status){
        $status = strtoupper(trim($status));
        if($this->_isHoliday){
            $this->_status = $status;
        }
        else{
            if(in_array($status, self::$_statuses)){
                $this->_status = $status;
            }
            else{
                $this->_status = 'PENDING';
            }
        }
    }

    public function setStartDate($start_date, $use_strtotime = true){
        if($use_strtotime) $this->_startDate = strtotime(trim($start_date));
        else $this->_startDate = $start_date;
    }

    public function setEndDate($end_date, $use_strtotime = true){
        $end_date = trim($end_date);

        if($end_date){
            if($use_strtotime) $this->_endDate = strtotime($end_date);
            else $this->_endDate = $end_date;
        }
    }

    public function save(){
        if($this->_title && $this->_startDate){
            if($this->_endDate < $this->_startDate) $this->_endDate = $this->_startDate;
            return ($this->_id) ? $this->_update() : $this->_insert();
        }

        return false;
    }

    public function delete(){
        if($this->_id){
            return db_execute("DELETE FROM events WHERE id = '{$this->_id}'");
        }

        return false;
    }

    private function _insert(){
        $sql = "
            INSERT INTO events (user_id, client_id, project_id, subproject_id, title, description, is_holiday, holiday_in, status, start_date, end_date, 
                create_date, created_by, update_date,updated_by)
            VALUES (
                '{$this->_userId}',
                '{$this->_clientId}',
                '{$this->_projectId}',
                '{$this->_subprojectId}',
                " . q($this->_title, true, false) . ",
                " . q($this->_description, true, false) . ",
                '" . ($this->_isHoliday ? 1 : 0) . "',
                '{$this->_holidayIn}',
                '" . $this->_status . "',
                FROM_UNIXTIME({$this->_startDate}),
                FROM_UNIXTIME({$this->_endDate}),
                NOW(), 
                '{$this->_createdBy}',
                NOW(),
                '{$this->_updatedBy}'
            )
        ";

        if(db_execute($sql)){
            $this->_id = db_insert_id();
            return true;
        }

        return false;
    }

    private function _update(){
        $sql = "
            UPDATE events SET
                user_id = '{$this->_userId}',
                client_id = '{$this->_clientId}',
                project_id = '{$this->_projectId}',
                subproject_id = '{$this->_subprojectId}',
                title = " . q($this->_title, true, false) . ",
                description = " . q($this->_description, true, false) . ",
                is_holiday = '" . ($this->_isHoliday ? 1 : 0) . "',
                holiday_in = '" . $this->_holidayIn . "',
                status = '" . $this->_status . "',
                start_date = FROM_UNIXTIME({$this->_startDate}),
                end_date = FROM_UNIXTIME({$this->_endDate}),
                updated_by = '{$this->_updatedBy}',
                update_date = NOW()
            WHERE id = '{$this->_id}'
        ";

        return db_execute($sql);
    }
    
    public function checkEvent() {
        if($this->_endDate < $this->_startDate) $this->_endDate = $this->_startDate;
       
        $sub_qry = '';
        if(!$this->_clientId && !$this->_userId) {
            $sub_qry = "title = " . q($this->_title, true, false) . " AND";
        }
        $sub_qry1 = '';
        if($this->_id)
            $sub_qry1 = "id != '{$this->_id}' AND";
        $sql = "SELECT id FROM events 
                WHERE user_id = '{$this->_userId}' AND 
                client_id = '" . ($this->_clientId ? $this->_clientId : 0) . "' AND 
                project_id = '" . ($this->_projectId ? $this->_projectId : 0) . "' AND 
                subproject_id = '" . ($this->_subprojectId ? $this->_subprojectId : 0) . "' AND 
                start_date = FROM_UNIXTIME({$this->_startDate}) AND 
                end_date = FROM_UNIXTIME({$this->_endDate}) AND
                holiday_in = '{$this->_holidayIn}' AND
                $sub_qry
                $sub_qry1
                is_holiday = '" . ($this->_isHoliday ? 1 : 0) . "'
            ";

        if($rs = db_get_row($sql)){
            return $rs['id'];
        }
        return false;
    }
}

