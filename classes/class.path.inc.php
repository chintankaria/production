<?php
    define("BASEDIR", realpath(dirname(__FILE__) . "/../"));

    class AppPath {
        public static $CLASSES = null;
        public static $CONFIGS = null;
        public static $INCLUDES = null;
        public static $LIBS = null;
        public static $FILES = null;
        public static $TMP = null;
        public static $TPL = null;
        public static $TPL_C = null;
        public static $TPL_CACHE = null;
        public static $HTML2PDF = null;

        public static function Init(){
            self::$CLASSES = BASEDIR . "/classes";
            self::$CONFIGS = BASEDIR . "/configs";
            self::$INCLUDES = BASEDIR . "/includes";
            self::$LIBS = BASEDIR . "/libs";
            self::$FILES = BASEDIR . "/files";
            self::$TMP = BASEDIR . "/tmp";
            self::$TPL = BASEDIR . "/tpl";
            self::$TPL_C = BASEDIR . "/tpl_c";
            self::$TPL_CACHE = BASEDIR . "/tpl_cache";
            self::$HTML2PDF = BASEDIR . "/html2pdf";
        }
    }

    AppPath::Init();
