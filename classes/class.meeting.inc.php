<?php
    class Meeting {
        public static $MeetingDurations = array(
                                            "30" => "00:30", "60" => "1:00", "90" => "1:30", "120" => "2:00",
                                            "150" => "2:30", "180" => "3:00", "210" => "3:30", "240" => "4:00",
                                            "270" => "4:30", "300" => "5:00", "330" => "5:30", "360" => "6:00",
                                            "ALL_DAY" => "All Day"
                                        );
        private static $_Meetings = null;
        private $_Revisions = null;

        private $_id = null;
        private $_meetingTypeId = null;
        private $_meetingTypeName = null;
        private $_dateTime = null;
        private $_meetingDuration = null;
        private $_title = null;
        private $_meetingMinutes = null;
        private $_actionItems = null;
        private $_revision = null;
        private $_status;
        private $_createDate = null;
        private $_createdBy = null;

        private $_parentId = null;
        private $_parent = null;

        private $_participantsId = null;
        private $_participants = null;
        private $_userId = null;
        private $_email = null;
        private $_fullName = null;

        public function __construct ($id = null) {
            $id = pintval($id);
            if($id > 0) {
                $sql = "SELECT 
                            a.id, a.parent_id, a.meeting_type_id, UNIX_TIMESTAMP(a.date_time) as date_time, a.title, a.meeting_minutes, 
                            a.action_items, a.revision, a.status,a.meeting_duration,
                            UNIX_TIMESTAMP(a.create_date) as create_date, b.name as meeting_type_name, CONCAT(c.first_name,' ',c.last_name) as created_by
                        FROM meeting a
                        INNER JOIN meeting_types b on b.id = a.meeting_type_id
                        INNER JOIN users c on c.email = a.created_by
                        WHERE a.id = {$id}
                        ";
                $row = db_get_row($sql);
                if($row) self::_Init($row, $this);
            }
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;

            $object->_id = $array["id"];
            $object->_parentId = $array["parent_id"];
            $object->_meetingTypeId = $array["meeting_type_id"];
            $object->_meetingTypeName = $array["meeting_type_name"];
            $object->_dateTime = $array["date_time"];
            $object->_meetingDuration = $array["meeting_duration"];
            $object->_title = $array["title"];
            $object->_meetingMinutes = $array["meeting_minutes"];
            $object->_actionItems = $array["action_items"];
            $object->_revision = $array["revision"];
            $object->_status = $array["status"];
            $object->_createDate = $array["create_date"];
            $object->_createdBy = $array["created_by"];
            return $object;
        }

        private static function _InitParticipants($array, $object = null) {
            if(!$object instanceof self) $object = new self;
            $object->_participantsId = $array["id"];
            $object->_userId = $array["user_id"];
            $object->_email = $array["email"];
            $object->_fullName = $array["full_name"];
            return $object;
        }
        
        public function getParticipants() {
            $this->_loadParticipants();
            return $this->_participants;
        }
        private function _loadParticipants(){
            if(is_null($this->_participants)) {
                $sql = "SELECT a.*,CONCAT(c.first_name,' ',c.last_name) as full_name
                        FROM meeting_participants a
                        LEFT JOIN users c on c.id = a.user_id
                        WHERE a.meeting_id = '{$this->_id}'
                        ORDER BY full_name,a.email
                    ";
                $pRows = db_get_all($sql);
                foreach($pRows as $pRow) $this->_participants[$pRow["id"]]=self::_InitParticipants($pRow);
            }
        }
        
        public function getParticipantsId() {
            return $this->_participantsId;
        }
        public function getUserId() {
            return $this->_userId;
        } 
        public function getEmail() {
            return $this->_email;
        }
        public function getFullName() {
            return $this->_fullName;
        }
        public function getId() { 
            return $this->_id; 
        }
        public function getParentId() {
            return $this->_parentId;
        } 
        public function getMeetingTypeId() {
            return $this->_meetingTypeId;
        }
        public function getMeetingTypeName() {
            return $this->_meetingTypeName;
        }
        public function getDateTime() {
            return mdyhm($this->_dateTime);
        }
        public function getMeetingDuration() {
            return $this->_meetingDuration;
        }
        public function getTitle() {
            return stripslashes($this->_title);
        }
        public function getMeetingMinutes() {
            return stripslashes($this->_meetingMinutes);
        }
        public function getActionItems() {
            return stripslashes($this->_actionItems);
        }
        public function getRevision() {
            return $this->_revision;
        }
        public function getStatus() {
            return ucwords(strtolower(str_replace("_", " ",$this->_status)));
        }
        public function getCreateDate() {
            return date("m/d/Y H:i", strtotime($this->_createDate));
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }

        private function _loadParent() {
            if(is_null($this->_parent) && $this->_parentId) 
                $this->_parent = new self($this->_parentId);
            if($this->_parent && $this->_parent->getId() < 1) {
                $this->_parentId = null;
                $this->_parent = false;
            }
        }

        public function isParent(){
            $this->_loadParent();
            return $this->_parentId;
        }

        public function getParent(){
            $this->_loadParent();
            return $this->_parent;
        }

        public function getParentName(){
            $this->_loadParent();
            if($this->_parent) return $this->_parent->getName();
            else return false;
        }

        public function setMeetingTypeId($meeting_type_id){
            $this->_meetingTypeId = trim($meeting_type_id);
            if(!$this->_meetingTypeId) e("Missing or invalid meeting type.");
        }

        public function setStatus($status){
            $this->_status = trim($status);
            if(!$this->_status) e("Missing or invalid meeting status.");
        }

        public function setTitle($title){
            $this->_title = trim($title);
            if(!$this->_title) e("Missing or invalid meeting title.");
        }

        public function setMeetingMinutes($meeting_minutes){
            $this->_meetingMinutes = trim($meeting_minutes);
            if(!$this->_meetingMinutes) e("Missing or invalid meeting minutes.");
        }

        public function setActionItems($action_items){
            $this->_actionItems = trim($action_items);
            #if(!$this->_actionItems) e("Missing or invalid meeting action items.");
        }

        public function setParentId($parent_id) {
            if(!$parent_id instanceof self)
                $parent = new Meeting($parent_id);
            if($parent_id && $parent->getId() > 0)
                $this->_parentId = $parent->getId();
        }

        public function setDateTime($date_time){
            $this->_dateTime = ymdhms(is_date($date_time));
            if(!$this->_dateTime) e("Missing or invalid meeting date time.");
        }

        public function setMeetingDuration($duration){
            $this->_meetingDuration = $duration;
            if(!$this->_meetingDuration) e("Missing or invalid meeting duration.");
        }

        public function setCreatedBy($created_by){
            $this->_createdBy = trim($created_by);
            if(!$this->_createdBy) e("Missing or invalid meeting creator.");
        }

        public function save(){
            return $this->_add();
        }

        private function _verifyParentId(){
            $sql = "
                SELECT id FROM meeting
                WHERE id = {$this->_parentId}
                AND (
                    parent_id IS NULL
                    OR NOT parent_id
                )
            ";
            if(!db_get_var($sql)) $this->_parentId = null;

            return $this->_parentId;
        }

        private function _add(){
			if($this->_title && $this->_dateTime){
                $this->_verifyParentId();
                $this->setStatus("ACTIVE"); 
                $revision = ($this->_parentId) ? $revision = ($this->_revision + 1) : 1;
                $sql = "
                    INSERT INTO meeting (parent_id, meeting_type_id, date_time, meeting_duration, title, meeting_minutes, action_items, revision, status, 
                    create_date, created_by)
                    VALUES (
                        '".n($this->_parentId)."',
                        '{$this->_meetingTypeId}',
                        '" . $this->_dateTime . "',
                        '" . $this->_meetingDuration . "',
                        " . q($this->_title) . ",
                        " . q($this->_meetingMinutes) . ",
                        " . q($this->_actionItems) . ",
                        $revision ,
                        '". $this->_status ."',
                        NOW(),
                        '".$this->_createdBy."'
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }
        public function Delete() {
            if($this->_id) {
                $sql = "UPDATE meeting SET
                            deleted = 1,
                            deleted_date = NOW(),
                            deleted_by = '".u()->getEmail()."'
                            WHERE id = {$this->_id}
                        ";
                return db_execute($sql);
            }
            return false;
        }
        public function markInactive($id) {
            if($id > 0) {
                $sql = "UPDATE meeting set status='INACTIVE' WHERE id=$id LIMIT 1";
                return db_execute($sql);
            }
            return false;
        }
        public function checkTitle() {
            $and = '';
            if($this->_parentId) {
                $this->_verifyParentId();
                $and .= "AND parent_id = 0 AND id NOT IN({$this->_id},{$this->_parentId})";
            }
            else {
                if($this->_id) $and = " AND id != {$this->_id} ";
                $and .= "AND parent_id = 0 ";
            }
            $sql = "SELECT id FROM meeting
                    WHERE title = " . q($this->_title) . " $and
                    ";
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        public function saveParticipants($user_id = 0,$email) {
            if($email !='') {
                $sql = "INSERT INTO meeting_participants (meeting_id, user_id, email) 
                        VALUES ('{$this->_id}', '$user_id','$email')";
                db_execute($sql);
                return true;
            }
            return false;
        }
        public static function GetMeetings($mtype_id = 0, $from_date = '', $to_date = '', $revision = false){
            $return = array();
            
            $user = u();
            $and = "";
            if($user->isAdmin()) {
                $and = "";
            }
            #else if($user->isMeetinMinutesManager()){
            else {
                $meeting_type_ids = MeetingTypes::getMeetingTypesIdsByUser();
                  
                #$and .= " AND a.created_by = '".$user->getEmail()."'";
                $and .=" AND a.meeting_type_id IN(".implode(", ", $meeting_type_ids).")";
            }
            if($mtype_id > 0)
                $and .= " AND a.meeting_type_id = $mtype_id";
            if($from_date && $to_date) {
                $from_date = strtotime($from_date);
                $to_date = strtotime($to_date)+ ((23 * 60 * 60) + (59 *60)+59);
                $and .= " AND (UNIX_TIMESTAMP(date_time) BETWEEN $from_date AND $to_date)";
            }
                $sql = "SELECT 
                            a.id, a.parent_id, a.meeting_type_id, UNIX_TIMESTAMP(a.date_time) as date_time, a.title, a.meeting_minutes, 
                            a.action_items, a.revision, a.status,a.meeting_duration,
                            a.create_date, b.name as meeting_type_name, CONCAT(c.first_name,' ',c.last_name) as created_by
                        FROM meeting a
                        INNER JOIN meeting_types b ON b.id = a.meeting_type_id
                        INNER JOIN users c on c.email = a.created_by
                        WHERE a.deleted = 0  and a.status='ACTIVE' $and
                        ORDER BY a.create_date DESC
                        ";
            $rows = db_get_all($sql);
            foreach($rows as $row){
                #if($row["parent_id"]) continue;
                self::$_Meetings[$row["id"]] = self::_Init($row);
                if($revision) {
                    $sql = "SELECT
                            a.id, a.parent_id, a.meeting_type_id, UNIX_TIMESTAMP(a.date_time) as date_time, a.title, 
                            a.meeting_minutes, a.action_items, a.revision, a.status,a.meeting_duration,
                            a.create_date, b.name as meeting_type_name, CONCAT(c.first_name,' ',c.last_name) as created_by
                        FROM meeting a
                        INNER JOIN meeting_types b ON b.id = a.meeting_type_id
                        INNER JOIN users c on c.email = a.created_by
                        WHERE a.deleted = 0  and a.status='INACTIVE' $and AND (a.parent_id = ".$row["parent_id"]." OR a.id = ".$row["parent_id"].")
                        ORDER BY a.create_date DESC
                    ";
                    $pRows = db_get_all($sql);
                    foreach($pRows as $pRow) {
                        //self::$_Meetings[$row["id"]]->_Revisions[$pRow["id"]] = self::_Init($row);
                        self::$_Meetings[$pRow["id"]] = self::_Init($pRow);
                    }
                }
            }
            return self::$_Meetings;
        }
        
        public function getMeetingRevisions(){
            $this->_loadRevisions();
            return $this->_participants;
        }
        private function _loadRevisions() {
            if(is_null($this->_Revisions)) {
                $sql = "SELECT
                            a.id, a.parent_id, a.meeting_type_id, UNIX_TIMESTAMP(a.date_time) as date_time, a.title,
                            a.meeting_minutes, a.action_items, a.revision, a.status,a.meeting_duration,
                            a.create_date, b.name as meeting_type_name, CONCAT(c.first_name,' ',c.last_name) as created_by
                        FROM meeting a
                        INNER JOIN meeting_types b ON b.id = a.meeting_type_id
                        INNER JOIN users c on c.email = a.created_by
                        WHERE a.deleted = 0  and a.status='INACTIVE' AND a.parent_id = {$this->_id}
                        ORDER BY a.create_date DESC
                ";
                $pRows = db_get_all($sql);
                foreach($pRows as $pRow) {
                    $this->_Revisions[$pRow["id"]] = self::_Init($row);
                }
            }
        }
    }
?>
