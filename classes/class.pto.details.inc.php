<?php

class PtoPolicy {

    private $_id = null;
    private $_time_period = null;
    private $_max_hours = null;
    private $_from_date = null;
    private $_to_date = null;
    private $_leaves = null;
    private $_location_id = null;
    private $_location_name = null;
    private $_max_service = null;
    private $_table = 'pto_policies';
    

    public function __construct($id = null) {
        $id = pintval($id);
        if ($id > 0) {
            $sql = "SELECT * FROM {$this->_table} WHERE id={$id} order by id";
            $row = db_get_row($sql);
            if ($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null) {
        if (!$object instanceof self)
            $object = new self;

        $object->_id = $array["id"];
        $object->_time_period = $array["time_period"];
        $object->_max_hours = $array["max_hours"];
        $object->_from_date = $array["from_date"];
        $object->_to_date = $array["to_date"];
        $object->_leaves = $array["leaves"];
        $object->_location_id = $array["location_id"];
        $object->_location_name = $array["location_name"];
        $object->_max_service = $array["max_service"];
        return $object;
    }

    public function getId() {
        return $this->_id;
    }

    public function gettime_period() {
        return $this->_time_period;
    }
    public function getMaxHours() {
        return $this->_max_hours;
    }
    public function getFromDate() {
        return $this->_from_date;
    }
    public function getToDate() {
        return $this->_to_date;
    }

    public function getleaves() {
        return $this->_leaves;
    }

    public function getLocation() {
        return $this->_location_id;
    }
    public function getLocationName() {
        return $this->_location_name;
    }
    public function getMaxService() {
        return $this->_max_service;
    }

    public function settime_period($time_period) {
        $this->_time_period = trim($time_period);
    }
    public function setMaxHours($max_hours) {
        $this->_max_hours = $max_hours;
    }
    public function setFromDate($from_date) {
        $this->_from_date = $from_date;
    }
    public function setToDate($to_date) {
        $this->_to_date = $to_date;
    }

    public function setleaves($leaves) {
        $this->_leaves = $leaves;
    }

    public function setLocation($location_id) {
        $this->_location_id = $location_id;
    }
    public function setMaxService($max_service) {
        $this->_max_service = $max_service;
    }
    public function SaveTiers() {
            return $this->_add();
    }

    public function savePolicy() {

        if ($this->_id)
            return $this->_updatePolicy();
        else
            return $this->_addPolicy();
    }

    public function delete() {
        if ($this->_id) {


             $sql_check = "select id from {$this->_table} where location_id = ".$this->_location_id." order by id desc LIMIT 1,1";
            if($rs = db_get_row($sql_check)){
                $id =  $rs['id'];

            if($id > 0){

               $sql = "
                    UPDATE {$this->_table} SET                    
                   to_date = null
                     WHERE id = $id
                    ";
                    
                     db_execute($sql);
            }
        }
            $sql = "DELETE FROM pto_tiers WHERE pol_id='{$this->_id}' ";
            if (db_execute($sql)) {
                $sql2 = "DELETE FROM {$this->_table} WHERE id='{$this->_id}' LIMIT 1";
                if (db_execute($sql2)) {
                    return true;
                }
            }
        }
        return false;
    }
    private function _addPolicy() {

         $sql_date = "select count(*) as cnt from {$this->_table} where location_id = ".$this->_location_id." and from_date >=  '$this->_from_date'   ";

        if($rs_date = db_get_row($sql_date))
                $cnt =  $rs_date['cnt'];


        if($cnt == 0)
        {

            if ($this->_from_date) {

                $sql_check = "select id from {$this->_table} where location_id = ".$this->_location_id." order by id desc";
                if($rs = db_get_row($sql_check)){
                    $id =  $rs['id'];

                if($id > 0){

                   $sql = "
                        UPDATE {$this->_table} SET                    
                        to_date = '" . date('Y-m-d',(strtotime ( '-1 day' , strtotime ( $this->_from_date) ) )) . "'
                         WHERE id = $id
                        ";

                        //echo $sql;die;
                         
                         db_execute($sql);
                }
            }
                
            $sql = "INSERT INTO {$this->_table} (max_hours, from_date, to_date, location_id)
                    VALUES(" . q($this->_max_hours) . "," . q($this->_from_date) . ", " . q($this->_to_date) . "," . q($this->_location_id) . ")";

            //echo $sql;die;  

                if (db_execute($sql)) {
                    $this->_id = db_insert_id();
                    return true;
                }
            }
        }
        else
            return false;
    }

     private function _updatePolicy() {

        //echo $this->_id;die;

        if ($this->_id) {
            $sql = "
                    UPDATE {$this->_table} SET                    
                    max_hours = " . q($this->_max_hours) . ",
                    from_date = " . q($this->_from_date) . ",
                    to_date = " . q($this->_to_date) . "
                    WHERE id = {$this->_id}
                ";

                //echo $sql;die;
                        
            return db_execute($sql);
        }
        return false;
    }

    private function _add() {

        if ($this->_time_period) {
            $sql = "INSERT INTO pto_tiers (pol_id, loc_id,  time_period, leaves, max_service)
                    VALUES(" . q($this->_id) . "," . q($this->_location_id) . "," . q($this->_time_period) . ", " . q($this->_leaves) . "," . q($this->_max_service) . ")";

            
            //echo $sql;die;        

            if (db_execute($sql)) {
                $this->_id = db_insert_id();
                return true;
            }
        }
    }

    private function _update() {
        if ($this->_id) {
            $sql = "
                    UPDATE {$this->_table} SET                    
                    pol_id = " . q($this->_id) . ",
                    time_period = " . q($this->_time_period) . ",
                    leaves = " . q($this->_leaves) . ",
                    location_id = " . q($this->_location_id) . ",
                    max_service = " . q($this->_max_service) . "
                    WHERE id = {$this->_id}
                ";
            //echo $sql;die;    
            return db_execute($sql);
        }
        return false;
    }
    
    public function checktime_period() {
        $and = '';
        if($this->_id) $and = " AND id != {$this->_id}";
        $sql = "SELECT id FROM {$this->_table}
                WHERE time_period = " . q($this->_time_period) . " $and ";

        if($rs = db_get_row($sql)){
            return $rs['id'];
        }
        return false;
    }
    
    public static function GetPolicies($id = 0,$location_id) {
        if ($location_id) {
            $sql = "SELECT * FROM pto_policies WHERE location_id = {$location_id} order by from_date desc ";
        } else {
            $sql = "SELECT * FROM pto_policies order by id ";
        }
        //echo $sql;

        $rows = db_get_all($sql);

        foreach ($rows as $row) {
            $return[$row["id"]] = self::_Init($row);
        }
        return $return;
    }
    public static function checkTiers($id) {
          if ($id) {
            $sql = "SELECT * FROM pto_tiers WHERE pol_id = {$id} order by id ";
        } 
      // echo $sql;

        $rows = db_get_all($sql);
     
        return $rows;
        }
    

    public static function GetLocationByName($location_id = 0) {



        $location = array();

        if($location_id)
            $sql = "SELECT id, name FROM employee_locations e where id = $location_id ";
        else
            $sql = "SELECT id, name FROM employee_locations e ";

        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $location[$row["id"]] = $row;
            }
        }
        //print_r($location);
        return $location;

        }
    
}

