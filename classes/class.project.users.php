<?php

class ProjectUsers {

    private $_id = null;
    private $_userId = null;
    private $_userName = null;
    private $_projectId = null;
    private $_projectRoleId = null;
    private $_projectRoleName = null;
    private $_startDate = null;
    private $_endDate = null;
    private $_billingRate = null;
    private $_authHours = null;
    private $_poNumber = null;
    private $_role = 'MEMBER';
    private $_recDate = null;
    private $_table = 'project_users';

    public function __construct($id = null) {
        $id = pintval($id);
        if ($id > 0) {
            $sql = "SELECT * FROM {$this->_table} WHERE a.id={$id} ";
            $row = db_get_row($sql);
            if ($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null) {
        if (!$object instanceof self)
            $object = new self;

        $object->_id = $array["id"];
        $object->_userId = $array["user_id"];
        $object->_userName = $array["user_name"];
        $object->_projectId = $array["project_id"];
        $object->_projectRoleId = $array["project_role_id"];
        $object->_projectRoleName = $array["project_role_name"];
        $object->_startDate = $array["start_date"];
        $object->_endDate = $array["end_date"];
        $object->_billingRate = $array["billing_rate"];
        $object->_authHours = $array["auth_hours"];
        $object->_poNumber = $array["po_number"];
        $object->_role = $array["role"];
        $object->_recDate = $array["rec_date"];

        return $object;
    }

    public function getId() {
        return $this->_id;
    }

    public function getUserId() {
        return $this->_userId;
    }

    public function getUserName() {
        return $this->_userName;
    }

    public function getProjectId() {
        return $this->_projectId;
    }

    public function getProjectRoleId() {
        return $this->_projectRoleId;
    }

    public function getProjectRoleName() {
        return $this->_projectRoleName;
    }

    public function getStartDate() {
        return $this->_startDate;
    }

    public function getEndDate() {
        return $this->_endDate;
    }

    public function getBillingRate() {
        return $this->_billingRate;
    }

    public function getAuthHours() {
        return $this->_authHours;
    }

    public function getPoNumber() {
        return $this->_poNumber;
    }

    public function getRole() {
        return $this->_role;
    }

    public function getRecordDate() {
        return $this->_recDate;
    }

    public function setId($id) {
        return $this->_id = $id;
    }

    public function setUserId($userId) {
        return $this->_userId = $userId;
    }

    public function setProjectId($projectId) {
        return $this->_projectId = $projectId;
    }

    public function setProjectRoleId($projectRoleId) {
        return $this->_projectRoleId = $projectRoleId;
    }

    public function setStartDate($startDate) {
        return $this->_startDate = $startDate;
    }

    public function setEndDate($endDate) {
        return $this->_endDate = $endDate;
    }

    public function setBillingRate($billingRate) {
        return $this->_billingRate = $billingRate;
    }

    public function setAuthHours($authHours) {
        return $this->_authHours = $authHours;
    }

    public function setPoNumber($poNumber) {
        return $this->_poNumber = $poNumber;
    }

    public function setRole($role) {
        return $this->_role = $role;
    }

    public function save() {
        if ($this->_id)
            return $this->_update();
        else
            return $this->_add();
    }

    private function _add() {
        if ($this->_userId && $this->_projectId) {
            $sql = "INSERT INTO {$this->_table} (user_id, project_id, project_role_id, start_date,	end_date, billing_rate,	auth_hours, po_number, role)
                    VALUES('{$this->_userId}', '{$this->_projectId}', '{$this->_projectRoleId}', '{$this->_startDate}', '{$this->_endDate}', '{$this->_billingRate}', '{$this->_authHours}', '{$this->_poNumber}', '{$this->_role}')";

            if (db_execute($sql)) {
                $this->_id = db_insert_id();
                return true;
            }
        }

        return false;
    }

    private function _update() {
        if ($this->_id && $this->_userId) {
            $sql = "UPDATE {$this->_table} SET                    
                    user_id = '{$this->_userId}',
                    project_id = '{$this->_projectId}',
                    project_role_id = '{$this->_projectRoleId}',
                    start_date = '{$this->_startDate}',
                    end_date = '{$this->_endDate}',
                    billing_rate = '{$this->_billingRate}',
                    auth_hours = '{$this->_authHours}',
                    po_number = '{$this->_poNumber}',
                    role = '{$this->_role}'
                    WHERE id = '{$this->_id}'";
            return db_execute($sql);
        }

        return false;
    }

    public function delete() {
        if ($this->_id) {

            $sql = "DELETE FROM {$this->_table}  WHERE id='{$this->_id}' LIMIT 1";
            if (db_execute($sql)) {
                return true;
            }
        }
        return false;
    }

    public static function GetProjectUsers($projectId = 0) {
        if ($projectId) {
            $sql = "SELECT pu.*, r.name as project_role_name, concat(u.first_name, ' ' ,u.last_name) as user_name FROM project_users as pu 
                    INNER JOIN users u ON u.id = pu.user_id  
                    INNER JOIN employee_project_role r ON r.id = pu.project_role_id  
                    WHERE pu.project_id = {$projectId} ";

            $rows = db_get_all($sql);

            foreach ($rows as $row) {
                $return[$row["id"]] = self::_Init($row);
            }
            return $return;
        }
    }
    
    public static function GetProjectUserRole1($projectId, $userId ) {
        if ($projectId) {
            $sql = "SELECT  project_role_id FROM  project_users WHERE user_id={$userId} AND project_id = {$projectId} LIMIT 1";

            $row = db_get_row($sql);

            if($row){
                return $row['project_role_id'];
            }
            return 0;
        }
    }
    
    public static function GetProjectUserRole($projectId, $userId ) {
        if ($projectId) {
            $sql = "SELECT  pu.*, r.name as project_role_name FROM  project_users  as pu
                    INNER JOIN employee_project_role r ON r.id = pu.project_role_id
                    WHERE pu.user_id={$userId} AND pu.project_id = {$projectId} ";

            $rows = db_get_all($sql);
            
            foreach ($rows as $row) {
                $return[$row["id"]] = $row;
            }
            return $return; 
        }
    }

}
