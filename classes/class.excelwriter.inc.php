<?php
    set_time_limit(0);
    require_once 'Spreadsheet/Excel/Writer.php';

    class ExcelWriter {
        private $_workbook = null;
        private $_worksheet = null;
        private $_cRow = 0;
        private $_cCol = 0;
        private $_sheets = array();
        private $_formatTitle = null;

        private $_matrix = array();

        public function __construct($save_to_file = null){
            $this->_workbook = new Spreadsheet_Excel_Writer($save_to_file);
            $this->_formatTitle = $this->_workbook->addFormat(array(
                'Size' => 12,
                'Bold' => true
            ));
            $this->_workbook->setVersion(8);
        }

        private function _getNextCell(){
            while($this->_matrix[$this->_cRow][$this->_cCol]){
                ++$this->_cCol;
            }

            $this->_matrix[$this->_cRow][$this->_cCol] = 1;
            return $this->_cCol;
        }

        private function _mergeCells($rowspan, $colspan){
            $this->_worksheet->mergeCells(
                $this->_cRow, $this->_cCol,
                $this->_cRow + $rowspan - 1, $this->_cCol + $colspan - 1
            );

            for($row = 0; $row < $rowspan; $row++){
                for($col = 0; $col < $colspan; $col++){
                    $this->_matrix[$this->_cRow + $row][$this->_cCol + $col] = 1;
                }
            }

            $this->_cCol += $colspan;
        }

        private function _getFormat($array){
            if($array){
                return $this->_workbook->addFormat($array);
            }
            else{
                return 0;
            }
        }

        private function _writeCell($data){
            if($data['Bold']){
                $format['Bold'] = true;
            }

            if($data['AlignRight']){
                $format['Align'] = 'right';
            }

            $this->_worksheet->write($this->_cRow, $this->_getNextCell(), $data['Value'], $this->_getFormat($format));

            if($data['Colspan'] > 1 || $data['Rowspan'] > 1){
                $colspan = $data['Colspan'] > 1 ? $data['Colspan'] : 1;
                $rowspan = $data['Rowspan'] > 1 ? $data['Rowspan'] : 1;
                $this->_mergeCells($rowspan, $colspan);
            }
        }

        private function _newRow(){
            $this->_cRow++;
            $this->_cCol = 0;
        }

        private function _resetCells(){
            $this->_cRow = 0;
            $this->_cCol = 0;
            $this->_matrix = array();
        }

        public function setData($array, $title = null, $sheet_name = null){
            if(!$array || !is_array($array)){
                return false;
            }

            $this->_resetCells();

            $sheet_name = trim($sheet_name);
            if(!$sheet_name){
                $sheet_name = "Sheet" . (count($this->_sheets) + 1);
            }

            $this->_worksheet = $this->_workbook->addWorkSheet($sheet_name);
            $this->_sheets[] = $sheet_name;

            $title = trim($title);
            if($title){
                // find total columns
                $tmp_row = array_slice($array, 0, 1);
                $total_cols = 0;
                foreach($tmp_row[0] as $v) $total_cols += $v["Colspan"];

                $this->_worksheet->write($this->_cRow, $this->_cCol, $title, $this->_formatTitle);
                $this->_worksheet->setMerge($this->_cRow, $this->_cCol, $this->_cRow, $this->_cCol + $total_cols - 1);
                $this->_cRow += 2;
            }

            foreach($array as $row){
                foreach($row as $col){
                    $this->_writeCell($col);
                }
                $this->_newRow();
            }
        }

        public function setHTMLTable($html_table, $title = null, $sheet_name = null, $remove_cols = null){
            $this->setData($this->parseHTMLTable($html_table, $remove_cols), $title, $sheet_name);
        }

        public function parseHTMLTable($html_table, $remove_cols = null){
            $return = array();
            $dom = new DOMDocument();

            if(!$remove_cols) $remove_cols = array();
            if(!is_array($remove_cols)) $remove_cols = array($remove_cols);

            $remove_cols = array_filter($remove_cols, 'intval');

            if(@$dom->loadHTML($html_table)){
                $dom->normalizeDocument();

                // Need the following to fix &nbsp; getting converted to Â in excel
                $weird_char = chr(194) . chr(160);

                $tables = $dom->getElementsByTagName('table');
                if($tables){
                    $rnum = 1;

                    foreach($tables as $table){
                        $table_rows = $table->getElementsByTagName('tr');

                        foreach($table_rows as $row){
                            $cnum = 1;
                            foreach($row->childNodes as $col){
                                if(preg_match('/t[d|h]/i', $col->nodeName)){
                                    if(!in_array($cnum, $remove_cols)){
                                        $value = trim(str_replace($weird_char, ' ', $col->textContent));
                                        if(!$value){
                                            $img = $col->getElementsByTagName('img');
                                            if($img->item(0)){
                                                $value = trim($img->item(0)->getAttribute('alt'));
                                            }
                                        }

                                        $colspan = $col->getAttribute('colspan');
                                        if($colspan < 1) $colspan = 1;

                                        $rowspan = $col->getAttribute('rowspan');
                                        if($rowspan < 1){
                                            $rowspan = 1;
                                        }
    
                                        $bold = (bool) preg_match('/th/', $col->nodeName);

                                        $align = $col->getAttribute('align');
                                        $align_right = $align == 'right';
    
                                        $return[$rnum][$cnum] = array(
                                            'Value' => $value,
                                            'Colspan' => $colspan,
                                            'Rowspan' => $rowspan,
                                            'Bold' => $bold,
                                            'AlignRight' => $align_right
                                        );
                                    }
                                    ++$cnum;
                                }
                            }
                            ++$rnum;
                        }

                        $return[$rnum][1] = array(
                            'Value' => ''
                        );

                        ++$rnum;
                    }
                }

                // print '<pre>'; print_r($return); exit;
                // var_export($return); exit;

                return $return;
            }

            return array();
        }

        public function send($file){
            $this->_workbook->send($file);
        }

        public function close(){
            $this->_workbook->close();
        }
    }
