<?php
    class MeetingTypes {
        private static $_DefaultTypes = null;
        private static $_MeetingTypes = null;
        private static $_StandardParticipants = null;

        private $_id = null;
        private $_name = null;
        private $_createDate = null;
        private $_createdBy = null;
        private $_updateDate = null;
        private $_updatedBy = null;

        private $_participantsId = null;
        private $_participants = null;
        private $_userId = null;
        private $_email = null;
        private $_fullName = null;

        public function __construct($id = null){
            if(!is_null($id)){
                $t = self::Get($id);
                $this->_id = $t->getId();
                $this->_name = $t->getName();
                $this->_createDate = $t->getCreateDate();
                $this->_createdBy = $t->getCreatedBy();
                $this->_updateDate = $t->getUpdateDate();
                $this->_updatedBy = $t->getUpdatedBy();
            }
        }

        public function getId(){ 
            return $this->_id; 
        }

        public function getName(){ 
            return (empty($this->_name) ? "Others" : $this->_name); 
        }

        public function getCreateDate() {
            return $this->_createDate;
        }

        public function getCreatedBy() {
            return $this->_createdBy;
        }

        public function getUpdateDate() {
            return $this->_updateDate;
        }

        public function getUpdatedBy() {
            return $this->_updatedBy;
        }

        public function setName($name){
            $this->_name = trim($name);
        }
        
        public function setCreatedBy($created_by){
            $this->_createdBy = trim($created_by);
        }
        
        public function setUpdatedBy($updated_by){
            $this->_updatedBy = trim($updated_by);
        }
        
        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function delete(){
            if($this->_id) {
                $sql_update = '';
                $this->deleteParticipants();
                $sql = "DELETE FROM meeting_types WHERE id='{$this->_id}' LIMIT 1";
                if(db_execute($sql)){
                    //db_execute($sql_update);
                    return true;
                }
            }
            return false;
        }
        public function checkName() {
            $and = '';
            if($this->_id) $and = " AND id != {$this->_id}";
            $sql = "SELECT id FROM meeting_types
                    WHERE name = " . q($this->_name) . " $and
                    ";
            
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }
        private function _add(){
            if($this->_name){
                $sql = "
                    INSERT INTO meeting_types (name,create_date,created_by, update_date, updated_by)
                    VALUES(
                        " . q($this->_name) . ",
                        NOW(),
                        '{$this->_createdBy}',
                        NOW(),
                        '{$this->_updatedBy}'
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_name){
                $sql = "
                    UPDATE meeting_types SET
                    name = " . q($this->_name) . ",
                    update_date = NOW(),
                    updated_by =  '{$this->_updatedBy}'
                    WHERE id = {$this->_id}
                ";
                return db_execute($sql);
            }

            return false;
        }

        public static function Get($id){
            if(self::$_MeetingTypes[$id]) return self::$_MeetingTypes[$id];
            else return self::$_DefaultTypes;
        }

        public static function GetAll(){
            return self::$_MeetingTypes;
        }

        public function GetParticipants() {
            $this->_loadParticipants();
            return $this->_participants;
        }
        public static function GetStandardParticipants($id = 0){
            if($id)
                return self::$_StandardParticipants[$id];
            else 
                return self::$_StandardParticipants;
        }

        private function _loadParticipants(){
            if(is_null($this->_participants)) {
                $sql = "SELECT a.*, CONCAT(c.first_name,' ',c.last_name) as full_name
                        FROM meeting_standard_participants a
                        INNER JOIN users c on c.id = a.user_id
                        WHERE a.meeting_type_id = '{$this->_id}'
                        ORDER BY full_name
                    ";
                $pRows = db_get_all($sql);
                foreach($pRows as $pRow) $this->_participants[$pRow["id"]]=self::_InitParticipants($pRow);
            }
        }

        public static function Init(){
            self::$_DefaultTypes = self::_Init(array("id" => 0, "name" => "Others"));
            if(u()->isAdmin()) {
            $sql = "
                SELECT a.* 
                FROM meeting_types a
                WHERE a.id > 0
                ORDER BY a.name
            ";

            $rows = db_get_all($sql);
            if($rows) {
                foreach($rows as $row) {
                    $sql = "SELECT a.*, CONCAT(c.first_name,' ',c.last_name) as full_name
                            FROM meeting_standard_participants a
                            INNER JOIN users c on c.id = a.user_id
                            WHERE a.meeting_type_id = '{$row["id"]}'
                            ORDER BY full_name
                        ";
                    $pRows = db_get_all($sql);
                    self::$_MeetingTypes[$row["id"]] = self::_Init($row);
                    foreach($pRows as $pRow) self::$_MeetingTypes[$row["id"]]->_participants[$pRow["id"]]=self::_InitParticipants($pRow);
                }
            }
            }
            else {
                self::_MeetingTypesByUser();
            }
            $sql = "SELECT a.*,CONCAT(c.first_name,' ',c.last_name) as full_name
                    FROM meeting_standard_participants a
                    INNER JOIN users c on c.id = a.user_id
                    ORDER BY full_name
                    ";
            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_StandardParticipants[$row["meeting_type_id"]][] = $row;
        }
        
        public function getMeetingTypesIdsByUser() {
            $meeting_types = MeetingTypes::getMeetingTypesByUser();
            $ids = array();
            if($meeting_types) {
                foreach($meeting_types as $key => $val) {
                    $ids[] = $val->getId();
                }
            }
            return $ids;
        }
        public function getMeetingTypesByUser() {
            return self::$_MeetingTypes;
        }
        private function _MeetingTypesByUser() {
            if(!u()->isAdmin()) {
                $sql = "SELECT meeting_type_id FROM meeting_standard_participants WHERE user_id = ".u()->getId()." GROUP BY meeting_type_id";
                $rIds = db_get_all($sql);
                if($rIds) {
                    foreach($rIds as $row) {
                        $ids[] = $row["meeting_type_id"];
                    }
                    $and = "AND a.id = 1";
                    if(is_array($ids) && count($ids) > 0) {
                        $and = "AND a.id IN(1,".implode(",",$ids).")";
                    }
                    $sql = "
                            SELECT a.*
                            FROM meeting_types a
                            WHERE a.id > 0 $and
                            ORDER BY a.name
                        ";

                    $rows = db_get_all($sql);
                    if($rows) {
                        foreach($rows as $row) {
                            $sql = "SELECT a.*, CONCAT(c.first_name,' ',c.last_name) as full_name
                                FROM meeting_standard_participants a
                                INNER JOIN users c on c.id = a.user_id
                                WHERE a.meeting_type_id = '{$row["id"]}'
                                ORDER BY full_name
                            ";
                            $pRows = db_get_all($sql);
                            self::$_MeetingTypes[$row["id"]] = self::_Init($row);
                            foreach($pRows as $pRow) self::$_MeetingTypes[$row["id"]]->_participants[$pRow["id"]]=self::_InitParticipants($pRow);
                        }
                    }
                }
            }   
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_name = $array["name"];
            $object->_createDate = $array["create_date"];
            $object->_createdBy = $array["created_by"];
            $object->_updateDate = $array["update_date"];
            $object->_updatedBy = $array["updated_by"];

            return $object;
        }
       
        public function getParticipantsId() {
            return $this->_participantsId;
        }
        public function getUserId() {
            return $this->_userId;
        } 
        public function getEmail() {
            return $this->_email;
        }
        public function getFullName() {
            return $this->_fullName;
        }
        private static function _InitParticipants($array, $object = null) {
            if(!$object instanceof self) $object = new self;
            $object->_participantsId = $array["id"];
            $object->_userId = $array["user_id"];
            $object->_email = $array["email"];
            $object->_fullName = $array["full_name"];
            return $object;
        }
        public function deleteParticipants($id = 0) {
            if($this->_id) {
                $and = '';
                if($id > 0) $and = " AND id=$id LIMIT 1";
                $sql = "DELETE FROM meeting_standard_participants WHERE meeting_type_id='{$this->_id}' $and";
                return db_execute($sql);
            }
            return false;
        }
        public function saveParticipants($user_id,$email) {
            if($user_id > 0 && $email !='') {
                $sql = "INSERT INTO meeting_standard_participants (meeting_type_id, user_id, email) 
                        VALUES ('{$this->_id}', '$user_id','$email')";
                db_execute($sql);
                return true;
            }
            return false;
        }
    }

    MeetingTypes::Init();
