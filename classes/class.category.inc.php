<?php
    class Category {
        private static $_DefaultCategory = null;
        private static $_Category = null;
        private static $_TaskTypeGroups = null;

        private $_id = null;
        private $_groupId = null;
        private $_groupName = null;
        private $_name = null;
        private $_categoryType = null;

        public function __construct($id = null, $type = null){
            if(!is_null($id)){
                if($id == 1) $type = 'OTHER';
                $t = self::Get($id, $type);
                $this->_id = $t->getId();
                $this->_groupId = $t->getGroupId();
                $this->_groupName = $t->getGroupName();
                $this->_name = $t->getName();
                $this->_categoryType = $t->getCategoryType();
            }
        }

        public function getId(){ return $this->_id; }
        public function getGroupId(){ return $this->_groupId; }

        public function getGroupName($italicize_default = false){
            if($italicize_default && $this->_groupId < 1) return "<i>{$this->_groupName}</i>";
            else return $this->_groupName;
        }

        public function getName(){ 
            return (empty($this->_name) ? "Others" : $this->_name); 
        }
        public function getCategoryType() {
            return $this->_categoryType;
        }


        public function setGroupId($id){
            if(self::$_TaskTypeGroups[$id]){
                $this->_groupId = $id;
                $this->_groupName = self::$_TaskTypeGroups[$id];
            }
            else {
                $this->_groupId = 0;
            }
        }

        public function setName($name){
            $this->_name = trim($name);
        }
        public function setCategoryType($type) {
            $this->_categoryType = trim($type);
        }
        
        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function delete(){
            if($this->_id) {
                $sql_update = "UPDATE files set category_id = 1 WHERE category_id='{$this->_id}'";
                $sql = "DELETE FROM category WHERE id='{$this->_id}' LIMIT 1";
                if(db_execute($sql)){
                    db_execute($sql_update);
                    return true;
                }
            }
            return false;
        }
        public function checkName() {
            $and = "";
            /*if($this->_groupId) {
                $and = " AND group_id = {$this->_groupId}";
            }
            else {
                $and = " AND group_id IS NULL";
            }
            */
            if($this->_id) $and .= " AND id != {$this->_id}";
            $sql = "SELECT id FROM category
                    WHERE category_type =".q($this->_categoryType)." AND name = " . q($this->_name) . " $and
                    ";
            
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        private function _add(){
            if($this->_name){
                $sql = "
                    INSERT INTO category (group_id, name, category_type)
                    VALUES(
                        " . n($this->_groupId) . ",
                        " . q($this->_name) . ",
                        " . q($this->_categoryType) . "
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_name){
                $sql = "
                    UPDATE category SET
                    group_id = " . n($this->_groupId) . ",
                    name = " . q($this->_name) . ",
                    category_type = " . q($this->_categoryType) . "
                    WHERE id = {$this->_id}
                ";
                return db_execute($sql);
            }

            return false;
        }

        public static function Get($id,$type = 'PROJECT_DOCUMENT'){
            if(self::$_Category[$type][$id]) return self::$_Category[$type][$id];
            else return self::$_DefaultCategory;
        }

        public static function GetAll($type = null){
            if(!in_array($type, array('PROJECT_DOCUMENT','GENERAL_DOCUMENT'))) 
                return self::$_Category;
            else
                return self::$_Category[$type];
        }

        public static function GetGroups(){
            return self::$_TaskTypeGroups;
        }

        public static function Init(){
            self::$_DefaultCategory = self::_Init(array("id" => 0, "group_id" => 0, "group_name" => "", "name" => "Others"));

            $sql = "
                SELECT a.id, a.group_id, COALESCE(b.name, 'Ungrouped') AS group_name, a.name, a.category_type 
                FROM category a
                LEFT JOIN department b ON b.id = a.group_id
                WHERE a.id > 0
                ORDER BY b.name, a.name
            ";

            $rows = db_get_all($sql);
            $others = '';
            if($rows) {
                foreach($rows as $row) {
                    if($row['id'] == 1) {
                        $others = $row;
                        self::$_Category[$row['category_type']][$row["id"]] = self::_Init($row);
                    }
                    else {
                        self::$_Category[$row['category_type']][$row["id"]] = self::_Init($row);
                    }
                }
                if($others) {
                    self::$_Category['PROJECT_DOCUMENT'][$others["id"]] = self::_Init($others);
                    self::$_Category['GENERAL_DOCUMENT'][$others["id"]] = self::_Init($others);
                }
            }

            $sql = "
                SELECT id, name FROM department 
                ORDER BY name
                ";

            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_TaskTypeGroups[$row["id"]] = $row["name"];
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_groupId = $array["group_id"];
            $object->_groupName = $array["group_name"];
            $object->_name = $array["name"];
            $object->_categoryType = $array["category_type"];
            return $object;
        }
    }

    Category::Init();
