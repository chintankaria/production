<?php

class PtoTiers {

    private $_id = null;
    private $_pol_id = null;
    private $_loc_id = null;
    private $_time_period = null;
    private $_leaves = null;
    private $_max_service = null;
    private $_table = 'pto_tiers';
    

    public function __construct($id = null) {
        $id = pintval($id);
        if ($id > 0) {
            $sql = "SELECT * FROM {$this->_table} WHERE id={$id} order by id";
            $row = db_get_row($sql);
            if ($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null) {
        if (!$object instanceof self)
            $object = new self;

        $object->_id = $array["id"];
        $object->_pol_id = $array["pol_id"];
        $object->_loc_id = $array["loc_id"];
        $object->_time_period = $array["time_period"];
        $object->_leaves = $array["leaves"];
        $object->_max_service = $array["max_service"];
        return $object;
    }

    public function getId() {
        return $this->_id;
    }

    public function getLocId() {
        return $this->_loc_id;
    }
    public function getPolId() {
        return $this->_pol_id;
    }
    public function getTimePeriod() {
        return $this->_time_period;
    }

    public function getLeaves() {
        return $this->_leaves;
    }
    public function getMaxService() {
        return $this->_max_service;
    }

    public function setPolID($pol_id) {
        $this->_pol_id = $pol_id;
    }
     public function setLocation($location_id) {
        $this->_loc_id = $location_id;
    }
    public function setTimePeriod($time_period) {
        $this->_time_period = $time_period;
    }

    public function setLeaves($leaves) {
        $this->_leaves = $leaves;
    }

    public function setMaxService($max_service) {
        $this->_max_service = $max_service;
    }

    public function save() {

        if ($this->_id)
            return $this->_update();
        else
            return $this->_add();
    }

    public function delete() {
        if ($this->_id) {
            $sql = "DELETE FROM {$this->_table} WHERE id='{$this->_id}' LIMIT 1";

            //echo $sql;die; 

            if (db_execute($sql)) {
                return true;
            }
        }
        //return false;
    }
    private function _add() {


        $sql_pol = "SELECT id FROM pto_policies order by id desc limit 0,1";
         if($rs = db_get_row($sql_pol)){
            $pol_id = $rs['id'];
        }
        
        $sql = "INSERT INTO {$this->_table} (pol_id, loc_id,  time_period, leaves, max_service)
                VALUES(" . $pol_id . "," . q($this->_loc_id) . "," . q($this->_time_period) . ", " . q($this->_leaves) . "," . q($this->_max_service) . ")";

        //echo $sql;die;  

        if (db_execute($sql)) {
            $this->_id = db_insert_id();
            return true;
        }
        return false;
    }

     private function _update() {
        if ($this->_id) {
           $sql = "
                    UPDATE {$this->_table} SET                    
                    time_period = " . q($this->_time_period) . ",
                    leaves = " . q($this->_leaves) . ",
                    loc_id = " . q($this->_loc_id) . ",
                    max_service = " . q($this->_max_service) . "
                    WHERE id = {$this->_id}
                ";
            //echo $sql;die;  
            return db_execute($sql);
        }
        return false;
    }
    
    public static function checkTiers($id) {
          if ($id) {
            $sql = "SELECT * FROM pto_tiers WHERE pol_id = {$id}  ";
        } 
      // echo $sql;

        $rows = db_get_all($sql);
     
        return $rows;
        }
    
}

