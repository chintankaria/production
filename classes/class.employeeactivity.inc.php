<?php
    class EmployeeActivity {
        private $_id = null;
        private $_employeeId = null;
        private $_employeeName = null;
        private $_title = null;
        private $_description = null;
        private $_sticky = null;
        private $_priority = null;
        private $_startDate = null;
        private $_endDate = null;
        private $_active = null;
        private $_createdBy = null;
        private $_createdOn = null;
        private $_updatedBy = null;
        private $_updatedOn = null;

        private static $_Sql = "
            SELECT a.id, a.employee_id, a.sticky, a.priority, DATE_FORMAT(a.start_date, '%m/%d/%Y') AS start_date,
                DATE_FORMAT(a.end_date, '%m/%d/%Y') AS end_date, a.active, a.created_by, UNIX_TIMESTAMP(a.created_on) AS created_on, a.updated_by,
                UNIX_TIMESTAMP(a.updated_on) AS updated_on, b.title, b.description,
                CONCAT(d.first_name, ' ', d.last_name) AS employee_name
            FROM employee_activity a
            INNER JOIN activity b ON b.id = a.activity_id
            INNER JOIN employees c ON c.id = a.employee_id
            INNER JOIN users d ON d.id = c.user_id
        ";

        public function __construct($id = null, $employee_id = null, $created_by = null){
		 
            $id = pintval($id);
			
            if($id > 0){
                $sql = self::$_Sql . "
                    WHERE a.id = {$id}
                ";

                if(!is_null($employee_id)){
                    $employee_id = pintval($employee_id);
                    $sql .= "
                        AND a.employee_id = {$employee_id}
                    ";
                }

                if(!is_null($created_by)){
                    $created_by = pintval($created_by);
                    $sql .= "
                        AND a.created_by = {$created_by}
                    ";
                }

                $row = db_get_row($sql);
                if($row) self::_Init($row, $this);
            }
        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_employeeId = $array["employee_id"];
            $object->_employeeName = $array["employee_name"];
            $object->_title = $array["title"];
            $object->_description = $array["description"];
            $object->_sticky = $array["sticky"];
            $object->_priority = $array["priority"];
            $object->_startDate = $array["start_date"];
            $object->_endDate = $array["end_date"];
            $object->_active = $array["active"];
            $object->_createdBy = $array["created_by"];
            $object->_createdOn = $array["created_on"];
            $object->_updatedBy = $array["updated_by"];
            $object->_updatedOn = $array["updated_on"];
            return $object;
        }

        public function getId(){ return $this->_id; }
        public function getEmployeeId(){ return $this->_employeeId; }
        public function getEmployeeName(){ return $this->_employeeName; }
        public function getTitle(){ return $this->_title; }
        public function getDescription(){ return $this->_description; }
        public function getSticky(){ return $this->_sticky; }
        public function getPriority(){ return $this->_priority; }
        public function getStartDate(){ return $this->_startDate; }
        public function getEndDate(){ return $this->_endDate; }
        public function getIsActive(){ return $this->_active; }
        public function getCreatedBy(){ return $this->_createdBy; }
        public function getCreatedOn(){ return $this->_createdOn; }

        public function setEmployeeId($employeeId){
            $this->_employeeId = intval($employeeId);
        }
        public function setTitle($title) {
            $this->_title = trim($title);
        }
        public function setDescription($description) {
            $this->_description = trim($description);
        }
        public function setSticky($sticky) {
            $this->_sticky = (trim($sticky) == '' ? 0 : 1);
        }
        public function setPriority($priority) {
            $this->_priority = (trim($priority) == '' ? 0 : 1);
        }
        public function setStartDate($startDate) {
            $this->_startDate = trim($startDate);
        }
        public function setEndDate($endDate) {
            $this->_endDate = trim($endDate);
        }

        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        private function _add(){
            if($this->_title){
                $sql = "
                    INSERT INTO activity (title, description, start_date, end_date, created_by, created_on)
                    VALUES (" . q($this->_title) . ", " . q($this->_description) . ", " . q(ymdhms(strtotime($this->_startDate))) . ", 
                            " . q(ymdhms(strtotime($this->_endDate))) . ", " . u()->getID() . ", NOW())
                    ";
                if(db_execute($sql)){
                    $activityId = db_insert_id();

                    $sql = "INSERT INTO employee_activity (employee_id, activity_id, sticky, priority, start_date, end_date,
                                                           created_by, created_on, updated_by, updated_on)
                            VALUES (" . q($this->_employeeId) . ", " . $activityId . ", " . $this->_sticky . ", " . $this->_priority . ",
                                    " . q(ymdhms(strtotime($this->_startDate))) . ", " . q(ymdhms(strtotime($this->_endDate))) . ",
                                    " . u()->getID() . ", NOW(), " . u()->getID() . ", NOW())";
                    if(db_execute($sql)){
                        $employeeActivityId = db_insert_id();

                        $sql = "INSERT INTO employee_activity_history (employee_activity_id, start_date, end_date,
                                                                       created_by, created_on)
                                VALUES (" . $employeeActivityId . ",
                                        " . q(ymdhms(strtotime($this->_startDate))) . ", " . q(ymdhms(strtotime($this->_endDate))) . ",
                                        " . u()->getID() . ", NOW())";
                        if(db_execute($sql)){
                            $this->_adjustActivities($this->_employeeId);
                            return $this->_title;
                        }
                    }
                }
            }

            return false;
        }

        private function _adjustActivities($employee_id)
        {
            $sql = "
                SELECT a.title, b.id, b.employee_id, b.activity_id, b.sticky, b.priority, b.start_date, b.end_date
                FROM `activity` a, `employee_activity` b
                WHERE
                    a.id = b.activity_id AND
                    b.employee_id = $employee_id AND
                    NOT b.sticky AND
                    b.active AND
                    b.end_date >= NOW()
                    ORDER BY b.start_date
                ";
            $rows = db_get_all($sql);
            $i = 0;

            $previousEmployeeActivityId = 0;
            $previousStartDate = '';
            $previousEndDate = '';
            $startDate = '';
            $endDate = '';

            foreach($rows as $row)
            {
                if($i == 0)
                {
                    $previousStartDate = strtotime($row["start_date"]);
                    $previousEndDate = strtotime($row["end_date"]);
                    $previousEmployeeActivityId = $row["id"];
                    $i++;
                    continue;
                }
                $startDate = strtotime($row["start_date"]);
                $endDate = strtotime($row["end_date"]);
                $currentEmployeeActivityId = $row["id"];
                if($startDate > $previousStartDate && $startDate < $previousEndDate)
                {
                    if($row["priority"] == '1')
                    {
                        $arr = $this->_adjustActivityDates($employee_id, $currentEmployeeActivityId, $startDate, $endDate, $previousEmployeeActivityId, $previousStartDate, $previousEndDate, "Expand");
                        $startDate = $arr['currentStartDate'];
                        $endDate = $arr['currentEndDate'];
                    }
                    else
                    {
                        $arr = $this->_adjustActivityDates($employee_id, $currentEmployeeActivityId, $startDate, $endDate, $previousEmployeeActivityId, $previousStartDate, $previousEndDate, "Increment");
                        $startDate = $arr['currentStartDate'];
                        $endDate = $arr['currentEndDate'];
                    }
                }
                elseif($startDate <= $previousEndDate)
                {
                    $arr = $this->_adjustActivityDates($employee_id, $currentEmployeeActivityId, $startDate, $endDate, $previousEmployeeActivityId, $previousStartDate, $previousEndDate, "Increment");
//                        $startDate = $arr['currentStartDate'];
//                        $endDate = $arr['currentEndDate'];
                    $startDate = '';
                    $endDate = '';
                }
                $previousStartDate = ($startDate != '') ? $startDate : strtotime($row["start_date"]);
                $previousEndDate = ($endDate != '') ? $endDate : strtotime($row["end_date"]);
                $previousEmployeeActivityId = $row["id"];
            }
        }

        private function _adjustActivityDates($employee_id, $current_employee_activity_id, $current_start_date, $current_end_date, $previous_employee_activity_id, $previous_start_date, $previous_end_date, $action)
        {
            if($action == "Increment")
            {
                $sql = "
                    SELECT a.title, b.id, b.employee_id, b.activity_id, b.sticky, b.priority, b.start_date, b.end_date
                    FROM `activity` a, `employee_activity` b
                    WHERE
                        a.id = b.activity_id AND
                        b.employee_id = $employee_id AND
                        NOT b.sticky AND
                        NOT b.priority AND
                        b.active AND
                        b.end_date > '" . date('Y-m-d', $current_start_date) . "'
                        ORDER BY b.end_date ASC LIMIT 1
                    ";
                $rows = db_get_row($sql);

                if($row)
                    $endDate = strtotime($row['end_date']);
                else
                    $endDate = $previous_end_date;

                $currentStartDate = $endDate + 86400;
                $currentEndDate = $currentStartDate + ($current_end_date - $current_start_date);

                // Adding the new values to history table.
                $sql = "SELECT * FROM employee_activity_history WHERE employee_activity_id = $current_employee_activity_id ORDER BY id DESC LIMIT 1";
                $row = db_get_row($sql);
                if($row)
                {
                    $sql = "INSERT INTO employee_activity_history (employee_activity_id, start_date, end_date,
                                                                   created_by, created_on)
                            VALUES (" . $current_employee_activity_id . ",
                                    '" . date('Y-m-d', $currentStartDate) . "', '" . date('Y-m-d', $currentEndDate) . "',
                                    " . u()->getID() . ", NOW())";
                    if(db_execute($sql)){
                        $employeeActivityHistoryId = db_insert_id();
                    }
                }

                // Updading employee_activity table with new values
                $sql = "UPDATE employee_activity SET start_date = '" . date('Y-m-d', $currentStartDate) . "', end_date = '" . date('Y-m-d', $currentEndDate) . "', updated_by = " . u()->getID() . ", updated_on = NOW() WHERE id = '$current_employee_activity_id'";
                db_execute($sql);

                return array('currentStartDate' => $currentStartDate, 'currentEndDate' => $currentEndDate);
            }
            elseif($action == "Expand")
            {
                $previousEndDate = $current_end_date + (($previous_end_date - $current_start_date) + 86400); // 86400 = seconds in a day.

                // Adding the new values to history table.
                $sql = "SELECT * FROM employee_activity_history WHERE employee_activity_id = $previous_employee_activity_id ORDER BY id DESC LIMIT 1";
                $row = db_get_row($sql);
                if($row)
                {
                    $sql = "INSERT INTO employee_activity_history (employee_activity_id, start_date, end_date,
                                                                   created_by, created_on)
                            VALUES (" . $previous_employee_activity_id . ",
                                    '" . $row['start_date'] . "', '" . date('Y-m-d', $previousEndDate) . "',
                                    " . u()->getID() . ", NOW())";
                    if(db_execute($sql)){
                        $employeeActivityHistoryId = db_insert_id();
                    }
                }

                // Updading employee_activity table with new values
                $sql = "UPDATE employee_activity SET end_date = '" . date('Y-m-d', $previousEndDate) . "', updated_by = " . u()->getID() . ", updated_on = NOW() WHERE id = '$previous_employee_activity_id'";
                db_execute($sql);

                return array('currentStartDate' => $current_start_date, 'currentEndDate' => $current_end_date);
            }
        }

        public static function GetEmployees($employee_id = null){
            $sql = "SELECT CONCAT(a.first_name, ' ', a.last_name) as name, b.id FROM users a, employees b
                    WHERE a.id = b.user_id AND NOT a.blocked";

            if($employee_id) $sql .= " AND b.id = " . q($employee_id);

            $rows = db_get_all($sql . ' ORDER BY first_name');
            if($rows) return $rows;
        }

        public static function GetEmployeeActivities($employee_id = 0){
            $return = array();

            if($employee_id) {
                $and .= " AND a.employee_id = {$employee_id} ";
            }

            $sql = self::$_Sql . "
                WHERE a.id > 0
                {$and}
                ORDER BY start_date
            ";
            $rows = db_get_all($sql);
            foreach($rows as $row) $return[$row["id"]] = self::_Init($row);
            return $return;
        }

        public static function GetEmployeeActivityHistory($employee_activity_id)
        {
            if($employee_activity_id)
            {
                $sql = "SELECT DATE_FORMAT(start_date, '%m/%d/%Y') AS start_date, DATE_FORMAT(end_date, '%m/%d/%Y') AS end_date,
                            DATE_FORMAT(created_on, '%m/%d/%Y %H:%i:%s') AS created_on
                        FROM employee_activity_history
                        WHERE employee_activity_id = $employee_activity_id ORDER by id";

                $rows = db_get_all($sql);

                if($rows) return $rows;
            }
        }

        public static function RealignActivities($employee_id, $ids)
        {
            foreach($ids as $k=>$id)
            {
                $sql = "
                    SELECT id, start_date, end_date, sticky
                    FROM `employee_activity`
                    WHERE
                        id = $id AND
                        employee_id = $employee_id AND
                        start_date > NOW() AND
                        active
                    ";
                $row1 = db_get_row($sql);

                if($row1['sticky'])
                {
                    $sql = "SELECT start_date FROM employee_activity WHERE id = " . $ids[$k+1];
                    $rw = db_get_row($sql);
                    if($rw)
                    {
                        $ed_dt = strtotime($rw['start_date']) + (strtotime($row1['end_date']) - strtotime($row1['start_date']));
                        $sql = "UPDATE employee_activity set start_date = '" . $rw['start_date'] . "', end_date = '" . date('Y-m-d', $ed_dt) . "' WHERE id = $id";
                        db_execute($sql);
                    }
                }
                elseif($row1)
                {
                    $sql = "SELECT id, start_date, end_date FROM employee_activity WHERE start_date > NOW() AND ('" . $row1['start_date'] . "' BETWEEN start_date AND end_date) AND NOT priority AND NOT sticky AND id != $id AND employee_id = $employee_id ORDER BY start_date";
                    $row2 = db_get_all($sql);
                    $i = 0;
                    $st_dt = '';
                    foreach($row2 as $row)
                    {
                        if($i == 0)
                        {
                            $st_dt = $row['start_date']; $i++;
                        }

                        $start_date = strtotime($row1['end_date']) + 86400;
                        $end_date = $start_date + (strtotime($row['end_date']) - strtotime($row['start_date']));
                        $sql = "UPDATE employee_activity SET start_date = '" . date('Y-m-d', $start_date) . "', end_date = '" . date('Y-m-d', $end_date) . "' WHERE id = " . $row['id'];
                        db_execute($sql);

                    }

                    $sql = "SELECT id, start_date, end_date FROM employee_activity WHERE start_date > NOW() AND ('" . $row1['end_date'] . "' BETWEEN start_date AND end_date) AND NOT priority AND NOT sticky AND employee_id = $employee_id ORDER BY start_date";
                    $row2 = db_get_all($sql);
                    $i = 0;
                    foreach($row2 as $row)
                    {
                        if($i == 0 && $st_dt == '')
                        {
                            $st_dt = $row['start_date']; $i++;
                        }

                        $start_date = strtotime($row1['end_date']) + (strtotime($row1['end_date']) - strtotime($row['start_date'])) + 86400;
                        $end_date = $start_date + (strtotime($row['start_date']) - strtotime($row['end_date']));
                        $sql = "UPDATE employee_activity SET start_date = '" . date('Y-m-d', $start_date) . "', end_date = '" . date('Y-m-d', $end_date) . "' WHERE id = " . $row['id'];
                        db_execute($sql);

                    }

                    if($st_dt == '') $st_dt = $row1['start_date'];
                    $ed_dt = strtotime($st_dt) + (strtotime($row1['end_date']) - strtotime($row1['start_date']));
                    $sql = "UPDATE employee_activity SET start_date = '" . $st_dt . "', end_date = '" . date('Y-m-d', $ed_dt) . "' WHERE id = $id AND employee_id = $employee_id";
                    db_execute($sql);

                }
            }
        }

    }

