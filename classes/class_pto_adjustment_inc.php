<?php

class PtoAdjustment {

    private $_id = null;
    private $_user_id = null;
    private $_adjusted_hours = null;
    private $_adjusted_date = null;
    private $_reason = null;
    private $_table = 'pto_adjustment';
    

    public function __construct($id = null) {
        $id = pintval($id);
        if ($id > 0) {
            $sql = "SELECT * FROM {$this->_table} WHERE id={$id} order by id";
            $row = db_get_row($sql);
            if ($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null) {
        if (!$object instanceof self)
            $object = new self;

        $object->_id = $array["id"];
        $object->_user_id = $array["user_id"];
        $object->_adjusted_hours = $array["adjusted_hours"];
        $object->_adjusted_date = $array["adjusted_date"];
        $object->_reason = $array["reason"];
        return $object;
    }

    public function getId() {
        return $this->_id;
    }

    public function getUserID() {
        return $this->_user_id;
    }
    public function getAdjustedHours() {
        return $this->_adjusted_hours;
    }
    public function getAdjustedDate() {
        return $this->_adjusted_date;
    }

    public function getReason() {
        return $this->_reason;
    }
 
    public function setUserID($user_id) {
        $this->_user_id = trim($user_id);
    }
    public function setAdjustedHours($adjusted_hours) {
        $this->_adjusted_hours = $adjusted_hours;
    }
    public function setAdjustedDate($adjusted_date) {
        $this->_adjusted_date = format_date($adjusted_date."-01","Y-m-d");
    }

    public function setReason($reason) {
        $this->_reason = $reason;
    }

    public function saveAdjustment() {

         if($this->_id) 
            return $this->_update();
         else
            return $this->_add();
    }

    
    private function _add() {
        
            $sql = "INSERT INTO {$this->_table} (user_id, adjusted_hours,  adjusted_date, reason)
                    VALUES(" . q($this->_user_id) . "," . q($this->_adjusted_hours) . "," . q($this->_adjusted_date) . ", " . q($this->_reason) . ")";
            
            if (db_execute($sql)) {
                $this->_id = db_insert_id();
                return true;
            }
        
    }
    private function _update() {

     if($this->_id) {

           $sql = "UPDATE {$this->_table} SET
                        user_id = '" . $this->_user_id . "',
                        adjusted_hours = '" . $this->_adjusted_hours . "',
                        adjusted_date = '" . $this->_adjusted_date . "',
                        reason = '" . $this->_reason . "'
                    WHERE id = {$this->_id}
                    ";

            if(db_execute($sql)) 
                return true;
            
        }
    }
    
}

