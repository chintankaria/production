<?php
    class SMTComments {
        private $_id = null;
        private $_smtId = null;
        private $_comment = null;
        private $_userId = null;
        private $_dateTime = null;
        private $_createDate = null;
        private $_createdBy = null;
        private $_deleteDate = null;
        private $_deletedBy = null;

        private static $_Comments = null;
        
        public function __construct ($id = null) {
            if($id > 0) {
                $sql = "SELECT a.id, a.comment, a.date_time, a.smtaction_id, a.create_date, a.created_by, a.deleted, a.deleted_date, a.deleted_by,
                            CONCAT(b.first_name,' ',b.last_name) as createdBy,
                            CONCAT(c.first_name,' ',c.last_name) as deletedBy
                        FROM smtaction_comments a
                        INNER JOIN users b on b.email = a.created_by
                        LEFT JOIN users c on c.email = a.deleted_by
                        WHERE a.id = {$id}
                        ";
                $row = db_get_row($sql);
                if($row) self::_Init($row, $this);
            }
        }
        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;

            $object->_id = $array["id"];
            $object->_smtId = $array["smtaction_id"];
            $object->_comment = $array["comment"];
            $object->_userId = $array["user_id"];
            $object->_dateTime = $array["date_time"];
            $object->_createDate = $array["create_date"];
            $object->_createdBy = $array["createdBy"];
            $object->_deleteDate = $array["deleted_date"];
            $object->_deletedBy = $array["deletedBy"];
            return $object;
        }

        public function getId() {
            return $this->_id;
        }

        public function getComment($length = 0){
            if ($length > 0) {
                $ret = substr($this->_comment, 0, $length);
                if (strlen($this->_comment) > $length)
                    $ret .= " ...";
            } else {
                $ret = nl2br($this->_comment);
            }

            return $ret;
        }
        public function setComment($comment){
            $this->_comment = trim($comment);
        }

        public function getSmtId() {
            return $this->_smtId;
        }

        public function setSmtId($id) {
            if($id)
                $this->_smtId = $id;
        }
        public function getDateTime() {
            return mdy(strtotime($this->_dateTime));
        }
        public function getUserId() {
            return $this->_userId;
        }
        
        public function getCreateDate() {
            return date("d M, Y h:i a", strtotime($this->_createDate));
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getDeleteDate() {
            return date("m/d/Y", strtotime($this->_deleteDate));
        }
        public function getDeletedBy() {
            return $this->_deletedBy;
        }

        public function save() {
            return $this->_add();
        }
        private function _add(){
            if (is_Null($this->_id) || $this->_id == 0) {
                $sql = "
                INSERT INTO smtaction_comments (
                        date_time, user_id, smtaction_id, comment, create_date, created_by)
                VALUES (
                    NOW(),
                    ".u()->getId().",
                    ". $this->_smtId .",
                    " . q($this->_comment) . ",
                    NOW(),
                    " . q(u()->getEmail()) . "
                ) ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            } 
            return false;
        }
        public function Delete($id) {
            if($id) {
                $sql = "UPDATE smtaction_comments SET
                            deleted = 1,
                            deleted_date = NOW(),
                            deleted_by = '".u()->getEmail()."'
                            WHERE id = {$id}
                        ";
                return db_execute($sql);
            }
            return false;
        }
        
        public static function GetAllComments($order='desc') {
            $sql = "SELECT a.id, a.comment, a.date_time, a.smtaction_id, a.create_date, a.created_by, a.deleted, a.deleted_date, a.deleted_by,
                        CONCAT(b.first_name,' ',b.last_name) as createdBy,
                        CONCAT(c.first_name,' ',c.last_name) as deletedBy
                    FROM smtaction_comments a
                    INNER JOIN users b on b.email = a.created_by
                    LEFT JOIN users c on c.email = a.deleted_by
                    ORDER BY a.create_date $order
                    ";
            $pRows = db_get_all($sql);
            foreach($pRows as $pRow) {
                self::$_Comments[$pRow["id"]] = self::_Init($pRow);
            }
            return self::$_Comments;
        }
        public static function GetComments($id,$order='desc') {
            $return = array();
            if($id > 0) { 
                $sql = "SELECT a.id, a.comment, a.date_time, a.smtaction_id, a.create_date, a.created_by, a.deleted, a.deleted_date, a.deleted_by,
                            CONCAT(b.first_name,' ',b.last_name) as createdBy,
                            CONCAT(c.first_name,' ',c.last_name) as deletedBy
                        FROM smtaction_comments a
                        INNER JOIN users b on b.email = a.created_by
                        LEFT JOIN users c on c.email = a.deleted_by
                        WHERE a.deleted =0 AND a.smtaction_id = $id
                        ORDER BY a.create_date $order
                        ";
                $pRows = db_get_all($sql);
                foreach($pRows as $pRow) {
                    self::$_Comments[$pRow["id"]] = self::_Init($pRow);
                    $return[$pRow["id"]] = self::_Init($pRow);
                }
            }
            return $return;
        }
    }
?>
