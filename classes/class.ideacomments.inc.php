<?php
class IdeaComments {
    private static $_Comments = null;

    private $_id = null;
    private $_parentId = null;
    private $_ideaId = null;
    private $_userId = null;
    private $_dateTime = null;
    private $_title = null;
    private $_comment = null;
    private $_rating = null;
    private $_createDate = null;
    private $_createdBy = null;
    private $_deleted = null;
    private $_deleteDate = null;
    private $_deletedBy = null;

    private $_childrens = null;
    private $_replies = null;
    private $_owner = null;
    private $_ownerPic = null;

    public function __construct($id = null) {
        $id = pintval($id);
        if($id > 0) {
            $sql = "SELECT a.*
                      CONCAT(b.first_name,' ',b.last_name) as owner,b.gender
                    FROM idea_tracker_comments a
                    INNER JOIN users b ON b.id = a.user_id
                    WHERE a.id = $id
                    ";
            $row = db_get_row($sql);
            if($row) self::_Init($row, $this);
        }
    }
    private static function _Init($array, $object = null){
        if(!$object instanceof self) $object = new self;

        $object->_id = $array["id"];
        $object->_parentId = $array["parent_id"];
        $object->_ideaId = $array["idea_tracker_id"];
        $object->_userId = $array["user_id"];
        $object->_dateTime = $array["date_time"];
        $object->_title = $array["title"];
        $object->_comment = $array["comment"];
        $object->_rating = $array["rating"]; 
        $object->_createDate = $array["create_date"];
        $object->_createdBy = $array["created_by"];
        $object->_deleted = $array["deleted"];
        $object->_deleteDate = $array["deleted_date"];
        $object->_deletedBy = $array["deleted_by"];

        $object->_owner = $array["owner"];
        $sql = "SELECT * FROM users_pic WHERE user_id = {$array["user_id"]}";
        $result = db_get_row($sql);
        if($result) {
            $pic = "img/u-{$array["id"]}/".$result["md5_name"];
            $pic = $result["md5_name"];
        }
        else {
            $pic = strtolower($array["gender"]).".jpeg";
        }
        $object->_ownerPic = $pic; 


        return $object;
    }
    public function getId() {
        return $this->_id;
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getOwner() {
        return $this->_owner;
    }
    public function getOwnerPic() {
        return $this->_ownerPic;
    }
    public function getDateTime() {
        return date("m/d/Y H:i", strtotime($this->_dateTime));
    }
    public function getTitle() {
        return $this->_title;
    }
    public function getComment() {
        return nl2br($this->_comment);
    }
    public function getCreateDate() {
        return date("m/d/Y H:i", strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }
    public function getDeleted() {
        return $this->_deleted;
    }
    public function getDeletedDate() {
        return date("m/d/Y H:i", strtotime($this->_deleteDate));
    }
    public function getDeletedBy() {
        return $this->_deletedBy;
    }

    public function setIdeaId($idea_id) {
        $this->_ideaId = $idea_id;
    }
    public function setParentId($parent_id) {
        $this->_parentId = $parent_id;
    }
    public function setUserId($user_id) {
        $this->_userId = $user_id;
    }
    public function setDateTime($date_time) {
        $this->_dateTime = ymdhms(is_date($date_time));
    }
    public function setTitle($title) {
        $this->_title = $title;
    }
    public function setComment($comment) {
        $this->_comment = $comment;
    }
    public function setCreatedBy($created_by){
        $this->_createdBy = trim($created_by);
        if(!$this->_createdBy) e("Missing or invalid idea creator.");
    }
    public function setDeletedBy($deleted_by){
        $this->_deletedBy = trim($deleted_by);
        if(!$this->_deletedBy) e("Missing or invalid idea deletor.");
    }
    public function save(){
        #if($this->_id) return $this->_update();
        #else return $this->_add();
        return $this->_add();
    }

    private function _add() {
        if($this->_title){
            $sql = "INSERT INTO idea_tracker_comments (user_id,idea_tracker_id, parent_id, date_time, title, comment, create_date, created_by)
                        VALUES(
                            '". n($this->_userId) ."',
                            '". $this->_ideaId ."',
                            '". $this->_parentId ."',
                            '". $this->_dateTime ."',
                            ". q($this->_title) . ",
                            ". q($this->_comment) . ",
                            NOW(),
                            '". $this->_createdBy ."'
                        )
                    ";
            if(db_execute($sql)){
                $this->_id = db_insert_id();
                return true;
            }
        }
        return false;
    }
    private function _update() {
        if($this->_id) {
            $sql = "UPDATE idea_tracker_comments SET
                    WHERE id = $this->_id
                    ";
            return db_execute($sql);
        }
        return false;
    }

    public function Delete() {
        if($this->_id) {
            $sql = "UPDATE idea_tracker_comments SET
                        deleted = 1,
                        deleted_date = NOW(),
                        deleted_by = '".u()->getEmail()."'
                        WHERE id = {$this->_id}
                    ";
            return db_execute($sql);
        }
        return false;
    }
    
    public function getReplies() {
        $this->_loadReplies;
        return $this->_replies;
    }
    public function getChildrens() {
        $this->_loadChildrens();
        return $this->_childrens;
    }
    private function _loadReplies() {
        if(is_null($this->_replies)) {
            $sql = "SELECT a.*,
                        CONCAT(b.first_name,' ',b.last_name) as owner,a.gender
                    FROM idea_tracker_comments a
                    INNER JOIN users b ON b.id = a.user_id
                    WHERE a.parent_id > 0 AND a.parent_id = {$this->_id}
                    ORDER BY a.date_time ASC
                    ";
            $rRows = db_get_all($sql);
            if($rRows) {
                foreach($rRows as $rRow) {
                    $this->_replies[$rRow["id"]] = self::_Init($rRow);
                }
            }
        }
    }
    private function _loadChildrens() {
        if(is_null($this->_childrens)) {
            $sql = "SELECT a.*,
                            CONCAT(b.first_name,' ',b.last_name) as owner,b.gender
                        FROM idea_tracker_comments a
                        INNER JOIN users b ON b.id = a.user_id
                        WHERE a.parent_id > 0 AND a.parent_id = {$this->_id}
                        ORDER BY a.date_time ASC
                        ";
            $cRows = db_get_all($sql);
            if($cRows) {
                foreach($cRows as $cRow) {
                    $this->_childrens[$cRow["id"]] = self::_Init($cRow);
                    $sql = "SELECT a.*,
                                    CONCAT(b.first_name,' ',b.last_name) as owner,b.gender
                                FROM idea_tracker_comments a
                                INNER JOIN users b ON b.id = a.user_id
                                WHERE a.parent_id > 0 AND a.parent_id = {$cRow["id"]}
                                ORDER BY a.date_time ASC
                                ";
                        $rRows = db_get_all($sql);
                        if($rRows) {
                            foreach($rRows as $rRow) { 
                                $this->_childrens[$cRow["id"]]->_replies[$rRow["id"]] = self::_Init($rRow);
                            }
                        }
                }
            }
        }
    } 
    public function GetCommentList($idea_id = 0) {
        if($idea_id > 0) {
            $sql = "SELECT a.*,
                        CONCAT(b.first_name,' ',b.last_name) as owner, b.gender
                    FROM idea_tracker_comments a
                    INNER JOIN users b ON b.id = a.user_id
                    WHERE a.parent_id = 0 AND a.idea_tracker_id = $idea_id
                    ORDER BY a.date_time ASC
                    ";
            $rows = db_get_all($sql);
            foreach($rows as $row){
                self::$_Comments[$row["id"]] = self::_Init($row);
                $sql = "SELECT a.*,
                            CONCAT(b.first_name,' ',b.last_name) as owner, b.gender
                        FROM idea_tracker_comments a
                        INNER JOIN users b ON b.id = a.user_id
                        WHERE a.parent_id > 0 AND a.parent_id = {$row["id"]}
                        ORDER BY a.date_time ASC
                        ";
                $cRows = db_get_all($sql);
                if($cRows) {
                    foreach($cRows as $cRow) {  
                        self::$_Comments[$row["id"]]->_childrens[$cRow["id"]] = self::_Init($cRow);
                        $sql = "SELECT a.*,
                                    CONCAT(b.first_name,' ',b.last_name) as owner, b.gender
                                FROM idea_tracker_comments a
                                INNER JOIN users b ON b.id = a.user_id
                                WHERE a.parent_id > 0 AND a.parent_id = {$cRow["id"]}
                                ORDER BY a.date_time ASC
                                ";
                        $rRows = db_get_all($sql);
                        if($rRows) {
                            foreach($rRows as $rRow) {  
                                self::$_Comments[$row["id"]]->_childrens[$cRow["id"]]->_replies[$rRow["id"]] = self::_Init($rRow);
                            }
                        }

                    }
                }
            }
            return self::$_Comments;
        }
    }
    public function getTotalComments($idea_id) {
        $total = 0;
        if($idea_id > 0) {
            $sql = "SELECT count(id) as total FROM idea_tracker_comments WHERE idea_tracker_id = $idea_id";
            $row = db_get_row($sql);
            if($row) 
                $total = $row["total"];
        }
        return $total;
    } 

}
?>
