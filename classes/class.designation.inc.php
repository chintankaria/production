<?php
    class Designation {
        private static $_Default = null;
        private static $_Designation = null;

        private $_id = null;
        private $_name = null;
        private $_description = null;
        private $_createdBy = null;
        private $_createDate = null;

        public function __construct($id = null){
            if(!is_null($id)){
                $t = self::Get($id);
                $this->_id = $t->getId();
                $this->_name = $t->getName();
            }
        }

        public function getId() { return $this->_id; }
        public function getName(){ 
            return (empty($this->_name) ? "Others" : $this->_name); 
        }
        public function getDescription() {
            return $this->_description;
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getCreateDate(){
            return $this->_createDate;
        }

        public function setName($name){
            $this->_name = trim($name);
        }
        public function setDescription($description) {
            $this->_description = $description;
        }
        
        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function delete(){
            if($this->_id) {
                $sql_update = "UPDATE employees set designation_id = 1 WHERE designation_id='{$this->_id}'";
                $sql = "DELETE FROM designation WHERE id='{$this->_id}' LIMIT 1";
                if(db_execute($sql)){
                    db_execute($sql_update);
                    return true;
                }
            }
            return false;
        }
        public function checkName() {
            $and = '';
            if($this->_id) $and = " AND id != {$this->_id}";
            $sql = "SELECT id FROM designation
                    WHERE name = " . q($this->_name) . " $and
                    ";
            
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        private function _add(){
            if($this->_name){
                $sql = "
                    INSERT INTO designation ( name, description, created_by, create_date)
                    VALUES(
                        " . q($this->_name) . ",
                        " . q($this->_description) . ",
                        '" . u()->getEmail() ."',
                        NOW()
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_name){
                $sql = "
                    UPDATE designation SET
                    name = " . q($this->_name) . ",
                    description = " . q($this->_description) . "
                    WHERE id = {$this->_id}
                ";
                return db_execute($sql);
            }

            return false;
        }

        public static function Get($id){
            if(self::$_Designation[$id]) return self::$_Designation[$id];
            else return self::$_Default;
        }

        public static function GetAll(){
            return self::$_Designation;
        }

        public static function Init(){
            self::$_Default= self::_Init(array("id" => 0, "name" => "Others"));

            $sql = "
                SELECT a.id, a.name, a.description,a.created_by, a.create_date
                FROM designation a
                WHERE a.id > 0
                ORDER BY a.name
            ";

            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_Designation[$row["id"]] = self::_Init($row);

        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_name = $array["name"];
            $object->_description = $array["description"];
            $object->_createdBy = $array["created_by"];
            $object->_createDate = $array["create_date"];

            return $object;
        }
    }

    Designation::Init();
