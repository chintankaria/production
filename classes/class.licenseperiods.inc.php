<?php
    class LicensePeriods {
        private static $_Default = null;
        private static $_LicensePeriods = null;

        private $_id = null;
        private $_duration = null;
        private $_daymonthyear = null;
        private $_createdBy = null;
        private $_createDate = null;

        public function __construct($id = null){
            if(!is_null($id)){
                $t = self::Get($id);
                $this->_id = $t->getId();
                $this->_duration = $t->getDuration();
                $this->_daymonthyear = $t->getDaymonthyear();
            }
        }

        public function getId() { return $this->_id; }
        public function getDuration(){ 
            return (empty($this->_duration) ? "30" : $this->_duration); 
        }
        public function getDaymonthyear() {
            return (empty($this->_daymonthyear) ? "DAY" : $this->_daymonthyear); 
        }
        public function getCreatedBy() {
            return $this->_createdBy;
        }
        public function getCreateDate(){
            return $this->_createDate;
        }

        public function setDuration($duration){
            $this->_duration = trim($duration);
        }
        public function setDaymonthyear($daymonthyear) {
            $this->_daymonthyear = $daymonthyear;
        }
        
        public function save(){
            if($this->_id) return $this->_update();
            else return $this->_add();
        }

        public function delete(){
            if($this->_id) {
                $sql = "DELETE FROM license_periods WHERE id='{$this->_id}' LIMIT 1";
                if(db_execute($sql)){
                    return true;
                }
            }
            return false;
        }

        public function checkDuration() {
            $and = '';
            if($this->_id) $and = " AND id != {$this->_id}";
            $sql = "SELECT id FROM license_periods
                    WHERE duration = " . q($this->_duration) . " AND daymonthyear = " . q($this->_daymonthyear) . " $and
                    ";
            
            if($rs = db_get_row($sql)){
                return $rs['id'];
            }
            return false;
        }

        private function _add(){
            if($this->_duration){
                $sql = "
                    INSERT INTO license_periods ( duration, daymonthyear, created_by, create_date)
                    VALUES(
                        " . q($this->_duration) . ",
                        " . q($this->_daymonthyear) . ",
                        '" . u()->getEmail() ."',
                        NOW()
                    )
                ";
                if(db_execute($sql)){
                    $this->_id = db_insert_id();
                    return true;
                }
            }

            return false;
        }

        private function _update(){
            if($this->_duration){
                $sql = "
                    UPDATE license_periods SET
                    duration = " . q($this->_duration) . ",
                    daymonthyear = " . q($this->_daymonthyear) . "
                    WHERE id = {$this->_id}
                ";
                return db_execute($sql);
            }

            return false;
        }

        public static function Get($id){
            if(self::$_LicensePeriods[$id]) return self::$_LicensePeriods[$id];
            else return self::$_Default;
        }

        public static function GetAll(){
            return self::$_LicensePeriods;
        }

        public static function Init(){
            self::$_Default = self::_Init(array("id" => 0, "duration" => "30", "daymonthyear" => "DAY"));

            $sql = "
                SELECT a.id, a.duration, a.daymonthyear, a.created_by, a.create_date
                FROM license_periods a
                ORDER BY daymonthyear, duration
            ";

            $rows = db_get_all($sql);
            if($rows) foreach($rows as $row) self::$_LicensePeriods[$row["id"]] = self::_Init($row);

        }

        private static function _Init($array, $object = null){
            if(!$object instanceof self) $object = new self;
            $object->_id = $array["id"];
            $object->_duration = $array["duration"];
            $object->_daymonthyear = $array["daymonthyear"];
            $object->_createdBy = $array["created_by"];
            $object->_createDate = $array["create_date"];

            return $object;
        }
    }

    LicensePeriods::Init();
