<?php
class Employee {

    private $_id = null;
    private $_employeeCode = null;
    private $_employeeCommunicationId = null;
    private $_departmentId = null;
    private $_designationId = null;
    private $_passportId = null;
    private $_dateOfJoin = null;
    private $_dateOfConfirm = null;
    private $_dateOfResign = null;
    private $_nationality = null;
    private $_departmentIds = null;
    private $_employeeTypeId = null;
    private $_employeeType = null;
    private $_locationId = null;
    private $_location = null;
    private $_isTimesheet = null;
    private $_isTrainer = null;

    private $_designationName = null;
    private $_departmentName = null;

    // Passport Info
    private $_passportNumber = null;
    private $_passportIssueDate = null;
    private $_passportExpiryDate = null;
    private $_passportPlaceOfIssue = null;
    private $_passportCountryOfResidence = null;

    // Visa Info
    private $_visaIds = null;

    private $_visaId = null;
    private $_visaCountry = null;
    private $_visaType = null;
    private $_visaIssueDate = null;
    private $_visaExpiryDate = null;
    private $_visaPlaceOfIssue = null;

    // Communication Info
    private $_personalEmail = null;
    private $_mobile1 = null;
    private $_mobile2 = null;
    private $_phone1 = null;
    private $_phone2 = null;
    private $_address = null;
    private $_city = null;
    private $_zip = null;
    private $_state = null;
    private $_countryId = null;
    private $_skypeId = null;
    private $_yahooId = null;
    private $_msnId = null;
    private $_gtalkId = null;
    private $_emergency_cnt = null;
    private $_relationship = null;
      
    private $_createDate = null;
    private $_createdBy = null;
    private $_updatedBy = null;
    private $_updateDate = null;
    private $_deletedBy = null;
    private $_deleteDate = null;
    private $_deleted = null;

    // Training Delivery
    private $_trainingSkillsIds = null;
    private $_trainingSkillsLevels = null;

    private $_trainingSkillId = null;
    private $_trainingSkill = null;
    private $_trainingSkillsLevel = null;

    // Product Knowledge
    private $_productKnowledgeIds = null;
    private $_productKnowledgeLevels = null;

    private $_productKnowledgeId = null;
    private $_productKnowledge = null;
    private $_productKnowledgeLevel = null;
        
    // Technology Practice
    private $_techPracticeIds = null;
    private $_techPracticeLevels = null;

    private $_techPracticeId = null;
    private $_techPractice = null;
    private $_techPracticeLevel = null;

    // Technology Skills
    private $_techSkillsIds = null;
    private $_techSkillsLevels = null;

    private $_techSkillId = null;
    private $_techSkill = null;
    private $_techSkillLevel = null;

    private $_passport = null;
    private $_visaInfo = null;
    private $_communication = null;
    private $_departments = null;
    private $_trainingSkills = null;
    private $_productKnowledges = null;
    private $_techPractices = null;
    private $_techSkills = null;

    
    public function __construct($id = null, $user_id = null){
        if($id > 0 || $user_id > 0) {
            if($id > 0 && $user_id > 0) {
                $where = " WHERE a.id = $id AND a.user_id = $user_id";
            }
            else if ($id > 0) {
                $where = " WHERE a.id = $id";
            }
            else if ($user_id > 0) {
                $where = " WHERE a.user_id = $user_id";
            }
            $sql = "SELECT a.*,d.passport_number,d.issue_date,d.expiry_date,d.place_of_issue, d.country_of_residence,
                        e.personal_email, e.mobile1, e.mobile2, e.phone1, e.phone2, e.address, e.city, e.zip, e.state, e.country_id,
                        e.skype_id, e.yahoo_id, e.msn_id, e.gtalk_id, e.emergency_cnt, e.relationship, f.name as designation_name,
                        g.name as employee_type, h.name as location, i.name as department_name
                    FROM employees a
                    LEFT JOIN passport d ON d.id = a.passport_id 
                    LEFT JOIN employee_communication e ON e.id = a.employee_communication_id
                    LEFT JOIN designation f ON f.id = a.designation_id
                    LEFT JOIN employee_types g ON g.id = a.employee_type_id
                    LEFT JOIN employee_locations h ON h.id = a.location_id
                    LEFT JOIN department i ON i.id = a.department_id
                    $where";

            $row = db_get_row($sql);
            if($row){
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_id = $array["id"];
        $object->_employeeCode = $array["employee_code"];
        $object->_employeeTypeId = $array["employee_type_id"];
        $object->_employeeType = $array["employee_type"];
        $object->_employeeCommunicationId = $array["employee_communication_id"];
        $object->_departmentId = $array["department_id"];
        $object->_designationId = $array["designation_id"];
        $object->_passportId = $array["passport_id"];
        $object->_dateOfJoin = $array["date_of_join"];
        $object->_dateOfConfirm = $array["date_of_confirmation"];
        $object->_dateOfResign = $array["date_of_resign"];
        $object->_nationality = $array["nationality"];
        $object->_locationId = $array["location_id"];
        $object->_location = $array["location"];
        $object->_isTimesheet = $array["is_timesheet"];
        $object->_isTrainer = $array["is_trainer"];

        $object->_designationName = $array["designation_name"];
        $object->_departmentName = $array["department_name"];
        $object->_passportNumber = $array["passport_number"];
        $object->_passportIssueDate = $array["issue_date"];
        $object->_passportExpiryDate = $array["expiry_date"];
        $object->_passportPlaceOfIssue = $array["place_of_issue"];
        $object->_passportCountryOfResidence = $array["country_of_residence"];

        $object->_personalEmail = $array["personal_email"];
        $object->_mobile1 = $array["mobile1"];
        $object->_mobile2 = $array["mobile2"];
        $object->_phone1 = $array["phone1"];
        $object->_phone2 = $array["phone2"];
        $object->_address = $array["address"];
        $object->_city = $array["city"];
        $object->_zip = $array["zip"];
        $object->_state = $array["state"];
        $object->_countryId = $array["country_id"];

        $object->_skypeId = $array["skype_id"];
        $object->_yahooId = $array["yahoo_id"];
        $object->_msnId = $array["msn_id"];
        $object->_gtalkId = $array["gtalk_id"];
        $object->_emergency_cnt = $array["emergency_cnt"];
        $object->_relationship = $array["relationship"];

        $object->_createdBy = $array["created_by"];
        $object->_createDate = $array["create_date"];
        $object->_updatedBy = $array["updated_by"];
        $object->_updateDate = $array["update_date"];
        $object->_deletedBy = $array["deleted_by"];
        $object->_deleteDate = $array["deleted_date"];
        $object->_deleted = $array["deleted"];
        
        return $object;
    }
    private static function _InitDepartment($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_departmentId = $array["department_id"];
        $object->_departmentName = $array["department_name"];

        return $object;
    }

    private static function _InitVisaInfo($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_visaId = $array["id"];
        $object->_visaType = $array["visa_type"];
        $object->_visaCountry = $array["country"];
        $object->_visaIssueDate = $array["issue_date"];
        $object->_visaExpiryDate = $array["expiry_date"];
        $object->_visaPlaceOfIssue = $array["place_of_issue"];

        return $object;
    }

    private static function _InitTrainingSkills($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_trainingSkillId = $array["training_skills_id"];
        $object->_trainingSkill = $array["training_skill"];
        $object->_trainingSkillLevel = $array["level"];

        return $object;
    }

    private static function _InitProductKnowledge($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_productKnowledgeId = $array["product_knowledge_id"];
        $object->_productKnowledge = $array["product_knowledge"];
        $object->_productKnowledgeLevel = $array["level"];

        return $object;
    }

    private static function _InitTechPractice($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_techPracticeId = $array["technology_practice_id"];
        $object->_techPractice = $array["technology_practice"];
        $object->_techPracticeLevel = $array["level"];

        return $object;
    }

    private static function _InitTechSkill($array, $object = null){
        if(!$object instanceof self) $object = new self;
        
        $object->_techSkillId = $array["technology_skills_id"];
        $object->_techSkill = $array["technology_skill"];
        $object->_techSkillLevel = $array["level"];

        return $object;
    }

    public function getId() {
        return $this->_id;
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getEmployeeCode() {
        return $this->_employeeCode;
    }
    public function getEmployeeTypeId() {
        return $this->_employeeTypeId;
    }
    public function getEmployeeType() {
        return $this->_employeeType;
    }
    public function getEmployeeCommunicationId() {
        return $this->_employeeCommunicationId;
    }
    
    public function getDesignationId() {
        return $this->_designationId;
    }
    public function getDesignationName() {
        return $this->_designationName;
    }
    public function getDateOfJoin() {
        return format_date($this->_dateOfJoin, 'd-M-Y');
    }
    public function getDateOfResign() {
        return format_date($this->_dateOfResign, 'd-M-Y');
    }
    public function getDateOfConfirmation() {
        return format_date($this->_dateOfConfirm, 'd-M-Y');
    }
    public function getNationality () {
        return $this->_nationality;
    }
    public function getLocationId () {
        return $this->_locationId;
    }
    public function getLocation () {
        return $this->_location;
    }
    public function isTimesheet () {
        return $this->_isTimesheet;
    }
    public function isTrainer () {
        return $this->_isTrainer;
    }
    public function getPassportId() {
        return $this->_passportId;
    }
    public function getPassportNumber() {
        return $this->_passportNumber;
    }
    public function getPassportUserId() {
       return $this->_user_id;
    }
    public function getPassportIssueDate($format = 'd-M-Y') {
        return format_date($this->_passportIssueDate,$format);
    }
    public function getPassportExpiryDate($format = 'd-M-Y') {
        return format_date($this->_passportExpiryDate,$format);
    }
    public function getPassportPlaceOfIssue() {
        return $this->_passportPlaceOfIssue;
    }
    public function getPassportCountryOfResidence() {
        return $this->_passportCountryOfResidence;
    }
    

    public function getPersonalEmail() {
        return $this->_personalEmail;
    }
    public function getMobile1() {
        return $this->_mobile1;
    }
    public function getMobile2() {
        return $this->_mobile2;
    }
    public function getPhone1(){
        return $this->_phone1;
    }
    public function getPhone2(){
        return $this->_phone2;
    }
    public function getSkypeId(){
        return $this->_skypeId;
    }
    public function getYahooId(){
        return $this->_yahooId;
    }
    public function getMsnId(){
        return $this->_msnId;
    }
    public function getGtalkId(){
        return $this->_gtalkId;
    }
    public function getEmergencyCnt(){
        return $this->_emergency_cnt;
    }
    public function getRelationship(){
        return $this->_relationship;
    }
    public function getAddress(){
        return $this->_address;
    }
    public function getCity(){
        return $this->_city;
    }
    public function getZip(){
        return $this->_zip;
    }
    public function getState(){
        return $this->_state;
    }
    public function getCountryId(){
        return $this->_countryId;
    }
    public function getCreateDate() {
        return date("m/d/Y H:i", strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }
    public function getUpdateDate() {
        return date("m/d/Y H:i", strtotime($this->_updateDate));
    }
    public function getUpdatedBy() {
        return $this->_updatedBy;
    }       
    public function getDeleted() {
        return $this->_deleted;
    }          
    public function getDeletedDate() {
        return date("m/d/Y H:i", strtotime($this->_deleteDate));
    }
    public function getDeletedBy() {
        return $this->_deletedBy;
    }
    
    // Visa Info
    public function getVisaId() {
        return $this->_visaId;
    }
    public function getVisaType() {
        return $this->_visaType;
    }
    public function getVisaCountry() {
        return $this->_visaCountry;
    }
    public function getVisaIssueDate($format='d-M-Y') {
        return format_date($this->_visaIssueDate,$format);
    }
    public function getVisaExpiryDate($format='d-M-Y') {
        return format_date($this->_visaExpiryDate,$format);
    }
    public function getVisaPlaceOfIssue() {
        return $this->_visaPlaceOfIssue;
    }
    
    public function setEmployeeCode($code) {
        $this->_employeeCode = $code;
    }
    public function setEmployeeTypeId($type_id) {
        $this->_employeeTypeId = $type_id;
    }
    public function setEmployeeCommunicationId($employee_communication_id) {
        $this->_employeeCommunicationId = $employee_communication_id;
    }
    public function setDepartmentId($department_id) {
        $this->_departmentId = $department_id;
    }
    public function setDepartmentIds($department_ids) {
       $this->_departmentIds = $department_ids;
    }
    public function setTrainingSkillsIds($training_skills_ids) {
       $this->_trainingSkillsIds = $training_skills_ids;
    }
    public function setTrainingSkillsLevels($training_skills_levels) {
       $this->_trainingSkillsLevels = $training_skills_levels;
    }
    public function setProductKnowledgeIds($prod_knowledge_ids) {
       $this->_productKnowledgeIds = $prod_knowledge_ids;
    }
    public function setProductKnowledgeLevels($prod_knowledge_levels) {
       $this->_productKnowledgeLevels = $prod_knowledge_levels;
    }
    public function setTechPracticeIds($tech_practice_ids) {
       $this->_techPracticeIds = $tech_practice_ids;
    }
    public function setTechPracticeLevels($tech_practice_levels) {
       $this->_techPracticeLevels = $tech_practice_levels;
    }
    public function setTechSkillsIds($tech_skills_ids) {
       $this->_techSkillsIds = $tech_skills_ids;
    }
    public function setTechSkillsLevels($tech_skills_levels) {
       $this->_techSkillsLevels = $tech_skills_levels;
    }
    public function setDesignationId($designation_id) {
        $this->_designationId = $designation_id;
    }
    public function setDateOfJoin($date_of_join) {
        if($date_of_join) {
            $this->_dateOfJoin = ymd(is_date($date_of_join));
        }
        else {
            $this->_dateOfJoin = '';
        }
    }
    public function setDateOfResign($date_of_resign) {
        if($date_of_resign) 
            $this->_dateOfResign = ymd(is_date($date_of_resign));
        else
            $this->_dateOfResign = '';
    }
    public function setDateOfConfirmation($date_of_confirm) {
        if($date_of_confirm)
            $this->_dateOfConfirm = ymd(is_date($date_of_confirm));
        else 
            $this->_dateOfConfirm = ''; 
    }
    public function setNationality ($nationality) {
        $this->_nationality = addslashes($nationality);
    }
    public function setLocationId ($location_id) {
        $this->_locationId = intval($location_id);
    }
    public function setIsTimesheet ($is_timesheet) {
        $this->_isTimesheet = intval($is_timesheet);
    }
    public function setIsTrainer ($is_trainer) {
        $this->_isTrainer = intval($is_trainer);
    }
    public function setPassportId($passport_id) {
        $this->_passportId = $passport_id;
    }
    public function setPassportUserId($user_id) {
        $this->_userId = $user_id;
    }
    public function setPassportNumber($passport_number) {
        $this->_passportNumber = $passport_number;
    }
    public function setPassportIssueDate($issue_date) {
        if($issue_date)
            $this->_passportIssueDate = ymd(is_date($issue_date));
    }
    public function setPassportExpiryDate($expiry_date) {
        if($expiry_date)
        $this->_passportExpiryDate = ymd(is_date($expiry_date));
    }
    public function setPassportPlaceOfIssue($place_of_issue) {
        $this->_passportPlaceOfIssue = addslashes($place_of_issue);
    }
    public function setPassportCountryOfResidence($country_of_residence) {
        $this->_passportCountryOfResidence = $country_of_residence;
    }
    public function setUserId($user_id) {
        $this->_userId = $user_id;
    }
    public function setPersonalEmail($email) {
        if($email)
            $this->_personalEmail = trim($email);
    }
    public function setMobile1($mobile1) {
        $this->_mobile1 = $mobile1;
    }
    public function setMobile2($mobile2) {
        $this->_mobile2 = $mobile2;
    }
    public function setPhone1($phone1){
        $this->_phone1 = trim($phone1);
    }
    public function setPhone2($phone2){
        $this->_phone2 = trim($phone2);
    }
    public function setSkypeId($skype_id){
        $this->_skypeId = trim($skype_id);
    }
    public function setYahooId($yahoo_id){
        $this->_yahooId = trim($yahoo_id);
    }
    public function setMsnId($msn_id){
        $this->_msnId = trim($msn_id);
    }
    public function setGtalkId($gtalk_id){
        $this->_gtalkId = trim($gtalk_id);
    }
    public function setEmergencyCnt($emergency_cnt){
        $this->_emergency_cnt = trim($emergency_cnt);
    }
    public function setRelationship($relationship){
        $this->_relationship = trim($relationship);
    }
    public function setAddress($address){
        if($address)
            $this->_address = addslashes($address);
    }
    public function setCity($city){
        if($city)
            $this->_city = trim($city);
    }
    public function setZip($zip){
        if($zip)
            $this->_zip = trim($zip);
    }
    public function setState($state){
        if($state)
            $this->_state = trim($state);
    }
    public function setCountryId($country_id){
        if($country_id)
            $this->_countryId = trim($country_id);
    }

    // Visa Info
    public function setVisaId($id) {
        $this->_visaId = $id;
    }
    public function setVisaType($visa_type) {
        $this->_visaType = addslashes(trim($visa_type));
    }
    public function setVisaCountry($country) {
        $this->_visaCountry = addslashes(trim($country));
    }
    public function setVisaIssueDate($idate) {
        if($idate)
            $this->_visaIssueDate = ymd(is_date($idate));
    }
    public function setVisaExpiryDate($edate) {
        if($edate) 
            $this->_visaExpiryDate = ymd(is_date($edate));
    }
    public function setVisaPlaceOfIssue($place_of_issue) {
        $this->_visaPlaceOfIssue = addslashes(trim($place_of_issue));
    }
    
    private function _loadVisaInfo() {
        $sql = "SELECT a.*
                FROM passport_visa_info a
                INNER JOIN passport b ON b.id = a.passport_id
                WHERE a.passport_id = '{$this->_passportId}'
                ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $this->_visaInfo[$row['id']] = self::_InitVisaInfo($row);        
                $this->_visaIds[$row['id']] = $row['id'];
            }
        }
    }
    public function getVisaInfo() {
        if(is_null($this->_visaInfo))
            $this->_loadVisaInfo();
        return $this->_visaInfo;
    }

    public function save() {
        if($this->_id) $this->_update();
        else $this->_add();
    }
    private function _loadDepartments() {
        $sql = "SELECT a.*,b.name as department_name
                FROM employee_department_map a
                INNER JOIN department b ON b.id = a.department_id
                WHERE employee_id = $this->_id
                ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $this->_departments[$row['id']] = self::_InitDepartment($row);        
                $this->_departmentIds[$row['department_id']] = $row['department_id'];
            }
        }
    }
    public function getDepartments() {
        if(is_null($this->_departments))
            $this->_loadDepartments();
        return $this->_departments;
    }
    public function getDepartmentId() {
        return $this->_departmentId;
    }
    public function getDepartmentName(){
        return $this->_departmentName;
    }

    private function _loadTrainingSkills() {
        $sql = "SELECT a.*,b.name as training_skill
                FROM employee_training_skills_map a
                INNER JOIN training_skills b ON b.id = a.training_skills_id
                WHERE employee_id = $this->_id
                ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $this->_trainingSkills[$row['id']] = self::_InitTrainingSkills($row);        
                $this->_trainingSkillsIds [$row['training_skills_id']] = $row['training_skills_id'];
            }
        }
    }
    public function getTrainingSkills() {
        if(is_null($this->_trainingSkills))
            $this->_loadTrainingSkills();
        return $this->_trainingSkills;
    }
    public function getTrainingSkillId() {
        return $this->_trainingSkillId;
    }
    public function getTrainingSkillName(){
        return $this->_trainingSkill;
    }
    public function getTrainingSkillLevel(){
        return $this->_trainingSkillLevel;
    }

    private function _loadProductKnowledges() {
        $sql = "SELECT a.*,b.name as product_knowledge
                FROM employee_product_knowledge_map a
                INNER JOIN product_knowledge b ON b.id = a.product_knowledge_id
                WHERE employee_id = $this->_id
                ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $this->_productKnowledges[$row['id']] = self::_InitProductKnowledge($row);        
                $this->_productKnowledgeIds[$row['product_knowledge_id']] = $row['product_knowledge_id'];
            }
        }
    }
    public function getProdutKnowledges() {
        if(is_null($this->_productKnowledges))
            $this->_loadProductKnowledges();
        return $this->_productKnowledges;
    }
    public function getProdutKnowledgeId() {
        return $this->_productKnowledgeId;
    }
    public function getProdutKnowledgeName(){
        return $this->_productKnowledge;
    }
    public function getProdutKnowledgeLevel(){
        return $this->_productKnowledgeLevel;
    }

    private function _loadTechPractices() {
        $sql = "SELECT a.*,b.name as technology_practice
                FROM employee_technology_practice_map a
                INNER JOIN technology_practice b ON b.id = a.technology_practice_id
                WHERE employee_id = $this->_id order by b.name
                ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $this->_techPractices[$row['id']] = self::_InitTechPractice($row);        
                $this->_techPracticeIds[$row['technology_practice_id']] = $row['technology_practice_id'];
            }
        }
    }
    public function getTechPractices() {
        if(is_null($this->_techPractices))
            $this->_loadTechPractices();
        return $this->_techPractices;
    }
    public function getTechPracticeId() {
        return $this->_techPracticeId;
    }
    public function getTechPracticeName(){
        return $this->_techPractice;
    }
    public function getTechPracticeLevel(){
        return $this->_techPracticeLevel;
    }

    private function _loadTechSkills() {
        $sql = "SELECT a.*,b.name as technology_skill
                FROM employee_technology_skills_map a
                INNER JOIN technology_skills b ON b.id = a.technology_skills_id
                WHERE employee_id = $this->_id
                ";
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $this->_techSkills[$row['id']] = self::_InitTechSkill($row);        
                $this->_techSkillsIds[$row['technology_skills_id']] = $row['technology_skills_id'];
            }
        }
    }
    public function getTechSkills() {
        if(is_null($this->_techSkills))
            $this->_loadTechSkills();
        return $this->_techSkills;
    }
    public function getTechSkillId() {
        return $this->_techSkillId;
    }
    public function getTechSkillName(){
        return $this->_techSkill;
    }
    public function getTechSkillLevel(){
        return $this->_techSkillLevel;
    }

    public function _add() {
        if($this->_passportId < 1) {
            $this->_addPassport();
        }
        if($this->_employeeCommunicationId < 1) {
            $this->_addCommunication();
        }
        //if($this->_userId && $this->_employeeCode && $this->_dateOfJoin) {
        if($this->_userId) {
            $sql = "INSERT INTO employees (user_id, employee_code, employee_type_id, employee_communication_id, department_id, designation_id, passport_id, 
                            date_of_join, date_of_confirmation, date_of_resign, nationality, location_id, is_timesheet, is_trainer,
                            created_by, create_date, updated_by, update_date)
                        VALUES (
                            '" . $this->_userId . "',
                            '" . $this->_employeeCode . "',
                            '" . $this->_employeeTypeId . "',
                            '" . $this->_employeeCommunicationId . "',
                            '" . $this->_departmentId . "',
                            '" . $this->_designationId . "',
                            '" . $this->_passportId . "',
                            '" . $this->_dateOfJoin . "',
                            '" . $this->_dateOfConfirm . "',
                            '" . $this->_dateOfResign . "',
                            '" . $this->_nationality . "',
                            '" . $this->_locationId . "',
                            '" . $this->_isTimesheet . "',
                            '" . $this->_isTrainer . "',
                            '" . u()->getEmail() . "',
                            NOW(),
                            '" . u()->getEmail() . "',
                            NOW()
                        );
                    ";
            if(db_execute($sql)) {
                $this->_id = db_insert_id();
                // Rob asked to remove multi-department support as of now (11-June-2012), we will do it later, if required.
                //$this->_addDepartments();
                $this->_addProductKnowledge();
                $this->_addTechPractice();
                $this->_addTechSkills();
                $this->_addTrainingSkills();

                return true;
            }
        }
        return false;
    }
    private function _addCommunication(){
        if($this->_personalEmail) {
            $sql = "INSERT INTO employee_communication (user_id, personal_email, mobile1,mobile2, phone1,phone2, address, city, zip, state, country_id, 
                        skype_id, yahoo_id, msn_id, gtalk_id, emergency_cnt, relationship)
                        VALUES (
                            '". $this->_userId."',
                            '". $this->_personalEmail."',
                            '". $this->_mobile1."',
                            '". $this->_mobile2."',
                            '". $this->_phone1."',
                            '". $this->_phone2."',
                            '". $this->_address."',
                            '". $this->_city."',
                            '". $this->_zip."',
                            '". $this->_state."',
                            '". $this->_countryId."',
                            '". $this->_skypeId."',
                            '". $this->_yahooId."',
                            '". $this->_msnId."',
                            '". $this->_gtalkId."',
                            '". $this->_emergency_cnt."',
                            '". $this->_relationship."'
                        );
                    ";
            if(db_execute($sql)) {
                $this->_employeeCommunicationId = db_insert_id();
            }
        }
    }
    private function _addPassport() {
        if($this->_passportNumber) {
            $sql = "INSERT INTO passport (user_id, passport_number, issue_date, expiry_date, place_of_issue, country_of_residence)
                    VALUES (
                            '". $this->_userId."',
                            '" . $this->_passportNumber ."',
                            '" . $this->_passportIssueDate ."',
                            '" . $this->_passportExpiryDate ."',
                            '" . $this->_passportPlaceOfIssue ."',
                            '" . $this->_passportCountryOfResidence ."'
                        );
               ";
            if(db_execute($sql)){
                $this->_passportId = db_insert_id();
            }
        }
    }
    public function saveVisaInfo() {
        if($this->_passportId) {

            // Now insert visa info
            $sql = "INSERT INTO passport_visa_info (passport_id, visa_type, country, issue_date, expiry_date, place_of_issue)
                    VALUES (
                            '" . $this->_passportId ."',
                            '" . $this->_visaType ."',
                            '" . $this->_visaCountry ."',
                            '" . $this->_visaIssueDate ."',
                            '" . $this->_visaExpiryDate ."',
                            '" . $this->_visaPlaceOfIssue ."'
                        );
               ";
            if(db_execute($sql)){
                $this->_visaId = db_insert_id();
                return true;
            }
        }
        return false;
    }
    public function deletedVisaInfo($id = 0) {
        $and = '';
        if($id) {
            $and = " AND id = $id";
        }
        if($this->_passportId) {
            // Delete visa info
            $sql = "DELETE FROM passport_visa_info 
                    WHERE passport_id = {$this->_passportId}"; 
            return db_execute($sql);
        }
        return false;
    }
    public function checkVisaInfo($passport_id, $visa_type,$country,$issue_date,$expiry_date,$place_of_issue ) {
        $issue_date = ymd(is_date($issue_date));
        $expiry_date = ymd(is_date($expiry_date));

        $sql = "SELECT id FROM passport_visa_info WHERE passport_id = '$passport_id' AND visa_type = '$visa_type' AND country = '$country' AND
                issue_date = '$issue_date' AND expiry_date = '$expiry_date' AND place_of_issue = '$place_of_issue'";
        $row = db_get_row($sql);
        if($row) {
            return $row['id'];
        }
        return false;
    }
    private function _addDepartments() {
        if(is_array($this->_departmentIds)) {
           if($this->_isTrainer) {
                foreach($this->_departmentIds as $v) {
                    $sql = "INSERT INTO employee_department_map (employee_id, department_id)
                                VALUES (
                                    {$this->_id},
                                    {$v}
                                );
                            ";
                    db_execute($sql);
                }
            }
        }
    }
    private function _addTrainingSkills() {
        if(is_array($this->_trainingSkillsIds)) {
            foreach($this->_trainingSkillsIds as $v) {
                $level = isset($this->_trainingSkillsLevels[$v]) ? $this->_trainingSkillsLevels[$v] : 'NONE';
                $sql = "INSERT INTO employee_training_skills_map (employee_id, training_skills_id, level)
                        VALUES (
                            {$this->_id},
                            {$v},
                            '$level'
                        );
                    ";
                db_execute($sql);
            }
        }
    }
    private function _addProductKnowledge() {
        if(is_array($this->_productKnowledgeIds)) {
            foreach($this->_productKnowledgeIds as $v) {
                $level = isset($this->_productKnowledgeLevels[$v]) ? $this->_productKnowledgeLevels[$v] : 'NONE';
                $sql = "INSERT INTO employee_product_knowledge_map (employee_id, product_knowledge_id, level)
                        VALUES (
                            {$this->_id},
                            {$v},
                            '$level'
                        );
                    ";
                db_execute($sql);
            }
        }
    }
    private function _addTechPractice() {
        if(is_array($this->_techPracticeIds)) {
            foreach($this->_techPracticeIds as $v) {
                $level = isset($this->_techPracticeLevels[$v]) ? $this->_techPracticeLevels[$v] : 'NONE';
                $sql = "INSERT INTO employee_technology_practice_map (employee_id, technology_practice_id, level)
                        VALUES (
                            {$this->_id},
                            {$v},
                            '$level'
                        );
                    ";
                db_execute($sql);
            }
        }
    }
    private function _addTechSkills() {
        if(is_array($this->_techSkillsIds)) {
            foreach($this->_techSkillsIds as $v) {
                $level = isset($this->_techSkillsLevels[$v]) ? $this->_techSkillsLevels[$v] : 'NONE';
                $sql = "INSERT INTO employee_technology_skills_map (employee_id, technology_skills_id, level)
                        VALUES (
                            {$this->_id},
                            {$v},
                            '$level'
                        );
                    ";
                db_execute($sql);
            }
        }
    }
    private function _update() {
        
        if($this->_passportId < 1) {
            $this->_addPassport();
        }
        else $this->_updatePassport();

        if($this->_employeeCommunicationId < 1) {
            $this->_addCommunication();
        }
        else $this->_updateCommunication();

        if($this->_id) {

            $sql = "UPDATE employees SET
                        user_id = '" . $this->_userId . "',
                        employee_code = '" . $this->_employeeCode . "',
                        employee_type_id = '" . $this->_employeeTypeId . "',
                        employee_communication_id = '" . $this->_employeeCommunicationId . "',
                        department_id = '" . $this->_departmentId . "',
                        designation_id = '" . $this->_designationId . "',
                        passport_id = '" . $this->_passportId . "',
                        date_of_join = '" . $this->_dateOfJoin . "',
                        date_of_confirmation = '" . $this->_dateOfConfirm . "',
                        date_of_resign = '" . $this->_dateOfResign . "',
                        nationality = '" . $this->_nationality . "',
                        location_id = '" . $this->_locationId . "',
                        is_timesheet = '" . $this->_isTimesheet . "',
                        is_trainer = '" . $this->_isTrainer . "',
                        updated_by = '" . u()->getEmail() . "',
                        update_date = NOW()
                    WHERE id = {$this->_id}
                    ";
            if(db_execute($sql)) {
                // Rob asked to remove multi-department support as of now (11-June-2012), we will do it later, if required.
                //$this->_updateDepartments();
                $this->_updateProductKnowledge();
                $this->_updateTechPractice();
                $this->_updateTechSkills();
                $this->_updateTrainingSkills();

                return true;
            }
        }
        return false;
    }
    private function _updateCommunication() {
        if($this->_employeeCommunicationId) {
            $sql = "UPDATE employee_communication SET 
                        user_id = '". $this->_userId."',
                        personal_email = '". $this->_personalEmail."',
                        mobile1 = '". $this->_mobile1."',
                        mobile2 = '". $this->_mobile2."',
                        phone1 = '". $this->_phone1."',
                        phone2 = '". $this->_phone2."',
                        address = '". $this->_address."',
                        city = '". $this->_city."',
                        zip = '". $this->_zip."',
                        state = '". $this->_state."',
                        country_id = '". $this->_countryId."',
                        skype_id = '". $this->_skypeId."',
                        yahoo_id = '". $this->_yahooId."',
                        msn_id = '". $this->_msnId."',
                        gtalk_id = '". $this->_gtalkId."',
                        emergency_cnt = '". $this->_emergency_cnt ."',
                        relationship = '". $this->_relationship."'
                    WHERE id = {$this->_employeeCommunicationId}
                    ";
            return db_execute($sql);
        }
        return false;
    }
    private function _updatePassport() {
        if($this->_passportId) {
            $sql = "UPDATE passport SET
                        user_id = '". $this->_userId."',
                        passport_number = '" . $this->_passportNumber ."',
                        issue_date = '" . $this->_passportIssueDate ."',
                        expiry_date = '" . $this->_passportExpiryDate ."',
                        place_of_issue = '" . $this->_passportPlaceOfIssue ."',
                        country_of_residence = '" . $this->_passportCountryOfResidence ."'
                    WHERE id = {$this->_passportId}
                    ";
            return db_execute($sql);
        }
        return false;
    }
    
    private function _updateDepartments() {
        if(is_array($this->_departmentIds)) {
            $sql = "DELETE FROM employee_department_map 
                    WHERE employee_id = {$this->_id} 
                    AND department_id NOT IN(".implode(",", $this->_departmentIds).")";
           db_execute($sql);
           foreach($this->_departmentIds as $v) {
                $id = 0;
                $id = $this->checkDepartmentId($v);
                if($id > 0) {
                    $sql = "UPDATE employee_department_map 
                                SET department_id = $v
                            WHERE employee_id = $this->_id AND department_id = $v";
                    db_execute($sql);
                } else {
                    $sql = "INSERT INTO employee_department_map (employee_id, department_id)
                            VALUES (
                                {$this->_id},
                                {$v}
                            );
                        ";
                    db_execute($sql);
                }
            } 
            return true;
        }
        return false;
    }
    public function checkDepartmentId($department_id) {
        if($department_id) {
            $sql = "SELECT id FROM employee_department_map WHERE employee_id = {$this->_id} AND department_id = $department_id";
            $row = db_get_row($sql);
            if($row) {
                return $row['id'];
            }
        }
        return false;
    }

    private function _updateTrainingSkills() {
        if(is_array($this->_trainingSkillsIds)) {
           if($this->_isTrainer) {
                $sql = "DELETE FROM employee_training_skills_map
                        WHERE employee_id = {$this->_id} 
                        AND training_skills_id NOT IN(".implode(",", $this->_trainingSkillsIds).")";
               db_execute($sql);

               foreach($this->_trainingSkillsIds as $v) {
                    $id = 0;
                    $id = $this->checkTrainingSkillsId($v);
                    $level = isset($this->_trainingSkillsLevels[$v]) ? $this->_trainingSkillsLevels[$v] : 'NONE';
                    if($id > 0) {
                        $sql = "UPDATE employee_training_skills_map SET 
                                     training_skills_id = $v,
                                     level = '$level'
                                WHERE employee_id = $this->_id AND training_skills_id = $v";
                        db_execute($sql);
                    } else {
                        $sql = "INSERT INTO employee_training_skills_map (employee_id, training_skills_id, level)
                                VALUES (
                                    {$this->_id},
                                    {$v},
                                    '{$level}'
                                );
                            ";
                        db_execute($sql);
                    }
                } 
            }
            else {
                $sql = "DELETE FROM employee_training_skills_map
                        WHERE employee_id = {$this->_id} 
                        AND training_skills_id IN(".implode(",", $this->_trainingSkillsIds).")";
               db_execute($sql);
            }
            return true;
        }
        return false;
    }
    public function checkTrainingSkillsId($training_skills_id) {
        if($training_skills_id) {
            $sql = "SELECT id FROM employee_training_skills_map WHERE employee_id = {$this->_id} AND training_skills_id = $training_skills_id";
            $row = db_get_row($sql);
            if($row) {
                return $row['id'];
            }
        }
        return false;
    }

    private function _updateProductKnowledge() {
        if(is_array($this->_productKnowledgeIds)) {
            $sql = "DELETE FROM employee_product_knowledge_map
                    WHERE employee_id = {$this->_id} 
                    AND product_knowledge_id NOT IN(".implode(",", $this->_productKnowledgeIds).")";
           db_execute($sql);
           foreach($this->_productKnowledgeIds as $v) {
                $id = 0;
                $id = $this->checkProductKnowledgeId($v);
                $level = isset($this->_productKnowledgeLevels[$v]) ? $this->_productKnowledgeLevels[$v] : 'NONE';
                if($id > 0) {
                    $sql = "UPDATE employee_product_knowledge_map SET 
                                 product_knowledge_id = $v,
                                 level = '$level'
                            WHERE employee_id = $this->_id AND product_knowledge_id = $v";
                    db_execute($sql);
                } else {
                    $sql = "INSERT INTO employee_product_knowledge_map (employee_id, product_knowledge_id, level)
                            VALUES (
                                {$this->_id},
                                {$v},
                                '{$level}'
                            );
                        ";
                    db_execute($sql);
                }
            } 
            return true;
        }
        return false;
    }
    public function checkProductKnowledgeId($prod_knowledge_id) {
        if($prod_knowledge_id) {
            $sql = "SELECT id FROM employee_product_knowledge_map WHERE employee_id = {$this->_id} AND product_knowledge_id = $prod_knowledge_id";
            $row = db_get_row($sql);
            if($row) {
                return $row['id'];
            }
        }
        return false;
    }

    private function _updateTechPractice() {
        if(is_array($this->_techPracticeIds)) {
            $sql = "DELETE FROM employee_technology_practice_map
                    WHERE employee_id = {$this->_id} 
                    AND technology_practice_id NOT IN(".implode(",", $this->_techPracticeIds).")";
           db_execute($sql);
           foreach($this->_techPracticeIds as $v) {
                $id = 0;
                $id = $this->checkTechPracticeId($v);
                $level = isset($this->_techPracticeLevels[$v]) ? $this->_techPracticeLevels[$v] : 'NONE';
                if($id > 0) {
                    $sql = "UPDATE employee_technology_practice_map SET 
                                 technology_practice_id = $v,
                                 level = '$level'
                            WHERE employee_id = $this->_id AND technology_practice_id = $v";
                    db_execute($sql);
                } else {
                    $sql = "INSERT INTO employee_technology_practice_map (employee_id, technology_practice_id, level)
                            VALUES (
                                {$this->_id},
                                {$v},
                                '{$level}'
                            );
                        ";
                    db_execute($sql);
                }
            } 
            return true;
        }
        return false;
    }
    public function checkTechPracticeId($tech_practice_id) {
        if($tech_practice_id) {
            $sql = "SELECT id FROM employee_technology_practice_map WHERE employee_id = {$this->_id} AND technology_practice_id = $tech_practice_id";
            $row = db_get_row($sql);
            if($row) {
                return $row['id'];
            }
        }
        return false;
    }

    private function _updateTechSkills() {
        if(is_array($this->_techSkillsIds)) {
            $sql = "DELETE FROM employee_technology_skills_map
                    WHERE employee_id = {$this->_id} 
                    AND technology_skills_id NOT IN(".implode(",", $this->_techSkillsIds).")";
           db_execute($sql);
           foreach($this->_techSkillsIds as $v) {
                $id = 0;
                $id = $this->checkTechSkillsId($v);
                $level = isset($this->_techSkillsLevels[$v]) ? $this->_techSkillsLevels[$v] : 'NONE';
                if($id > 0) {
                    $sql = "UPDATE employee_technology_skills_map SET 
                                 technology_skills_id = $v,
                                 level = '$level'
                            WHERE employee_id = $this->_id AND technology_skills_id = $v";
                    db_execute($sql);
                } else {
                    $sql = "INSERT INTO employee_technology_skills_map (employee_id, technology_skills_id, level)
                            VALUES (
                                {$this->_id},
                                {$v},
                                '{$level}'
                            );
                        ";
                    db_execute($sql);
                }
            } 
            return true;
        }
        return false;
    }
    public function checkTechSkillsId($tech_skills_id) {
        if($tech_skills_id) {
            $sql = "SELECT id FROM employee_technology_skills_map WHERE employee_id = {$this->_id} AND technology_skills_id = $tech_skills_id";
            $row = db_get_row($sql);
            if($row) {
                return $row['id'];
            }
        }
        return false;
    }


    public static function GetEmployeesTotalByDepartment($department_id = null) {
        $return = array();

        $where = '';
        if($department_id) {
            $where = " WHERE b.id = $department_id";
        }
        $sql = "SELECT count( a.id ) AS total, a.department_id, b.name AS department_name
                FROM `employees` a
                LEFT JOIN department b ON b.id = a.department_id
                INNER JOIN users c ON c.id = a.user_id AND NOT c.blocked
                $where
                GROUP BY a.department_id
                ORDER BY department_name";

        $rows = db_get_all($sql);
        foreach($rows as $row) {
            if(!$row['department_id']) {
                $row['department_name'] = 'None';
            }
            $return[$row['department_id']]['id'] = $row['department_id'];
            $return[$row['department_id']]['total'] = $row['total'];
            $return[$row['department_id']]['name'] = $row['department_name'];
        }

        return $return;
        
    }

    public static function GetEmployeesTotalByDesignation($designation_id = null) {
        $return = array();

        $where = '';
        if($designation_id) {
            $where = " WHERE b.id = $designation_id";
        }
        $sql = "SELECT count( a.id ) AS total, a.designation_id, b.name AS designation_name
                FROM `employees` a
                LEFT JOIN designation b ON b.id = a.designation_id
                INNER JOIN users c ON c.id = a.user_id AND NOT c.blocked
                $where
                GROUP BY a.designation_id
                ORDER BY designation_name";

        $rows = db_get_all($sql);
        foreach($rows as $row) {
            if(!$row['designation_id']) {
                $row['designation_name'] = 'None';
            }
            $return[$row['designation_id']]['id'] = $row['designation_id'];
            $return[$row['designation_id']]['total'] = $row['total'];
            $return[$row['designation_id']]['name'] = $row['designation_name'];
        }

        return $return;
        
    }

    public static function GetEmployeesTotalByLocation($location_id = null) {
        $return = array();

        $where = '';
        if($location_id) {
            $where = " WHERE b.id = $location_id";
        }
        $sql = "SELECT count( a.id ) AS total, a.location_id, b.name AS location
                FROM `employees` a
                LEFT JOIN employee_locations b ON b.id = a.location_id
                INNER JOIN users c ON c.id = a.user_id AND NOT c.blocked
                $where
                GROUP BY a.location_id
                ORDER BY location";

        $rows = db_get_all($sql);
        foreach($rows as $row) {
            if(!$row['location_id']) {
                $row['location'] = 'None';
            }
            $return[$row['location_id']]['id'] = $row['location_id'];
            $return[$row['location_id']]['total'] = $row['total'];
            $return[$row['location_id']]['name'] = $row['location'];
        }

        return $return;
        
    }

    public static function GetEmployeesByLocation($location_id = null) {
        $return = array();

        $where = '';
        if($location_id) {
            $where = " WHERE a.id = $location_id";
        }
        $sql = "SELECT a.*,d.passport_number,d.issue_date,d.expiry_date,d.place_of_issue, d.country_of_residence,
                        e.personal_email, e.mobile1, e.mobile2, e.phone1, e.phone2, e.address, e.city, e.zip, e.state, e.country_id,
                        e.skype_id, e.yahoo_id, e.msn_id, e.gtalk_id, f.name as designation_name,
                        g.name as employee_type, h.name as location, i.name as department_name,
                        b.first_name, b.last_name, b.blocked,b.email
                    FROM employees a
                    INNER JOIN users b ON b.id = a.user_id AND NOT b.blocked
                    LEFT JOIN passport d ON d.id = a.passport_id 
                    LEFT JOIN employee_communication e ON e.id = a.employee_communication_id
                    LEFT JOIN designation f ON f.id = a.designation_id
                    LEFT JOIN employee_types g ON g.id = a.employee_type_id
                    LEFT JOIN employee_locations h ON h.id = a.location_id
                    LEFT JOIN department i ON i.id = a.department_id
                    $where
                    ORDER BY location, b.first_name
                "; 

        $rows = db_get_all($sql);
        foreach($rows as $row) {
            if(!$row['location_id']) {
                $row['location'] = 'None';
            }
            $return[$row['location']][$row['id']] = $row;
            $sql = "SELECT a.*,b.name as technology_practice
                FROM employee_technology_practice_map a
                INNER JOIN technology_practice b ON b.id = a.technology_practice_id
                WHERE employee_id = '{$row['id']}'";
            
            $return[$row['location']]['employee'][$row['id']] = $row;
            $return[$row['location']]['employee'][$row['id']]['tech_practice'] = array();
            $tRows = db_get_all($sql);
            foreach($tRows as $tRow) {
                $return[$row['location']]['employee'][$row['id']]['tech_practice'][$tRow['id']] = $tRow;
            }
        } 

        return $return;
    }

    public static function getYearsOfService($current_date,$doj) {
        
        $d1 = new DateTime($current_date);
        $d2 = new DateTime($doj);

        $diff = $d2->diff($d1);

        $yrCnt =  $diff->y;
        return $yrCnt;
    }

    public static function getEarnedPTO($yos,$loc,$current_date) {

        $current_date = format_date($current_date,"Y-m-d");

        //echo $yos.','.$loc.'--'.$current_date."<br>";

        $sql = "SELECT id FROM pto_policies WHERE location_id = $loc and '".$current_date."' >= from_date order by id desc limit 1";
        $row = db_get_row($sql);
        if($row) 
        {       
            $pol_id = $row['id'];

            $sql_tier  = "SELECT leaves FROM pto_tiers WHERE pol_id = $pol_id and max_service >= $yos limit 1";

            $row_tier = db_get_row($sql_tier);
            if($row_tier)
                 $earned = round(($row_tier['leaves'] * 8 / 12),2);
            else
            {
                $sql_tier2  = "SELECT leaves FROM pto_tiers WHERE pol_id = $pol_id and max_service is null limit 1";

                $row_tier2 = db_get_row($sql_tier2);

                if($row_tier2)
                    $earned = round(($row_tier2['leaves'] * 8 / 12),2);
                else
                    $earned = "No tiers defined";
            }
            
        }else
            $earned = "No policy defined";

        return $earned;

    }
    public static function getExcesLimit($loc,$current_date) {

        $current_date = format_date($current_date,"Y-m-d");

        $sql = "SELECT max_hours FROM pto_policies WHERE location_id = $loc and '".$current_date."' >= from_date order by id desc limit 1";
        $row = db_get_row($sql);
        if($row) 
            $max_hours = $row['max_hours'];
        else
            $max_hours = 0;

        return $max_hours;

    }
    public static function getUsedPTO($current_date,$user_id) {

        $c_year = format_date($current_date,"Y");
        $c_month = format_date($current_date,"m");


        $sql = "SELECT sum(pto_minutes)/60 as pto_minutes FROM timecard_data WHERE user_id = $user_id and pto_minutes != 0 and year(timecard_date) = '".$c_year."' and month(timecard_date) = '".$c_month."'";
        $row = db_get_row($sql);
        if($row) 
            $used_pto = round($row['pto_minutes'],2);
        else
            $used_pto = 0;
       

        return number_format($used_pto,2);

    }

    public static function getAdjustment($current_date,$user_id) {

        $return = array();

        $c_year = format_date($current_date,"Y");
        $c_month = format_date($current_date,"m");


        $sql = "SELECT id,adjusted_hours,reason FROM pto_adjustment WHERE user_id = $user_id and year(adjusted_date) = '".$c_year."' and month(adjusted_date) = '".$c_month."'";
        $row = db_get_row($sql);
        if($row) {
            $return['id'] = $row['id'];
            $return['adjusted_hours'] = $row['adjusted_hours'];
            $return['reason'] = $row['reason'];
        }
              
        return $return;

    }



}
?>
