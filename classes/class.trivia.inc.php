<?php
class Trivia {
    private static $_Trivia = null;
    private static $_totalResults = null;
    private static $_totalPages = null;

    private $_id = null;
    private $_dateTime = null;
    private $_userId = null;
    private $_title = null;
    private $_description = null;
    private $_status = null;
    private $_rating = null;
    private $_rejectedReason = null;
    private $_closeDateTime = null;
    
    private $_createDate = null;
    private $_createdBy = null;
    private $_updateDate = null;
    private $_updatedBy = null;
    private $_approvedDate = null;
    private $_approvedBy = null;
    private $_rejectedDate = null;
    private $_rejectedBy = null;

    private $_deleted = null;
    private $_deleteDate = null;
    private $_deletedBy = null;

    public function __construct ($id = null, $deleted = 0) {
        if($id) {
            $sql = "SELECT a.*, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by,
                        CONCAT(d.first_name,' ',d.last_name) as rejected_by, CONCAT(e.first_name,' ',e.last_name) as approved_by
                    FROM trivia a
                    INNER JOIN users b on b.email = a.created_by
                    INNER JOIN users c on c.email = a.updated_by
                    LEFT JOIN users d on d.email = a.rejected_by
                    LEFT JOIN users e on e.email = a.approved_by
                    WHERE a.deleted = $deleted AND a.id = $id
                    ";
            $row = db_get_row($sql);
            if($row) {
                self::_Init($row, $this);
            }
        }
    }

    private static function _Init ($array, $object = null) {
        if(!$object instanceof self) $object = new self;

        $object->_id = $array['id'];
        $object->_userId = $array["user_id"];
        $object->_dateTime = $array["date_time"];
        $object->_closeDateTime = $array["close_date"];
        $object->_title = $array["title"];
        $object->_description = $array["description"];
        $object->_status = $array["status"];
        $object->_rating = $array["rating"];
        $object->_rejectedReason = $array["rejected_reason"];
        $object->_createDate = $array["create_date"];
        $object->_createdBy = $array["created_by"];
        $object->_updateDate = $array["update_date"];
        $object->_updatedBy = $array["updated_by"];
        $object->_approvedDate = $array["approved_date"];
        $object->_approvedBy = $array["approved_by"];
        $object->_rejectedDate = $array["rejected_date"];
        $object->_rejectedBy = $array["rejected_by"];
        $object->_deleted = $array["deleted"];
        $object->_deleteDate = $array["deleted_date"];
        $object->_deletedBy = $array["deleted_by"];

        return $object;
    }

    public function isOwner(){
        return ($this->_userId == u()->getId()) ? $this->_id : false;
    }
    public function getId() {
        return $this->_id;
    }
    public function getUserId() {
        return $this->_userId;
    }
    public function getDateTime($format = 'm/d/Y') {
        return date($format, strtotime($this->_dateTime));
    }
    public function getCloseDateTime($format = 'm/d/Y') {
        return date($format, strtotime($this->_closeDateTime));
    }
    public function getTitle() {
        return $this->_title;
    }
    public function getDescription() {
        return $this->_description;
    }
    public function getShortDescription($word_count=500) {
        return short_string(strip_tags($this->_description), $word_count);
    }
    public function getStatus() {
        return ucwords(strtolower(str_replace("_", " ",$this->_status)));
    }
    public function getRawStatus() {
        return $this->_status;
    }
    public function getRating() {
        return $this->_rating;
    }
    public function getRejectedReason() {
        return $this->_rejectedReason;
    }
    public function getCreateDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_createDate));
    }
    public function getCreatedBy() {
        return $this->_createdBy;
    }
    public function getUpdateDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_updateDate));
    }
    public function getUpdatedBy() {
        return $this->_updatedBy;
    }
    public function getApprovedDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_approvedDate));
    }
    public function getApprovedBy() {
        return $this->_approvedBy;
    }
    public function getRejectedDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_rejectedDate));
    }
    public function getRejectedBy() {
        return $this->_rejectedBy;
    }
    public function getDeleted() {
        return $this->_deleted;
    }
    public function getDeletedDate($format = 'm/d/Y') {
        return date($format, strtotime($this->_deleteDate));
    }
    public function getDeletedBy() {
        return $this->_deletedBy;
    }

    public function getTotalResults() {
        return self::$_totalResults;
    }
    public function getTotalPages() {
        return self::$_totalPages;
    }

    public function setUserId($user_id) {
        $this->_userId = $user_id;
    }
    public function setDateTime($date_time) {
        $this->_dateTime = ymdhms(is_date($date_time));
    }
    public function setCloseDateTime($date_time) {
        // By default 30th day will be the closing date for the Trivia from the date of creation
        if(!is_date($date_time))
            $date_time = date("m/d/Y H:i:s",strtotime("+30 days", strtotime($date_time)));

        $this->_closeDateTime = ymdhms(is_date($date_time));
    }
    public function setTitle($title) {
        $this->_title = $title;
    }
    public function setDescription($description) {
        $this->_description = $description;
    }
    public function setStatus($status) {
        $this->_status = strtoupper($status);
    }
    public function setRating($rating) {
        $this->_rating = $rating;
    }
    public function setRejectedReason($reason) {
        $this->_rejectedReason = $reason;
    }
    public function setTotalResults($total_results) {
        $this->_totalResults = $total_results;
    }
    public function setTotalPages($total_pages) {
        $this->_totalPages = $total_pages;
    }

    public function checkTitle() {
        $and = '';
        if($this->_id) $and = " AND id != {$this->_id} ";
        $sql = "SELECT id FROM trivia
                WHERE title = " . q($this->_title) . " $and
                ";
        if($rs = db_get_row($sql)){
            return $rs['id'];
        }
        return false;
    }

    public function save(){
        if($this->_id) return $this->_update();
        else return $this->_add();
    }

    private function _add() {
        if($this->_title && $this->_dateTime){
            $sql = "INSERT INTO trivia (user_id, date_time, close_date, title, description, status, create_date, created_by, update_date, updated_by)
                        VALUES (
                            '". n($this->_userId) ."',
                            '". $this->_dateTime ."',
                            '". $this->_closeDateTime ."',
                            ". q($this->_title) . ",
                            ". q($this->_description) . ",
                            '". $this->_status ."',
                            NOW(),
                            '". u()->getEmail()."',
                            NOW(),
                            '". u()->getEmail() ."'
                        )                        
                    "; 
            if(db_execute($sql)){
                $this->_id = db_insert_id();
                return true;
            }
        }
        return false;
    }

    private function _update() {
        if($this->_id) {
            $sql = "UPDATE trivia SET
                        date_time = '". $this->_dateTime ."',
                        close_date = '". $this->_closeDateTime ."',
                        title = ". q($this->_title) . ",
                        description = ". q($this->_description) . ",
                        status = '". $this->_status ."',
                        update_date =  NOW(),
                        updated_by = '". u()->getEmail() ."'
                    WHERE id = $this->_id
                    ";
            if(db_execute($sql)) {
                return true;
            }
        }
        return false;
    }
    public function Delete() {
        if($this->_id) {
            $sql = "UPDATE trivia_responses SET
                        deleted = 1,
                        deleted_date = NOW(),
                        deleted_by = '".u()->getEmail()."'
                        WHERE trivia_id = {$this->_id}
                    ";
            if(db_execute($sql)) {
                $sql = "UPDATE trivia SET
                        deleted = 1,
                        deleted_date = NOW(),
                        deleted_by = '".u()->getEmail()."'
                        WHERE id = {$this->_id}
                    ";
                $str = "Deleted trivia responses under project <i>{$this->_name}({$this->_id})</i>";
                watchdog("SUCCESS", "DELETE", $str);
                if(db_execute($sql)) {
                    return true;
                }
            }
        }
        return false;
    }
    public function Reject() {
        if($this->_id) {
            $sql = "UPDATE trivia SET
                        status = 'REJECTED',
                        rejected_reason = ". q($this->_rejectedReason) . ",
                        rejected_date = NOW(),
                        rejected_by = '".u()->getEmail()."'
                        WHERE id = {$this->_id}
                    ";
            if(db_execute($sql)) {
                return true;
            }
        }
        return false;
    }
    public function Approve() {
        if($this->_id) {
            $sql = "UPDATE trivia SET
                        status = 'OPEN',
                        approved_date =  NOW(),
                        approved_by = '". u()->getEmail() ."'
                        WHERE id = {$this->_id}
                    ";
            $this->_status = 'OPEN';
            if(db_execute($sql)) {
                return true;
            }
        }
        return false;
    }
    public function RateTrivia() {
        if($this->_id) {
            if($this->_rating == null) 
                $this->_rating = 0;

            $sql = "UPDATE trivia SET rating = {$this->_rating} WHERE id = {$this->_id} ";
            if(db_execute($sql)) {
                return true;
            }
        }
        return false;
    }
    public static function GetTriviaList($status = array(), $from_date = null, $to_date = null, $p = 0, $page_limit = 10) {
        $return = array();
        $and = "";
        
        $total_pages=0;
        $total_results=0;
        if(u()->isAdmin()) {
            $and = "";
        }
        else {
            $and .= " AND a.user_id =". u()->getId();
        }
        if($status){
            $and .= " AND a.status IN ('". implode("','",$status)."')";
        }
        if(is_date($from_date) && is_date($to_date)) {
            $from_date = ymd($from_date);
            $to_date = ymd($to_date);
            $and .= " AND (DATE_FORMAT(a.date_time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date')";
        }
        $sql = "SELECT SQL_CALC_FOUND_ROWS a.*,CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by,
                    CONCAT(d.first_name,' ',d.last_name) as rejected_by , CONCAT(e.first_name,' ',e.last_name) as approved_by 
                FROM trivia a
                INNER JOIN users b on b.email = a.created_by
                INNER JOIN users c on c.email = a.updated_by
                LEFT JOIN users d on d.email = a.rejected_by
                LEFT JOIN users e on e.email = a.approved_by
                WHERE a.deleted = 0 $and
                ORDER BY a.date_time DESC ";
        if($p > 0) {
            $sql .= " LIMIT ".(($p-1)*$page_limit).",".$page_limit;
        }
        $rows = db_get_all($sql);
        if($rows) {
            if($p > 0) {
                $sql = "SELECT FOUND_ROWS() AS total";
                $ttl = db_get_row($sql);
                $total_results = $ttl["total"];
                if($total_results > 0) { 
                    self::$_totalResults = $total_results; 
                    $total_pages = ceil($total_results/$page_limit);
                    self::$_totalPages = $total_pages; 
                }
            }
        }
        foreach($rows as $row){
            self::$_Trivia[$row["id"]] = self::_Init($row);
        }
        if(!u()->isAdmin() && $status == "ALL") {
        } 
        return self::$_Trivia;
    }

    public static function GetUserTriviaList($status = array(), $from_date = null, $to_date = null, $p = 0, $page_limit = 10) {
        $return = array();
        $and = "";
        
        $total_pages=0;
        $total_results=0;
        if(u()->isAdmin()) {
            $and = "";
        }
        else {
            $and .= " AND a.user_id =". u()->getId();
        }
        if($status){
            $and .= " AND a.status IN ('". implode("','",$status)."')";
        }
        if(is_date($from_date) && is_date($to_date)) {
            $from_date = ymd($from_date);
            $to_date = ymd($to_date);
            $dt_and = " AND (DATE_FORMAT(a.date_time, '%Y-%m-%d') BETWEEN '$from_date' AND '$to_date')";
        }
        $sql = "( SELECT a.id, a.user_id,a.date_time,a.title,a.description,a.status,a.rating,a.rejected_reason,a.create_date,a.update_date,a.close_date, 
                    CONCAT( b.first_name, ' ', b.last_name ) AS created_by, CONCAT( c.first_name, ' ', c.last_name ) AS updated_by, 
                    CONCAT( d.first_name, ' ', d.last_name ) AS rejected_by , CONCAT(e.first_name,' ',e.last_name) as approved_by
                    FROM trivia a
                    INNER JOIN users b ON b.email = a.created_by
                    INNER JOIN users c ON c.email = a.updated_by
                    LEFT JOIN users d ON d.email = a.rejected_by
                    LEFT JOIN users e ON e.email = a.approved_by
                    WHERE a.deleted =0 $and $dt_and
                    ORDER BY a.date_time DESC 
                )
            UNION (
                SELECT a.id, a.user_id,a.date_time,a.title,a.description,a.status,a.rating,a.rejected_reason,a.create_date,a.update_date,a.close_date,
                    CONCAT( b.first_name, ' ', b.last_name ) AS created_by, CONCAT( c.first_name, ' ', c.last_name ) AS updated_by, 
                    CONCAT( d.first_name, ' ', d.last_name ) AS rejected_by , CONCAT(e.first_name,' ',e.last_name) as approved_by
                FROM trivia a
                INNER JOIN users b ON b.email = a.created_by
                INNER JOIN users c ON c.email = a.updated_by
                LEFT JOIN users d ON d.email = a.rejected_by
                LEFT JOIN users e ON e.email = a.approved_by
                WHERE a.deleted =0 AND a.status = 'OPEN' AND a.user_id != ".u()->getId()." $dt_and 
                ORDER BY a.date_time DESC
            )
        ";
        $table = 'tmp_trivia_' . md5(uniqid(mt_rand(), true));
        $sql = " CREATE TEMPORARY TABLE {$table} $sql";
        db_execute($sql);

        $sql = "SELECT SQL_CALC_FOUND_ROWS * FROM $table ORDER BY date_time DESC";
        if($p > 0) {
            $sql .= " LIMIT ".(($p-1)*$page_limit).",".$page_limit;
        }
        $rows = db_get_all($sql);
        if($rows) {
            if($p > 0) {
                $sql = "SELECT FOUND_ROWS() AS total";
                $ttl = db_get_row($sql);
                $total_results = $ttl["total"];
                if($total_results > 0) { 
                    self::$_totalResults = $total_results; 
                    $total_pages = ceil($total_results/$page_limit);
                    self::$_totalPages = $total_pages; 
                }
            }
        }
        foreach($rows as $row){
            self::$_Trivia[$row["id"]] = self::_Init($row);
        }
        if(!u()->isAdmin() && $status == "ALL") {
        } 
        return self::$_Trivia;
    }


    public function getOpen() {
        $this->_loadUsersTrivia('OPEN');
        return $this->_Trivia;
    }
    public function getClosed() {
        $this->_loadUsersTrivia('CLOSED');
        return $this->_Trivia;
    }
    public function getQueued() {
        $this->_loadUsersTrivia('QUEUED');
        return $this->_Trivia;
    }
    public function getRejected() {
        $this->_loadUsersTrivia('REJECTED');
        return $this->_Trivia;
    }
    public function getDrafts() {
        $this->_loadUsersTrivia('DRAFT');
        return $this->_Trivia;
    }
    private function _loadUsersTrivia($status = 'DRAFT') {
        $and = "";
        if(!u()->isAdmin()) {
            $and .= " AND a.user_id =".u()->getId();
        } 
        $sql = "SELECT a.*, CONCAT(b.first_name,' ',b.last_name) as created_by, CONCAT(c.first_name,' ',c.last_name) as updated_by,
                    CONCAT(d.first_name,' ',d.last_name) as rejected_by , CONCAT(e.first_name,' ',e.last_name) as approved_by
                FROM trivia a
                INNER JOIN users b on b.email = a.created_by
                INNER JOIN users c on c.email = a.updated_by
                LEFT JOIN users d on d.email = a.rejected_by
                LEFT JOIN users e on e.email = a.approved_by
                WHERE a.deleted = 0 AND a.status = '$status' $and
                ORDER BY a.date_time DESC
                ";
        $rows = db_get_all($sql);
        foreach($rows as $row){
            self::$_Trivia[$row["id"]] = self::_Init($row);
        }
    }

    public function getTotalResponses() {
        $total = 0;
        if($this->_id) {
            $sql = "SELECT count(id) as total FROM trivia_responses WHERE parent_id = 0 AND trivia_id = {$this->_id}";
            $row = db_get_row($sql);
            if($row)
                $total = $row["total"];
        }
        return $total;
    }

    public function isClosingDate() {
        if($this->_id) {
            $today_ts = time(); 
            $today = ymdhms($today_ts);

            if(!is_date($this->_closeDateTime)) {
                $this->_closeDateTime = date("Y-m-d H:i:s", strtotime("+30 days", strtotime($this->_dateTime)));
            }
            $close_date = ymdhms(is_date($this->_closeDateTime));
            $close_date_ts = strtotime($close_date);
            if($today_ts >= $close_date_ts) { 
                return true;
            }
        }
        return false;
    }

    public function closeTrivia ($id, $close_date) {
        if(!is_date($close_date)) {
            $close_date = date("m/d/Y H:i:s",time());
        }
        $close_date = ymdhms(is_date($close_date));

        $sql = "UPDATE trivia SET close_date = '$close_date', status='CLOSED' WHERE id='$id' LIMIT 1";
        debug($sql);
        return db_execute($sql);
    }

}
?>
