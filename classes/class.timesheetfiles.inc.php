<?php

include_once('class.client.inc.php');
include_once('class.project.inc.php');
include_once('class.timesheetfile.inc.php');

class TimesheetFiles {

    public static function GetAll($include_file_version = false, $is_public = true){
        $project_ids_clause = "";
        if(!u()->isAdmin()) {
            $project_ids = u()->getProjectIds();
            $project_ids_clause = " AND a.project_id IN (".implode(", ", $project_ids).")";
        }
        
        $sql = "(
                 SELECT 
                    a.id, a.category_id,a.other_category_name, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version, a.mime_type,a.notes,
                    a.is_public, a.created_by,a.deleted_by,a.parent_id,
                    UNIX_TIMESTAMP( a.upload_date ) AS upload_date, c.name AS category_name, d.name AS client_name, 
                    e.name AS project_name, f.id AS parent_project_id, f.name AS parent_project_name 
                FROM files a
                LEFT JOIN category c ON c.id = a.category_id
                INNER JOIN clients d ON d.id = a.client_id
                LEFT JOIN projects e ON e.id = a.project_id
                LEFT JOIN projects f ON f.id = e.parent_id
                WHERE a.parent_id = 0 AND a.deleted = 0 $project_ids_clause AND  a.is_public = $is_public
                ORDER BY a.id DESC
                )
                UNION (
                 SELECT 
                    a.id, a.category_id,a.other_category_name, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version, a.mime_type,a.notes,
                    a.is_public, a.created_by,a.deleted_by,a.parent_id,
                    UNIX_TIMESTAMP( a.upload_date ) AS upload_date, c.name AS category_name, d.name AS client_name, 
                    e.name AS project_name, f.id AS parent_project_id, f.name AS parent_project_name 
                FROM files a
                LEFT JOIN category c ON c.id = a.category_id
                INNER JOIN clients d ON d.id = a.client_id
                LEFT JOIN projects e ON e.id = a.project_id
                LEFT JOIN projects f ON f.id = e.parent_id
                WHERE a.parent_id = 0 AND a.deleted = 0 AND  a.is_public = $is_public
                ORDER BY a.id DESC
                )
            ";
        foreach(db_get_all($sql) as $file){
            $return[$file['id']] = TimesheetFile::Create($file, null, $include_file_version);
        }

        return $return;
    }

    public static function GetAllByDocumentType($doc_type='PROJECT_DOCUMENT', $include_file_version = false, $is_public = true){
        $project_ids_clause = "";
        
        $sql = "
             SELECT 
                a.id, a.category_id, a.other_category_name, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version, a.mime_type,a.notes,
                a.is_public, a.created_by,a.deleted_by,a.parent_id,
                UNIX_TIMESTAMP( a.upload_date ) AS upload_date, c.name AS category_name, d.name AS client_name, 
                e.name AS project_name, f.id AS parent_project_id, f.name AS parent_project_name 
            FROM files a
            LEFT JOIN category c ON c.id = a.category_id
            INNER JOIN clients d ON d.id = a.client_id
            LEFT JOIN projects e ON e.id = a.project_id
            LEFT JOIN projects f ON f.id = e.parent_id
            WHERE a.parent_id = 0 AND a.deleted = 0 AND a.doc_type = '$doc_type' AND a.is_public = $is_public
            ORDER BY a.id DESC
            ";
        foreach(db_get_all($sql) as $file){
            $return[$file['id']] = TimesheetFile::Create($file);
        }

        return $return;
    }
    public static function GetByClient($client, $include_children = false, $include_file_version = false){
        if($client instanceof Client) $client_id = $client->getId();
        else $client_id = intval($client);
        
        if($include_children) $project_ids_clause = "";
        else $project_ids_clause = "AND a.project_id = '0'";

        $return = array();
        if($client_id){
            $sql = "
                 SELECT 
                    a.id, a.category_id, a.other_category_name, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version, a.mime_type,a.notes,
                    a.is_public, a.created_by,a.deleted_by,a.parent_id,
                    UNIX_TIMESTAMP( a.upload_date ) AS upload_date, c.name AS category_name, d.name AS client_name, 
                    e.name AS project_name, f.id AS parent_project_id, f.name AS parent_project_name 
                FROM files a
                LEFT JOIN category c ON c.id = a.category_id
                INNER JOIN clients d ON d.id = a.client_id
                LEFT JOIN projects e ON e.id = a.project_id
                LEFT JOIN projects f ON f.id = e.parent_id
                WHERE a.parent_id = 0 AND a.deleted = 0 AND a.client_id ='$client_id'
                ORDER BY a.id DESC
                ";
            
            foreach(db_get_all($sql) as $file){
                $return[$file['id']] = TimesheetFile::Create($file,null, $include_file_version);
            }
        }

        return $return;
    }

    public static function GetByProject($project, $include_children = false, $include_file_version = false){
        if($project instanceof Project) $project_id = $project->getId();
        else $project_id = intval($project);

        if($include_children) $project_ids_clause = "";
        else $project_ids_clause = "OR c.parent_id = '{$project_id}'";
        
        $return = array();
        if($project_id){
            if($include_children) {
                $sql = "
                         SELECT 
                            a.id, a.category_id, a.other_category_name, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version, a.mime_type, 
                            a.notes,a.is_public, a.created_by,a.deleted_by,
                            UNIX_TIMESTAMP( a.upload_date ) AS upload_date, c.name AS category_name, d.name AS client_name, e.name AS project_name, 
                            f.id AS parent_project_id, f.name AS parent_project_name
                        FROM files a
                        INNER JOIN category c ON c.id = a.category_id
                        INNER JOIN clients d ON d.id = a.client_id
                        LEFT JOIN projects e ON e.id = a.project_id
                        LEFT JOIN projects f ON f.id = e.parent_id
                        WHERE a.parent_id = 0 AND a.deleted = 0 AND (a.project_id ='$project_id' OR e.parent_id ='$project_id')
                        ORDER BY a.id DESC
                        ";
            }
            else {
                $sql = "
                         SELECT 
                            a.id, a.category_id, a.other_category_name, a.client_id, a.project_id, a.md5_name, a.file_name, a.file_size, a.file_version, a.mime_type,a.notes,
                            a.is_public, a.created_by,a.deleted_by,a.parent_id,
                            UNIX_TIMESTAMP( a.upload_date ) AS upload_date, c.name AS category_name, d.name AS client_name, 
                            e.name AS project_name, f.id AS parent_project_id, f.name AS parent_project_name 
                        FROM files a
                        LEFT JOIN category c ON c.id = a.category_id
                        INNER JOIN clients d ON d.id = a.client_id
                        LEFT JOIN projects e ON e.id = a.project_id
                        LEFT JOIN projects f ON f.id = e.parent_id
                        WHERE a.parent_id = 0 AND a.deleted = 0 AND a.project_id ='$project_id'
                        ORDER BY a.id DESC
                        ";
            }
            $files = db_get_all($sql);
            foreach($files as $file)
                $list[$file['category_name']][$file['id']] = $file;

            if(is_array($list) && count($list) > 0) {
                ksort($list);
                foreach($list as $cat_name => $file_list) {
                    foreach ($file_list as $file) {
                        $return[$file['id']] = TimesheetFile::Create($file, null, $include_file_version);
                    }
                }
            }
            
        }

        return $return;
    }

    public static function Get($id){
        return new TimesheetFile($id);
    }

    public static function SaveUploadedFiles($id = 0,$user_name, $category, $client, $project = null, $notes = null, $is_public = 0, $var = "file",$type= 'PROJECT_DOCUMENT', $faction = null, $other_category_name = null){
        $category_id = 0;
        $client_id = 0;
        $project_id = 0;

        if($category){
            if($category instanceof Category) $category_id = $category->getId();
            else $category_id = pintval($category);
        }

        if(strtoupper($type) == 'PROJECT_DOCUMENT') {
            if($client instanceof Client) $client_id = $client->getId();
            else $client_id = intval($client);

            if($client_id < 1) return false;

            if($project){
                if($project instanceof Project) $project_id = $project->getId();
                else $project_id = pintval($project);
            }
        }
        $notes = addslashes($notes);
        $files = array();
        foreach($_FILES[$var]['error'] as $k => $v){
            if(!$v){
                $files[] = array(
                    'name' => addslashes($_FILES[$var]['name'][$k]),
                    'type' => $_FILES[$var]['type'][$k],
                    'tmp_name' => $_FILES[$var]['tmp_name'][$k],
                    'size' => $_FILES[$var]['size'][$k],
                );
            }
        }

        foreach($files as $file){
            echo '==='.$other_category_name;
            $timesheet_file = new TimesheetFile($id);
            $timesheet_file->setCreatedBy($user_name);
            $timesheet_file->setCategoryId($category_id);
            $timesheet_file->setOtherCategoryName($other_category_name);
            $timesheet_file->setClientId($client_id);
            $timesheet_file->setProjectId($project_id);
            $timesheet_file->setNotes($notes);
            $timesheet_file->setIsPublic($is_public);
            $timesheet_file->setDocType($type);
            $timesheet_file->setFileAction($faction);
            $timesheet_file->setFile($file);
            if($timesheet_file->save()) {
                $str = "File <i>{$file["name"]}</i> saved.";
                s("File {$file["name"]} saved.");
                watchdog('SUCCESS','FILE', $str);
            }
            else { 
                $str = "Failed to save file <i>{$file["name"]}</i>.";
                e("Failed to save file {$file["name"]}.");
                watchdog('FAILED','FILE', $str);
            }
        }
    }

    public static function DeleteFiles($id,$user_name){
        #foreach($file_ids as $id){
            $file = new TimesheetFile($id);
            $file->setDeletedBy($user_name);
            if($file->delete()) {
                s("File {$file->getFileName()} deleted.");
                $str = "File <i>{$file->getFileName()}</i> deleted.";
                watchdog('SUCCESS','FILE', $str);
            }
            else { 
                e("Failed to delete {$file->getFileName()}");
                $str = "Failed to delete <i>{$file->getFileName()}</i>";
                watchdog('FAILED','FILE', $str);
            }
        #}
    }
    
    public static function SortByAssociations($array){
    	$return = array();
    	foreach($array as $v){
    		$names = array($v->getClientName());
    		if($v->getParentProjectName()) $names[] = $v->getParentProjectName();
    		$names[] = $v->getProjectName();
    		
    		$return[implode(' / ', $names)][] = $v;
    	}
    	
    	uksort($return, "strnatcasecmp");
    	
    	return $return;
    }
}

