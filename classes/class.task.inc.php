<?php

include_once("class.project.inc.php");
include_once("class.tasktype.inc.php");

class Task {

    private $_id = null;
    private $_userId = null;
    private $_userName = null;
    private $_user = null;
    private $_clientId = null;
    private $_clientName = null;
    private $_projectId = null;
    private $_projectName = null;
    private $_project = null;
    private $_detail = null;
    private $_taskDate = null;
    private $_taskStatus = null;
    private $_durationMins = null;
    private $_projectParentId = null;
    private $_taskTypeId = null;
    private $_taskTypeName = null;
    private $_projectRoleId = null;
    private $_mainProjectId = null;
    private $_billable = null;
    private static $_Status = array("PENDING", "SUBMITTED", "APPROVED", "REVIEW");
    private static $_Sql = "
            SELECT 
                a.id, a.user_id, a.project_id, a.project_role_id, a.billable, c.parent_id AS project_parent_id, c.client_id, a.detail, UNIX_TIMESTAMP(a.task_date) AS task_date,
                a.duration_mins, a.task_type_id, a.task_status, b.first_name, b.middle_name, b.last_name,  c.name AS project_name, d.name as client_name
            FROM tasks a
            INNER JOIN users b ON b.id = a.user_id
            INNER JOIN projects c ON c.id = a.project_id
            INNER JOIN clients d ON d.id = c.client_id
            LEFT JOIN employee_project_role e ON e.id = a.project_role_id
        ";

    public function __construct($id = null, $uid = false) {
        $id = pintval($id);
        if ($id > 0) {
            $user = u();
            if (!$uid)
                if (!$user->isAdmin())
                    $and = "AND b.id = {$user->getId()}";
            $sql = self::$_Sql . "
                    WHERE a.id = {$id}
                    {$and}
                ";
        }

        $this->_taskStatus = self::$_Status[0];
        $row = db_get_row($sql);
        if ($row)
            self::_Init($row, $this);
    }

    private static function _Init($array, $object = null) {
        if (!$object instanceof self)
            $object = new self;
        $object->_id = $array["id"];
        $object->_userId = $array["user_id"];
        $object->_userName = concat(" ", $array["first_name"], $array["middle_name"], $array["last_name"]);
        $object->_clientId = $array["client_id"];
        $object->_clientName = $array["client_name"];
        $object->_projectId = $array["project_id"];
        $object->_projectName = $array["project_name"];
        $object->_projectRoleId = $array["project_role_id"];
        $object->_billable = $array["billable"];
        if (isset($array["detail"]) && strlen($array['detail'])) {
            $detail = $array['detail'];
            $detail = htmlspecialchars_decode($detail);
        } else {
            $detail = '';
        }
        $object->_detail = $detail;
        $object->_taskDate = $array["task_date"];
        $object->_taskStatus = $array["task_status"];
        $object->_durationMins = $array["duration_mins"];
        $object->_projectParentId = $array["project_parent_id"];
        $object->_mainProjectId = Project::GetMainProject($array["project_parent_id"]);

        $task_type = TaskType::Get($array["task_type_id"]);
        $object->_taskTypeId = $task_type->getId();
        $object->_taskTypeName = $task_type->getName();

        return $object;
    }

    public function getId() {
        return $this->_id;
    }

    public function getUserId() {
        return $this->_userId;
    }

    public function getUserName() {
        return $this->_userName;
    }

    public function getClientId() {
        return $this->_clientId;
    }

    public function getClientName() {
        return $this->_clientName;
    }

    public function getProjectId() {
        return $this->_projectId;
    }

    public function getProjectName() {
        return $this->_projectName;
    }

    public function getProjectRoleId() {
        return $this->_projectRoleId;
    }

    public function getBillable() {
        return $this->_billable;
    }

    public function getDetail() {
        return stripslashes($this->_detail);
    }

    public function getTaskDate() {
        return $this->_taskDate;
    }

    public function getTaskStatus() {
        return $this->_taskStatus;
    }

    public function isSubmitted() {
        return $this->_submitStatus == "SUBMITTED";
    }

    public function isPending() {
        return !$this->_submitStatus;
    }

    public function isApproved() {
        return $this->_submitStatus == "APPROVED";
    }

    public function getDurationMins() {
        return $this->_durationMins;
    }

    public function getProjectParentId() {
        return $this->_projectParentId;
    }

    public function getMainProjectId() {
        return $this->_mainProjectId;
    }

    public function isSubprojectTask() {
        return (bool) $this->_projectParentId;
    }

    public function getTaskTypeId() {
        return $this->_taskTypeId;
    }

    public function getTaskTypeName($italicize_default = false) {
        if ($italicize_default && $this->_taskTypeId < 1)
            return "<i>{$this->_taskTypeName}</i>";
        else
            return $this->_taskTypeName;
    }

    public function getUser() {
        
    }

    public function getProject() {
        if (is_null($this->_project))
            $this->_project = new Project($this->_projectId);
        return $this->_project;
    }

    public function setDetail($detail) {

        if (strlen(trim($detail)) > 0)
        //$this->_detail =str_replace("'","",str_replace('"',"",$detail));
            $this->_detail = $detail;
        else
            $this->_detail = "";
    }

    public function setProjectId($project, $subproject = null) {
        if ($subproject) {
            if ($subproject instanceof Project)
                $project_id = $subproject->getId();
            else
                $project_id = pintval($subproject);
        }

        if ($project_id < 1) {
            if ($project instanceof Project)
                $project_id = $project->getId();
            else
                $project_id = pintval($project);
        }

        if ($project_id > 0)
            $this->_projectId = $project_id;
    }

    public function setProjectRoleId($projectRoleId) {
        $this->_projectRoleId = $projectRoleId;
    }

    public function setBillable($billable) {
        $this->_billable = $billable;
    }

    public function setTaskDate($date) {
        $this->_taskDate = is_date($date);
    }

    public function setDurationMins($duration, $convert_to_minutes = true) {
        if ($convert_to_minutes)
            $this->_durationMins = hm2m($duration);
        else
            $this->_durationMins = $duration;
    }

    public function setTaskStatus($status) {
        if (in_array($status, self::$_Status))
            $this->_taskStatus = $status;
        else
            $this->_taskStatus = self::$_Status[0];
    }

    public function setTaskTypeId($id) {
        if ($id) {
            $task_type = TaskType::Get($id);
            $this->_taskTypeId = $task_type->getId();
        } else {
            $this->_taskTypeId = "-1";
        }
    }

    public function save($user_id = false) {

        if ($this->_id)
            return $this->_update($user_id);
        else
            return $this->_add($user_id);
    }

    private function _add($user_id = false) {

        if ($user_id) {
            $this->_userId = $user_id;
        } else {
            $this->_userId = u()->getId();
        }
        if (!$this->_taskTypeId)
            $this->_taskTypeId = -1;

        if ($this->_taskDate && $this->_durationMins && $this->_userId && $this->_projectId) {
            $detail = $this->_detail;
            $detail = addslashes($detail);

          echo  $sql = "
                    INSERT INTO tasks (user_id, project_id, project_role_id, billable,  detail, task_type_id, task_date, task_status, duration_mins)
                    VALUES (
                        {$this->_userId},
                        {$this->_projectId},
                        {$this->_projectRoleId},  
                        {$this->_billable},
                        '" . $detail . "',
                        " . $this->_taskTypeId . ",
                        '" . ymd($this->_taskDate) . "',
                        '{$this->_taskStatus}',
                        {$this->_durationMins}
                    )
                    ON DUPLICATE KEY UPDATE
                    id = LAST_INSERT_ID(id),
                    duration_mins = duration_mins + {$this->_durationMins},
                    detail = IF(detail IS NOT NULL AND TRIM(detail) != '', CONCAT_WS('\n\n', detail, '" . $detail . "'), '" . $detail . "')
                ";

            //debug($sql);
            //exit;
            if (db_execute($sql)) {
                $this->_taskDate = strtotime($this->_taskDate);
                $this->_id = db_insert_id();
                return $this->_id;
            }
        }

        return false;
    }

    private function _update($user_id = false) {

        if (!$this->_taskTypeId)
            $this->_taskTypeId = -1;

        if ($this->_taskDate && $this->_durationMins && $this->_userId && $this->_projectId) {
            $user = new User($user_id);
            if ($user->getId() < 1)
                $user = u();

            //if(!$user->isAdmin()) 
            if ($user_id)
                $and = "AND user_id = {$user->getId()}";

            $detail = $this->_detail;
            $detail = htmlspecialchars($detail);
            $detail = addslashes($detail);

           echo $sql = "
                    UPDATE tasks SET
                        project_id = {$this->_projectId},
                        project_role_id = {$this->_projectRoleId},
                        billable = {$this->_billable},
                        detail = '" . $detail . "',
                        task_type_id = " . n($this->_taskTypeId) . ",
                        task_date = '" . ymd($this->_taskDate) . "',
                        task_status = '{$this->_taskStatus}',
                        duration_mins = {$this->_durationMins}
                    WHERE id = {$this->_id}
                    {$and}
                ";
            $return = db_execute($sql);

            if ($return)
                return $return;
            else {
                // Update failed. Try creating a new task with same parameters.
                // This is to cover a case where the user wants to change task type for an existing task.
                // If creation succeeds, we will delete (HARD DELETE) the current task.
                $new_task = new self();
                $new_task->setProjectId($this->_projectId);
                $new_task->setTaskDate($this->_taskDate);
                $new_task->setTaskTypeId($this->_taskTypeId);
                $new_task->setDetail($this->_detail);
                $new_task->setDurationMins($this->_durationMins, false);

                if ($new_task->save()) {
                    $this->_hardDelete();
                    return true;
                } else
                    return false;
            }
        }

        return false;
    }

    private function _hardDelete() {
        if ($this->_id > 0) {
            $user = u();
            if (!$user->isAdmin())
                $and = "AND user_id = {$user->getId()}";

            $sql = "
                    DELETE FROM tasks
                    WHERE id = {$this->_id}
                    {$and}
                ";

            return db_execute($sql);
        }

        return false;
    }

    public function delete($user_id = false, $task_id = false) {

        if ($task_id)
            $task_id = $task_id;
        else
            $task_id = $this->_id;

        if ($task_id > 0) {
            $sql = "
                    DELETE FROM tasks WHERE id = {$task_id}
                ";
            return db_execute($sql);
        }

        return false;
    }

    private function _getSubmittedClause($user) {
        if ($user instanceof User)
            $user_id = $user->getId();
        else
            $user_id = intval($user);

        $date = is_date($date);
        $year = date("Y", $date);
        $week = intval(date("W", $date));


        if (u()->getId() == $user_id)
            return "";
        else
            return $s;
    }

    public static function GetTasks(&$params = null) {
        $return = array();

        $user = u();

        // Parse params
        $user_clause = "";
        $project_clause = "";
        $client_clause = "";
        $date_clause = "";

        if ($user->isAdmin()) {
            $user_id = pintval($params["user_id"]);
            if ($user_id > 0)
                $user_clause = "AND a.user_id = {$user_id}";
        } else
            $user_clause = "AND a.user_id = {$params["user_id"]}";

        $client_id = pintval($params["client_id"]);
        if ($client_id)
            $client_clause = "AND c.client_id = {$client_id}";

        $project_id = pintval($params["project_id"]);
        if ($project_id)
            $project_clause = "AND a.project_id = {$project_id}";

        $from = is_date($params["from"]);
        $to = is_date($params["to"]);
        $month_year = $params["month_year"];

        if ($from) {
            if ($to)
                $date_clause = "AND a.task_date BETWEEN '" . ymd($from) . "' AND '" . ymd($to) . "'";
            else
                $date_clause = "AND a.task_date >= '" . ymd($from) . "'";
        }
        if ($month_year) {
            $date_clause = "AND date_format(a.task_date, '%b-%Y') = '" . $month_year . "'";
        }
        $params = array(
            "user_id" => $user_id,
            "client_id" => $client_id,
            "project_id" => $project_id,
            "from" => $from,
            "to" => $to,
            "month_year" => $month_year
        );
        $sql = self::$_Sql . "
                WHERE a.id > 0
                {$user_clause}
                {$project_clause}
                {$client_clause}
                {$date_clause}
                ORDER BY a.task_date DESC
            ";

        $rows = db_get_all($sql);
        foreach ($rows as $row)
            $return[$row["id"]] = self::_Init($row);

        return $return;
    }

    public static function GetTasksByProject($project) {
        if ($project instanceof Project)
            $project_id = $project->getId();
        else
            $project_id = pintval($project);

        $return = array();

        if ($project_id > 0) {
            $user = u();
            if (!$user->isAdmin())
                $and = "AND b.id = {$user->getId()}";
            $sql = self::$_Sql . "
                    WHERE a.id > 0
                    {$and}
                    AND a.project_id = {$project_id}
                    ORDER BY a.task_date DESC
                ";

            $rows = db_get_all($sql);
            foreach ($rows as $row)
                $return[$row["id"]] = self::_Init($row);
        }

        return $return;
    }

    public static function GetTasksByUser($user, $project = null, $start_date = null, $end_date = null) {
        if ($user instanceof User)
            $user_id = $user->getId();
        else
            $user_id = pintval($user);

        $return = array();

        $where_project = "";
        if ($project) {
            if ($project instanceof Project)
                $project_id = $project->getId();
            else
                $project_id = pintval($project);
            if ($project_id > 0)
                $where_project = "AND a.project_id = {$project_id}";
        }

        if ($start_date && $end_date)
            $where_date_range = "AND task_date BETWEEN '{$start_date}' AND '{$end_date}'";
        else
            $where_date_range = "";

        if ($user_id > 0) {
             $sql = self::$_Sql . "
                    WHERE a.id > 0
                    AND a.user_id = {$user_id}
                     {$where_project}
                    {$where_date_range}
                    ORDER BY a.task_date DESC
                ";

            $rows = db_get_all($sql);
            foreach ($rows as $row)
                $return[$row["id"]] = self::_Init($row);
        }

        return $return;
    }

    public static function GetTasksByUserAndProject($user, $project) {
        return self::GetTasksByUser($user, $project);
    }

    public static function getIsoWeeksInYear($year) {
        $date = new DateTime;
        $date->setISODate($year, 53);
        return ($date->format("W") === "53" ? 53 : 52);
    }

    public static function getWeek($date) {
        $week = date('W', strtotime($date));
        $day = date('N', strtotime($date));
        $max_weeks = self::getIsoWeeksInYear(date('Y', strtotime($date)));

        if ($day == 7 && $week != $max_weeks && $week != 53) {
            return ltrim( ++$week, '0');
        } elseif ($day == 7) {
            return 1;
        } else {
            return ltrim($week, '0');
        }
    }

    public static function GetTimecardByUser($user = null, $date = null, $manager_id = null) {
        if (is_null($user))
            $user = u();

        if (!$user instanceof User)
            $user = new User($user);
        if ($user->getId() < 1)
            return array();

        $days = get_dates_of_month($date, false, true);

        $return = array();
        $wi = 0;
        foreach ($days as $day) {

            if ($wi % 7 == 0 || $wi == 0) {

                $w = self::getWeek(date('Y-m-d', $day));

                $return[$w]["week"] = $w;
                $return[$w]["from"] = $day;
            }

            $return[$w]["to"] = $day;
            $return[$w]["total"] = 0;

            $return[$w]["times"][$day] = array();
            $return[$w]["timecard_data"][$day] = array();
            $return[$w]["timecard_submitted"] = false;

            $wi++;
        }
        // echo '<pre>'; print_r($return); echo '</pre>';
        $end_date = ymd(end($days));
        $start_date = ymd(reset($days));

        $sql_and = '';
        if (!u()->isAdmin() && ($user->getId() != $manager_id)) {
            $project_sql = "SELECT d.project_id FROM project_users d 
												WHERE d.project_id > 0
												AND d.user_id = $manager_id
												AND d.role = 'MANAGER'
											 )";
        } else {
            $project_sql = "SELECT d.project_id FROM project_users d 
												WHERE d.project_id > 0 
                                                AND d.project_id != 1000
												AND d.user_id = {$user->getId()}
											 ";
        }

        $project_ids = array();
        $rs = db_get_all($project_sql);
        if (count($rs)) {
            $ids = array();
            foreach ($rs as $v)
                $ids[] = $v['project_id'];
            $project_ids = implode(',', $ids);
        }

        if (count($project_ids))
            $sql_and = "AND project_id IN(	$project_ids )";

        $sql = "
                SELECT UNIX_TIMESTAMP(task_date) AS task_date, SUM(duration_mins) as duration_mins,task_status
                FROM tasks
                WHERE id > 0
                AND user_id = {$user->getId()}
				" . $sql_and . "
                AND task_date BETWEEN '{$start_date}' AND '{$end_date}'
                GROUP BY task_date
                ORDER BY task_date
            ";
        $rs = db_get_all($sql);
        if ($rs) {
            foreach ($rs as $v) {
                $w = date("W", $v["task_date"]);
                if (date("N", $v["task_date"]) == 7) {
                    $w += 1;
                }

                $w = self::getWeek(date('Y-m-d', $v["task_date"]));

                $v["task_date"] = strtotime(date('Y-m-d', $v["task_date"]));
                $return[$w]["times"][$v["task_date"]] = array('project_task_date' => $v['task_date'],
                    'project_duration_mins' => $v['duration_mins'],
                    'task_status' => $v['task_status'],
                );
                if ($v["duration_mins"] > 0)
                    $return[$w]["total"] += $v["duration_mins"];
            }
        }

        $sql_and = "AND project_id = 1000";
        $sql = "
                SELECT UNIX_TIMESTAMP(task_date) AS task_date, SUM(duration_mins) as duration_mins,task_status
                FROM tasks
                WHERE id > 0
                AND user_id = {$user->getId()}
				" . $sql_and . "
                AND task_date BETWEEN '{$start_date}' AND '{$end_date}'
                GROUP BY task_date
                ORDER BY task_date
            ";
        $rs = db_get_all($sql);
        if ($rs) {
            foreach ($rs as $v) {
                $w = date("W", $v["task_date"]);

                if (date("N", $v["task_date"]) == 7) {
                    $w += 1;
                }

                $w = self::getWeek(date('Y-m-d', $v["task_date"]));

                $v["task_date"] = strtotime(date('Y-m-d', $v["task_date"]));
                $return[$w]["times"][$v["task_date"]] += array('pto_task_date' => $v['task_date'],
                    'pto_duration_mins' => $v['duration_mins'],
                    'task_status' => $v['task_status'],
                );
                if ($v["duration_mins"] > 0)
                    $return[$w]["pto_total"] += $v["duration_mins"];
            }
        }
        $sql = "SELECT a.id, UNIX_TIMESTAMP(a.timecard_date) as timecard_date, a.billable_minutes, a.pto_minutes, a.total_minutes,
                        a.unassigned_minutes, a.is_holiday, a.status
                    FROM timecard_data a
                    WHERE user_id = {$user->getId()}
                    AND timecard_date BETWEEN '{$start_date}' AND '{$end_date}'
                    GROUP BY timecard_date
                    ORDER BY timecard_date
                ";
        $rs = db_get_all($sql);
        if ($rs) {
            foreach ($rs as $v) {
                $w = date("W", $v["timecard_date"]);

                if (date("N", $v["timecard_date"]) == 7) {
                    $w += 1;
                }
                $w = self::getWeek(date('Y-m-d', $v["timecard_date"]));
                $v["timecard_date"] = strtotime(date('Y-m-d', $v["timecard_date"]));
                $return[$w]["timecard_data"][$v["timecard_date"]] += array('timecard_date' => $v['timecard_date'],
                    'billable_minutes' => $v['billable_minutes'],
                    'pto_minutes' => $v['pto_minutes'],
                    'total_minutes' => $v['total_minutes'],
                    'unassigned_minutes' => $v['unassigned_minutes'],
                    'is_holiday' => $v['is_holiday'],
                    'status' => $v['status'],
                    'myDate' => date('Y-m-d', $v['timecard_date'])
                );


                if ($v['status'] == 'SUBMITTED')
                    $return[$w]["timecard_submitted"] = true;
            }
        }

        return $return;
    }

    public static function GetTasksReports(&$params = null) {
        $return = array();

        $user = u();

        // Parse params
        $user_clause = "";
        $project_clause = "";
        $subproject_clause = "";
        $client_clause = "";
        $date_clause = "";

        if ($user->isAdmin()) {
            $user_id = pintval($params["user_id"]);
            if ($user_id > 1)
                $user_clause = "AND a.user_id = {$user_id}";
        }
        else if ($params["user_id"] == 0) {
            //$user_clause = "AND a.user_id = {$user->getId()}";
        } else {
            $user_clause = "AND a.user_id = {$params["user_id"]}";
        }
        $user_clause_mem = "AND a.user_id = {$user->getId()}";
        $client_id = pintval($params["client_id"]);
        if ($client_id)
            $client_clause = "AND c.client_id = {$client_id}";



        if ($params["project_id"] && !$params["subproject_id"]) {
            $project_id = pintval($params["project_id"]);
            $project_clause = "AND (c.parent_id = {$project_id} OR a.project_id = {$project_id})";
        } else if ($params["project_id"] && $params["subproject_id"]) {
            $project_id = pintval($params["project_id"]);
            $subproject_id = pintval($params["subproject_id"]);
            $subproject_clause = "AND c.parent_id = {$project_id}";
            $project_clause = "AND a.project_id = {$subproject_id}";
        }

        $from = is_date($params["from"]);
        $to = is_date($params["to"]);
        $month_year = $params["month_year"];

        if (strlen(trim($from))) {
            if ($to)
                $date_clause = "AND a.task_date BETWEEN '" . ymd($from) . "' AND '" . ymd($to) . "'";
            else
                $date_clause = "AND a.task_date >= '" . ymd($from) . "'";
        }
        if (strlen(trim($month_year))) {
            $date_clause = "AND date_format(a.task_date, '%b-%Y') = '" . trim($month_year) . "'";
        }
        if (!strlen(trim($from)) && !strlen(trim($month_year))) {
            $current_date = date("M-Y");
            $date_clause = "AND date_format(a.task_date, '%b-%Y') = '" . $current_date . "'";
        }
        $params = array(
            "user_id" => $user_id,
            "client_id" => $client_id,
            "project_id" => $project_id,
            'parent_id' => $parent_id,
            "subproject_id" => $subproject_id,
            "from" => $from,
            "to" => $to,
            "month_year" => $month_year,
            'grp_by' => $params['grp_by'],
        );
        if ($params['grp_by']) {
            $duration_mins = "SUM(a.duration_mins) AS duration_mins,";
            $group_by = "GROUP BY ";
            $roll_up = " WITH ROLLUP ";
            if (in_array('client_grp', $params['grp_by'])) {
                if (in_array('project_grp', $params['grp_by']) || in_array('subproject_grp', $params['grp_by']) || in_array('users_grp', $params['grp_by']))
                    $comma = ',';
                else
                    $comma = '';
                $client_grp = "c.client_id $comma";
            }
            if (in_array('project_grp', $params['grp_by'])) {
                if (in_array('subproject_grp', $params['grp_by']) || in_array('users_grp', $params['grp_by'])) {
                    $comma = ',';
                } else {
                    $comma = '';
                }
                $project_grp = "c.parent_id $comma";
            }

            if (in_array('subproject_grp', $params['grp_by'])) {
                if (in_array('users_grp', $params['grp_by']))
                    $comma = ',';
                else
                    $comma = '';
                $subproject_grp = "a.project_id $comma";
                //$null_check = "AND c.parent_id != ''";
            }
            if (in_array('users_grp', $params['grp_by'])) {
                $users_grp = "a.user_id ";
            }
        } else {
            $duration_mins = "a.duration_mins AS duration_mins,";
            $order_by = "ORDER BY task_date DESC";
        }

        if (!u()->isAdmin()) {
            $project_id_report = report_filter("c.id", "AND");
        }
        $pri_id_member = report_filter_member("c.id", "AND");

        if (!u()->isAdmin()) {
            $sql_member = "
	            SELECT 
	                a.id, a.user_id, a.project_id, c.parent_id AS project_parent_id, c.client_id, a.detail, UNIX_TIMESTAMP(a.task_date) AS task_date,
	                " . $duration_mins . " a.task_type_id, a.task_status,
	                b.first_name, b.middle_name, b.last_name,  c.name AS project_name,  d.name AS client_name
	            FROM tasks a
	            INNER JOIN users b ON b.id = a.user_id
	            INNER JOIN projects c ON c.id = a.project_id
	            INNER JOIN clients d ON d.id = c.client_id
				";


            $sql_member .= "
	                WHERE a.id > 0
					 
	 				{$user_clause_mem}
	                {$project_clause}
					{$subproject_clause}
	                {$client_clause}
	                {$date_clause}
					{$null_check}
					{$group_by}
					{$client_grp}
					{$project_grp}
					{$subproject_grp}
					{$users_grp}
					{$order_by}
					 
	            ";
            //echo $sql_member.'<br>';
            $rows_mem = db_get_all($sql_member);
            foreach ($rows_mem as $row)
                $return[$row["id"]] = self::_Init($row);
            //debug($return_mem);
        }
        $sql = "
            SELECT 
                a.id, a.user_id, a.project_id, c.parent_id AS project_parent_id, c.client_id, a.detail, UNIX_TIMESTAMP(a.task_date) AS task_date,
                " . $duration_mins . " a.task_type_id, a.task_status,
                b.first_name, b.middle_name, b.last_name,  c.name AS project_name,  d.name AS client_name
            FROM tasks a
            INNER JOIN users b ON b.id = a.user_id
            INNER JOIN projects c ON c.id = a.project_id
            INNER JOIN clients d ON d.id = c.client_id
			";


        $sql .= "
                WHERE a.id > 0
				{$project_id_report}
 				{$user_clause}
                {$project_clause}
				{$subproject_clause}
                {$client_clause}
                {$date_clause}
				{$null_check}
				{$group_by}
				{$client_grp}
				{$project_grp}
				{$subproject_grp}
				{$users_grp}
				{$order_by}
				 
            ";
        //echo $sql.'<br>';
        $rows = db_get_all($sql);
        foreach ($rows as $row) {
            $return[$row["id"]] = self::_Init($row);
        }

        /* if($return_mem)
          $return = array_merge($return,$return_mem);
          else $return;
          debug($return); */
        return $return;
    }

    public function getParentName() {
        $sql = " SELECT name FROM projects WHERE id= " . $this->getProjectParentId();
        $row = db_get_row($sql);
        return $row['name'];
    }

    public function getSubProjectName() {
        $sql = " SELECT name FROM projects WHERE id= " . $this->getProjectId();
        $row = db_get_row($sql);
        return $row['name'];
    }

    public static function getHoursIncurred($userIds = 0, $projectId, $projectRoleId) {

        $sql = "SELECT sum(duration_mins) as HoursIncurred FROM tasks WHERE user_id = {$userIds} AND project_id = {$projectId} AND project_role_id = {$projectRoleId}";
        $row = db_get_row($sql);
        return $row['HoursIncurred'];
    }

}
