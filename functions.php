<?php
    // Session functions
    function session_get($var, $delete = true){
        $tmp = $_SESSION[$var];
        if($delete) $_SESSION[$var] = "";
        return $tmp;
    }

    function session_put($var, $value, $append = true){
        if($append) $_SESSION[$var][] = $value;
        else $_SESSION[$var] = $value;
    }

    function is_email($s){
        // check email here.
        return trim($s);
    }

    function is_date($d, $default_current_ts = false){
        $d = trim($d);
        if($d && !ctype_digit($d)) $d = strtotime($d);

        if($d) return $d;
        elseif($default_current_ts) return time();
        else return false;
    }

    function format_date($ts, $format = 'd-M-Y') {
        if($ts == "0000-00-00") return '';
        if(is_date($ts)) {
            if($ts) {
                if(is_numeric($ts)) {
                    $ts = $ts;
                }
                else {
                    $ts = strtotime($ts);
                }
                return date($format, $ts);
            }
        }
    }

    function mdy($ts){
        if($ts) return date("m/d/Y", $ts);
        else return false;
    }

    function mdyhm($ts){
        if($ts) return date("m/d/Y H:i", $ts);
        else return false;
    }

    function mdyhms($ts){
        if($ts) return date("m/d/Y H:i:s", $ts);
        else return false;
    }

    function ymd($ts){
        if($ts) {
            if(is_numeric($ts)) {
                $ts = $ts;
            }
            else {
                $ts = strtotime($ts);
            }
            return date("Y-m-d", $ts);
        }
        else return false;
    }
	 
    function ymdhms($ts){
        if($ts) return date("Y-m-d H:i:s", $ts);
        else return false;
    }
	 
    function get_dates_in_range($from_ts, $to_ts){
        $return = array();
        $ts = $from_ts;

        while($ts <= $to_ts){
            $return[] = $ts;
            $ts = strtotime(ymd($ts) . " +1 day");
        }

        return $return;
    }

    function get_dates_of_week($date = null, $ymd = false){
        $date = ymd(is_date($date, true));
        $date_ts = strtotime($date);

        $dow = date("N", $date_ts);
        if($dow > 1 && $dow < 7){
            $start_date_ts = strtotime("{$date} last sunday");
            $end_date_ts = strtotime("{$date} next saturday");
        }
        elseif($dow == 7){
            $start_date_ts = $date_ts;
            $end_date_ts = strtotime("{$date} next saturday");
        }
        elseif($dow == 6){
            $start_date_ts = strtotime("{$date} last sunday");
            $end_date_ts = $date_ts;
        }
    
        // $days = range($start_date_ts, $end_date_ts, 86400);
        $days = get_dates_in_range($start_date_ts, $end_date_ts);

        if($ymd) return array_map("ymd", $days);
        else return $days;
    }

    function get_dates_of_month($date = null, $ymd = false, $week_boundaries = false){
        $date = date("Y-m-01", is_date($date, true));

        $fdom_ts = strtotime($date);
        $ldom_ts = strtotime(ymd($fdom_ts) . " +1 month -1 second");

        if($week_boundaries){
            $dow = date("N", $fdom_ts);
            if($dow < 7) $first_day_ts = strtotime(ymd($fdom_ts) . " last sunday");
            else $first_day_ts = $fdom_ts;
            
            $dow = date("N", $ldom_ts);
            if($dow != 6 ) $last_day_ts = strtotime(ymd($ldom_ts) . " next saturday");
            else $last_day_ts = $ldom_ts;
        }
        else{
            $first_day_ts = $fdom_ts;
            $last_day_ts = $ldom_ts;
        }

        // $days = range($first_day_ts, $last_day_ts, 86400);
        $days = get_dates_in_range($first_day_ts, $last_day_ts);

        if($ymd) return array_map("ymd", $days);
        else return $days;
    }

    // Misc functions
    function debug($var){
        if(in_array(gettype($var), array("object", "array"))){
            print "<pre>";
            print_r($var);
            print "</pre>";
        }
        else print "{$var}<br>";
    }

    function q($s, $escape = true, $null_if_empty = true){
        $s = trim($s);
        if($s){
            if($escape && !get_magic_quotes_gpc()) return _::$Db->qstr($s);
            else return "'{$s}'";
        }
        elseif($null_if_empty) return "NULL";
        else return "''";
    }

    function n($s){
        $s = trim($s);
        if($s) return $s;
        else return "NULL";
    }

    function e($s = null){
        $s = trim($s);
        if($s) session_put("errors", $s);
        else return session_get("errors");
    }

    function s($s = null){
        $s = trim($s);
        if($s) session_put("success", $s);
        else return session_get("success");
    }

    function p($a = null){
        if($a && is_array($a)) session_put("post", $a, false);
        else{
            $p = session_get("post");
            if($p) return $p;
            else return array();
        }
    }

    function u($u = null){
        static $user = null;

        if($u){
            if($u instanceof User) $u = $u->getId();
            else $u = intval($user);
            session_put("user", $u, false);
            $user = null;
        }
        else{
            if(is_null($user)){
                $u = session_get("user", false);
                if($u instanceof User) $u = $u->getId();
                $user = new User($u);
            }

            return $user;
        }
    }
    
    function watchdog($status, $action, $message, $created_by = null){
        if($created_by == null || $created_by == "") {
            $created_by = u()->getEmail();
        }
        $watchdog = new Watchdog();
        $watchdog->setStatus($status);
        $watchdog->setAction($action);
        $watchdog->setMessage($message);
        $watchdog->setHostName($_SERVER['REMOTE_ADDR']);
        $watchdog->setCreadedBy($created_by);
        $watchdog->log(); 
    }

    function h2m($h){
        return abs($h) * 60;
    }

    function hm2m($hm){
        $hm = trim($hm);
        if(is_numeric($hm)) return $hm * 60;
        else{
            list($h, $m) = preg_split("/[^\d]+/", $hm, 2);
            $h = intval($h);
            $m = intval($m);
            return ($h * 60) + $m;
        }
    }

    function m2hm($mins,$seperator = ":"){
        $mins = abs(intval($mins));
        if(!$mins) return "";

        $h = floor($mins / 60);
        $m = floor($mins % 60);
        return intval(str_pad(intval($h), 2, intval("0"), STR_PAD_LEFT)) . "$seperator" . intval(str_pad(intval($m), 2, intval("0"), STR_PAD_LEFT));
    }

    function pintval($var){
        $var = intval($var);
        if($var > 0) return $var;
        else return 0;
    }

    function get_project_ids_clause($condition, $field, $user_id = 0){
        $user = u();
        if($user->isAdmin() && $user_id == 0) return "";
        if($user_id > 0) {
            $user = new User($user_id);
        }
        else if($user->isAdmin() && $user_id != $user->getId()) {
            $user = new User($_GET["user_id"]);
        }
        $ids = $user->getProjectIds();
        if(!$ids) $ids = array(0);

        $ids = implode(", ", $ids);
        return "{$condition} {$field} IN ({$ids})";
    }

    function gen_password($length = 10){
        $allowable_characters = 'abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789';
        $len = strlen($allowable_characters) - 1;
        $pass = '';

        for ($i = 0; $i < $length; $i++){
            $pass .= $allowable_characters[mt_rand(0, $len)];
        }

        return $pass;
    }

    function concat($sep){
        $args = func_get_args();
        $sep = array_shift($args);
        
        $args = array_filter(array_map("trim", $args));
        return implode($sep, $args);
    }

    // DB functions
    function db_execute($sql){
        return _::$Db->Execute($sql);
    }

    function db_get_all($sql){
        $rs = db_execute($sql);
        if($rs) return $rs->GetAll();
        else return array();
    }

    function db_get_row($sql){
        $rs = db_get_all($sql);
        return $rs[0];
    }

    function db_get_var($sql){
        $rs = db_get_row($sql);
        if($rs) return array_shift($rs);
        else return false;
    }

    function db_insert_id(){
        return _::$Db->Insert_ID();
    }

    function db_affected_rows(){
        return _::$Db->Affected_Rows();
    }

    function db_txn_begin(){
        return _::$Db->StartTrans();
    }

    function db_txn_complete(){
        return _::$Db->CompleteTrans();
    }

    function db_txn_fail(){
        return _::$Db->FailTrans();
    }

    function db_txn_hasfailed(){
        return _::$Db->HasFailedTrans();
    }

    // Smarty functions
    function sm_assign($var, $value){
        _::$Smarty->assign($var, $value);
    }

    function sm_display($tpl){
        _::$Smarty->display($tpl);
    }

    function sm_fetch($tpl, $params = null){
        if(is_array($params)){
            foreach($params as $k => $v){
                sm_assign($k, $v);
            }
        }
        return _::$Smarty->fetch($tpl);
    }

    // HTTP functions
    function redirect($url, $debug = false){
        if($debug) debug($url);
        else{
            ob_clean();
            header("Location: {$url}");
        }
        exit;
    }

    function error403(){
        ob_clean();
        header("HTTP/1.1 403 Access Denied");
        exit;
    }

    function error404(){
        ob_clean();
        header("HTTP/1.1 404 Not Found");
        exit;
    }

    // ACL functions
    function require_perms(){
        // Current only 2 permissions/acl are to be considered.
        // 1. admin, 2. authuser
        // check permission by calling User::hasPerms()
        // if access fails...simply show access denied template.

        static $user = null;
        if(is_null($user)) $user = u();
        if(!$user->hasPerms(func_get_args())){
            sm_display("access.denied.html");
            exit;
        }
    }

    function add_get_param($params, $url = null){
        if(is_null($url)) $u_parts = parse_url($_SERVER["REQUEST_URI"]);
        else $u_parts = parse_url($url);

        $path = $u_parts["path"];
        if(!$path) $path = "/";
        $qs = array();

        if($u_parts["query"]) parse_str($u_parts["query"], $qs);

        foreach($params as $k => $v) $qs[$k] = $v;
        return "{$u_parts["scheme"]}://{$u_parts["host"]}{$path}?" . http_build_query($qs);
    }

	function report_filter_manager($tbl_id = false, $op = false){
		if(stristr($_SERVER['SCRIPT_NAME'], 'report.list.php') == 'report.list.php' && !u()->isAdmin()) {
			$projectByManager = "SELECT project_id  
								 FROM project_users  
								 WHERE user_id = ".u()->getId()." AND (role= 'MANAGER'  OR role= 'MEMBER')";
								 
			$projectByManager = db_get_all($projectByManager);
			$uids_by_manager = array();
			$parent_id = array();
			foreach($projectByManager as $uids){
				$sql = "SELECT parent_id FROM projects WHERE id = {$uids['project_id']}";
				$parent_id = db_get_row($sql);
				if($parent_id){
					foreach($parent_id as $puids){
 					
						if($puids){
							$uids_by_manager[] = $puids; 
						}
						else {
							$uids_by_manager[] = $uids['project_id'];
						}
				   }
				}
			}
			if($tbl_id) $tbl_id = $tbl_id;
			else $tbl_id = "a.id";
			$uids_by_manager = implode(", ", $uids_by_manager);
			if($op)
			$project_id_report = " $tbl_id IN($uids_by_manager)";
			else
			$project_id_report = "AND $tbl_id IN($uids_by_manager)";
		}
        return $project_id_report;
	
	}
	
	function report_filter($tbl_id = false, $op = false){
		if(stristr($_SERVER['SCRIPT_NAME'], 'report.list.php') == 'report.list.php' && !u()->isAdmin()) {
			$projectByManager = "SELECT project_id  
								 FROM project_users  
								 WHERE user_id = ".u()->getId()." AND (role= 'MANAGER')";
								 
			$projectByManager = db_get_all($projectByManager);
			foreach($projectByManager as $uids){
				$uids_by_manager[] = $uids['project_id'];
			}
			if($uids_by_manager)
			$uids_by_manager = implode(", ", $uids_by_manager);
			
			if($tbl_id) $tbl_id = $tbl_id;
			else $tbl_id = '';
			
			if($op) $op = $op;
			else $op = '';
			
			if (count($uids_by_manager))
                $all_project_id_clause	= "$op $tbl_id IN($uids_by_manager)";
            else
                $all_project_id_clause	= "";
		}
		return $all_project_id_clause;
	}
	
	function report_filter_member($tbl_id = false, $op = false){
		if(stristr($_SERVER['SCRIPT_NAME'], 'report.list.php') == 'report.list.php' && !u()->isAdmin()) {
			$projectByManager = "SELECT project_id  
								 FROM project_users  
								 WHERE user_id = ".u()->getId()." AND (role= 'MEMBER')";
								 
			$projectByManager = db_get_all($projectByManager);
			foreach($projectByManager as $uids){
				$uids_by_manager[] = $uids['project_id'];
			}
			if($uids_by_manager)
			$uids_by_manager = implode(", ", $uids_by_manager);
			
			if($tbl_id) $tbl_id = $tbl_id;
			else $tbl_id = '';
			
			if($op) $op = $op;
			else $op = '';
			
			$all_project_id_clause	= "$op $tbl_id IN($uids_by_manager)";
		}
		return $all_project_id_clause;
	}
	function cmp($a, $b)
	{
	    if ($a['date'] == $b['date']) {
	        return 0;
	    }
	    return ($a['date'] > $b['date']) ? -1 : 1;
	}
	
    function modulo($n, $m, $arr){
        return ($n+$m)%count($arr);
    }

    function backtrace() {
        // Output everything in a table
        $return='<table width = "100%"><tr style="font-weight : bold; background-color : #cccccc">
            <td>Function</td><td>Args</td><td>File</td><td>Line</td></tr>';

        // Get the backtrace information
        $debug_array = debug_backtrace();

        // Run through the array
        foreach($debug_array as $key=>$data) {
            $return.='<tr valign="top" style="background-color : #eeeeee">'.
                '<td>'.$data['function'].'</td><td>';
            $return.='&nbsp;';
            $return.='</td>';
            $return.='<td>'.$data['file'].'</td>'.
                '<td>'.$data['line'].'</td></tr>';
            }
        debug($return.'</table>');
    }

    function map_assoc($array){
        $return = array();
        foreach($array as $v) $return[$v] = $v;
        return $return;
    }

    function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
    }

    // This function shortens the string down to maximum lengt defined in $max_length. 
    // If the string is longer the function finds the last occurance of a space and adds three dots at the end to illustrate that it is more text.
    function short_string($string, $max_length){
        if (strlen($string) > $max_length){
            $string = substr($string, 0, $max_length);
            $pos = strrpos($string, " ");
            if($pos === false) {
                return substr($string, 0, $max_length);
                #return substr($string, 0, $max_length)."...";
            }
            return substr($string, 0, $pos);
            #return substr($string, 0, $pos)."...";
        }
        else
            return $string;
    }
    
    function getCountryList(){
        $country = array();
        $sql = "SELECT country_id, country_name FROM country ORDER BY FIELD(country_name, 'United States', 'India') desc, country_name asc" ;
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                $country[$row["country_id"]] = $row;
            }
        }
        return $country;
    }
    function getStandardHours() {
        $sql = "SELECT * FROM standard_hours";
        #$rows = db_get_all($sql);
        $row = db_get_row($sql);
        if($row) 
            return $row;
        return false;
    }

    function secondsToTime($seconds) {
        // extract hours
        $hours = floor($seconds / (60 * 60));
     
        // extract minutes
        $divisor_for_minutes = $seconds % (60 * 60);
        $minutes = floor($divisor_for_minutes / 60);
     
        // extract the remaining seconds
        $divisor_for_seconds = $divisor_for_minutes % 60;
        $seconds = ceil($divisor_for_seconds);
     
        // return the final array
        $obj = array(
            "hour" => (int) $hours,
            "min" => (int) $minutes,
            "sec" => (int) $seconds,
        );
        return $obj;
    }

    function calculate_weekly_timecard($user_id,$date1, $date2,$blnUseRawData=false) {
        include_once("classes/class.client.inc.php");
        include_once("classes/class.project.inc.php");
        include_once("classes/class.timecard.inc.php");
        include_once("classes/class.task.inc.php");
        $return = array("productivity_factor" => $productivity_factor,
                    "billable_factor" => $billable_factor,
                    "public_holidays" => $public_holidays,
                    "utilization" => $utilization_arr,
                    "day_count" => $day_count,
                );

        if(!is_array($user_id)) {
            $user = new User($user_id);
        }
        $standard_hours = getStandardHours();
        $standard_hours_in_mins = $standard_hours["standard_hours_in_mins"];
        $standard_hours = m2hm($standard_hours["standard_hours_in_mins"]);
        $total_mins = 0;
        $total_pto = 0;
        $day_count = 0;
        $public_holiday_count = 0;
        $billable_minutes = 0;
        $utilization_arr=array();
        $public_holidays = array();
        $data = array();
        $productivity_factor = 0;

        if(!is_array($user_id) && $user->getId() < 1 && $blnUseRawData) {
            return $return;
        }
        if($blnUseRawData) {
            // Get billable & non-billable projects
            $projects = array();
            $o_clients = Client::GetClients($user->getId());
            foreach($o_clients as $o_client){
                $o_projects = $o_client->getProjectsByUser($user->getId(), true);
                foreach($o_projects as $o_project){
                    $projects[$o_project->getId()]['is_billable'] = intval($o_project->getIsBillable());
                    $o_subprojects = $o_client->getSubprojectsByUser($user->getId(), $o_project->getId());
                    foreach($o_subprojects as $o_subproject){
                        $projects[$o_subproject->getId()]['is_billable'] = intval($o_subproject->getIsBillable());
                    }
                }
            }
            // Get task details
            $o_tasks = $user->getTasksByDateRange(ymd(is_date($date1)), ymd(is_date($date2)));
            $sql = "SELECT * FROM timecard_data WHERE user_id = {$user->getId()} AND (timecard_date BETWEEN '$date1' AND '$date2')";
            $rows = db_get_all($sql);
            $tResult = array();
            $blnTresult = false;
            if($rows) {
                $blnTresult = true;
                foreach($rows as $row) {
                    $tResult[$row['timecard_date']] = $row;
                    $timecard_data[$row['timecard_date']] = array();
                }
            }
            $num_days=array();
            foreach($o_tasks as $o_task){
               $day = getdate($o_task->getTaskDate());
               $task_date = date("Y-m-d",$o_task->getTaskDate()); 
                
                if(!isset($tasks[$task_date]['total_mins']))
                    $tasks[$task_date]['total_mins'] = 0;
                if(!isset($tasks[$task_date]['total_billable_mins']))
                    $tasks[$task_date]['total_billable_mins'] = 0;
                if(!isset($tasks[$task_date]['total_pto_mins']))
                    $tasks[$task_date]['total_pto_mins'] = 0;

                #if($o_task->getClientId() != 1000) {
                    if($projects[$o_task->getProjectId()]['is_billable']) {
                        if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                            // Weekend
                            $tasks[$task_date]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $holiday_billable_mins += $o_task->getDurationMins();
                        }
                        else if($blnTresult && $tResult[$task_date]['is_holiday']) {
                            // Public Holiday
                            $tasks[$task_date]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $holiday_billable_mins += $o_task->getDurationMins();
                            $public_holiday_count++;
                            $tasks[$task_date]['is_holiday'] = 1;
                        }
                        else {
                            // Regular day Billable
                            $tasks[$task_date]['total_billable_mins'] += $o_task->getDurationMins();
                            $billable_minutes += $o_task->getDurationMins();
                            $num_days[$task_date] = $num_days[$task_date];
                            $total_mins += $o_task->getDurationMins();
                        }
                        $tasks[$task_date]['total_mins'] += $o_task->getDurationMins();
                    } 
                    else {
                        // Regular day
                        if ($o_task->getProjectId() == 1000) {
                            // PTO
                            if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                                // Weekend
                            }
                            else {
                                $tasks[$task_date]['total_pto_mins'] += $o_task->getDurationMins();
                                $total_pto_mins += $o_task->getDurationMins();
                            }
                        }
                        else {
                            if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                                // Weekend
                            }
                            else {
                                $tasks[$task_date]['total_mins'] += $o_task->getDurationMins();
                                $total_mins += $o_task->getDurationMins();
                            }
                            $num_days[$task_date] = $num_days[$task_date];
                        }
                    }
                
            }
            $day_count = count($num_days);
            
            foreach($timecard_data as $d => $v) {
                
                $timecard_data[$d]['total_mins'] = 0;
                $timecard_data[$d]['total_billable_mins'] = 0;
                $timecard_data[$d]['total_holiday_billable_mins'] = 0;
                $timecard_data[$d]['total_pto_mins'] = 0;
                $timecard_data[$d]['is_holiday'] = 0; 
                $timecard_data[$d]['unassigned_mins'] = 0;
                $timecard_data[$d]['standard_hours_in_mins'] = 0;
                $timecard_data[$d]['utilization'] = 'N/A';
                $timecard_data[$d]['week_number'] = 0;

                $utilization = 'N/A';
                $timecard_data[$d]['week_number'] = date("W",strtotime($d));
                $public_holidays[$d]['is_holiday'] = 0;
                if(array_key_exists($d, $tasks)) {
                    $v = $tasks[$d];
                    $public_holidays[$d]['is_holiday'] = $v['is_holiday'] ;
                    $timecard_data[$d] = $v;
                    $timecard_data[$d]['is_holiday'] = $v['is_holiday'] ;
                    $timecard_data[$d]['unassigned_mins'] = $standard_hours_in_mins - $v['total_billable_mins'] - $v['total_pto_mins'];
                    $timecard_data[$d]['standard_hours_in_mins'] = $standard_hours_in_mins - $v['total_pto_mins'];
                    $utilization = ($v['total_billable_mins'] / $standard_hours_in_mins - $v['total_pto_mins']) * 100;
                    if($utilization > 0)
                        $utilization = round($utilization, 2);

                    $TM += ($v['total_mins'] + $v['total_billable_mins'] + $v['total_holiday_billable_mins']);
                    $TPTO += $v['total_pto_mins'];
                    $TBM += $v['total_billable_mins'];
                    $THBM += $v['total_holiday_billable_mins'];
                }
                $timecard_data[$d]['utilization'] = $utilization;
                $utilization_arr[$d] = $utilization;
                
            }
            if($TM > 0) {
                $productivity_factor = round(($TM / (($day_count * $standard_hours_in_mins) - $TPTO) * 100), 2);
                $billable_utilization_factor = round((($TBM + $THBM) / (($day_count * $standard_hours_in_mins) - $TPTO) * 100), 2);
            }

            $return = array(
                        "public_holidays" => $public_holidays,
                        "utilization" => $utilization_arr,
                        "timecard_data" => $timecard_data,
                        "total_mins" => $TM,
                        "total_billable_mins" => $TBM,
                        "total_holiday_billable_mins" => $THBM,
                        "total_PTO" => $TPTO,
                        "total_standard_hours_in_mins" => ($day_count * $standard_hours_in_mins),
                        "day_count" => $day_count,
                        "productivity_factor" => $productivity_factor,
                        "billable_utilization_factor" => $billable_utilization_factor,
                    );

            return $return;
        }
        else {
            // Get Timecard details

            $timecard = Timecard::GetTimecard($user_id,ymd($date1), ymd($date2));
        
            $TM = 0;  // Total minutes worked
            $TBM = 0; // Total minutes worked on billable projects
            $TPTO= 0; // Total Personal Time Off
            $THBM=0;  // Total billable hours in public holidays
            if(is_array($timecard) && count($timecard) > 0) {
                foreach($timecard as $uid => $val) {
                    foreach($val as $v){
                        $TDM = 0; // Total minutes worked in a day
                        $BM = 0;  // Billable minuted worked in day
                        $PTO = 0; // Personal Time Off in a day
                        $HBM=0;   // billable hours in public holiday
                        
                        $std_hours = 0; // Remaining standard hours
                        
                        $utilization = 'N/A';

                        $blnHoliday=false;
                        $blnWeekday=false;
                        $day = '';
                        // get the day to check the weekdays i.e. Sunday & Saturday
                        $timecard_date = $v->getTimecardDate();
                        $day = getdate($timecard_date);


                        if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                            // If the day is an off day i.e., Saturday, Sunday, and Member enters no time for an off day, 
                            $blnWeekday = true;
                            $utilization = 'N/A';
                            $unassigned = 'N/A';
                            $std_hours = 'N/A';
                            $HBM = intval($v->getBillableMinutes());
                            $THBM += intval($v->getBillableMinutes());
                        }
                        else if($v->getIsHoliday()) {
                            // If Public Holiday is selected for that day, and Member enters no time for a public holiday,
                            $blnHoliday=true;
                            $utilization = 'N/A';
                            $unassigned = 'N/A';
                            $std_hours = 'N/A';
                            $HBM = intval($v->getBillableMinutes());
                            $THBM += intval($v->getBillableMinutes());
                            //$TM += intval($v->getTotalMinutes());
                            $public_holiday_count++;
                        }
                        else {
                            // If the day is a regular working day, 
                            //$TM += intval($v->getTotalMinutes());
                            //$TM += intval($v->getTotalMinutes());
                            $TBM += intval($v->getBillableMinutes());
                            $BM = intval($v->getBillableMinutes());
                            $TDM = intval($v->getTotalMinutes());
                            $TPTO += intval($v->getPTOMinutes());
                            $PTO = intval($v->getPTOMinutes());
                            $utilization = ($BM / $standard_hours_in_mins - $PTO) * 100;
                            if($utilization > 0)
                                $utilization = round($utilization, 2);
                            else
                                $utilization = 'N/A';
                            $std_hours = ($standard_hours_in_mins - $PTO);
                            $day_count++;
                        }
                        $TM += intval($v->getTotalMinutes());
                        $utilization_arr[date("m-d-Y",$v->getTimecardDate())] = $utilization;
                        $data[date("m-d-Y",$v->getTimecardDate())]['standard_hours_in_mins'] = $std_hours;
                        $data[date("m-d-Y",$v->getTimecardDate())]['total_mins'] = $v->getTotalMinutes();
                        $data[date("m-d-Y",$v->getTimecardDate())]['total_pto'] = $v->getPTOMinutes();
                        $data[date("m-d-Y",$v->getTimecardDate())]['total_bill_mins'] = $v->getBillableMinutes();
                        $data[date("m-d-Y",$v->getTimecardDate())]['total_unassigned_mins'] = $v->getUnassignedMinutes();
                        $data[date("m-d-Y",$v->getTimecardDate())]['is_holiday'] = $v->getIsHoliday();
                        $data[date("m-d-Y",$v->getTimecardDate())]['week_number'] = $v->getWeekNumber();
                        $data[date("m-d-Y",$v->getTimecardDate())]['utilization'] = $utilization;

                        $public_holidays[date("m-d-Y",$v->getTimecardDate())]['is_holiday'] = $v->getIsHoliday();
                    }
                }
            }
            if($TM > 0) {
                $productivity_factor = round(($TM / (($day_count * $standard_hours_in_mins) - $TPTO) * 100), 2);
                $billable_utilization_factor = round((($TBM + $THBM) / (($day_count * $standard_hours_in_mins) - $TPTO) * 100), 2); 
            }
            $return = array(
                        "public_holidays" => $public_holidays,
                        "utilization" => $utilization_arr,
                        "timecard_data" => $data,
                        "total_mins" => $TM,
                        "total_billable_mins" => $TBM,
                        "total_holiday_billable_mins" => $THBM,
                        "total_PTO" => $TPTO,
                        "total_standard_hours_in_mins" => ($day_count * $standard_hours_in_mins),
                        "day_count" => $day_count,
                        "productivity_factor" => $productivity_factor,
                        "billable_utilization_factor" => $billable_utilization_factor,
                    );
            return $return;
        }
        return false;
    }

    function calculate_productivity_utilization($user_id,$date1, $date2) {
        
        include_once("classes/class.client.inc.php");
        include_once("classes/class.project.inc.php");
        include_once("classes/class.timecard.inc.php");
        include_once("classes/class.task.inc.php");
        $return = array("productivity_factor" => $productivity_factor,
                    "billable_factor" => $billable_factor,
                    "public_holidays" => $public_holidays,
                    "utilization" => $utilization_arr,
                    "day_count" => $day_count,
                );

        if(!is_array($user_id)) {
            $user = new User($user_id);
        }
        $standard_hours = getStandardHours();
        $standard_hours_in_mins = $standard_hours["standard_hours_in_mins"];
        $standard_hours = m2hm($standard_hours["standard_hours_in_mins"]);
        $total_mins = 0;
        $total_pto = 0;
        $day_count = 0;
        $public_holiday_count = 0;
        $billable_minutes = 0;
        $utilization_arr=array();
        $public_holidays = array();
        $data = array();
        $productivity_factor = 0;

        // Get billable & non-billable projects
            $projects = array();
            $o_clients = Client::GetClients($user->getId());
            foreach($o_clients as $o_client){
                $o_projects = $o_client->getProjectsByUser($user->getId(), true);
                foreach($o_projects as $o_project){
                    $projects[$o_project->getId()]['is_billable'] = intval($o_project->getIsBillable());
                    $o_subprojects = $o_client->getSubprojectsByUser($user->getId(), $o_project->getId());
                    foreach($o_subprojects as $o_subproject){
                        $projects[$o_subproject->getId()]['is_billable'] = intval($o_subproject->getIsBillable());
                    }
                }
            }
            // Get task details
            $o_tasks = $user->getTasksByDateRange(ymd(is_date($date1)), ymd(is_date($date2)));
            $sql = "SELECT * FROM timecard_data WHERE user_id = {$user->getId()} AND (timecard_date BETWEEN '$date1' AND '$date2')";
            $rows = db_get_all($sql);
            $tResult = array();
            $blnTresult = false;
            if($rows) {
                $blnTresult = true;
                foreach($rows as $row) {
                    $tResult[$row['timecard_date']] = $row;
                    $timecards[$row['timecard_date']] = array();
                }
            }
            $num_days=array();
            foreach($o_tasks as $o_task){
               $day = getdate($o_task->getTaskDate());
               $task_date = date("Y-m-d",$o_task->getTaskDate()); 
                
                    if(!isset($client_tasks[$o_task->getClientName()]['total_mins']))
                        $client_tasks[$o_task->getClientName()]['total_mins'] = 0;
                    if(!isset($client_tasks[$o_task->getClientName()]['total_billable_mins']))
                        $client_tasks[$o_task->getClientName()]['total_billable_mins'] = 0;
                    if(!isset($client_tasks[$o_task->getClientName()]['total_holiday_billable_mins']))
                        $client_tasks[$o_task->getClientName()]['total_holiday_billable_mins'] = 0;
                    if(!isset($client_tasks[$o_task->getClientName()]['total_pto']))
                        $client_tasks[$o_task->getClientName()]['total_pto'] = 0;

                    
                    if(!isset($project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_mins']))
                        $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_mins'] = 0;
                    if(!isset($project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_billable_mins']))
                        $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_billable_mins'] = 0;
                    if(!isset($project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_holiday_billable_mins']))
                        $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_holiday_billable_mins'] = 0;
                    if(!isset($project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_pto']))
                        $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_pto'] = 0;

                    if($projects[$o_task->getProjectId()]['is_billable']) {
                        if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                            // Weekend
                            $tasks[$task_date]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $client_tasks[$o_task->getClientName()]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $holiday_billable_mins += $o_task->getDurationMins();
                        }
                        else if($blnTresult && $tResult[$task_date]['is_holiday']) {
                            // Public Holiday
                            $tasks[$task_date]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $client_tasks[$o_task->getClientName()]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_holiday_billable_mins'] += $o_task->getDurationMins();
                            $holiday_billable_mins += $o_task->getDurationMins();
                            $public_holiday_count++;
                            $tasks[$task_date]['is_holiday'] = 1;
                        }
                        else {
                            // Regular day Billable
                            $tasks[$task_date]['total_billable_mins'] += $o_task->getDurationMins();
                            $client_tasks[$o_task->getClientName()]['total_billable_mins'] += $o_task->getDurationMins();
                            $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_billable_mins'] += $o_task->getDurationMins();
                            $billable_minutes += $o_task->getDurationMins();
                            $num_days[$task_date] = $task_date;
                            $total_mins += $o_task->getDurationMins();
                        }
                        #$tasks[$task_date]['total_mins'] += $o_task->getDurationMins();
                        $client_tasks[$o_task->getClientName()]['is_billable'] = 1; 
                        $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['is_billable'] = 1;
                    } 
                    else {
                        // Regular day
                        if ($o_task->getProjectId() == 1000) {
                            // PTO
                            if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                                // Weekend
                            }
                            else {
                                $tasks[$task_date]['total_pto_mins'] += $o_task->getDurationMins();
                                $client_tasks[$o_task->getClientName()]['total_pto_mins'] += $o_task->getDurationMins();
                                $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_pto_mins'] += $o_task->getDurationMins();
                                $total_pto_mins += $o_task->getDurationMins();
                            }
                        }
                        else {
                            if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                                // Weekend
                            }
                            else {
                                $tasks[$task_date]['total_mins'] += $o_task->getDurationMins();
                                $client_tasks[$o_task->getClientName()]['total_mins'] += $o_task->getDurationMins();
                                $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['total_mins'] += $o_task->getDurationMins();
                                $total_mins += $o_task->getDurationMins();
                            }
                            $num_days[$task_date] = $task_date;
                        }
                        $client_tasks[$o_task->getClientName()]['is_billable'] = 0; 
                        $project_tasks[$o_task->getClientName()][$o_task->getProjectName()]['is_billable'] = 0;
                    }
                
            }
            $TM = 0;  // Total minutes worked
            $TBM = 0; // Total minutes worked on billable projects
            $TPTO= 0; // Total Personal Time Off
            $THBM=0;  // Total billable hours in public holidays
            
            
            $tmp = explode("-", $date1);
            $cnt = getNumberOfDays($date1,$date2);
            $m = $tmp[1];
            $y = $tmp[0];
            $dCnt = date('t', strtotime($date1));
            for($j = $tmp[2]; $j <= $cnt; $j++) {
                
                if($j <= $dCnt) {
                    $dt1= date("Y-m-d", mktime(0,0,0, date("m",strtotime($date1)),$j,date("Y",strtotime($date1))));
                    $dArr[$dt1] = $dt1;
                } 
                else {
                    $dt2 = date("m",strtotime($date2))."/".$dCnt."/".date("Y",strtotime($date2));
                    $dt1= date("Y-m-d", strtotime($dt2));
                    $dArr[$dt1] = $dt1;
                    $dCnt++;
                }

                $day = getdate(strtotime($dt1));
                $blnDTfound=false;
                if(strtolower($day['weekday']) == 'sunday' || strtolower($day['weekday']) == 'saturday') {
                    // Weekend
                }
                else if($tResult[$task_date]['is_holiday']) {
                    // Public Holiday
                }
                else { 
                    if($blnTresult) {
                        foreach($tResult as $d => $v) {
                            if(strtotime($d) == strtotime($dt1)) {
                                $blnDTfound=true;
                                if(!array_key_exists($d,$num_days))
                                    $num_days[$d] = $d;
                            }
                        }
                    }
                    if(!$blnDTfound){
                        $num_days[$dt1] = $dt1;
                    }
                }
                if($j == $dCnt) $dCnt = 1;
            }
            $day_count = count($num_days);
            $client_count = count($client_tasks); 
                foreach($client_tasks as $client_name => $v) {

                        $utilization = 'N/A';
                        
                            $timecard_data[$client_name]['total_mins'] = 0;
                            $timecard_data[$client_name]['total_billable_mins'] = 0;
                            $timecard_data[$client_name]['total_holiday_billable_mins'] = 0;
                            $timecard_data[$client_name]['total_pto_mins'] = 0;
                            $timecard_data[$client_name]['unassigned_mins'] = 0;
                            //$timecard_data[$client_name]['standard_hours_in_mins'] = 0;
                            $timecard_data[$client_name]['utilization'] = 'N/A';

                            $utilization_arr[$client_name]= $utilization;
                            $timecard_data[$client_name]= $v;
                            $timecard_data[$client_name]['total_pto_mins'] += $v['total_pto_mins'];
                            //$timecard_data[$client_name]['standard_hours_in_mins'] += $standard_hours_in_mins;
                            //$timecard_data[$client_name]['unassigned_mins'] += $standard_hours_in_mins - $v['total_billable_mins'] - $v['total_pto_mins'];

                            $timecard_data[$client_name]['total_mins']= ($v['total_mins'] + $v['total_billable_mins'] + $v['total_holiday_billable_mins']);
                            $TM += ($v['total_mins'] + $v['total_billable_mins'] + $v['total_holiday_billable_mins']);
                            if(strpos($client_name, "PTO") === false) {
                                $utilization = @((($v['total_holiday_billable_mins'] + $v['total_billable_mins']) / $TM) / $client_count )* 100;
                            }
                            if($utilization > 0)
                                $utilization = round($utilization, 2) ."%";
                            $TPTO += $v['total_pto_mins'];
                            $TBM += $v['total_billable_mins'];
                            $THBM += $v['total_holiday_billable_mins'];
                            $timecard_data[$client_name]['utilization'] = $utilization;
                            $utilization_arr[$client_name] = $utilization;

                    if(is_array($project_tasks[$client_name]) && count($project_tasks[$client_name]) > 0) {     
                            // Project
                            $project_count = count($project_tasks[$client_name]);
                            $utilization = 'N/A';
                    
                            foreach($project_tasks[$client_name] as $project_name => $v1) {
                                $timecard_data[$client_name]['projects'][$project_name]['total_mins'] = 0;
                                $timecard_data[$client_name]['projects'][$project_name]['total_billable_mins'] = 0;
                                $timecard_data[$client_name]['projects'][$project_name]['total_holiday_billable_mins'] = 0;
                                $timecard_data[$client_name]['projects'][$project_name]['total_pto_mins'] = 0;
                                $timecard_data[$client_name]['projects'][$project_name]['unassigned_mins'] = 0;
                                //$timecard_data[$client_name]['projects'][$project_name]['standard_hours_in_mins'] = 0;
                                $timecard_data[$client_name]['projects'][$project_name]['utilization'] = 'N/A';

                                $timecard_data[$client_name]['projects'][$project_name]= $v1;
                                $timecard_data[$client_name]['projects'][$project_name]['total_pto_mins'] += $v1['total_pto_mins'];
                                //$timecard_data[$client_name]['projects'][$project_name]['standard_hours_in_mins'] += $standard_hours_in_mins;
                                //$timecard_data[$client_name]['projects'][$project_name]['unassigned_mins'] += $standard_hours_in_mins - $v1['total_billable_mins'] - $v1['total_pto_mins'];

                                $timecard_data[$client_name]['projects'][$project_name]['total_mins']=($v1['total_mins'] + $v1['total_billable_mins'] + $v1['total_holiday_billable_mins']);
                                $pTM += ($v1['total_mins'] + $v1['total_billable_mins'] + $v1['total_holiday_billable_mins']);
                                if(strpos($project_name, "PTO") === false) {
                                    @$utilization = ((($v1['total_holiday_billable_mins'] + $v1['total_billable_mins']) / $pTM) / $project_count )* 100;
                                }
                                if($utilization > 0)
                                    $utilization = floor($utilization)."%";
                                $pTPTO += $v1['total_pto_mins'];
                                $pTBM += $v1['total_billable_mins'];
                                $pTHBM += $v1['total_holiday_billable_mins'];
                                $timecard_data[$client_name]['projects'][$project_name]['utilization'] = $utilization;
                            }
                            if($pTM > 0) {
                                $productivity_factor = @round(($pTM / (($day_count * $standard_hours_in_mins) - $pTPTO) * 100), 2);
                                $billable_utilization_factor = @round((($pTBM + $pTHBM) / (($day_count * $standard_hours_in_mins) - $pTPTO) * 100), 2);
                            }
                        }
                }
                
            if($TM > 0) {
                $productivity_factor = @round(($TM / (($day_count * $standard_hours_in_mins) - $TPTO) * 100), 2);
                $billable_utilization_factor = @round((($TBM + $THBM) / (($day_count * $standard_hours_in_mins) - $TPTO) * 100), 2);
            }

            $return["public_holidays"] = $public_holidays;
            $return["utilization"] = $utilization_arr;
            $return["total_standard_hours_in_mins"] = ($day_count * $standard_hours_in_mins);
            $return["day_count"] = $day_count;
            $return["timecard_data"] = $timecard_data;
            $return["total_mins"] = $TM;
            $return["total_billable_mins"] = $TBM;
            $return["total_holiday_billable_mins"] = $THBM;
            $return["total_PTO"] = $TPTO;
            $return["productivity_factor"] = $productivity_factor."%";
            $return["billable_utilization_factor"] = $billable_utilization_factor."%";

            return $return;

    }

    function getNumberOfDays($date1,$date2) {
        $start_ts = is_date($date1);
        $end_ts = is_date($date2, true);

        $diff = ($end_ts - $start_ts);
        return round($diff / (60 * 60 * 24));
    }

    function remove_whitespace($str) {
        if($str) {
            $str = preg_replace('/\s\s+/', ' ', $str);
        }
        return $str;
    }

    function download_file($file) {
        if(!file_exists($file))
            die('file not exist!');
        $size = filesize($file);
        $name = basename($file);
        if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT']))
            $UserBrowser = "Opera";
        elseif (ereg('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT']))
            $UserBrowser = "IE";
        else
            $UserBrowser = '';

        /// important for download im most browser
        $mime_type = ($UserBrowser == 'IE' || $UserBrowser == 'Opera') ? 'application/octetstream' : 'application/octet-stream';

        @ob_end_clean(); /// decrease cpu usage extreme
        header('Content-Type: ' . $mime_type);
        header('Content-Disposition: attachment; filename="'.$name.'"');
        header("Expires: 0");
        header('Accept-Ranges: bytes');
        header("Cache-control: private");
        header('Pragma: private');
        header('Last-Modified: '.gmdate('D, d M Y H:i:s', filemtime($file)).' GMT', true, 200);
        /////  multipart-download and resume-download
        if(isset($_SERVER['HTTP_RANGE'])){
            list($a, $range) = explode("=",$_SERVER['HTTP_RANGE']);
            str_replace($range, "-", $range);
            $size2 = $size-1;
            $new_length = $size-$range;
            header("HTTP/1.1 206 Partial Content");
            header("Content-Length: $new_length");
            header("Content-Range: bytes $range$size2/$size");
        }
        else{
            $size2=$size-1;
            header("Content-Length: ".$size);
        }

        $chunksize = 1*(1024*1024);
        $bytes_send = 0;
        if ($file = fopen($file, 'r')){
            if(isset($_SERVER['HTTP_RANGE']))
                fseek($file, $range);
            while(!feof($file) and (connection_status()==0)){
                $buffer = fread($file, $chunksize);
                print($buffer);//echo($buffer); // is also possible
                flush();
                $bytes_send += strlen($buffer);
            }
            fclose($file);
        }
        else
            die('error can not open file');
        if(isset($new_length))
            $size = $new_length;
        
    }

