<?php
$wgHooks['SkinTemplateContentActions'][] = 'dynamicTabs'; //Register function

$wgExtensionCredits['other'][] = array(
	'name' => 'DynamicTabs',
	'version' => '0.5.3',
	'author' => 'Micke Nordin, David Öhlin',
	'url' => 'http://www.mediawiki.org/wiki/Extension:DynamicTabs',
	'description' => 'Adds and removes custom tabs',
);

function dynamicTabs(&$content_actions) {
	global $wgUser, $wgTitle, $wgCanonicalNamespaceNames, $wgScriptPath;
 	$organizedArray = getTabsConfig(); //Call the function that reads Mediawiki:DynamicTabs
	
	if ($wgTitle->mNamespace != 0) {//Bugfix when used with Extension:DPL
		$nsVar = $wgCanonicalNamespaceNames[$wgTitle->mNamespace]; //Get namespace name
	} else {
		$nsVar = MWNamespace::getCanonicalName(NS_MAIN);
	}
 
	if($nsVar != MWNamespace::getCanonicalName(NS_SPECIAL) ) {//don't show tabs in special namespace, bug or feature?
		if(isset($organizedArray["nsTabs"])) {
			foreach($organizedArray["nsTabs"] as $it) {//Loop array of the tabs
				$page = Title::newFromText( trim($it) . ':' . $wgTitle->getText());
				$href = $page->getLocalUrl();
				$content_actions[trim(strtolower($it))] = Array(
				'class' => (trim($it) == $nsVar?'selected':''),
				'text' => $it,
				'href' => $href,
				);
			}
		}
 
		if(isset($organizedArray["intLinks"])) {//Display internal links
			foreach($organizedArray["intLinks"] as $it) {
				$arr = myParser($it, '|');
				$page = Title::newFromText($arr[0]);
  				$href = $page->getLocalUrl();
				$content_actions[$arr[1]] = Array(
				'class' => ($arr[0] == $wgTitle->getText()?'selected':''),
				'text' => $arr[1],
				'href' => $href,
				);
			}
		}
 
		if(isset($organizedArray["extLinks"])) {//Display external links
			foreach($organizedArray["extLinks"] as $it) {				
				$arr = myParser($it, ' ');
				$content_actions[$arr[1]] = Array(
				'text' => $arr[1],
				'href' => $arr[0],
				);
			}
		}
 	}
 	
	foreach($content_actions as $key => $value)	{//Loop all tabs
		if ( $wgUser->isAnon() ) {//If user is anonymous
			if(isset($organizedArray["hideAnonymous"]) and 
			in_array($key, $organizedArray["hideAnonymous"])) {//And it's a tab we want to hide
				unset($content_actions[$key]); //Hide it
			}
		} else if ( !in_array('sysop', $wgUser->getGroups()) and !$wgUser->isAnon() ) {//If user is not sysop
			if(isset($organizedArray["hideUser"]) and in_array($key, $organizedArray["hideUser"])) {//And it's a tab we want to hide
				unset($content_actions[$key]); //Hide it
			}
 
		} else if(in_array('sysop', $wgUser->getGroups()) and 
		isset($organizedArray["hideSysop"]) and in_array($key, $organizedArray["hideSysop"]) ) {//Hide tabs from sysops
			unset($content_actions[$key]); //Hide tab
		}
	}
	
 	return true;
}

function getTabsConfig() { 
	$organizedArray = array(); //initialize variable if config page is empty, to avoid pollution of the template if PHP error messages are activated
	$dynTabsPage = Article::newFromId(Title::newFromText("Mediawiki:DynamicTabs")->getArticleId()); //Get MediaWiki:DynamicTabs
	if(isset($dynTabsPage)) {//If that worked
		$dynTabsText = explode("*", $dynTabsPage->getRawText()); //Make an array of that string
		foreach($dynTabsText as $list) {//Loop array
			$list = trim($list); //Remove surrounding white spaces
 
			if (strpos($list, "hideAnonymous")!== false) { //Do the tabs that should be hidden from anonymous viewer 
				$organizedArray["hideAnonymous"] = array_slice( explode("#", $list), 1); //Remove title and make an array of the rest
				foreach($organizedArray["hideAnonymous"] as $key => $value) {//Loop  array
					$organizedArray["hideAnonymous"][$key] = trim($value); //And remove white spaces
				}		
			}
 
			if (strpos($list, "hideUser")!== false) { //Do the tabs that should be hidden :from users 
				$organizedArray["hideUser"] = array_slice( explode("#", $list), 1); //Remove title and make an array of the rest
				foreach($organizedArray["hideUser"] as $key => $value) {//Loop  array
					$organizedArray["hideUser"][$key] = trim($value); //And remove white spaces
				}
			}
	
		    if (strpos($list, "hideSysop")!== false) { //Do the tabs that should be hidden from sysops
				$organizedArray["hideSysop"] = array_slice( explode("#", $list), 1);
				foreach($organizedArray["hideSysop"] as $key => $value) {//Remove title and make an array of the rest
					$organizedArray["hideSysop"][$key] = trim($value); //And remove white spaces
				}
		    }
	
		    if (strpos($list, "nsTabs") !== false) {//Do the tabs that should be added based on namespaces
				$organizedArray["nsTabs"] = array_slice( explode("#", $list), 1); //Remove title
		    }
	
		    if (strpos($list, "intLinks") !== false) {//Do static internal links
				$organizedArray["intLinks"] = array_slice( explode("#", $list), 1); //Remove title
		    }
	
		    if (strpos($list, "extLinks") !== false) {//Do static external links
				$organizedArray["extLinks"] = array_slice( explode("#", $list), 1); //Remove title
		    }
	    } 
	}
	    	
	return $organizedArray; //Return the organized arrays stored in another array     
}

function myParser($string, $explodeBy) {
	$stringArr = explode($explodeBy, trim($string, "[] \n")); //explode by $explodeBy var

	if(count($stringArr) < 2) {//If there was no second argument, put the string in the second slot
		$arr[] = trim($string, "[] \n");		
		$arr[] = trim($string, "[] \n");
	} else {//else all is well
		$arr = $stringArr;
	}

	return $arr; //return the parsed array
}
