This is an extension for MediaWiki: http://mediawiki.org

Usage:
On the wiki, create a page called Mediawiki:DynamicTabs with the following content:

Six sections are avalible, any and all can be excluded:
*hideAnonymous - this section holds tabs that should be hidden from anonymous users. Possible options are
#nstab-main -tab for main namespace 
#nstab-mediawiki -tab for mediawiki namespace 
#nstab-project -tab for project namespace 
#nstab-user -tab for user namespace 
#nstab-image -tab for file namespace 
#nstab-template -tab for template namespace 
#nstab-category -tab for category namespace 
#nstab-custom_namespace_here -tab for any custom namespace you have created
#talk -tab for talkpages 
#viewsource -edit tab for anonumous users  
#edit - edit tab for registered users 
#history - hostory tabs

*hideUsers - this section holds tabs that should be hidden from ordinary users
tabs are specified as a hash sign (#) followed by the name of the tab. Possible options are:
#nstab-main -tab for main namespace 
#nstab-mediawiki -tab for mediawiki namespace 
#nstab-project -tab for project namespace 
#nstab-user -tab for user namespace 
#nstab-image -tab for file namespace 
#nstab-template -tab for template namespace 
#nstab-category -tab for category namespace 
#nstab-custom_namespace_here -tab for any custom namespace you have created
#talk -tab for talkpages 
#viewsource -edit tab for anonumous users  
#edit - edit tab for registered users 
#history - hostory tabs
#move - move tab
#watch - watch tab

*hideSysop - this section hold tabs that should be hidden from sysops.
Possible options are:
#nstab-main -tab for main namespace 
#nstab-mediawiki -tab for mediawiki namespace 
#nstab-project -tab for project namespace 
#nstab-user -tab for user namespace 
#nstab-image -tab for file namespace 
#nstab-template -tab for template namespace 
#nstab-category -tab for category namespace 
#nstab-custom_namespace_here -tab for any custom namespace you have created
#talk -tab for talkpages 
#edit - edit tab 
#history - hostory tabs
#move - move tab
#protect - protection tab
#delete - delete tab
#watch - watch tab

*nsTabs - this section allows you to add links to the same page in a specified namespace, similar to how the talk tab works.
If you have a custom namespace called e.g. "Help" where users can ask questions related to the topic of an article, rather than
discuss how to write the article it self (which is what the talkpage is for) you can do this:
#help - adds a link to the corresponding page in help-namespace to every page 

*intLinks - this sections hold internal static links to pages on the wiki, for example:
#[[Main_page|Main]]
#[[A page]]
#Main_page|Main
#A page

*extLinks - this section holds external static links to pages off wiki, for example:
#[http://mediawiki.org] - will display http://mediawiki.org as link text
#[http://en.wikipedia.org wikipedia] - will display wikipedia as link text
#http://google.com - will display http://google.com as link text
#http://xkcd.com xkcd - will display xkcd as link text


For example your Mediawiki:DynamicTabs could look like this:

*hideUser
#viewsource
#edit
#talk
#history
#watch
#nstab-main
*hideSysop
#talk
#nstab-main
*nsTabs
#help
*intLinks
#[[Huvudsida|huvud]]
*extLinks
#[http://www.google.se google]

Installation:
Put the folder "DynamicTabs" with its content in the "extensions" directory of your MediaWiki installation.
Finaly, add this to LocalSettings.php:

#DynamicTabs extension
require_once("$IP/extensions/DynamicTabs/DynamicTabs.php");

