<?php
    include_once("config.nologin.php");
    if(u()->isGuest()) redirect("user.login.php");

    if(u()->getId() > 0 && u()->isBlocked()){
        sm_display("access.denied.html");
        exit;
    }

