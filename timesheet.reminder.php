<?php
include_once("config.nologin.php");

if($_SERVER['HTTP_HOST']){
    // Disable script if any one calling from Web Browser.
    print "<font size='20pt'>:-)</font>";
    exit;
}
# Get the current process ID.
$pid = posix_getpid();
$PID = "[$pid]";

$app_url = "http://groovesw.com/timesheet/htdocs";
$app_url = "http://timesheet.globalss.com/htdocs";
$app_url = "http://influence.globalss.com/htdocs";

write_log("timesheet reminder started...");
generate_timesheet_reminder();
write_log("timesheet reminder completed...");

function generate_timesheet_reminder() {
    include_once("classes/class.timecard.inc.php");

    global $PID;
    global $timesheet_settings;
    global $app_url;
    $timesheet_url = $app_url; 

    if(is_array($timesheet_settings['reminder_day']) && count($timesheet_settings['reminder_day']) > 0) {
        //$users = User::GetUsers(true);

        $isReminderEnabled = false; 
        $isNofifyBM = false; 
        $reminder_text = ''; 

        $hour = date("H");
        $minutes = floor(date("i")/10) * 10;
        $current_week_number = date("W");
        $current_week_day = date("D");
        $current_date = date("m/d/Y");
        $current_date_ts = strtotime($current_date);

        $day_names = array("Mon" => "Monday","Tue" =>"Tuesday","Wed" => "Wednesday","Thu" => "Thursday", "Fri" => "Friday", "Sat" => "Saturday", "Sun" =>"Sunday");
        $days = get_dates_of_week(strtotime("-1 week",$current_date_ts),true);

        $timesheet_dead_line = get_timesheet_dead_line(true);

        $from_date = array_shift($days);
        $to_date = array_pop($days);
        $submission_week = date("W", strtotime($from_date));
        $users_to_remind = array();
        $blnReminder=false;
        $blnDeadLine=false;
        if((date("D",strtotime($timesheet_dead_line)) == $current_week_day) && (date("H:i",strtotime($timesheet_dead_line)) == "$hour:$minutes")){ 
            $blnDeadLine=true; 
            write_log("timesheet deadline crossed...");
        }
        $users_to_remind = get_time_card_not_submitted_members($from_date, $to_date);
        foreach($timesheet_settings['reminder_day'] as $k => $d) {
            write_log("timesheet reminder day is -> {$day_names[$d]} and today is -> {$day_names[$current_week_day]}");
            write_log("timesheet reminder time is -> {$timesheet_settings['reminder_time'][$k]} and current time is -> $hour:$minutes");

            if($d != $current_week_day)  
                continue;
            if( $timesheet_settings['reminder_time'][$k] != "$hour:$minutes") 
                continue;
            
            $isReminderEnabled = isset($timesheet_settings['enable_reminder'][$k]) ? $timesheet_settings['enable_reminder'][$k] : 0;
            $isNofifyBM = isset($timesheet_settings['notify_reminder_bm'][$k]) ? $timesheet_settings['notify_reminder_bm'][$k] : 0;
            $reminder_text = $timesheet_settings['reminder_text'][$k];
            $blnReminder=false;
            if($isReminderEnabled) {
                if($users_to_remind) {
                    write_log(count($users_to_remind)." members found to remind the timsheet...");
                    foreach($users_to_remind as $uid => $row) {
                        $redirect_url = "$timesheet_url/user.login.php?url=".urlencode("task.add.multiple.php?date=$from_date&user_id={$row['id']}");
                        $full_name = $row['full_name'];
                        $user_email = $row['email'];
                        
                        sm_assign("timesheet_url", $timesheet_url);
                        sm_assign("blnDeadLine", $blnDeadLine);
                        sm_assign("redirect_url", $redirect_url);
                        sm_assign("full_name", $full_name);
                        sm_assign("week_number", $submission_week);
                        sm_assign("timesheet_dead_line", $timesheet_dead_line);
                        sm_assign("reminder_text", $reminder_text);

                        if(!$blnDeadLine) { 
                            // sending reminder to individual user
                            // Testing email ids
                            //$email = "shekhar@globalss.com";
                            #$test_emails = array("pranav.deshpande@globalss.com", "narayan.pahwa@globalss.com", "shekhar@globalss.com");
                            #foreach($test_emails as $email_id) {
                                _::Mail($from,
                                    array($user_email => $full_name),
                                    "Timecard submission reminder",
                                    sm_fetch("timecard.submission.letter.html")
                                );
                                $blnReminder=true;
                                $str = "Timesheet submission Reminder sent to $full_name($user_email) for the week number #$submission_week....";
                                write_log("timesheet submission reminder sent to $full_name($user_email) for the week number #$submission_week....");
                                watchdog('SUCCESS','CRON', "$str", "$PID Timesheet Reminder");
                            #}
                        }
                    }
                    ////
                    if($isNofifyBM && $blnReminder) {
                        // A consolidated notification to business manager, that users are not submitted the timecard for the week
                        if(is_array($users_to_remind) && count($users_to_remind) > 0) {
                            sm_assign("blnDeadLine", $blnDeadLine);
                            sm_assign("isNofifyBM", $isNofifyBM);
                            sm_assign("week_number", $submission_week);
                            sm_assign("timesheet_dead_line", $timesheet_dead_line);
                            sm_assign("users_to_remind", $users_to_remind);
                            notifyBM("Timecard submission report");
                            write_log("consolidated timesheet submission report to Business Managers sent....");
                        }
                    }
                    else {
                        write_log("Timesheet reminder notification to Business Manager is disabled....");
                    }

                }
            }
            else {
                write_log("timesheet submision reminder is disabled...no reminder will be sent to any members...");
            }
        }
        
        if($blnDeadLine) {
            // A consolidated notification to business manager that time sheet dead line crossed and still the member did not submitted the time card.
            if(is_array($users_to_remind) && count($users_to_remind) > 0) {
                sm_assign("blnDeadLine", $blnDeadLine);
                sm_assign("week_number", $submission_week);
                sm_assign("timesheet_dead_line", $timesheet_dead_line);
                sm_assign("users_to_remind", $users_to_remind);
                notifyBM("Timesheet deadline crossed."); 
                write_log("timesheet deadline crossed...a consolidated members list notification to Business Managers sent...");
            }
        }

        # Last Week Report on every Wednesday
        $weekly_timecard_report_day = $timesheet_settings['weekly_timecard_report_day'];
        $weekly_timecard_report_sent_datetime = date("Y-m-d", strtotime($timesheet_settings['weekly_timecard_report_sent_datetime']));
        $weekly_timecard_report_sent = $timesheet_settings['weekly_timecard_report_sent'];
        $current_datetime = date("Y-m-d", time());

        if(strtolower($current_week_day) == strtolower($weekly_timecard_report_day)) {
            if(strtotime($weekly_timecard_report_sent_datetime) <= strtotime($current_datetime) && $weekly_timecard_report_sent == 0) {
                #$days = get_dates_of_week(strtotime($from_date),true);
                #$last_from_date = array_shift($days);
                #$last_to_date = array_pop($days);
                $last_week_no = date("W", strtotime($from_date));
                write_log("Collecting last week($last_week_no) member timecard...");
                $last_week_timecard = get_time_card_not_submitted_members($from_date, $to_date,true);
                sm_assign("last_week_report", 1);
                sm_assign("week_number", $last_week_no);
                sm_assign("from_date", $from_date);
                sm_assign("to_date", $to_date);
                sm_assign("last_week_timecard", $last_week_timecard);
                notifyBM("Last week Timecard report");
                $sql = "UPDATE timesheet_settings SET settings_value=1,update_date=NOW() WHERE settings_key = 'weekly_timecard_report_sent' LIMIT 1";
                db_execute($sql);
                $sql = "UPDATE timesheet_settings SET settings_value=NOW(),update_date=NOW() WHERE settings_key = 'weekly_timecard_report_sent_datetime' LIMIT 1";
                db_execute($sql);
                write_log("Last week($last_week_no) member timecard report...sent to Business Managers...");
            }
            else if(strtotime("+1 day",strtotime($weekly_timecard_report_sent_datetime)) == strtotime($current_datetime) && $weekly_timecard_report_sent == 1) {
                // Resetting back the Weekly Report Sent Flag, so that next weekly report will go out.
                $sql = "UPDATE timesheet_settings SET settings_value=0,update_date=NOW() WHERE settings_key = 'weekly_timecard_report_sent' LIMIT 1";
                db_execute($sql);
            }
        }
    }
    else {
        // No reminder set
        watchdog('SUCCESS','CRON', "No Reminder configured to send.", "$PID Timesheet Reminder");
    }
}

function get_time_card_not_submitted_members($from_date, $to_date, $last_week_report=false) {
    global $timesheet_url;
    $users_to_remind = array();
    if($last_week_report) {
        $sql = "SELECT a.id, a.first_name, a.last_name, a.email, b.week_number, sum( b.billable_minutes ) AS billable_minutes, sum( b.pto_minutes ) AS pto_minutes, 
                sum( b.total_minutes ) AS total_minutes, ( sum( b.total_minutes ) - sum( b.billable_minutes )) AS nonbillable_minutes 
            FROM users a
            LEFT JOIN timecard_data b ON b.user_id = a.id AND (b.timecard_date BETWEEN '$from_date' AND '$to_date')
            WHERE a.id > 1 AND NOT a.blocked 
            GROUP BY a.id 
            ORDER BY a.first_name
            ";
        $rRows = db_get_all($sql);
        if($rRows) {
            foreach($rRows as $row) {
                if(isTimesheetRequired($row['id'])) {
                    $row['week_number'] = isset($row['week_number']) ? $row['week_number'] : $last_week_no;
                    $row['billable_minutes'] = isset($row['billable_minutes']) ? m2hm($row['billable_minutes']) : '';
                    $row['nonbillable_minutes'] = isset($row['nonbillable_minutes']) ? m2hm($row['nonbillable_minutes']) : '';
                    $row['pto_minutes'] = isset($row['pto_minutes']) ? m2hm($row['pto_minutes']) : '';
                    $row['total_minutes'] = isset($row['total_minutes']) ? m2hm($row['total_minutes']) : '';

                    $full_name = "{$row['first_name']} {$row['last_name']}";
                    $user_email = $row['email'];
                    $row['full_name'] = $full_name;
                    $users_to_remind[$row['id']]= $row;
                }
            }
        }
    }
    else {
        $sql = "SELECT a.id,a.first_name,a.last_name, a.email,b.week_number, count(b.status) as submitted
                FROM users a
                LEFT JOIN timecard_data b ON b.user_id = a.id AND (b.timecard_date BETWEEN '$from_date' AND '$to_date') AND b.status = 'SUBMITTED'
                WHERE b.week_number IS NULL AND a.id > 1 AND NOT a.blocked
                GROUP BY a.id 
                ORDER BY a.first_name
                ";
        $cnt=0;
        $rows = db_get_all($sql);
        if($rows) {
            foreach($rows as $row) {
                if(isTimesheetRequired($row['id'])) {
                    #if($cnt > 0) continue;
                    #$cnt++;
                    $full_name = "{$row['first_name']} {$row['last_name']}";
                    $user_email = $row['email'];
                    //$user_email = "shekhar@globalss.com";

                    $users_to_remind[$row['id']] = array("id" => $row['id'], "full_name" => $full_name, "email" => $user_email);
                }
            }
        }
    }
    
    return $users_to_remind; 
}
function notifyBM($subject) {
    global $PID;
    global $app_url;
    $timesheet_url = $app_url; 
    $bManager = User::GetBusinessManagers(); 
    if($bManager) {
        foreach($bManager as $u_id => $o_u) {
            if(in_array("business_manager", $o_u->getRoles())) {
                $name = $o_u->getFullName();
                sm_assign("name", $name);
                sm_assign("timesheet_url", $timesheet_url);
                $email = $o_u->getEmail();
                //$email = "shekhar@globalss.com";
                // Testing email ids
                #$test_emails = array("pranav.deshpande@globalss.com", "narayan.pahwa@globalss.com", "shekhar@globalss.com");
                #foreach($test_emails as $email_id) {
                _::Mail($from,
                    array($email => $name),
                    $subject,
                    sm_fetch("timecard.submission.letter.html")
                );
                $str = "<b>$subject</b> notification to Business Manager($name -> $email) sent....";
                write_log("timesheet notification to Business Manager($name -> $email) sent....");
                watchdog('SUCCESS','CRON', "$str", "$PID Timesheet Reminder");
                #}
            }
        }
    }
}
function get_timesheet_dead_line($returnTime=false) {
    global $timesheet_settings;

    $dead_line_day = $timesheet_settings['timesheet_deadline_day'];
    $dead_line_time = $timesheet_settings['timesheet_deadline_time'];
    $current_date_ts = time();
    $current_days = get_dates_of_week($current_date_ts);
    if($current_days) {
        foreach($current_days as $k => $ts) {
            $days[date('D',$ts)] = date("m/d/Y", $ts);
        } 
    }
    $timesheet_dead_line = $days[$dead_line_day];
    if($returnTime)
        $timesheet_dead_line = date("m/d/Y h:i A", strtotime("$timesheet_dead_line $dead_line_time"));

    return $timesheet_dead_line;
}

function isTimesheetRequired($user_id) {
    $blnRequired = false;
    if($user_id) {
        $sql = "SELECT id,is_timesheet FROM employees WHERE is_timesheet = 1 AND user_id = '$user_id'  LIMIT 1";
        $row = db_get_row($sql);
        if($row) {
            if($row['is_timesheet'] == 1) {
                $blnRequired = true;
            }
        }
    }
    return $blnRequired;
}

function write_log($str) {
    global $PID;
    $path =dirname(__FILE__);
    $path = "$path/logs";
    if(!is_dir($path)) @mkdir("$path",0777);
    #$fname="$path/timecard_reminder_".date("Y-m-d").".log";
    $fname="$path/timecard_reminder.log";

    if(!$fh = fopen($fname, "a")){
        // error while opening file.
        die("could not open log file $fname\n"); exit;
    }
    else{
        $str = "[".date("F j, Y, g:i:s a")."]$PID : $str\n";
        if(!fwrite($fh,$str)){
            // error while writing log file
            die("could not write log $fname file\n"); exit;
        }
        fclose($fh);
    }
}
?>
