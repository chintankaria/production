<?php

require_once('LicenseHelper.php');

$productCode = 'db3v';
$expiryYmd = '2012-05-05';
$l = new LicenseHelper();
$licenseKey = $l->generateLicense($productCode, $expiryYmd, array(1, 2, 8));
print "Generated for {$productCode} {$expiryYmd}: {$licenseKey}\n";
$license = $l->verifyLicense($licenseKey);
print_r($license);

print "module check\n";
print_r("1 => " . $license->hasModule(1) . "\n");
print_r("2 => " . $license->hasModule(2) . "\n");
print_r("4 => " . $license->hasModule(4) . "\n");
print_r("8 => " . $license->hasModule(8) . "\n");
