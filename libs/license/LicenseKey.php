<?php

class LicenseKey2 {
	private $_productCode = null;
	private $_expiryTs = null;
	private $_expiryDate = null;
	private $_modules = null;

	public function __construct($productCode, $expiryTs, $modules = 0) {
		$this->_productCode = $productCode;
		$this->_expiryTs = $expiryTs;
		$this->_expiryDate = date('Y-m-d H:i:s', $expiryTs);
		$this->_modules = $modules;
	}
	
	public function getProductCode() {
		return $this->_productCode;
	}

	public function getExpiryTs() {
		return $this->_expiryTs;
	}

	public function getExpiryDate() {
		return $this->_expiryDate;
	}

	public function isPerpetual() {
		return false;
	}

	public function isExpired() {
		return $this->_expiryTs > time();
	}

   public function hasModule($module_code) {
      return (bool) ($this->_modules & $module_code);
   }
}
