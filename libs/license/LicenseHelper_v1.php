<?php

require_once 'LicenseKey_v1.php';

class LicenseHelper_v1 {
	const SECONDS_PER_DAY = 86400;
	
	private static $_codes = array('D', 'Z', 'U', '5', 'N', '9', 'S', 'G', '0', 'X');
	private static $_validity = array('Z', 'A', 'X');
	private static $_permKeyIdentifiers = array('AEGSS','49GSS','257GS');

	public function __construct() {

	}
	
	public function generateLicense($productCode, $duration = 0, $startYmd = null) {
		$productCode = trim($productCode);
		if(strlen($productCode) == 4) {
			$productCode = strtoupper($productCode);
		} else {
			return null;
		}

		$duration = intval($duration);
		if(!$duration) {
			return $this->_verifyPermLicense($this->_generatePermLicense($productCode));
		} elseif($duration > 0 && $duration < 4) {
			return $this->_verifyTempLicense($this->_generateTempLicense($productCode, $duration, $startYmd));
		} else {
			return null;
		}
	}

	public function verifyLicense($license) {
		$licenseResult = null;

		$license = $this->_isWellFormed($license);
		if($license) {
			$licenseResult = $this->_verifyPermLicense($license);
			if(!$licenseResult) {
				$licenseResult = $this->_verifyTempLicense($license);
			}
		}

		return $licenseResult;
	}
	
	private function _generateTempLicense($productCode, $duration, $startYmd = null) {
		$license = $this->_generateRandomString(1);
		$seed = ord($license) % 10;
		
		$time = null;
		if($startYmd) {
			$time = strtotime($startYmd);
		}
		
		if(!$time) {
			$time = strtotime(date('Y-m-d', time()));
		}

		foreach(str_split($time) as $t) {
			$license .= self::$_codes[($t + $seed) % 10];
		}

		$license .= self::$_validity[$duration - 1];
		return $license . $productCode;
	}
	
	private function _generatePermLicense($productCode) {
		$licenseChars = str_split($this->_generateRandomString(12));
		$adapterIdentifierChars = str_split(self::$_permKeyIdentifiers[rand(0, count(self::$_permKeyIdentifiers) - 1)]);

		shuffle($adapterIdentifierChars);
		for($i = $this->_getHashCode($productCode) % 3; $adapterIdentifierChars; $i += 2) {
			$licenseChars[$i] = array_pop($adapterIdentifierChars);
		}

		return implode('', $licenseChars) . $productCode;
	}

	private function _verifyPermLicense($license) {
		$licenseResult = null;

		$licenseChars = str_split(substr($license, 0, 12));
		$adapterIdentifierChars = array(5);

		for($i = $this->_getHashCode(substr($license, -4)) % 3, $j = 0; $j < 5; $i += 2, $j++) {
			$adapterIdentifierChars[$j] = $licenseChars[$i];
		}

		sort($adapterIdentifierChars, SORT_STRING);
		$adapterIdentifier = implode('', $adapterIdentifierChars);

		if(in_array($adapterIdentifier, self::$_permKeyIdentifiers)) {
			$licenseResult = new LicenseKey_v1($license);
		}

		return $licenseResult;
	}

	private function _verifyTempLicense($license) {
		$licenseResult = null;

		$licenseChars = str_split(substr($license, 0, 12));

		$validityCodeIndex = array_search(array_pop($licenseChars), self::$_validity);
		if($validityCodeIndex !== false) {
			$validityMonths = $validityCodeIndex + 1;

			$seed = ord(array_shift($licenseChars)) % 10;

			$time = '';
			foreach($licenseChars as $c) {
				$code = array_search($c, self::$_codes);
				if($code === false) {
					break;
				}

				if($code < $seed) {
					$code += 10;
				}

				$time .= $code - $seed;
			}
 
			$expiryTime = $time + (self::SECONDS_PER_DAY * 30 * $validityMonths) + self::SECONDS_PER_DAY - 1;
			$licenseResult = new LicenseKey_v1($license, $time, $expiryTime);
		}

		return $licenseResult;
	}

	private function _generateRandomString($length) {
		static $pool = null;
		static $poolLength = 0;
		if($pool === null) {
			$pool = array_merge(range('A', 'Z'), range(0, 9));
			$poolLength = count($pool);
		}

		$randomString = '';
		for($i = 0; $i < $length; $i++) {
			$randomString .= $pool[rand(0, $poolLength - 1)];
		}

		return $randomString;
	}

	private function _getHashCode($string) {
		$hash = 0;
		$length = strlen($string);
		if($hash == 0 && $length > 0) {
			$offset = 0;
			foreach(str_split($string) as $s) {
				$hash = 31 * $hash + ord($s);
			}
		}

		return $this->_getIntval32bits($hash);
	}

	private function _getIntval32bits($value) {
		$value = ($value & 0xFFFFFFFF);
		if ($value & 0x80000000) {
			$value = -((~$value & 0xFFFFFFFF) + 1);
		}

		return $value;
	}

	private function _isWellFormed($license) {
		$licenseParts = str_split(preg_replace('/[^A-Z0-9]/', '', $license), 4);
		if(count($licenseParts) == 4 && strlen($licenseParts[3]) == 4) {
			return implode('', $licenseParts);
		}

		return null;
	}
}

