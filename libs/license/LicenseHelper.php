<?php
require_once('LicenseKey.php');

class LicenseHelper {
	const MAX_LENGTH = 12;
	const RADIX = 16;

	private static $_codes = array('6', 'Y', 'G', 'H', 'Z', '9', '8', 'U', 'K', 'V', 'S', 'O', 'E', 'I', 'C', 'J');
	private static $_noise = array('3', 'R', 'T', 'B', 'W', 'Q', '4', 'F', 'P', 'X', 'M', '2', 'N', 'D', 'L', 'A', '0', '5', '1', '7');

	public function __construct() {

	}

	public function generateLicense($productCode, $expiryYmd, $aSelectedModules = array()) {
		// print "ENCODING\n";
		$productCode = strtoupper(trim($productCode));
		if(strlen($productCode) != 4) {
			throw new Exception("Invalid product code: {$productCode}");
		}

		$expiryYmdParsed = date('Y-m-d', strtotime($expiryYmd));
		if($expiryYmdParsed != $expiryYmd) {
			throw new Exception("Expiry date must be a valid date in the format YYYY-MM-DD: {$expiryYmd}");
		}

		$modules = 0;
		foreach($aSelectedModules as $module) {
			$code = intval($module);
			if(($code > 0) && !($code & ($code - 1))) {
				// $selectedModules += $code;
				$modules |= $code;
			} else {
				throw new Exception("Module {$module['module_name']} has an invalid code {$module['module_code']}. Module codes must be a power of 2.");
			}
		}

		$seed = $this->_getSeed($productCode);
		$seedHex = dechex($seed);
		// print "SEED = {$seed} (0x{$seedHex})\n";
		
		$expiryYmd = str_replace('-', '', $expiryYmdParsed);
		$expiryYmdHex = str_pad(dechex($expiryYmd), 7, "0", STR_PAD_LEFT);
		// print "EXPIRY = {$expiryYmd} (0x{$expiryYmdHex})\n";
		
		$modulesHex = dechex($modules);
		// print "MODULES = {$modules} (0x{$modulesHex})\n";

		$licenseContent = "{$seedHex}{$expiryYmdHex}{$modulesHex}";
		// print "LICENSE CONTENT = {$licenseContent}\n";
		$noiseLength = self::MAX_LENGTH - strlen($licenseContent);
		if($noiseLength < 0) {
			throw new Exception('License content too long to fit in ' . self::MAX_LENGTH . ' characters.');
		}

		$noiseIndexes = range(0, self::MAX_LENGTH - 1);
		shuffle($noiseIndexes);
		$noiseIndexes = array_slice($noiseIndexes, 0, $noiseLength);
		$noiseIndexes = array_combine($noiseIndexes, $noiseIndexes);

		$licenseContent = str_split($licenseContent);
		$licenseOutput = array();
		foreach(range(0, self::MAX_LENGTH - 1) as $k) {
			if(isset($noiseIndexes[$k])) {
				$t = self::$_noise[array_rand(self::$_noise)];
				// print "noise == {$t}\n";
				$licenseOutput[] = $t;
			} else {
				$t = array_shift($licenseContent);
				$tEncoded = $this->_encode($t, $seed);
				// print "{$t} == {$tEncoded}\n";
				$licenseOutput[] = $tEncoded;
			}
		}

		$licenseOutput = implode('', $licenseOutput) . $productCode;
		return implode('-', str_split($licenseOutput, 4));
	}

	public function verifyLicense($argLicenseKey) {
		// print "DECODING\n";
		$licenseKeyChecked = $this->_checkLicenseKey($argLicenseKey);
		if(!$licenseKeyChecked) {
			throw new InvalidLicenseException($argLicenseKey);
		}
		
		list($licenseKey, $productCode) = str_split($licenseKeyChecked, self::MAX_LENGTH);
		print "licenseKey = {$licenseKey}\n";
		print "productCode = {$productCode}\n";
		$seed = $this->_getSeed($productCode);
		print "SEED = {$seed}\n";

		// remove noise from key
		$chars = array();
		foreach(str_split($licenseKey) as $c) {
			if(!in_array($c, self::$_noise)) {
				$code = $this->_decode($c, $seed);
				if($code === false || (!$chars && $code != $seed)) {
					throw new InvalidLicenseException($argLicenseKey);
				} else {
					$chars[] = $code;
				}
			}
		}

		// ignore the first element as it only used for validation
		array_shift($chars);
		
		$dateChars = str_split(hexdec(implode('', array_splice($chars, 0, 7))));

		$y = implode('', array_splice($dateChars, 0, 4));
		$m = implode('', array_splice($dateChars, 0, 2));
		$d = implode('', array_splice($dateChars, 0, 2));
		$expiryYmd = "{$y}-{$m}-{$d}";
		// print "EXPIRY = {$expiryYmd}\n";
		
		$modules = hexdec(implode('', $chars));
		// print "MODULES = {$modules}\n";

		$licenseKey = new LicenseKey2($productCode, strtotime($expiryYmd), $modules);
		return $licenseKey;
	}

	private function _getHashCode($string) {
		$hash = 0;
		$length = strlen($string);
		if($hash == 0 && $length > 0) {
			$offset = 0;
			foreach(str_split($string) as $s) {
				$hash = 31 * $hash + ord($s);
			}
		}

		return $this->_getIntval32bits($hash);
	}

	private function _getIntval32bits($value) {
		$value = ($value & 0xFFFFFFFF);
		if ($value & 0x80000000) {
			$value = -((~$value & 0xFFFFFFFF) + 1);
		}

		return $value;
	}

	private function _checkLicenseKey($licenseKey) {
		$licenseKey = strtoupper(trim($licenseKey));
		$licenseKey = preg_replace('/[^a-zA-Z0-9]/', '', $licenseKey);
		if(strlen($licenseKey) == self::MAX_LENGTH + 4) {
			return $licenseKey;
		}

		return false;
	}

	private function _getSeed($str) {
		return $this->_getHashCode($str) % self::RADIX;
	}
	
	private function _encode($num, $seed) {
		return self::$_codes[(hexdec($num) + $seed) % self::RADIX];
	}

	private function _decode($char, $seed) {
		$code = array_search($char, self::$_codes);
		if($code !== false) {
			if($code < $seed) {
				$code += self::RADIX;
			}

			$retval = dechex($code - $seed);
			// print "{$char} == {$code} == {$retval}\n";
			return $retval;
		}

		return false;
	}
}

class InvalidLicenseException extends Exception {
	public function __construct($licenseKey) {
		parent::__construct("Invalid license {$licenseKey}");
	}
}

$l = new LicenseHelper();
print_r($l->verifyLicense('GGZZ-2HO5-KJTZ-0001'));
