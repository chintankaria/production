<?php

class LicenseKey_v1 {
	private $_productCode = null;
	private $_key = null;
	private $_keyFormatted = null;
	private $_startTs = null;
	private $_startDate = null;
	private $_expiryTs = null;
	private $_expiryDate = null;

	public function __construct($key, $startTs = null, $expiryTs = null) {
		$this->_productCode = substr($key, -4);
		$this->_key = $key;
		$this->_keyFormatted = implode('-', str_split($key, 4));
		
		if($startTs) {
			$this->_startTs = $startTs;
			$this->_startDate = date('Y-m-d H:i:s', $startTs);
		}
		
		if($expiryTs) {
			$this->_expiryTs = $expiryTs;
			$this->_expiryDate = date('Y-m-d H:i:s', $expiryTs);
		}
	}

	public function getProductCode() {
		return $this->_productCode;
	}

	public function getKey($formatted = false) {
		return $formatted ? $this->_keyFormatted : $this->_key;
	}

	public function getStartTs() {
		return $this->_startTs;
	}

	public function getStartDate() {
		return $this->_startDate;
	}

	public function getExpiryTs() {
		return $this->_expiryTs;
	}

	public function getExpiryDate() {
		return $this->_expiryDate;
	}

	public function isPerpetual() {
		return (boolean) $this->_expiryTs;
	}

	public function isExpired() {
		return $this->_expiryTs && $this->_expiryTs > time();
	}
}

