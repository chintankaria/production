<?php
    error_reporting(E_ALL ^ E_NOTICE ^ E_USER_NOTICE);
    if(strtolower($_SERVER['HTTP_HOST']) == 'timesheet.globalss.com'){
        header("Location: http://influence.globalss.com");
        exit;
    }
    ob_start();
    set_include_path(get_include_path() . ":/home2/groovesw/pearlibs:/usr/share/pear");

    include_once("classes/class.path.inc.php");
    include_once("classes/class.underscore.inc.php");

    // Smarty configuration.
    include_once("libs/smarty/Smarty.class.php");
    class MySmarty extends Smarty {
        public function __construct(){
            $this->Smarty();
            $this->template_dir = AppPath::$TPL;
            $this->compile_dir = AppPath::$TPL_C;
            $this->use_sub_dirs = true;
            $this->config_dir = AppPath::$CONFIGS;
            $this->cache_dir = AppPath::$TPL_CACHE;

            $this->left_delimiter = "{{";
            $this->right_delimiter = "}}";

            $this->force_compile = true;
        }
    }

    _::$Smarty = new MySmarty();

    // ADODB configuration.
    include_once("config.db.php");
    include_once("libs/adodb/session/adodb-session2.php");
    ADODB_Session::config($DB_DRIVER, $DB_HOST, $DB_USER, $DB_PASS, $DB_NAME, array("table" => "sessions"));
    ADODB_Session::Persist($connectMode=false);
    session_start();
    ini_set('session.gc_maxlifetime', 86400);
    
    $dsn = "{$DB_DRIVER}://{$DB_USER}:{$DB_PASS}@{$DB_HOST}/{$DB_NAME}";
    _::$Db = ADONewConnection($dsn);
    _::$Db->SetFetchMode(ADODB_FETCH_ASSOC);

    include_once("functions.php");
    include_once("classes/class.watchdog.inc.php");
    include_once("classes/class.settings.inc.php");
    include_once("classes/class.user.inc.php");

    // Load the timesheet settings
    $timesheet_settings = array();
    $time_sheet_settings = Settings::GetAll();
    if(is_array($time_sheet_settings) && count($time_sheet_settings) > 0) {
        foreach($time_sheet_settings as $k =>$oVal) {
            $timesheet_settings[$k] = $oVal->getSettingsValue();
        }
    }

    
    // Smarty vars
    sm_assign("sep", "<span class='sep'>|</span>");
    sm_assign("spc", "&nbsp;&nbsp;&nbsp;");
    sm_assign("errors", e());
    sm_assign("messages", s());
    sm_assign("post", p());
    sm_assign("loginuser", u());

    sm_assign("emptyarray", array());
	if(strpos($_SERVER['REQUEST_URI'], '/timesheet-test') === false) $t = "";
	else $t = "timesheet-test/";
    $timesheet_url = "http://{$_SERVER['HTTP_HOST']}/{$t}htdocs/";
    sm_assign("timesheet_url", "http://{$_SERVER['HTTP_HOST']}/{$t}htdocs/");
    sm_assign("support_email", "admin.timesheet@globalss.com");
    $client_website_url = "http://globalss.com";
    sm_assign("client_website_url", $client_website_url);	
   
